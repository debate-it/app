import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/app.dart';
import 'package:debateit/src/services.dart';
import 'package:debateit/src/settings/notifications.dart';

import 'package:debateit/firebase_options.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final config = DefaultFirebaseOptions.currentPlatform;

  await Firebase.initializeApp(options: config);

  await initHiveForFlutter();
  await Future.wait([
    ConfigProvider.initialize(),
    NotificationSettings.initialize()
  ]);

  await Firebase.initializeApp();

  // Enable Crashlytics if the platform is not Web
  if (!kIsWeb) {
    final errorHandler = FlutterError.onError;

    FlutterError.onError = (error) {
      FirebaseCrashlytics.instance.recordFlutterError(error);
      errorHandler?.call(error);
    };
  }

  Services.initialize();
  
  await Services.authentication.authenticate();
  await Services.notificationService.init();

  // Run the app and pass in the SettingsController. The app listens to the
  // SettingsController for changes, then passes it further down to the
  // SettingsView.
  runApp(const DebateItApp());
}
