import 'package:flutter/material.dart';
import 'package:debateit_graphql/queries/profile.dart';

import 'package:debateit/src/repositories/base.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/notification.dart';


class NotificationRepository extends Repository {
  NotificationService get notificationService => Services.notificationService;

  Future<int> countNewNotifications() async {
    final result = await client.fetch(query: ProfileQueries.CountNewNotifications);

    // ignore: avoid_dynamic_calls
    return result.data!['profile']['indicators']['notifications'] as int;
  }
  
  Future<void> updateNotificationSeen(BuildContext context) async {
    await client.mutate(
      mutation: 'mutation { updateNotificationsSeen }',
      variables: {}
    );
  }
  
  // void handleNotification(UpdateRecord update) { 
  //   final record = update.intoNotification();
 
  //   notificationService?.sendNotification(record); 
  //   // _notifications.list.add(record);

  //   notifyListeners();
  // }
  
}
