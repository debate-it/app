import 'package:debateit/src/repositories/base.dart';
import 'package:debateit/src/services.dart';
import 'package:debateit_graphql/mutations/actions.dart';
import 'package:debateit_graphql/queries/user.dart';

import 'package:debateit_models/user/main.dart';

class UserRepository extends Repository {
  const UserRepository();

  static Future<UserProfile> read(String id) async {
    final result = await Services.client.fetch(query: UserQueries.User, variables: { 'id': id });

    if (result.exception != null) {
      throw result.exception!;
    }

    return UserProfile.fromJson(result.data!['user'] as Map<String, dynamic>);
  }

  Future<UserProfile> follow(String id) async {
    final result = await client.mutate(
      mutation: UserQueries.follow,
      variables: { 'id': id });

    return UserProfile.fromJson(result as Map<String, dynamic>);
  }

  Future<UserProfile> updateUserAbout(Map<String, String> data) async {
    final result = await client.mutate<Map<String, dynamic>>(
      mutation: ActionQueries.updateInfo,
      variables: {'options': data}
    );

    return UserProfile.fromJson(result);
  }
}
