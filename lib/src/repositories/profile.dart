import 'package:debateit/src/repositories/base.dart';
import 'package:debateit_graphql/queries/profile.dart';
import 'package:debateit_models/user/main.dart';


class ProfileIndicators {
  int notifications = 0;
  int debates = 0;
  int requests = 0;

  ProfileIndicators({ this.notifications = 0, this.debates = 0, this.requests = 0 });
  ProfileIndicators.empty() : this();

  ProfileIndicators.fromJson(Map<String, dynamic> json)
    : notifications = json['notifications'] as int,
      requests = json['requests'] as int,
      debates = json['debates'] as int;

  ProfileIndicators copyWith({ int? notifications, int? debates, int? requests }) =>
    ProfileIndicators(
      notifications: notifications ?? this.notifications,
      requests: requests ?? this.requests,
      debates: debates ?? this.debates
    );
}

class ProfileRepository extends Repository {
  UserProfile? _profile;
  ProfileIndicators? _indicators;

  UserProfile? get user => _profile;
  ProfileIndicators get indicators => _indicators ?? ProfileIndicators.empty();


  Future<UserProfile?> fetch() async {
    if (_profile != null) return _profile;

    final result = await client.fetch(query: ProfileQueries.Info);

    if (result.exception != null) {
      throw result.exception!;
    }
    
    final payload = result.data!['profile'] as Map<String, dynamic>;

    _profile = UserProfile.fromJson(payload['info'] as Map<String, dynamic>);
    _indicators = ProfileIndicators.fromJson(payload['indicators'] as Map<String, dynamic>);

    return _profile;
  }

  Future<void> setUser(UserProfile user) async {
    _profile = user;

    final result = await client.fetch(query: ProfileQueries.Indicators);

    if (result.exception != null) {
      throw result.exception!;
    }
    
    final payload = result.data!['profile'] as Map<String, dynamic>;

    _indicators = ProfileIndicators.fromJson(payload['indicators'] as Map<String, dynamic>);
  }

  void clear() {
    _profile = null;
    _indicators = null;
  }
}
