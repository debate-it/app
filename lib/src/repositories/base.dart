import 'package:debateit/src/services.dart';
import 'package:debateit_graphql/graphql.dart';


abstract class Repository {
  BackendClient get client => Services.client;

  const Repository();
}
