import 'package:debateit_graphql/queries/debate.dart';
import 'package:debateit_models/discussions/debate/main.dart';

import 'package:debateit/src/repositories/base.dart';

class DebateRepository extends Repository {
  const DebateRepository();

  Future<Debate> read(String id) async {
    final result = await client.fetch(query: DebateQueries.Debate, variables: { 'id': id });

    if (result.exception != null) {
      throw result.exception!;
    }

    return Debate.fromJson(result.data!['debate'] as Map<String, dynamic>);
  }
}
