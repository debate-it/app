import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/initiate/categories.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';



class OpponentSearchFilter extends StatefulWidget {
  const OpponentSearchFilter({super.key, required this.onSubmit});

  final void Function(Map<String, dynamic>) onSubmit;

  @override
  State<OpponentSearchFilter> createState() => _OpponentSearchFilterState();
}

class _OpponentSearchFilterState extends State<OpponentSearchFilter> {
  final form = FormGroup({
    'category': FormControl<String>(),
    'country': FormControl<String>(),
    'occupation': FormControl<String>(),
    'education': FormControl<String>(),
    'politics': FormControl<String>(),
    'religion': FormControl<String>()
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: Insets.sm),
      child: InkWell(
        onTap: () {
          showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            builder: showFilterForm
          );
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Insets.md, vertical: Insets.lg),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text('Filters'),
              Icon(Icons.filter_list, size: 20),
            ],
          )
        ),
      )
    );
  }

  Widget showFilterForm(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: ReactiveForm(
          formGroup: form,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: Insets.md, horizontal: Insets.sm),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AppDropdownField<String>(
                  formControlName: 'category',
                  labelText: 'Best category',
                  isExpanded: true,
                  items: [
                    for (final category in Categories)
                      DropdownMenuItem<String>(
                        value: category, 
                        child: ListTile(
                          dense: true,
                          title: Text(category),
                          trailing: form.control('category').value == category
                            ? IconButton(
                              icon: const Icon(Icons.remove_circle_outline),
                              onPressed: () {
                                form.control('category').value = null;
                              },
                            )
                            : null,
                        )
                      )
                  ]
                ),
                
                const SizedBox(height: Insets.md),
                const AppTextField(formControlName: 'country', labelText: 'Country'),
                const SizedBox(height: Insets.md),
    
                const AppTextField(formControlName: 'occupation', labelText: 'Occupation'),
                const SizedBox(height: Insets.md),
    
                const AppTextField(formControlName: 'education', labelText: 'Education'),
                const SizedBox(height: Insets.md),
    
                const AppTextField(formControlName: 'politics', labelText: 'Politics'),
                const SizedBox(height: Insets.md),
    
                const AppTextField(formControlName: 'religion', labelText: 'Religion'),
                const SizedBox(height: Insets.md),
    
                PrimaryBtn(label: 'Search', onPressed: search)
              ]
            ),
          )
        ),
      ),
    );
  }

  void search() {
    Navigator.of(context).pop();

    final cleaned = Map.fromEntries(form.value.entries.where((element) => element.value != null));
      
    widget.onSubmit(cleaned);
  }
}
