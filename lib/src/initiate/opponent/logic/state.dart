part of 'bloc.dart';

class OpponentSearchState extends Equatable {
  const OpponentSearchState({this.opponents = const []});

  final List<UserProfile> opponents;

  @override
  List<Object?> get props => [opponents];
}
