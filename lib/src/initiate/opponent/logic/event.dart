part of 'bloc.dart';

abstract class OpponentSearchEvent extends Equatable {
  const OpponentSearchEvent();
}

class OpponentAdded extends OpponentSearchEvent {
  const OpponentAdded({required this.opponent});

  final UserProfile opponent;

  @override
  List<Object?> get props => [opponent];

}

class OpponentRemoved extends OpponentSearchEvent {
  const OpponentRemoved({required this.opponent});

  final UserProfile opponent;
  @override
  List<Object?> get props => [opponent];

}
