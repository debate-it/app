import 'package:bloc/bloc.dart';
import 'package:debateit_models/models.dart';
import 'package:equatable/equatable.dart';

part 'event.dart';
part 'state.dart';

class OpponentSearchBloc extends Bloc<OpponentSearchEvent, OpponentSearchState> {
  OpponentSearchBloc() : super(const OpponentSearchState()) {
    on<OpponentAdded>((event, emit) {
      if (state.opponents.any((element) => element.id == event.opponent.id)) {
        return;
      }
      
      emit(OpponentSearchState(opponents: [...state.opponents, event.opponent]));
    });

    on<OpponentRemoved>((event, emit) => 
      emit(OpponentSearchState(
        opponents: state.opponents
          .where((element) => element.id != event.opponent.id)
          .toList()
      ))
    );
  }

}
