import 'package:debateit/src/widgets/dialogs/info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/user/main.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/initiate/opponent/filter.dart';
import 'package:debateit/src/initiate/opponent/logic/bloc.dart';

import 'package:debateit/src/config/pagination/user.dart';
import 'package:debateit/src/discussions/user/views/preview.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';


class OpponentSearchList extends StatefulWidget {
  const OpponentSearchList({super.key, this.multiple = false, required this.onSubmit});

  final bool multiple;
  final void Function(List<UserProfile>) onSubmit;

  @override
  State<OpponentSearchList> createState() => _OpponentSearchListState();
}

class _OpponentSearchListState extends State<OpponentSearchList> {
  late PaginationBloc<UserProfile> pagination;
  PersistentBottomSheetController? controller;

  final OpponentSearchBloc bloc = OpponentSearchBloc();

  Map<String, dynamic>? filters = {};

  bool participantsSubmitted = false;

  @override
  void initState() {
    pagination = PaginationBloc(
      client: Services.client,
      config: FollowerSearchPaginationConfig(Services.profileService.info!.profile.id, onUserSelected));

    bloc.stream.listen(_blocOnChange);

    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  void _blocOnChange(OpponentSearchState state) {
    if (state.opponents.isEmpty) {
      controller?.close();
      controller = null;
    }
    
    if (!widget.multiple && state.opponents.length == 1) {
      submitRecipients();
    } 
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 100),
      child: BlocProvider(
        create: (_) => OpponentSearchBloc(),
        child: PaginationProvider.fromBloc(
          padding: const EdgeInsets.all(Insets.sm),
          header: OpponentSearchFilter(onSubmit: setmatchMakingConfig),
          bloc: pagination
        )
      ),
    );
  }
  
  void setmatchMakingConfig(Map<String, dynamic> filters) {
    setState(() { 
      pagination.config = OpponentSearchPaginationConfig(filters, onUserSelected); 
      pagination.add(const PageRefresh());
    });
  }

  void submitRecipients() {
    try {
      widget.onSubmit(bloc.state.opponents);
      
      setState(() { participantsSubmitted = true; });
    } on Exception catch (err) {
      showDialog(context: context, builder: (_) => InfoDialog(message: err.toString()));
    }
  }
 
  void onUserSelected(UserProfile profile) {
    bloc.add(OpponentAdded(opponent: profile));

    if (widget.multiple && bloc.state.opponents.length + 1 > 9) {
      // SnackbarHelper.showSnackBar(context, 'Can only add up to 9 participants for a panel');
    }
    
    setState(() {
      controller ??= showBottomSheet(
          context: context, 
          builder: (_) => BottomSheet(
            onClosing: () { },
            builder: (context) => BlocBuilder<OpponentSearchBloc, OpponentSearchState>(
              bloc: bloc,
              builder: (context, state) => ListTile(
                title: Text('${state.opponents.length} opponents selected', 
                  style: context.text.titleLarge
                    ?.copyWith(color: context.colors.text)),
            
                trailing: Icon(Icons.arrow_drop_up, color: context.colors.text),
            
                onTap: () async {
                  await showModalBottomSheet(
                    context: context, 
                    isDismissible: true,
                    builder: (context) => buildRecipientList(context, bloc)
                  );
            
                  if (participantsSubmitted && mounted) {
                    controller?.close();
                  }
                },
              ),
            )));
    });


  }
  
  Widget buildRecipientList(BuildContext context, OpponentSearchBloc bloc) {
    return BlocBuilder<OpponentSearchBloc, OpponentSearchState>(
      bloc: bloc,
      builder: (BuildContext context, state)  {
        if (state.opponents.isEmpty) {
          Future.microtask(() => Navigator.of(context).pop());
        }

        final size = Size.fromHeight(MediaQuery.of(context).size.height / 1.5 - 60);
        final constraints = BoxConstraints.tight(size);

        return Container(
          constraints: constraints,
          padding: const EdgeInsets.all(Insets.md),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(Insets.md),
                child: Text('${state.opponents.length} selected opponents',
                  textAlign: TextAlign.center,
                  style: context.text.titleLarge)),

              Expanded(
                // constraints: constraints,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      for (final opponent in state.opponents)
                        UserPreview(
                          minimal: true,
                          user: opponent,
                          // onTap: (opponent) => bloc.add(OpponentRemoved(opponent: opponent)),
                          side: IconButton(
                            icon: Icon(Icons.close, color: context.colors.primary),
                            onPressed: () => bloc.add(OpponentRemoved(opponent: opponent))))
                    ]
                  )
                )
              ),
              
              PrimaryBtn(label: 'Send Panel Request', onPressed: submitRecipients)
            ]
          )
        );
      }
    );
  }
}
