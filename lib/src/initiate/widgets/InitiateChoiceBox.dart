import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';


class InitiateChoiceBox extends StatelessWidget {
  final String text;
  final String path;
  final IconData icon;

  const InitiateChoiceBox({super.key, required this.text, required this.path, required this.icon });

  @override
  Widget build(BuildContext context) {
    final container = Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: const EdgeInsets.all(Insets.sm),
          child: CircleAvatar(
            radius: 30,
            backgroundColor: context.colors.surface,
            child: Icon(icon, color: context.colors.accent, size: 28))),

        Text(text.toUpperCase(),
          textAlign: TextAlign.center,
          style: context.text.caption?.copyWith(color: context.colors.text))
      ]
    );

    return InkWell(onTap: () => _openOpponentSearch(context), child: container);
  }

  void _openOpponentSearch(BuildContext context) {
    Navigator.of(context).pop();
    Beamer.of(context).beamToNamed(path); 
  }
}
