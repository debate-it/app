import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:flutter/material.dart';


typedef DiscussionSizeHandler = void Function(DiscussionSize);

class SizeOption extends StatelessWidget {
  const SizeOption({super.key, required this.size, required this.onTap, this.selected, this.showRounds = true });

  final bool showRounds;

  final DiscussionSize? selected;
  
  final DiscussionSize size;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(Insets.md),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Corners.med),
            border: selected == size
              ? Border.all(color: context.colors.primary)
              : null
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(size.label.toUpperCase(),
                style: context.text.bodyLarge?.copyWith(
                  color: selected == size
                    ? context.colors.primary
                    : context.colors.secondary)),

              Text(explanation, 
                softWrap: true,
                textAlign: TextAlign.center,
                style: context.text.bodyMedium)
            ]))));
  }

  String get explanation {
    final base = '${size.argumentLength} words';

    if (showRounds) {
      return '$base\n${size.rounds} rounds';
    }

    return base;
  }
}
