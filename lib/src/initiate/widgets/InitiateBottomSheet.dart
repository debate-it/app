import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit/src/initiate/widgets/InitiateChoiceBox.dart';


class InitiateBottomSheet extends StatelessWidget {
  final AnimationController controller;

  InitiateBottomSheet({ super.key, required TickerProvider ticker }) 
    : controller = BottomSheet.createAnimationController(ticker);

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      elevation: 12,
      onClosing: () {},
      animationController: controller, 
      builder: buildSheetContent,
      backgroundColor: context.colorScheme.background);
  }

  Widget buildSheetContent(BuildContext context) {
    final options = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: const <Widget>[
        Expanded(child: InitiateChoiceBox(text: 'Debate', path: '/initiate/debate', icon: Icons.chat)),
        Expanded(child: InitiateChoiceBox(text: 'Panel', path: '/initiate/panel', icon: Icons.people)),
        Expanded(child: InitiateChoiceBox(text: 'Post', path: '/initiate/post', icon: Icons.text_snippet)),
        Expanded(child: InitiateChoiceBox(text: 'Challenge', path: '/initiate/challenge', icon: Icons.record_voice_over_sharp)),
      ],
    );

    final header = Padding( 
      padding: const EdgeInsets.all(Insets.xs),
      child: Text('Initiate...',
        textAlign: TextAlign.center,
        style: context.text.titleLarge
          ?.copyWith(color: context.colorScheme.primary)));

    return Container(
      padding: const EdgeInsets.all(Insets.md),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [header, options]));
  }
}
