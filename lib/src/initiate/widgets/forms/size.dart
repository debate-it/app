import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/widgets/misc/text.dart';
import 'package:debateit/src/initiate/widgets/SizeOption.dart';

class DiscussionSizeValueAccessor extends ControlValueAccessor<DiscussionSize, String> {
  @override
  DiscussionSize? viewToModelValue(String? value) {
    if (value == null) {
      return null;
    }

    return DiscussionSizeExtension.parse(value);
  }

  @override
  String? modelToViewValue(DiscussionSize? value) {
    return value?.name;
  }

}

class ReactiveDiscussionSizeFields extends ReactiveFormField<DiscussionSize, String> {
  ReactiveDiscussionSizeFields({
    required super.formControlName,
    super.validationMessages,
    bool showRounds = true,
  })
    : super(
      valueAccessor: DiscussionSizeValueAccessor(),
      builder: (ReactiveFormFieldState<DiscussionSize, String> state) {
        return DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Corners.lg),
            border: Border.all(
              color: state.errorText != null 
                ? state.context.colors.error 
                : Colors.transparent)),

          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  for (final value in DiscussionSize.values)
                    SizeOption(
                      size: value,
                      selected: state.control.value,
                      showRounds: showRounds,
                      onTap: () => state.didChange(value.name))
                ]
              ),

              if (state.errorText != null) 
                ErrorText(state.errorText!)
            ],
          )
        );
      }
    );

  @override
  ReactiveFormFieldState<DiscussionSize, String> createState()
    => ReactiveFormFieldState<DiscussionSize, String>();
}
