import 'package:debateit/src/discussions/shared/DiscussionTag.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';


class ReactiveTagFormField extends StatefulWidget {
  const ReactiveTagFormField({super.key, required this.formControl});

  final FormArray<String> formControl;

  @override
  State<ReactiveTagFormField> createState() => _ReactiveTagFormFieldState();
}

class _ReactiveTagFormFieldState extends State<ReactiveTagFormField> {
  final TextEditingController controller = TextEditingController();

  final form = FormGroup({
    'tag': FormControl<String>()
  });

  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
      formGroup: form, 
      child: Column(
        children: [
          AppTextField(
            controller: controller,
            formControl: form.control('tag') as FormControl<String>,
            labelText: 'Tags',
            suffix: ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: 20, maxWidth: 20),
              child: IconButton( 
                padding: EdgeInsets.zero,
                alignment: Alignment.centerRight,
                icon: const Icon(Icons.add, size: 18),
                onPressed: widget.formControl.value!.length < 3
                  ? () => addTag(form.control('tag').value as String)
                  : null
                )
            )),

          const SizedBox(height: Insets.sm),

          Wrap(
            spacing: 10.0, runSpacing: 5.0,
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              for (final tag in widget.formControl.value!)
                DiscussionTag(tag: Tag(name: tag!), onClose: () => setState(() => widget.formControl.value!.remove(tag)))
            ]
          ),
        ]
      )
    );
  }
  
  void addTag(String name) {
    final tag = Tag(name: name);

    if (tag.name.isEmpty || tag.name.contains(RegExp(r'\s'))) {
      return;
    }

    if (widget.formControl.value!.length == 3) {
      return;
    }
    
    if (widget.formControl.value!.any((element) => element == tag.name)) {
      return;
    }
    
    setState(() {
      controller.clear(); 
      widget.formControl.value!.add(name);
    });
  }
}

// class _ReactiveTagFormField extends ReactiveFormField<String, String> {
//   _ReactiveTagFormField({
//     required FormArray<String> formArray,
//     required TextEditingController controller
//   })
//     : super(
//       formControlName: formControlName,
//       builder:(ReactiveFormFieldState<String, String> state)  {
        
        
//         return Column(
//           children: [
//             TextField(
//               controller: controller,
//               decoration: InputDecoration(
//                 label: const Text('Tags'),
//                 suffix: IconButton( 
//                   padding: EdgeInsets.zero,
//                   icon: const Icon(Icons.add),
//                   iconSize: 20,
//                   onPressed: state.value!.length < 3
//                     ? () => addTag(controller.text)
//                     : null)
//               )
//             ),

//             const SizedBox(height: Insets.sm),

//             Wrap(
//               spacing: 10.0, runSpacing: 5.0,
//               alignment: WrapAlignment.center,
//               crossAxisAlignment: WrapCrossAlignment.center,
//               children: [
//                 for (final tag in state.value!)
//                   DiscussionTag(tag: tag, onClose: () => state.value!.remove(tag))
//               ]
//             )
//           ] 
//         );
//       }
//     );
// }
