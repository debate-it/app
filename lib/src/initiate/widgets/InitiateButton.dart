import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit/src/initiate/widgets/InitiateBottomSheet.dart';

class InitiateButton extends StatelessWidget {
  final TickerProvider ticker;

  const InitiateButton({super.key, required this.ticker});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: context.colorScheme.primary,
      foregroundColor: context.colorScheme.onPrimary,

      shape: const CircleBorder(),

      onPressed: () => _showInitiateBottomSheet(context),

      child: const Icon(Icons.add_circle_outline,
        semanticLabel: 'Initiate Debate'));
  }

  void _showInitiateBottomSheet(BuildContext context) { 
    showModalBottomSheet(
      context: context, 
      builder: (context) => InitiateBottomSheet(ticker: ticker)
    );
  }
}
