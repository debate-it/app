import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/repositories/user.dart';
import 'package:debateit/src/discussions/user/views/preview.dart';


class ChoseOpponentField extends StatefulWidget {
  const ChoseOpponentField({super.key, required this.opponentId});

  final String opponentId;

  @override
  State<ChoseOpponentField> createState() => _ChoseOpponentFieldState();
}

class _ChoseOpponentFieldState extends State<ChoseOpponentField> {
  UserProfile? profile;

  Future<void> _fetchUser() async {
    profile ??= await UserRepository.read(widget.opponentId);
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: context.colorScheme.surface,
        borderRadius: BorderRadius.circular(Corners.lg),
        border: Border.all(color: context.colors.accent)),

      child: FutureBuilder(
        future: _fetchUser(),
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (profile != null) {
            return UserPreview(
              user: profile!, 
              minimal: true, transparent: true,
              side: const Text('Selected\nOpponent', textAlign: TextAlign.center));
          }

          return const Center(child: Loader());
        }
      )
    );
  }
}
