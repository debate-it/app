import 'package:flutter/material.dart';

import 'package:debateit/src/initiate/views/challenge.dart';
import 'package:debateit/src/initiate/views/debate.dart';
import 'package:debateit/src/initiate/views/panel.dart';
import 'package:debateit/src/initiate/views/post.dart';


enum InitiateView { debate, panel, post, challenge }

extension InitiateViewExtension on InitiateView {
  static InitiateView? parse(String value) {
    switch (value) {
      case 'debate':
        return InitiateView.debate;

      case 'panel':
        return InitiateView.panel;

      case 'post':
        return InitiateView.post;

      case 'challenge':
        return InitiateView.challenge;

      default:
        return null;
    }
  }
}

class InitiatePage extends StatelessWidget {
  const InitiatePage({super.key, this.view});

  InitiatePage.fromViewName({super.key, String? viewName})
    : view = InitiateViewExtension.parse(viewName!);

  final InitiateView? view;

  @override
  Widget build(BuildContext context) {
    switch (view) {
      case InitiateView.debate:
        return const InitiateDebate();

      case InitiateView.panel:
        return const InitiatePanel();

      case InitiateView.post:
        return const InitiatePost();

      case InitiateView.challenge:
        return const InitiateChallenge();

      default:
        return const Scaffold(body: Center(child: Text('Discussion type not found')));
    }
  }
}
