import 'package:beamer/beamer.dart';
import 'package:debateit/src/widgets/navigation/icons/back.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/request.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';

import 'package:debateit/src/initiate/categories.dart';

import 'package:debateit/src/initiate/opponent/list.dart';

import 'package:debateit/src/initiate/widgets/forms/size.dart';
import 'package:debateit/src/initiate/widgets/forms/tags.dart';

import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';


enum InitiateDebateStage {
  fillingInfo,
  choosingOpponent
}

class InitiatePanel extends StatefulWidget {
  const InitiatePanel();
  
  @override
  State<InitiatePanel> createState() => _InitiatePanelState();
}

class _InitiatePanelState extends State<InitiatePanel> {
  final controller = PageController();

  final form = FormGroup({
    'opponents': FormControl<List<String>>(),
    'position': FormControl<String>(),

    'discussion': FormGroup({
      'isOpenRequest': FormControl<bool>(value: false),
      'topic': FormControl<String>(
        validators: [
          Validators.required, 
          Validators.maxLength(2000), 
          WordCountValidation.maxWordLength(100)
        ]
      ),
      'category': FormControl<String>(validators: [Validators.required]),
      'tags': FormArray<String>([]),
      'size': FormControl<DiscussionSize>(validators: [Validators.required])
    })
  });

  // InitiateDebateStage _stage = InitiateDebateStage.fillingInfo;

  // static const opponentSelectionRoute = 'opponent-selection';

  @override
  Widget build(BuildContext context) {
    return ScrollablePageView(
      leading: CustomBackIcon(
        onPressed: () {
          if (controller.page != null && controller.page! >= 1.0) {
            controller.animateToPage(0, duration: const Duration(milliseconds: 500), curve: Curves.easeInOutCubicEmphasized);
          } else {
            Beamer.of(context).beamBack();
          }
        },
      ),
      body: ConstrainedBox(
        constraints: BoxConstraints.loose(MediaQuery.of(context).size),
        child: PageView(
          controller: controller,
          allowImplicitScrolling: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            InitiatePanelForm(form: form.control('discussion') as FormGroup, onSave: onInfoSave),
            OpponentSearchList(multiple: true, onSubmit: submitDebateRequest)
          ]
        )
      ),
    );
  }

  Future<void> onInfoSave() async {
    final isOpenRequest = form.control('discussion.isOpenRequest').value as bool;
    
    if (isOpenRequest) {
      return submit();
    } 

    await showModalBottomSheet(
      context: context, 
      isScrollControlled: true,
      builder: (context) => BottomSheetTextField(
        inputLabel: 'Initial position',

        wordLimit: (form.control('discussion.size').value as DiscussionSize).argumentLength,

        validators: [Validators.required],

        onSubmit: (position) async {
          form.control('position').value = position;
          controller.animateToPage(1, duration: const Duration(milliseconds: 300), curve: Curves.easeInCubic);
        }
      )
    );
  }

  Future<void> submitDebateRequest(List<UserProfile> recipients) async {
    if (recipients.length < 2) {
      throw Exception('You need to select at least 2 opponents');
    }
    
    if (recipients.length > 9) {
      throw Exception('You can select at most 9 opponents');
    }

    form.control('opponents').value = recipients.map((e) => e.id).toList();

    await submit();
  }

  Future<void> submit() async {
    var failed = false;
    var message = 'Request was sent successfully';

    final navigator = Beamer.of(context);

    try {
      final isOpenRequest = form.control('discussion.isOpenRequest').value as bool;
      final mutation = isOpenRequest
        ? OpenRequestQueries.CreateOpenRequest
        : RequestQueries.CreatePanelRequest;

      final payload = <String, dynamic>{
        'position': form.control('position').value,
        'discussion': Map.fromEntries(
          (form.control('discussion').value as Map<String, dynamic>)
            .entries
            .where((element) => element.key != 'isOpenRequest')
        )
      };

      if (isOpenRequest) {
        payload['type'] = OpenRequestType.Panel.parameter;
      } else {
        payload['recipients'] = form.control('opponents').value as List<String>;
      }

      await Services.client.mutate(mutation: mutation, variables: payload); 
    } catch (err) {
      message = err.toString().replaceAll('Exception:', '').trim();
      failed = true;
    }

    await showDialog(context: context, builder: (_) => InfoDialog(message: message));

    if (!failed && mounted) {
      navigator.beamToReplacementNamed('/?tab=discussions');
    }
  }
}

class InitiatePanelForm extends StatelessWidget {
  const InitiatePanelForm({super.key, required this.form, required this.onSave});

  final FormGroup form;
  final VoidCallback onSave;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Insets.md),
      child: ReactiveFormBuilder(
        form: () => form,
        builder: (context, form, child) => Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListTile(
              title: Text('Create open panel request?',
                style: context.text.bodyMedium?.copyWith(color: context.colors.text)),
              trailing: ReactiveSwitch.adaptive(formControlName: 'isOpenRequest')),

            AppTextField(
              formControlName: 'topic',
              labelText: 'Topic',
              minLines: 3, maxLines: 10,
              maxLength: 100,
              counterType: CounterType.words,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              validationMessages: {
                ValidationMessage.required: (_) => 'Please enter your topic',
                WordCountValidationMessage.maxWordCount: (_) => 'Your topic should be at most 100 words'
              },
            ),

            const SizedBox(height: Insets.lg),

            AppDropdownField<String>(
              formControlName: 'category',
              labelText: 'Category',
              items: [
                for (final category in Categories)
                  DropdownMenuItem<String>(value: category, child: Text(category))
              ]
            ),

            const SizedBox(height: Insets.lg),

            ReactiveDiscussionSizeFields(formControlName: 'size'),

            const SizedBox(height: Insets.lg),

            ReactiveTagFormField(formControl: form.control('tags') as FormArray<String>),
            
            const SizedBox(height: Insets.lg),

            SubmitButton(label: 'Continue', onPressed: onSave)
          ],
        )
      ),
    );
  }
}
