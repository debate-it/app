import 'package:beamer/beamer.dart';
import 'package:debateit/src/widgets/navigation/icons/back.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/widgets/dialogs/info.dart';

import 'package:debateit/src/initiate/categories.dart';

import 'package:debateit/src/initiate/widgets/forms/size.dart';
import 'package:debateit/src/initiate/widgets/forms/tags.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';

import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';


class InitiateChallenge extends StatefulWidget {
  const InitiateChallenge();
  
  @override
  State<InitiateChallenge> createState() => _InitiateChallengeState();
}

class _InitiateChallengeState extends State<InitiateChallenge> {
  final form = FormGroup({
    'hours': FormControl<int>(value: 24),
    'position': FormControl<String>(),
    'discussion': FormGroup({
      'topic': FormControl<String>(
        validators: [
          Validators.required, 
          Validators.maxLength(2000), 
          WordCountValidation.maxWordLength(100)
        ]
      ),
      'category': FormControl<String>(validators: [Validators.required]),
      'tags': FormArray<String>([]),
      'size': FormControl<DiscussionSize>(validators: [Validators.required])
    })
  });


  @override
  Widget build(BuildContext context) {
    return ScrollablePageView(
      leading: const BackIcon(),
      padding: const EdgeInsets.all(Insets.md),
      body: ReactiveForm(
        formGroup: form,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
              AppTextField(
                formControlName: 'discussion.topic',
                labelText: 'Topic',
                minLines: 3, maxLines: 10,
                maxLength: 100,
                counterType: CounterType.words,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.newline,
                validationMessages: {
                  ValidationMessage.required: (_) => 'Please enter your topic',
                  WordCountValidationMessage.maxWordCount: (_) => 'Your topic should be at most 100 words'
                },
              ),
    
              const SizedBox(height: Insets.lg),
    
              AppDropdownField<String>(
                formControlName: 'discussion.category',
                labelText: 'Category',
                items: [
                  for (final category in Categories)
                    DropdownMenuItem<String>(value: category, child: Text(category))
                ]
              ),
    
              const SizedBox(height: Insets.lg),
    
              ReactiveDiscussionSizeFields(formControlName: 'discussion.size'),
    
              const SizedBox(height: Insets.lg),
    
              ReactiveTagFormField(formControl: form.control('discussion.tags') as FormArray<String>),
              
              const SizedBox(height: Insets.lg),
    
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Challenge Duration'),
                      Builder(
                        builder: (context) =>
                          Text('${(ReactiveForm.of(context)! as FormGroup).control('hours').value} hours', 
                            style: context.text.bodyMedium))
                    ],
                  ),
                  ReactiveSlider(
                    formControlName: 'hours',
                    min: 8.0, max: 48.0, divisions: 40,
                    activeColor: context.colorScheme.primary)
                ],
              ),
                
              const SizedBox(height: Insets.lg),
    
              SubmitButton(label: 'Continue', onPressed: submit)
          ],
        )
      ),
    );
  }

  Future<void> submit() async {
    final navigator = Beamer.of(context);

    var canceled = true;

    await showModalBottomSheet(
      context: context, 
      isScrollControlled: true,
      builder: (context) => BottomSheetTextField(
        inputLabel: 'initial position',

        wordLimit: (form.control('discussion.size').value as DiscussionSize).argumentLength,

        validators: [Validators.required],

        onSubmit: (position) async {
          form.control('position').value = position;
          canceled = false;
        }
      )
    );
        
    if (canceled) {
      return;
    }

    try {
      final result = await ChallengeProvider.initiate(form.value); 

      await showDialog(context: context, builder: (_) => 
        const InfoDialog(message: 'Your challenge was created successfully'));

      navigator.beamToReplacementNamed('/discussion/${result!.id}');
    } catch (err) {
      await showDialog(context: context, builder: (_) => 
        InfoDialog(message: err.toString().replaceAll('Exception:', '').trim()));
    }
  }
}
