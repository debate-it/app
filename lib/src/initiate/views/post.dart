import 'package:beamer/beamer.dart';
import 'package:debateit/src/widgets/navigation/icons/back.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/post/provider.dart';

import 'package:debateit/src/initiate/categories.dart'; 
import 'package:debateit/src/initiate/widgets/forms/tags.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';


class InitiatePost extends StatefulWidget {
  const InitiatePost();
  
  @override
  State<InitiatePost> createState() => _InitiatePostState();
}

class _InitiatePostState extends State<InitiatePost> {
  final form = FormGroup({
    'discussion': FormGroup({
      'topic': FormControl<String>(
        validators: [
          Validators.required, 
          Validators.maxLength(2000), 
          WordCountValidation.maxWordLength(100)
        ]
      ),
      'category': FormControl<String>(validators: [Validators.required]),
      'tags': FormArray<String>([]),
      'size': FormControl<DiscussionSize>(value: DiscussionSize.long)
    })
  });


  @override
  Widget build(BuildContext context) {
    return ScrollablePageView(
      leading: const BackIcon(),
      
      padding: const EdgeInsets.all(Insets.md),
      body: ReactiveForm(
        formGroup: form,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
              AppTextField(
                formControlName: 'discussion.topic',
                labelText: 'Topic',
                minLines: 3, maxLines: 10,
                maxLength: 100,
                counterType: CounterType.words,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.newline,
                validationMessages: {
                  ValidationMessage.required: (_) => 'Please enter your topic',
                  WordCountValidationMessage.maxWordCount: (_) => 'Your topic should be at most 100 words'
                },
              ),
    
              const SizedBox(height: Insets.lg),
    
              AppDropdownField<String>(
                formControlName: 'discussion.category',
                labelText: 'Category',
                items: [
                  for (final category in Categories)
                    DropdownMenuItem<String>(value: category, child: Text(category))
                ]
              ),
    
              const SizedBox(height: Insets.lg),
    
              ReactiveTagFormField(formControl: form.control('discussion.tags') as FormArray<String>),
              
              const SizedBox(height: Insets.lg),
    
              SubmitButton(label: 'Continue', onPressed: submit)
          ],
        )
      ),
    );
  }

  Future<void> submit() async {
    final navigator = Beamer.of(context);

    try {
      final result = await PostProvider.initiate(context, form.value);

      if (result == null) {
        throw Exception('There was a problem creating your post');
      }

      await showDialog(context: context, builder: (context) => 
        const InfoDialog(message: 'Your post has been submitted'));

      if (!mounted) {
        return;
      }

      navigator.beamToReplacementNamed('/discussion/${result.id}');
        
    } on Exception catch (err)  {
      await showDialog(context: context, builder: (context) =>
        InfoDialog(message: err.toString()));
    }
  }
}
