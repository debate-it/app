import 'package:beamer/beamer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/request.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';

import 'package:debateit/src/initiate/categories.dart';

import 'package:debateit/src/initiate/opponent/list.dart';

import 'package:debateit/src/initiate/widgets/opponent_field.dart';
import 'package:debateit/src/initiate/widgets/forms/size.dart';
import 'package:debateit/src/initiate/widgets/forms/tags.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';

import 'package:debateit/src/widgets/navigation/icons/back.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';


enum InitiateDebateStage {
  fillingInfo,
  choosingOpponent
}

class InitiateDebate extends StatefulWidget {
  const InitiateDebate();
  
  @override
  State<InitiateDebate> createState() => _InitiateDebateState();
}

class _InitiateDebateState extends State<InitiateDebate> {
  final controller = PageController();

  final form = FormGroup({
    'opponent': FormControl<String>(),

    'info': FormGroup({
      'isOpenRequest': FormControl<bool>(value: false),
      'topic': FormControl<String>(
        validators: [
          Validators.required, 
          Validators.maxLength(2000), 
          WordCountValidation.maxWordLength(100)
        ]
      ),
      'category': FormControl<String>(validators: [Validators.required]),
      'tags': FormArray<String>([]),
      'size': FormControl<DiscussionSize>(validators: [Validators.required])
    })
  });

  // InitiateDebateStage _stage = InitiateDebateStage.fillingInfo;

  // static const opponentSelectionRoute = 'opponent-selection';
  @override
  void didChangeDependencies() {
    final route = Beamer.of(context).currentBeamLocation.state as BeamState;
    final opponent = route.queryParameters['opponent'];

    if (opponent != null) {
      form.control('opponent').value = opponent;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ScrollablePageView(
      leading: CustomBackIcon(
        onPressed: () {
          if (controller.page != null && controller.page! >= 1.0) {
            controller.animateToPage(0, duration: const Duration(milliseconds: 500), curve: Curves.easeInOutCubicEmphasized);
          } else {
            Beamer.of(context).beamBack();
          }
        },
      ),
      body: ConstrainedBox(
        constraints: BoxConstraints.loose(MediaQuery.of(context).size),
        child: PageView(
          controller: controller,
          allowImplicitScrolling: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            InitiateDebateForm(
              form: form.control('info') as FormGroup, 
              opponent: form.control('opponent').value as String?,
              onSave: onInfoSave
            ),
    
            OpponentSearchList(onSubmit: submitDebateRequest)
          ]
        )
      ),
    );
  }

  void onInfoSave() {
    final isOpenRequest = form.control('info.isOpenRequest').value as bool;
    final chosenOpponent = form.control('opponent').value as String?;
    
    if (isOpenRequest || chosenOpponent != null) {
      submit();
    } else {
      controller.animateToPage(1, duration: const Duration(milliseconds: 300), curve: Curves.easeInCubic);
    }
  }

  Future<void> submitDebateRequest(List<UserProfile> recipients) async {
    form.control('opponent').value = recipients.single.id;

    await submit();
  }

  Future<void> submit() async {
    var failed = false;
    var message = 'Request was sent successfully';

    final navigator = Beamer.of(context);

    try {
      final isOpenRequest = form.control('info.isOpenRequest').value as bool;
      final mutation = isOpenRequest
        ? OpenRequestQueries.CreateOpenRequest
        : RequestQueries.CreateDebateRequest;

      final payload = <String, dynamic>{
        'discussion': Map.fromEntries(
          (form.control('info').value as Map<String, dynamic>)
            .entries
            .where((element) => element.key != 'isOpenRequest')
        ) 
      };


      if (isOpenRequest) {
        payload['type'] = OpenRequestType.Debate.parameter;
      } else {
        payload['recipient'] = form.control('opponent').value as String;
      }

      await Services.client.mutate(mutation: mutation, variables: payload); 
    } catch (err) {
      message = err.toString().replaceAll('Exception:', '').trim();
      failed = true;
    }

    await showDialog(context: context, builder: (_) => InfoDialog(message: message));

    if (!failed && mounted) {
      navigator.beamToReplacementNamed('/?tab=discussions');
    }
  }
}

class InitiateDebateForm extends StatelessWidget {
  const InitiateDebateForm({super.key, required this.form, required this.onSave, this.opponent});

  final FormGroup form;
  final VoidCallback onSave;
  final String? opponent;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Insets.md),
      child: ReactiveForm(
        formGroup: form,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (opponent != null)
              ChoseOpponentField(opponentId: opponent!)
            else 
              ListTile(
                title: Text('Create open debate request?',
                  style: context.text.bodyMedium?.copyWith(color: context.colors.text)),
                trailing: ReactiveSwitch.adaptive(formControlName: 'isOpenRequest')),
                
            const SizedBox(height: Insets.md),

            AppTextField(
              formControlName: 'topic',
              labelText: 'Topic',
              minLines: 3, maxLines: 10,
              maxLength: 100,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              counterType: CounterType.words,
              validationMessages: {
                ValidationMessage.required: (_) => 'Please enter your topic',
                WordCountValidationMessage.maxWordCount: (_) => 'Your topic should be at most 100 words'
              },
            ),

            const SizedBox(height: Insets.lg),

            AppDropdownField<String>(
              formControlName: 'category',
              labelText: 'Category',
              items: [
                for (final category in Categories)
                  DropdownMenuItem<String>(value: category, child: Text(category))
              ],
              validationMessages: {
                ValidationMessage.required: (_) => 'Please, select your category'
              },
            ),

            const SizedBox(height: Insets.lg),

            ReactiveDiscussionSizeFields(formControlName: 'size'),

            const SizedBox(height: Insets.lg),

            ReactiveTagFormField(formControl: form.control('tags') as FormArray<String>),
            
            const SizedBox(height: Insets.lg),

            SubmitButton(label: 'Continue', onPressed: onSave)
          ],
        )
      ),
    );
  }
}
