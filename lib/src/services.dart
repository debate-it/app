import 'package:debateit/src/repositories/notifications.dart';
import 'package:debateit/src/repositories/profile.dart';
import 'package:debateit/src/services/notification.dart';
import 'package:debateit/src/services/profile.dart';
import 'package:get_it/get_it.dart';

import 'package:debateit_graphql/graphql.dart';
import 'package:debateit_authentication/authentication.dart';

abstract class Services {
  static final di = GetIt.instance;
  
  static BackendClient get client => di.get<BackendClient>();

  static AuthenticationRepository get authentication => di.get<AuthenticationRepository>();
  static ProfileRepository get profile => di.get<ProfileRepository>();
  static ProfileService get profileService => di.get<ProfileService>();

  static NotificationRepository get notificationRepository => di.get<NotificationRepository>();
  static NotificationService get notificationService => di.get<NotificationService>();


  static void initialize() {
    // final client = BackendClient(
    //   const BackendClientConfig(
    //     backendUri: 'http://10.0.2.2:9000/graphql',
    //     websocketUri: 'ws://10.0.2.2:9000/graphql'));
    
    final client = BackendClient(
      const BackendClientConfig(
        backendUri: 'https://api.debateit.io/graphql',
        websocketUri: 'wss://api.debateit.io/graphql'));

    final authenticationRepository = AuthenticationRepository(client: client);
    final authenticationBloc = AuthenticationBloc(authenticationRepository: authenticationRepository);

    di.registerSingleton<BackendClient>(client);

    di.registerLazySingleton(() => authenticationRepository);
    di.registerLazySingleton(() => authenticationBloc);
    di.registerLazySingleton(() => ProfileRepository());
    di.registerLazySingleton(() => ProfileService(authenticationBloc));

    di.registerSingleton(NotificationService());
    di.registerSingleton(NotificationRepository());
  }
}
