import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/graphql.dart';
import 'package:debateit_graphql/queries/profile.dart';
import 'package:debateit_authentication/authentication.dart';

import 'package:debateit/src/services.dart';


class ProfileIndicators {
  int notifications = 0;
  int debates = 0;
  int requests = 0;

  ProfileIndicators({ this.notifications = 0, this.debates = 0, this.requests = 0 });

  ProfileIndicators.fromJson(Map<String, dynamic> json)
    : notifications = json['notifications'] as int,
      requests = json['requests'] as int,
      debates = json['debates'] as int;

  ProfileIndicators copyWith({ int? notifications, int? debates, int? requests }) =>
    ProfileIndicators(
      notifications: notifications ?? this.notifications,
      requests: requests ?? this.requests,
      debates: debates ?? this.debates
    );
}

class ProfileData {
  UserProfile profile;
  ProfileIndicators indicators;

  ProfileData({ required this.profile, required this.indicators });
}

class ProfileService extends ChangeNotifier {
  ProfileData? _info;

  ProfileData? get info => _info;

  bool _initialized = false;
  bool get initialzied => _initialized;

  BackendClient get client => Services.client;

  ProfileService(AuthenticationBloc bloc) {
    bloc.stream.listen((event) {
      if (event.status == AuthenticationStatus.authenticated) {
        _info = ProfileData(
          profile: event.user!,
          indicators: ProfileIndicators()
        );

        _fetchProfileInfo();
      }
    });
  }

  void refetch() {
    _fetchProfileInfo();
  }

  void updateProfile(UserProfile update) {
    _info?.profile = update;
    notifyListeners();
  }
  
  void updateImage(String url) {
    info?.profile.info.picture = url;
    notifyListeners();
  } 

  Future<void> updateExtraInfo(Map<String, dynamic> payload) async {
    final result = await Services.client.mutate<Map<String, dynamic>>(
      mutation: ProfileQueries.UpdateAboutInfo, 
      variables: { 'options': payload }
    );

    _info?.profile = UserProfile.fromJson(result);
    notifyListeners();
  }
 
  Future<void> updateIndicators() async {
    final result = await client.fetch(query: ProfileQueries.Indicators);

    // ignore: avoid_dynamic_calls
    final data = result.data!['profile']['indicators'] as Map<String, dynamic>;

    _info?.indicators = ProfileIndicators(
      debates: data['debates'] as int,
      requests: data['requests'] as int,
      notifications: data['notifications'] as int
    );

    notifyListeners();
  }

  Future<void> updateStats() async {
    final result = await client.fetch(query: ProfileQueries.Stats);

    // ignore: avoid_dynamic_calls
    final info = result.data!['profile']['info'] as Map<String, dynamic>;

    final stats = info['stat'] as Map<String, dynamic>;
    final progress = info['progress'] as Map<String, dynamic>;

    _info = ProfileData(
      indicators: _info!.indicators,
      profile: UserProfile(
        id: _info!.profile.id,

        info: _info!.profile.info,

        relation: _info!.profile.relation,
        lastNotification: _info!.profile.lastNotification,

        username: _info!.profile.username,
        fullname: _info!.profile.fullname,

        stats: UserStats.fromJson(stats),
        progress: UserProgress.fromJson(progress)
      )
    );

    notifyListeners();
  }
  
  Future<void> setDeviceToken(String? token) async {
    await client.mutate(mutation: ProfileQueries.SetDeviceToken, variables: { 'token': token });
  }

  Future<void> unsetDeviceToken()  async {
    await client.mutate(mutation: ProfileQueries.UnsetDeviceToken);
  }

  
  Future<void> _fetchProfileInfo() async {
    final result = await client.fetch(query: ProfileQueries.Info);
    final data = result.data!['profile'] as Map<String, dynamic>;
    
    _info = ProfileData(
      profile: UserProfile.fromJson(data['info'] as Map<String, dynamic>),
      indicators: ProfileIndicators.fromJson(data['indicators'] as Map<String, dynamic>));

    _initialized = true;

    bootstrapAnalytics();
    notifyListeners();
  } 

  Future<void> bootstrapAnalytics() async {
    final analytics = FirebaseAnalytics.instance;

    await analytics.setUserId(id: _info!.profile.id);

    await Future.wait([
      if (_info!.profile.info.country != null)
        analytics.setUserProperty(name: 'country', value: _info!.profile.info.country),

      if (_info!.profile.info.education != null)
        analytics.setUserProperty(name: 'education', value: _info!.profile.info.education),
      
      if (_info!.profile.info.occupation != null)
        analytics.setUserProperty(name: 'occupation', value: _info!.profile.info.occupation),
      
      if (_info!.profile.info.religion != null)
        analytics.setUserProperty(name: 'religion', value: _info!.profile.info.religion),

      if (_info!.profile.info.politics != null)
        analytics.setUserProperty(name: 'politics', value: _info!.profile.info.politics)
    ]);
  }
}
