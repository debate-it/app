import 'dart:async';
import 'dart:convert';

import 'package:beamer/beamer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart'
  hide NotificationSettings;

import 'package:debateit/src/app.dart';
import 'package:debateit/src/settings/notifications.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/profile.dart';


enum NotificationSetting {
  requests,
  followers,
  discussion,
  comments,
  votes
}

Future<void> backgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

class NotificationService {
  static final _instance = NotificationService._internal();
  static const VAPID_KEY = 'BNl50FMRuXOiDb9nZIt9HPBfU8UQoHlipO8ecK7Q2hhKioyA-51bEypIjFdwV-779Z9LRKiEz6hxVgB-KRT31CI';

  ProfileService get _profile => Services.profileService;

  bool _initialized = false;

  late final _plugin = FlutterLocalNotificationsPlugin();
  late Map<dynamic, bool?> _settings;

  factory NotificationService() {
    return _instance;
  }

  NotificationService._internal() {
    _profile.addListener(_initPlugin);

    final box = Hive.box<bool?>(NotificationSettings.boxName);

    _settings = box.toMap();

    box.listenable().addListener(() {
      _settings = box.toMap();
    });
  }

  int _idCounter = 0;
  int get _uniqueId {
    return _idCounter++;
  }

  Future<void> init() async {
    await _instance._initPlugin();
    await _instance._initLocalNotifications();
  }

  
  Future<void> _initPlugin() async {
    FirebaseMessaging.instance.setAutoInitEnabled(true);

    final messaging = FirebaseMessaging.instance;
    
    final settings = await messaging.getNotificationSettings();

    if (settings.authorizationStatus == AuthorizationStatus.notDetermined) {
      await messaging.requestPermission();
    }

    await _saveToken(messaging.getToken(vapidKey: kIsWeb ? VAPID_KEY : null));

    messaging.onTokenRefresh.listen(_saveToken);
    
    if (_initialized) {
      return;
    }

    
    final message = await FirebaseMessaging.instance.getInitialMessage();

    if (message != null) {
      _onSelectNotification(message);
    }
    
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      _onSelectNotification(message);
    });

    FirebaseMessaging.onMessage.listen((message) {
      _updateAppState(message);
      _displayNotification(message); 
    });

    FirebaseMessaging.onBackgroundMessage(backgroundHandler);

    _initialized = true;
  }

  Future<void> _initLocalNotifications() async {

    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    const initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/launcher_icon');
    // final initializationSettingsIOS = IOSInitializationSettings(
    //         onDidReceiveLocalNotification: onDidReceiveLocalNotification);

    // const initializationSettingsMacOS = MacOSInitializationSettings();

    const initializationSettings = InitializationSettings(android: initializationSettingsAndroid);

    await _plugin.initialize(initializationSettings,
      onDidReceiveNotificationResponse: _onSelectLocalNotification);
  }

  void destroyPlugin() {
    _initialized = false;
  }

  Future<void> _saveToken(FutureOr<String?> token) async  {
    if (Services.authentication.isLoggedIn) {
      await _profile.setDeviceToken(await token);
    }
  }

  void _updateAppState(RemoteMessage message) {
    switch (message.data['type']) {
      case 'UserFollow':
        _profile.updateStats();
        break;

      case 'DebateRequestSent':
      case 'DebateCreated':
      case 'DebateArgumentAdded':
      case 'PanelRequestSent':
      case 'PanelStarted':
        _profile.updateIndicators();
        break;

      case 'DebateFinished':
      case 'PanelFinished':
        _profile.updateStats();
        _profile.updateIndicators();
        break;
    }
  }

  NotificationSetting _mapType(RemoteMessage message) {
    switch (message.data['type']) {
      case 'UserFollow':
        return NotificationSetting.followers;


      case 'DebateRequestSent':
      case 'PanelRequestSent':
      case 'PanelRequestAccepted':
      case 'RequestRejected':
      case 'RequestCandidacySubmitted':
        return NotificationSetting.requests;


      case 'DebateCreated':
      case 'DebateArgumentAdded':
      case 'DebateFinished':

      case 'PanelStarted':
      case 'PanelFinished':

      case 'ChallengeFinished':
      case 'ChallengeWinnerChosen':
        return NotificationSetting.discussion;


      case 'CommentCreated':
      case 'ChallengeResponse':
        return NotificationSetting.comments;


      case 'Vote':
        return NotificationSetting.votes;


      default:
        throw Exception('Invalid exception type');
    }
  }

  String _getGroupKey(RemoteMessage message) {
    final link = message.data['link'] as String;
    final tokens = link.split('/');
    
    final target = tokens[0];
    final id = tokens[1];

    if (['Debate', 'Panel', 'Post', 'Challenge'].contains(target)) {
      return 'com.debateit.notifications.discussion.$id';
    }

    return 'com.debateit.notifications';
  }

  Future<void> _displayNotification(RemoteMessage message) async {
    final setting = _mapType(message);
    final isEnabled = _settings[setting.name] ?? true;

    if (!isEnabled) {
      return;
    }

    final androidDetails = AndroidNotificationDetails(
      'com.debateit.notification',
      'Debate It - Notifications',
      visibility: NotificationVisibility.public,
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',

      groupKey: _getGroupKey(message),

      colorized: true,
      // color: Theme.of(DebateItApp.navigator.currentContext!).colorScheme.surface,

    );

    final details = NotificationDetails(android: androidDetails);

    await _plugin.show(
      _uniqueId,
      message.notification!.title,
      message.notification!.body, 
      details,
      payload: jsonEncode(message.data)
    );
  }

  void _onSelectLocalNotification(NotificationResponse notification) {
    final payload = notification.payload;

    if (payload == null) {
      return;
    }

    final data = jsonDecode(payload) as Map<String, dynamic>;

    final link = data['link'] as String;

    final tokens = link.split('/');

    final target = tokens[0];
    final id = tokens[1];

    _redirect(target, id);
  }

  static void _onSelectNotification(RemoteMessage message) {
    final link = message.data['link'] as String;
    final tokens = link.split('/');
    
    final target = tokens[0];
    final id = tokens[1];

    _redirect(target, id);
  }

  static void _redirect(String target, String id) {
    final router = Beamer.of(DebateItApp.navigator.currentContext!);

    switch (target) {
      case 'DebateRequest':
        router.beamToNamed('/request/$id');
        break;

      case 'Debate':
        router.beamToNamed('/discussion/$id');
        break;

      case 'Challenge':
        router.beamToNamed('/discussion/$id');
        break;
        
      case 'Panel':
        router.beamToNamed('/discussion/$id');
        break;
        
      case 'Post':
        router.beamToNamed('/discussion/$id');
        break;
        
      case 'UserProfile':
        router.beamToNamed('/user/$id');
        break;
    }
  }
}
