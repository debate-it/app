import 'package:debateit/src/app.dart';
import 'package:flutter/material.dart';

class SnackbarService {
  ScaffoldMessengerState get messenger => DebateItApp.messenger.currentState!;

  void show() {
    messenger.showSnackBar(const SnackBar(content: SizedBox()));
  }
}
