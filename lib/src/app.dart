import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';


import 'package:beamer/beamer.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_authentication/authentication.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/home/home.dart';

import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/views/about.dart';
import 'package:debateit/src/discussions/user/views/followers.dart';
import 'package:debateit/src/discussions/user/views/subscriptions.dart';

import 'package:debateit/src/profile/views/user.dart';
import 'package:debateit/src/profile/views/about.dart';
import 'package:debateit/src/profile/views/settings.dart';
import 'package:debateit/src/profile/views/followers.dart';
import 'package:debateit/src/profile/views/subscriptions.dart';
import 'package:debateit/src/profile/views/notifications.dart';

import 'package:debateit/src/initiate/main.dart';

import 'package:debateit/src/discussions/category/views/full.dart';
import 'package:debateit/src/discussions/user/views/page.dart';
import 'package:debateit/src/discussions/request/views/open.dart';
import 'package:debateit/src/discussions/main.dart';

import 'package:debateit/src/authentication/authentication.dart';

import 'package:debateit_core/widgets/brand/loader.dart';


class DebateItAppProvider extends StatelessWidget {
  const DebateItAppProvider({ super.key, required this.child });

  final Widget child;

  @override
  Widget build(BuildContext context){
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => Services.di.get<AuthenticationBloc>())
      ], 
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => ConfigProvider())
        ],
        child: child
      )
    );
  }
}

/// The Widget that configures your application.
class DebateItApp extends StatefulWidget {
  static final navigator = GlobalKey<NavigatorState>();
  static final messenger = GlobalKey<ScaffoldMessengerState>();

  const DebateItApp({super.key});

  @override
  State<DebateItApp> createState() => _DebateItAppState();
}

class _DebateItAppState extends State<DebateItApp> {
  @override
  Widget build(BuildContext context) {
    // Glue the SettingsController to the MaterialApp.
    //
    // The AnimatedBuilder Widget listens to the SettingsController for changes.
    // Whenever the user updates their settings, the MaterialApp is rebuilt.
    // final size = MediaQuery.of(context).size;



    return DebateItAppProvider(
      child: Builder(
        builder: (context) {
          return BlocListener<AuthenticationBloc, AuthenticationState>(
            listener: (context, state) {
              routerDelegate.update();
            },
            child: DefaultTextStyle(
              style: context.text.bodyMedium!.copyWith(color: context.colors.text),
              child: MaterialApp.router(
                // Providing a restorationScopeId allows the Navigator built by the
                // MaterialApp to restore the navigation stack when a user leaves and
                // returns to the app after it has been killed while running in the
                // background.
                restorationScopeId: 'app',
                
                // Provide the generated AppLocalizations to the MaterialApp. This
                // allows descendant Widgets to display the correct translations
                // depending on the user's locale.
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                supportedLocales: const [
                  Locale('en', ''), // English, no country code
                ],
            
                scaffoldMessengerKey: DebateItApp.messenger,
                
                // Use AppLocalizations to configure the correct application title
                // depending on the user's locale.
                //
                // The appTitle is defined in .arb files found in the localization
                // directory.
                onGenerateTitle: (BuildContext context) => 'Debate It',
                
                // Define a light and dark color theme. Then, read the user's
                // preferred ThemeMode (light, dark, or system default) from the
                // SettingsController to display the correct theme.
                theme: context.theme,
                // darkTheme: context.theme,
                // themeMode: settingsController.themeMode,
                routerDelegate: routerDelegate,
                routeInformationParser: BeamerParser(),
                backButtonDispatcher: BeamerBackButtonDispatcher(
                  alwaysBeamBack: true, 
                  delegate: routerDelegate))),
          );
    }));

  }

  final routerDelegate = BeamerDelegate(
    guards: [
      BeamGuard(
        guardNonMatching: true,
        pathPatterns: ['/login', '/signup', '/activation', '/forgot-password'],
        check: (context, location) {
          final auth = BlocProvider.of<AuthenticationBloc>(context);

          return auth.state.status == AuthenticationStatus.authenticated;
        },
        beamToNamed: (origin, target) => '/login'
        // showPage: const BeamPage(name: '/login', type: BeamPageType.fadeTransition, child: AuthenticationPage())
      )
    ],

    clearBeamingHistoryOn: { '/', '/login' },

    locationBuilder: RoutesLocationBuilder(
      routes: {
        '/': (context, state, extra) => const RootPage(),

        // Auth routes
        '/login': (context, state, extra) => const AuthenticationPage(),
        '/signup': (context, state, extra) => const SignupPage(),
        '/activation': (context, state, extra) => const ActivationPage(),
        '/forgot-password': (context, state, extra) => const ForgotPasswordPage(),
        
        '/category-subscription': (context, state, extra) => const SizedBox(),
        '/tutorial': (context, state, extra) => const SizedBox(),

        // Profile routes
        '/profile': (context, state, extra) => const ProfilePage(),
        '/profile/about': (context, state, extra) => ProfileAboutPage(provider: extra as UserProvider?),
        '/profile/followers': (context, state, extra) => const ProfileFollowersPage(),
        '/profile/subscriptions': (context, state, extra) => const ProfileSubscriptionsPage(),
        '/profile/settings': (context, state, extra) => const ProfileSettingsPage(),
        
        '/notifications': (context, state, extra) => const ProfileNotificationPage(),

        // User routes
        '/user/:id': (context, state, extra) => UserPage(id: state.pathParameters['id']!),
        '/user/:id/about': (context, state, extra) => UserAboutPage(id: state.pathParameters['id']!, provider: extra as UserProvider?),
        '/user/:id/followers': (context, state, extra) => UserFollowers(id: state.pathParameters['id']),
        '/user/:id/subscriptions': (context, state, extra) => UserSubscriptionsPage(id: state.pathParameters['id']),
        
        // Discussion routes
        '/request/:id': (context, state, extra) => OpenRequestPage(id: state.pathParameters['id']!),
        '/discussion/:id': (context, state, extra) => 
          BeamPage(
            type: BeamPageType.slideTransition,
            child: DiscussionPage<Discussion, EntityProvider<Discussion>>(
              id: state.pathParameters['id']!,
              provider: extra as EntityProvider<Discussion>?
            ),
          ),
        '/discussion/category/:id': (context, state, extra) => CategoryPage(id: state.pathParameters['id']!),
        
        '/initiate/:discussionType': (context, state, extra) =>
          InitiatePage.fromViewName(viewName: state.pathParameters['discussionType']),
      }
    )
  );
}

class RootPage extends StatelessWidget {
  const RootPage({super.key});

  @override
  Widget build(BuildContext context) {
    final beamer = Beamer.of(context);
    final auth = BlocProvider.of<AuthenticationBloc>(context, listen: true);

    if (auth.state.status != AuthenticationStatus.authenticated) {
      Future.microtask(() => beamer.beamToNamed('/login'));
      return const Center(child: Loader());
    }

    return HomePage.tab((beamer.configuration.state as BeamState?)?.queryParameters['tab']);
  }
}
