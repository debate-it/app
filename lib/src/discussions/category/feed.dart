import 'package:flutter/material.dart';

import 'package:debateit/src/discussions/category/provider.dart';


class CategoryFeed extends StatelessWidget {
  const CategoryFeed({super.key});
  
  @override
  Widget build(BuildContext context) {
    return CategoryProvider.of(context).feed(context);
  }
}
