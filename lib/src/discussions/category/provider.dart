import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';
import 'package:debateit_graphql/queries/category.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/config/pagination/category.dart';


class CategoryProvider extends ChangeNotifier {
  CategoryProvider(this.category);

  static CategoryProvider of(BuildContext context, { bool listen = false })
    => Provider.of<CategoryProvider>(context, listen: listen);

  final DiscussionCategory category;

  static Future<DiscussionCategory> fetch(BuildContext context, String id) async {
    final client = Services.client;

    final result = await client.fetch(
      query: CategoryQueries.Category,
      variables: { 'id': id }
    ); 

    return DiscussionCategory.fromJson(result.data!['category'] as Map<String, dynamic>);
  }

  Future<void> subscribe(BuildContext context) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: CategoryQueries.Subscribe,
      variables: { 'id': category.id }
    );

    // ignore: avoid_dynamic_calls
    category.stat?.followers = result['stat']['followers'] as int;
    category.following = result['isSubscribed'] as bool;
    
    notifyListeners();
  }
  
  Widget feed(BuildContext context) {
    return SliverPaginationProvider(
      client: Services.client,
      key: const ValueKey<String>('category-debates'),
      config: CategoryFeedConfig(category.id),
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(5));
  }
}
