import 'package:debateit/src/discussions/category/feed.dart';
import 'package:debateit/src/discussions/category/provider.dart';
import 'package:debateit/src/discussions/category/widgets/info.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';
import 'package:debateit/src/widgets/pages/PageAppBar.dart';
import 'package:debateit_core/core.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:provider/provider.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({super.key, required this.id});

  final String id;

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> with TickerProviderStateMixin {
  TabController? controller;

  @override
  void initState() {
    controller = TabController(length: 2, vsync: this);
    super.initState(); 
  }

  @override
  Widget build(BuildContext context) {
    return BarlessView(
      padding: const EdgeInsets.all(Insets.xs),
      body: buildContent(context));
  }

  Widget buildContent(BuildContext context) {
    return FutureBuilder(
      future: CategoryProvider.fetch(context, widget.id),
      builder: (context, AsyncSnapshot<DiscussionCategory> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.hasError) {
          // Future.microtask(() => SnackbarHelper.showSnackBarError(context, snapshot.error! as Exception));

          return Text(snapshot.error.toString()); 
        } else {
          return ChangeNotifierProvider(
            create: (_) => CategoryProvider(snapshot.data!),
            child: buildView(context)
          );
        }
      }
    );
  }

  Widget buildView(BuildContext context) {
    return const CustomScrollView(
      slivers: [
        FlexibleViewAppBar(pinned: true, expandedHeight: 300, background: CategoryInfo()),
        CategoryFeed()
      ],
    );
  }
}
