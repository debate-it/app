import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';


class CategoryTile extends StatelessWidget {
  final DiscussionCategory category;

  const CategoryTile({ super.key, required this.category });

  @override
  Widget build(BuildContext context) {
    final colors = context.colors;

    final label = Text(category.name,
      style: context.text.titleLarge);

    final icon = Padding(
      padding: const EdgeInsets.all(Insets.sm),
      child: Icon(Icons.check, color: colors.secondary, size: 24));

    final header = Row(
      children: [ icon, label ]);

    final description = Text(category.description!,
      style: context.text.titleMedium);

    return Card(
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(Insets.md),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [ header, description ])));
  }
}

class CategorySelectionTile extends StatelessWidget {
  final DiscussionCategory category;

  final bool isSelected;
  final Function(DiscussionCategory) onSelected;

  const CategorySelectionTile({
    super.key,
    required this.category,
    required this.isSelected,
    required this.onSelected
  });

  @override
  Widget build(BuildContext context) {
    final colors = context.colors;

    final label = Text(category.name, style: context.text.titleLarge);

    final icon = Padding(
      padding: const EdgeInsets.all(Insets.sm),
      child: Icon(Icons.check, color: colors.secondary, size: 24));

    const spacer = Spacer();

    final checkbox = IgnorePointer(
      child: Checkbox(
        value: isSelected, 
        activeColor: context.colors.primary,
        checkColor: context.colors.text,
        onChanged: (_) {}));

    final header = Row(
      children: [ icon, label, spacer, checkbox ]);

    final description = Text(category.description!,
      style: context.text.titleMedium);

    return Card(
      child: InkWell(
        onTap: () => onSelected(category),
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(Insets.md),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [ header, description ]))));
  }
}
