import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';


class CategoryTabs extends StatelessWidget implements PreferredSizeWidget {
  final TabController controller; 

  const CategoryTabs({ super.key, required this.controller });
  
  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: context.colorScheme.background,
      child: TabBar(
        controller: controller,

        labelColor: context.colorScheme.onBackground,
        labelStyle: context.text.headlineMedium,

        tabs: const [ Tab(text: 'Debates'), Tab(text: 'Challenges') ]
      )
    );
  }
}
