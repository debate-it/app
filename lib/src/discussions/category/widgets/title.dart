import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit/src/discussions/category/provider.dart';
import 'package:debateit/src/discussions/category/widgets/follow.dart';


class CategoryTitleRow extends StatelessWidget {
  const CategoryTitleRow({ super.key });

  @override
  Widget build(BuildContext context) { 
    final category = CategoryProvider.of(context).category;

    final name = Text(category.name,
      style: context.text.headlineMedium
        ?.copyWith(fontWeight: FontWeight.w200));

    const followButton = CategoryFollowButton();

    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ name, followButton ]));
  }
}
