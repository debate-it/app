import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/category/provider.dart';


class CategoryDescription extends StatelessWidget {
  const CategoryDescription({ super.key });

  @override
  Widget build(BuildContext context) {
    final category = CategoryProvider.of(context).category;

    if (category.description == null) {
      return const SizedBox();
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Insets.md),
      child: Text(category.description!,
        textAlign: TextAlign.start,
        textWidthBasis: TextWidthBasis.parent,
        style: context.text.bodyLarge));
  }
}
