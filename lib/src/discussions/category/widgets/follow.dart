import 'package:debateit/src/discussions/category/provider.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:flutter/material.dart';



class CategoryFollowButton extends StatefulWidget {
  const CategoryFollowButton({ super.key });

  @override
  State<CategoryFollowButton> createState() => _CategoryFollowButtonState();
}

class _CategoryFollowButtonState extends State<CategoryFollowButton> {
  bool loading = false;

  @override
  Widget build(BuildContext context) { 
    final provider = CategoryProvider.of(context, listen: true);
    final following = provider.category.following ?? false;

    return PrimaryBtn(
      loading: loading,
      label: following ? 'FOLLOWING' : 'FOLLOW',
      icon: following ? Icons.check : null,
      onPressed: () => follow(context));
  }

  Future<void> follow(BuildContext context) async {
    final provider = CategoryProvider.of(context);

    try {
      setState(() { loading = true; });

      await provider.subscribe(context);

    } finally {
      setState(() { loading = false; });
    }
  }
}
