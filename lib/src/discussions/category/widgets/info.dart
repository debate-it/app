import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/category/provider.dart';

import 'package:debateit/src/discussions/category/widgets/description.dart';
import 'package:debateit/src/discussions/category/widgets/stats.dart';
import 'package:debateit/src/discussions/category/widgets/title.dart';


class CategoryInfo extends StatelessWidget {
  const CategoryInfo({ super.key });

  @override
  Widget build(BuildContext context) {
    final category = CategoryProvider.of(context).category; 

    final avatar = CircleAvatar(
      radius: 60.0,
      backgroundColor: context.colorScheme.surface,
      child: Text(category.name[0],
        style: context.text.headlineLarge
          ?.copyWith(fontWeight: FontWeight.w300)));
    
    const titleRow = CategoryTitleRow();
    const description = CategoryDescription();
    const stats = CategoryStats();

    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [ avatar, titleRow, description, stats ]));
  }
}
