import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/category/provider.dart';


class CategoryStats extends StatelessWidget {
  const CategoryStats({ super.key });

  @override
  Widget build(BuildContext context) { 
    final category = CategoryProvider.of(context, listen: true).category;
    final style = context.text.bodyLarge;

    return Container(
      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 3),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text('${category.stat?.followers} followers', style: style),
          Padding(
            padding: const EdgeInsets.all(Insets.xs),
            child: Icon(Icons.lens, size: 5, color: style?.color )),
          Text('${category.stat?.discussions} discussions', style: style)
        ],
      )
    );
  }
}
