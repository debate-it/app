import 'package:debateit/src/services.dart';
import 'package:debateit_graphql/queries/request.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/discussions/request/open/provider.dart';


typedef ResultBuilder<T> = T? Function(Map<String, dynamic>);

class RequestProvider<T extends Request> extends EntityProvider<T> {

  RequestProvider(T request)
    : super(entity: request);

  static RequestProvider<T> of<T extends Request>(BuildContext context, { bool listen = false }) {
    if (T == OpenRequest) {
      return OpenRequestProvider.of(context, listen: listen) as RequestProvider<T>;
    } else {
      return Provider.of<RequestProvider<T>>(context, listen: listen);
    }
  }

  T get request => entity;

  void updateRequest(T updated) {
    entity = updated;
    notifyListeners();
  }

  Future<TResult?> accept<TResult>({
    required BuildContext context,
    required T request, 
    required ResultBuilder<TResult> builder,  
    String? argument
  }) {

    return _resolve(
      context: context,
      request: request,
      builder: builder,
      argument: argument,
      status: ResolveStatus.accept);
  }
  
  Future<TResult?> reject<TResult>({
    required BuildContext context,
    required T request, 
    ResultBuilder<TResult>? builder,  
  }) {

    return _resolve(
      context: context,
      request: request,
      builder: builder,
      status: ResolveStatus.reject);
  }

  Future<TResult?> _resolve<TResult>({
    required BuildContext context,
    required T request, 
    required ResolveStatus status,
    ResultBuilder<TResult>? builder,  
    String? argument
  }) async {
    final client = Services.client;

    final mutation = _getMutation();
    final payload = { 'id': request.id, 'status': status.parameter };

    // Update the payload for accepting a panel request
    if (request is PanelRequest && status == ResolveStatus.accept) {
      if (argument == null) {
        throw Exception('Panel argument is not provided');
      }

      payload['argument'] = argument;
    }

    final result = await client.mutate<Map<String, dynamic>?>(mutation: mutation, variables: payload);

    // if (result == null) {
    //   throw Exception('Error occured while accepting request');
    // }

    if (status == ResolveStatus.reject) {
      return null;
    }

    return builder!(result!);
  }

  String _getMutation() {
    if (T == PanelRequest) {
      return RequestQueries.ResolvePanelRequest;
    }

    if (T == DebateRequest) {
      return RequestQueries.ResolveDebateRequest;
    }

    throw Exception('Invalid request type');
  }

  Future<OpenRequestCandidacy?> submitCandidacy(BuildContext context, { String? argument }) async {
    final client = Services.client;

    final mutation = _getCandidacyMutation();
    final payload = { 'id': request.id };

    if ((request as OpenRequest).type == OpenRequestType.Panel) {
      if (argument == null) {
        throw Exception('Panel argument is not provided');
      }

      payload['argument'] = argument;
    }

    final result = await client.mutate<Map<String, dynamic>>(mutation: mutation, variables: payload);

    return OpenRequestCandidacy.fromJson(result);
  }

  Future resolveOpenRequest(BuildContext context, List<OpenRequestCandidacy> candidates, [String? argument]) async {
    final client = Services.client;
    final request = entity as OpenRequest;

    final mutation = _getOpenRequestResolveMutation();
    final payload = { 'id': request.id, 'candidates': candidates.map((entry) => entry.id).toList() };

    if (request.type == OpenRequestType.Panel) {
      if (argument == null) {
        throw Exception('Panel argument not provided');
      }

      payload['argument'] = argument;
    }

    await client.mutate(mutation: mutation, variables: payload);
  }

  String _getCandidacyMutation() {
    final request = entity as OpenRequest;
    
    switch (request.type) {
      case OpenRequestType.Debate:
        return OpenRequestQueries.SubmitDebateCandidacy;

      case OpenRequestType.Panel:
        return OpenRequestQueries.SubmitPanelCandidacy;
    }
  }

  String _getOpenRequestResolveMutation() {
    final request = entity as OpenRequest;
    
    switch (request.type) {
      case OpenRequestType.Debate:
        return OpenRequestQueries.ResolveOpenDebateRequest;

      case OpenRequestType.Panel:
        return OpenRequestQueries.ResolveOpenPanelRequest;
    }
  }

  // Non need to fetch full request, as its full body appears
  // in pagination queries
  @override
  Future<void> fetch() async {}
}
