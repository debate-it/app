import 'package:debateit/src/discussions/request/shared/body.dart';
import 'package:debateit/src/discussions/request/shared/buttons.dart';
import 'package:debateit/src/discussions/request/shared/header.dart';
import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/request/shared/receiver.dart';
import 'package:debateit/src/discussions/request/shared/sender.dart';
import 'package:debateit/src/discussions/request/shared/status.dart';
import 'package:debateit/src/services.dart';
import 'package:debateit/src/widgets/misc/countdown_label.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';
import 'package:provider/provider.dart';

abstract class PanelRequestWidget extends StatelessWidget {

  const PanelRequestWidget({ super.key });

  factory PanelRequestWidget.from({ Key? key, required PanelRequest request, bool outgoing = false }) =>
    outgoing
      ? _OutgoingPanelRequestWidget(key: key, request: request)
      : _IncomingPanelRequestWidget(key: key, request: request);
}

class _IncomingPanelRequestWidget extends PanelRequestWidget {
  final PanelRequest request;
  // final bool isIncoming;

  // const PanelRequestWidget({ required this.request, this.isIncoming = true });
  const _IncomingPanelRequestWidget({ super.key, required this.request });

  @override
  Widget build(BuildContext context) { 
    const header = RequestHeader<PanelRequest>();

    const cardPadding = EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.md);
    const infoPadding = EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md);

    final user = Services.profileService.info!.profile;
    final idx = request.recipients.indexWhere((element) => element.user.id == user.id);

    final recipient = idx >= 0 ? request.recipients[idx] : null;
 
    final info = Container(
      padding: infoPadding,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[ 
          // RequestSender<PanelRequest>(isIncoming: isIncoming), 
          if (request.expiration == null)
            const RequestSender<PanelRequest>()
          else
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const RequestSender<PanelRequest>(), 
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('STARTS IN', style: context.text.bodyLarge),
                    CountdownLabel(date: request.expiration, timeoutLabel: 'Expired',)
                  ])
              ]),

          const RequestBody<PanelRequest>(), 
          if (recipient != null && recipient.status == RequestStatus.sent)
            const RequestButtons<PanelRequest>()
          else
            const RequestStatusMessage<PanelRequest>()
        ]));
    
    final card = Card(
      elevation: 12.0,
      borderOnForeground: false,
      color: context.colorScheme.surface,
      child: Container(
        padding: cardPadding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[header, info]))); 

    return ChangeNotifierProvider.value(value: RequestProvider(request), child: card);
  } 
}

class _OutgoingPanelRequestWidget extends PanelRequestWidget {
  final PanelRequest request;
  // final bool isIncoming;

  // const PanelRequestWidget({ required this.request, this.isIncoming = true });
  const _OutgoingPanelRequestWidget({ super.key, required this.request });

  @override
  Widget build(BuildContext context) { 
    const header = RequestHeader<PanelRequest>();

    const cardPadding = EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.md);
    const infoPadding = EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md);

    final user = Services.profileService.info!.profile;
    final idx = request.recipients.indexWhere((element) => element.user.id == user.id);

    final recipient = idx >= 0 ? request.recipients[idx] : null;
 
    final info = Container(
      padding: infoPadding,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[ 
          const RequestBody<PanelRequest>(), 
          const RequestReceiver<PanelRequest>(),
          
          if (recipient != null && recipient.status == RequestStatus.sent)
            const RequestButtons<PanelRequest>()
          else
            const RequestStatusMessage<PanelRequest>()
        ]));
    
    final card = Card(
      elevation: 12.0,
      borderOnForeground: false,
      color: context.colorScheme.surface,
      child: Container(
        padding: cardPadding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[header, info]))); 

    return ChangeNotifierProvider.value(value: RequestProvider(request), child: card);
  } 
}
