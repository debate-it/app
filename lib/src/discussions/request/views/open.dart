import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/main.dart';
import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/request/open/full.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';


class OpenRequestPage extends StatelessWidget {
  const OpenRequestPage({super.key, required this.id});

  final String id;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetch(),
      builder: (BuildContext context, AsyncSnapshot<OpenRequest> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.hasError) {
          showDialog(
            context: context,
            builder: (_) => Dialog(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(snapshot.error.toString()),
                  PrimaryBtn(
                    label: 'Continue',
                    onPressed: () => Navigator.of(context).pop())
                ],
              ),
            )
          );
        }

        return OpenRequestFullView(request: snapshot.data!);
      },
    );
  }

  Future<OpenRequest> fetch() async {
    final result = await Services.client.fetch(
      query: OpenRequestQueries.OpenRequest,
      variables: { 'id': id }
    );

    return OpenRequest.fromJson(result.data!['openRequest'] as Map<String, dynamic>);
  }
}
