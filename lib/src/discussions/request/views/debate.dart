import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/request/shared/body.dart';
import 'package:debateit/src/discussions/request/shared/buttons.dart';
import 'package:debateit/src/discussions/request/shared/header.dart';
import 'package:debateit/src/discussions/request/shared/receiver.dart';
import 'package:debateit/src/discussions/request/shared/sender.dart';
import 'package:debateit/src/discussions/request/shared/status.dart';


abstract class DebateRequestWidget extends StatelessWidget {

  const DebateRequestWidget({ super.key });

  factory DebateRequestWidget.from({ Key? key, required DebateRequest request, bool outgoing = false }) =>
    outgoing
      ? _OutgoingDebateRequestWidget(key: key, request: request)
      : _IncomingDebateRequestWidget(key: key, request: request);
}

class _IncomingDebateRequestWidget extends DebateRequestWidget {
  final DebateRequest request;

  const _IncomingDebateRequestWidget({ super.key, required this.request });

  @override
  Widget build(BuildContext context) { 
    const header = RequestHeader<DebateRequest>();

    final actions = request.status == RequestStatus.sent
      ? const RequestButtons<DebateRequest>() 
      : const SizedBox();

    const cardPadding = EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.md);
    const infoPadding = EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md);
 
    final info = Container(
      padding: infoPadding,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[ 
          const RequestSender<DebateRequest>(), 
          const RequestBody<DebateRequest>(),

          if (request.status != RequestStatus.sent)
            const RequestStatusMessage<DebateRequest>()
        ]
      )
    );
    
    final card = Card(
      elevation: 12.0,
      borderOnForeground: false,
      color: context.colorScheme.surface,
      child: Container(
        padding: cardPadding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[header, info, actions]))); 

    return ChangeNotifierProvider.value(value: RequestProvider(request), child: card);
  } 
}

class _OutgoingDebateRequestWidget extends DebateRequestWidget {
  final DebateRequest request;

  const _OutgoingDebateRequestWidget({ super.key, required this.request });

  @override
  Widget build(BuildContext context) {
    const header = RequestHeader<DebateRequest>();

    const cardPadding = EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.md);
    const infoPadding = EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md);
 
    final info = Container(
      padding: infoPadding,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const <Widget>[
          RequestReceiver<DebateRequest>(), 
          RequestBody<DebateRequest>(),
          RequestStatusMessage<DebateRequest>()
        ],
      )
    );
    
    final card = Card(
      elevation: 12.0,
      borderOnForeground: false,
      color: context.colorScheme.surface,
      child: Container(
        padding: cardPadding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[header, info]))); 

    return ChangeNotifierProvider.value(value: RequestProvider(request), child: card);
  }
}
