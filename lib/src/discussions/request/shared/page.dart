import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class PrivateRequestPage<T extends Request> extends StatelessWidget {
  PrivateRequestPage({ super.key, required this.request, RequestProvider<T>? provider, required this.content }) 
    : provider = provider ?? RequestProvider<T>(request);
  
  final T request;
  final RequestProvider<T> provider;
  final Widget content;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        const FlexibleViewAppBar(),

        SliverToBoxAdapter(child: content),

        const SliverPadding(padding: EdgeInsets.all(Insets.xs))
      ]
    );
  }
}
