import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/discussions/request/provider.dart';


class RequestStatusMessage<T extends Request> extends StatelessWidget {

  const RequestStatusMessage({ super.key });

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<T>(context).request;

    if (requestIsNotResolved(context, request)) {
      return const SizedBox();
    }

    final baseStyle = context.text.titleLarge;
    final isAccepted = requestIsAccepted(context, request);

    final statusStyle = baseStyle
      ?.copyWith(color: isAccepted
          ? context.colorScheme.primary
          : context.colorScheme.secondary);

    return Container(
      padding: const EdgeInsets.all(Insets.md),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan( 
          children: [
            TextSpan(text: 'Request was ', style: baseStyle),
            TextSpan(text: isAccepted ? 'accepted' : 'rejected', style: statusStyle)
          ]
        ),
      ),
    );
  }

  bool requestIsNotResolved(BuildContext context, Request request) {
    if (request is DebateRequest) {
      return request.status == RequestStatus.sent;
    }

    if (request is OpenRequest) {
      return request.resolved;
    }

    if (request is PanelRequest) {
      final user = Services.profileService.info!.profile;

      return request.recipients
        .where((element) => element.user.id == user.id)
        .every((element) => element.status == RequestStatus.sent);
    }
    
    throw Exception('Invalid request type');
  }

  bool requestIsAccepted(BuildContext context, Request request) {
    if (request is DebateRequest) {
      return request.status == RequestStatus.accepted;
    }
    
    if (request is OpenRequest) {
      return request.resolved;
    }

    if (request is PanelRequest) {
      final user = Services.profileService.info!.profile;

      return request.recipients
        .where((element) => element.user.id == user.id)
        .every((element) => element.status == RequestStatus.accepted);
    }

    throw Exception('Invalid request type');
  }
}
