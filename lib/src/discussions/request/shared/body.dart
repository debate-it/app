import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionTags.dart';
import 'package:debateit/src/widgets/misc/labeled_text.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';



class RequestBody<T extends Request> extends StatelessWidget {
  const RequestBody();

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<T>(context).entity;

    final topic = LabeledText(topic: request.info.topic, label: _getTopicLabel(request));
    final tags = DiscussionTags(tags: request.info.tags);

    const gap = Padding(padding: EdgeInsets.only(top: Insets.lg));
    final maxBodyWidth = MediaQuery.of(context).size.width / 2;
    
    return Container( 
      padding: const EdgeInsets.all(Insets.xs),
      constraints: BoxConstraints(maxWidth: maxBodyWidth),
      child: Column( 
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[ topic, gap, tags ]));
  }

  String _getTopicLabel(T request) {
    if (request is DebateRequest) {
      return 'Debate Request';
    }

    if (request is PanelRequest) {
      return 'Panel Request';
    }

    if (request is OpenRequest) {
      return 'Open ${request.type.parameter} Request';
    }

    throw Exception('Invalid request type');
  }
}
