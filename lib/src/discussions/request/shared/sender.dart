import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionAuthor.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';


class RequestSender<T extends Request> extends StatelessWidget {
  final bool isIncoming;
  const RequestSender({ this.isIncoming = true });

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<T>(context).request;
    
    if (isIncoming) {
      return DiscussionAuthor(sender: request.initiator);
    }

    if (request is DebateRequest) {
      return DiscussionAuthor(sender: request.recipient);
    }

    if (request is PanelRequest) {
      return buildPanelRequestRecipients(context, request);
    }

    throw Exception('Invalid outgoing request type');
  }

  Widget buildPanelRequestRecipients(BuildContext context, PanelRequest request) {

    final recipients = request.recipients
      .asMap().entries
      .map((entry) => PositionedDirectional(
        height: 40,
        start: entry.key * 25,
        child: UserAvatar(
          radius: 40,
          gradient: entry.value.user.progress.color,
          picture: entry.value.user.info.picture,
          name: entry.value.user.fullname ??
            entry.value.user.username)))
      .toList();
    
    return Container(
      padding: const EdgeInsets.all(Insets.sm),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

          Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.75),
            child: Stack(
              fit: StackFit.passthrough,
              children: recipients)),

          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('ACCEPTED',
                style: context.text.titleLarge?.copyWith(color: context.colors.primary)),
              Text('${request.accepted}/${request.recipients.length}', style: context.text.titleMedium)
            ]
          )
                 
        ],
      )
    );
  }
}
