import 'package:debateit/src/widgets/utility/cropped_topic_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/home/home.dart';
import 'package:debateit/src/home/logic/bloc.dart';

import 'package:debateit/src/discussions/request/provider.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/misc/labeled_text.dart';
import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';


typedef RequestResolver = void Function(BuildContext);

class RequestButtons<T extends Request> extends StatelessWidget {
  const RequestButtons();
  
  @override
  Widget build(BuildContext context) {
    final acceptButton = SecondaryBtn(
      onPressed: () => _acceptRequest(context),
      label: 'ACCEPT', icon: Icons.check);
      
    final rejectButton = PrimaryBtn(
      onPressed: () => _rejectRequest(context),
      label: 'REJECT', icon: Icons.not_interested);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[ 
        Expanded(child: acceptButton),
        const Padding(padding: EdgeInsets.symmetric(horizontal: Insets.xs)),
        Expanded(child: rejectButton)
      ]
    );
  }
  
  Future<void> _acceptRequest(BuildContext context) async { 
    final provider = RequestProvider.of<T>(context);
    final info = provider.request.info;

    String? argument;
    
    if (T == PanelRequest) {
      argument = await requestPanelArgument(context, topic: info.topic, size: info.size);
    }

    // ignore: use_build_context_synchronously
    await _resolveRequest(context, status: ResolveStatus.accept, argument: argument);
  }


  Future<void> _rejectRequest(BuildContext context) async {
    await _resolveRequest(context, status: ResolveStatus.reject);
  }

  ResultBuilder<dynamic>? _getBuilder(T request, ResolveStatus status) {
    if (status == ResolveStatus.reject) {
      return null;
    }

    if (request is PanelRequest) {
      return (json) => PanelRequest.fromJson(json);
    }
    
    if (request is DebateRequest) {
      return status == ResolveStatus.accept
        ? (json) => Debate.fromJson(json)
        : (json) => DebateRequest.fromJson(json);
    }

    throw Exception('Invalid request type');
  }

  Future<void> _resolveRequest(BuildContext context, { required ResolveStatus status, String? argument }) async {
    final provider = RequestProvider.of<T>(context);

    final pagination = BlocProvider.of<PaginationBloc<Request>>(context);
    final profile = Services.profileService;

    final builder = _getBuilder(provider.request, status);
    final controller = BlocProvider.of<HomePageBloc>(context);
    
    // SnackbarHelper.showSnackBar(context, _getOnSubmitMessage(status)); 

    switch (status) {
      case ResolveStatus.accept:
        final result = await provider.accept(
          context: context, request: provider.request, 
          argument: argument, builder: builder!);
          
        if (result != null && result is! Debate) {
          provider.updateRequest(result as T);
        }

        pagination.add(PageRemoveItem(provider.request.id));
        profile.updateIndicators();

        if (result is Debate) {
          controller.add(const HomePageTabChanged(tab: HomePageTab.discussions));
        }

        break;

      case ResolveStatus.reject:
        final result = await provider.reject(
          context: context, request: provider.request,
          builder: builder);

        if (result != null && result is! Debate) {
          provider.updateRequest(result as T);
        }

        pagination.add(PageRemoveItem(provider.request.id));
        profile.updateIndicators();

        break;

      default:
        throw Exception('Invalid resolve status');
    }
    
  }
  
  Future<String?> requestPanelArgument(BuildContext context, { required String topic, required DiscussionSize size }) async {
    return showModalBottomSheet<String>(
      context: context, 
      useRootNavigator: true,
      isScrollControlled: true,
      builder: (ctx) => BottomSheetTextField(
        inputLabel: 'panel argument', 
        wordLimit: size.argumentLength,
        header: CroppedTopicViewer(text: topic, label: 'Panel Topic')));
  }


  String _getOnSubmitMessage(ResolveStatus status) {
    switch (status) {
      case ResolveStatus.accept:
        return 'Accepting request...';
      case ResolveStatus.reject:
        return 'Rejecting request...';
    }
  }
  
  // String _getOnResolvedMessage(ResolveStatus status) {
  //   switch (status) {
  //     case ResolveStatus.accept:
  //       return 'Request accepted.';
  //     case ResolveStatus.reject:
  //       return 'Request rejected.';
  //   }
  // }
}
