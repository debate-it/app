import 'package:debateit/src/discussions/request/provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';


class RequestHeader<T extends Request> extends StatelessWidget {
  const RequestHeader();

  @override
  Widget build(BuildContext context) {
    final creationDate = _buildCreationDate(context);
    final debateTypeInfo = _buildDebateTypeInfo(context); 

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      textBaseline: TextBaseline.alphabetic,
      children: <Widget>[ creationDate, const Spacer(), debateTypeInfo ]);
  }

  Widget _buildCreationDate(BuildContext context) {
    final request = RequestProvider.of<T>(context).request; 
    
    final icon = Padding(
      padding: const EdgeInsets.symmetric(horizontal: Insets.xs),
      child: Icon(Icons.calendar_today, size: 10, color: context.colorScheme.secondary));

    final date = request.time;
    final dateFormat = DateFormat('h:mm a, MMM d, yy');

    final dateLabel = Text(dateFormat.format(date.toLocal()), style: context.text.bodyMedium);

    return Row(children: [ icon, dateLabel ]);
  }

  Widget _buildDebateTypeInfo(BuildContext context) {
    final request = RequestProvider.of<T>(context).request;
    
    final category = Text(request.info.category.name,
      style: context.text.bodyMedium?.copyWith(color: context.colorScheme.primary));

    final size = Text(request.info.size.label.toUpperCase(), style: context.text.bodySmall);

    final divider = Icon(Icons.lens, size: 5, 
      color: context.colorScheme.onSurface);

    final debateTypeInfo = <Widget>[ size, divider, category ];
    
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Insets.xs),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: padWidgets(context: context, widgets: debateTypeInfo)));
  }

  List<Widget> padWidgets({ required BuildContext context, required List<Widget> widgets }) =>
    widgets
      .map((el) => Padding(padding: const EdgeInsets.symmetric(horizontal: Insets.xs), child: el))
      .toList(); 
}
