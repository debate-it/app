import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionAuthor.dart';

import 'package:debateit/src/widgets/misc/countdown_label.dart';


class RequestReceiver<T extends Request> extends StatelessWidget {
  const RequestReceiver({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = RequestProvider.of<T>(context);
    
    if (provider.request is DebateRequest) {
      return DiscussionAuthor(sender: (provider.request as DebateRequest).recipient);
    }

    final request = provider.request as PanelRequest;

    final recipients = Text('${request.accepted}/${request.recipients.length} accepted',
      style: context.text.titleLarge);

    final expiration = CountdownLabel(
      date: request.expiration,
      timeoutLabel: 'Request expired',
      builder: (_) => const SizedBox());


    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.xs),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [ recipients, expiration ]
      )
    );
  }
}
