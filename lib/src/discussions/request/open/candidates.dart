import 'package:debateit/src/config/pagination/misc.dart';
import 'package:debateit/src/discussions/request/open/provider.dart';
import 'package:debateit/src/discussions/user/views/preview.dart';
import 'package:debateit/src/services.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';
import 'package:debateit_pagination/pagination.dart';



typedef CandidacyOnSelect = void Function(OpenRequestCandidacy);

class OpenRequestCandidates extends StatelessWidget {
  const OpenRequestCandidates({super.key});

  @override
  Widget build(BuildContext context) {
    final provider = OpenRequestProvider.of(context);
    
    return SliverPaginationProvider<OpenRequestCandidacy>(
      client: Services.client,
      padding: const EdgeInsets.all(Insets.md),
      config: OpenRequestCandidacyPaginationConfig(
        id: provider.request.id, 
        onSelect: (candidate) => provider.selectCandidate(candidate)));
  }
}

class OpenRequestCandidate extends StatelessWidget {
  final OpenRequestCandidacy candidate;
  final CandidacyOnSelect onSelect;

  const OpenRequestCandidate({ super.key, required this.candidate, required this.onSelect });

  @override
  Widget build(BuildContext context) {
    final provider = OpenRequestProvider.of(context, listen: true);
    final isSelected = provider
      .selectedCandidates.any((selected) => selected.id == candidate.id);

    return UserPreview(
      user: candidate.profile, 

      side: !provider.entity.resolved
        ? IgnorePointer(
          child: Checkbox(
            onChanged: (_) {  }, value: isSelected,
            activeColor: context.colors.secondary))
        : null,

      onTap: (_) => select(context, candidate));
  }

  void select(BuildContext context, OpenRequestCandidacy candidate) {
    final provider = OpenRequestProvider.of(context);

    if (!provider.entity.resolved) {
      onSelect(candidate);
    }
  }
}
