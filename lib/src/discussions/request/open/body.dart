import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/open.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionTags.dart';


class OpenRequestBody extends StatelessWidget {
  final bool full;

  const OpenRequestBody({super.key, this.full = false });

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<OpenRequest>(context).entity;

    final topic = Text(request.info.topic,
      maxLines: full ? null : 3, 
      overflow: full ? null : TextOverflow.ellipsis, 
      style: context.text.bodyMedium);

    final tags = DiscussionTags(tags: request.info.tags);

    const gap = Padding(padding: EdgeInsets.only(top: Insets.lg));
    final maxBodyWidth = MediaQuery.of(context).size.width / 2;
    
    return Container( 
      padding: const EdgeInsets.all(Insets.xs),
      constraints: BoxConstraints(maxWidth: maxBodyWidth),
      child: Column( 
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[ topic, gap, tags ]));
  }
}
