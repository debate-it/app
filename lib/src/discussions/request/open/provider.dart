import 'package:debateit/src/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/request.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/request/provider.dart';

import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';
import 'package:debateit/src/widgets/misc/labeled_text.dart';


class OpenRequestFeedProvider extends ChangeNotifier {
  List<OpenRequest>? requests;

  Future<void> getOpenRequests(BuildContext context) async {
    final client = Services.client;

    final result = await client.fetch(query: OpenRequestQueries.Sample);

    requests = (result.data!['openRequestSample'] as List<dynamic>)
      .map<OpenRequest>((entry) => OpenRequest.fromJson(entry as Map<String, dynamic>))
      .toList();

    notifyListeners();
  }

  void removeRequest(String id) {
    if (requests == null) {
      return;
    }

    requests!.removeWhere((element) => element.id == id);
    notifyListeners();
  }

}

class OpenRequestProvider extends RequestProvider<OpenRequest> {
  OpenRequestProvider(super.request);

  final List<OpenRequestCandidacy> selectedCandidates = [];

  static OpenRequestProvider of(BuildContext context, { bool listen = false }) =>
    Provider.of<OpenRequestProvider>(context, listen: listen);

  void selectCandidate(OpenRequestCandidacy candidate) {
    if (selectedCandidates.any((selected) => selected.id == candidate.id)) {
      return;
    }

    selectedCandidates.add(candidate);

    notifyListeners();
  }

  void removeCandidate(OpenRequestCandidacy candidate) {
    selectedCandidates.removeWhere((element) => element.id == candidate.id);
    
    notifyListeners();
  }
  
  
  Future<void> submitSelectedCandidates(BuildContext context) async {
    final client = Services.client;

    final navigator = Navigator.of(context);

    final query = entity.type == OpenRequestType.Debate
      ? OpenRequestQueries.ResolveOpenDebateRequest
      : OpenRequestQueries.ResolveOpenPanelRequest;

    if (entity.type == OpenRequestType.Panel) {
      if (selectedCandidates.length < 2) {
        throw Exception('Panel should have at least 2 participants selected');
      }

      if (selectedCandidates.length > 9) {
        throw Exception("Panel can't have more than 9 participants selected");
      }
    }

    final variables = {
      'id': entity.id,
      'candidates': selectedCandidates
        .map((e) => e.id)
        .toList()
    };

    // Get panel argument
    if (entity.type == OpenRequestType.Panel) {
      final argument = await showModalBottomSheet<String>(
        context: context, 
        useRootNavigator: true,
        isScrollControlled: true,
        builder: (ctx) => BottomSheetTextField(
          inputLabel: 'panel argument', 
          wordLimit: entity.info.size.argumentLength,
          header: LabeledText(topic: entity.info.topic, label: 'Panel Topic')));

      if (argument == null) {
        return;
      }

      variables['argument'] = argument;
    }

    // try {
      final result = await client.mutate(mutation: query, variables: variables);
      
      navigator.popUntil((route) => route.settings.name == '/');

      if (entity.type == OpenRequestType.Panel) {
        // Redirect to the panel page
        navigator.pushNamed('/discussions/panel',
          arguments: { 'id': (result as Map<String, dynamic>)['id'] as String });
      } else {
        // Redirect to active debate view
        navigator.pushReplacementNamed('/', arguments: { 'tab': HomePageTab.discussions });
      }
    // } on Exception catch (error) {
    //   SnackbarHelper.showSnackBarError(context, error);
    // }

  }
}
