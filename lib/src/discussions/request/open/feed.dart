import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';
import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/discussions/request/open/preview.dart';
import 'package:debateit/src/discussions/request/open/provider.dart';


class OpenRequestFeed extends StatefulWidget {
  const OpenRequestFeed({super.key});

  @override
  State<OpenRequestFeed> createState() => _OpenRequestFeedState();
}

class _OpenRequestFeedState extends State<OpenRequestFeed> {
  final provider = OpenRequestFeedProvider();

  @override
  void initState() {
    provider.getOpenRequests(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: provider,
      child: Builder(
        builder: (context) {
          final provider = Provider.of<OpenRequestFeedProvider>(context);

          if (provider.requests == null) {
            return const Loader();
          }

          return SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(Insets.xs),
            child: _buildOpenRequests(context, provider.requests!));
        },
      ),
    );

  }

  Widget _buildOpenRequests(BuildContext context, List<OpenRequest> requests) {
    return 
      Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: requests
          .map((entry) => OpenRequestWidget(request: entry))
          .toList());
  }
}
