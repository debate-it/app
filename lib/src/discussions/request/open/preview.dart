import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/request/open/provider.dart';
import 'package:debateit/src/discussions/request/open/body.dart';
import 'package:debateit/src/discussions/request/open/sender.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/request/shared/header.dart';

import 'package:debateit/src/widgets/dialogs/info.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';
import 'package:debateit/src/widgets/utility/cropped_topic_viewer.dart';



class OpenRequestWidget extends StatelessWidget {
  final OpenRequest request;

  const OpenRequestWidget({super.key, required this.request});

  @override
  Widget build(BuildContext context) {

    const cardPadding = EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.md);
    const infoPadding = EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md);

    final user = Services.profileService.info!.profile;
 
    final info = Container(
      padding: infoPadding,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[ 
          const RequestHeader<OpenRequest>(),
          const OpenRequestSender(), 
          const OpenRequestBody(),

          const Divider(),
          
          if (user.id == request.initiator.id)
            const OpenRequestResponseCounter()
        ]));

    final maxWidth = MediaQuery.of(context).size.width - (Insets.sm * 2);

    final content = Container(
      padding: cardPadding,
      
      constraints: BoxConstraints(maxWidth: maxWidth),

      child: info
    );

    
    return ChangeNotifierProvider(
      key: ValueKey(request.id),
      create: (_) => OpenRequestProvider(request), 
      child: Builder(
        builder: (context) => Card(
          elevation: 12.0,
          borderOnForeground: false,
          child: InkWell(
            onTap: getCardOnTap(context: context, initiator: request.initiator), 
            child: content))));
  }

  void openRequestPage(BuildContext context) {
    context.beamToNamed('/request/${request.id}');
  }

  VoidCallback? getCardOnTap({ required BuildContext context, required UserProfile initiator }) {
    final user = Services.profileService.info!.profile;
    final provider = OpenRequestProvider.of(context);

    if (user.id == initiator.id) {
      return () => openRequestPage(context);
    }

    return () async {
      final sent = await showDialog<bool>(
        context: context, 
        builder: (context) {
          
          return Dialog(
            insetPadding: const EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.lg),
            backgroundColor: Colors.transparent,
            child: ChangeNotifierProvider.value(
              value: provider,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(Insets.sm),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: const [
                      RequestHeader<OpenRequest>(),
                      OpenRequestSender(), 
                      OpenRequestBody(full: true),
                      OpenRequestCandidacyButton()
                    ]
                  )
                )
              )
            )
          );
      });

      if (sent != null && sent) {
        // ignore: use_build_context_synchronously
        Provider.of<OpenRequestFeedProvider>(context, listen: false)
          .removeRequest(provider.request.id);
      }
    };
  }
}

class OpenRequestResponseCounter extends StatelessWidget {
  const OpenRequestResponseCounter({super.key});

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<OpenRequest>(context).request;

    return Text('Your request has ${request.candidacies} candidacies',
      textAlign: TextAlign.center,
      style: context.text.headlineSmall
        ?.copyWith(color: context.colors.primary));
  }
}

class OpenRequestCandidacyButton extends StatelessWidget {
  const OpenRequestCandidacyButton({ super.key });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Insets.sm),
      child: OutlinedBtn(
        // icon: Icons.send,
        label: 'Submit Candidacy',
        onPressed: () => submit(context)));
  }

  Future<void> submit(BuildContext context) async {
    final provider = RequestProvider.of<OpenRequest>(context);
    final info = provider.request.info;

    String? argument;

    if (provider.request.type == OpenRequestType.Panel) {
      argument = await requestPanelArgument(context, topic: info.topic, size: info.size);

      if (argument == null) {
        return;
      }
    }

    // ignore: use_build_context_synchronously
    await provider.submitCandidacy(context, argument: argument);

    await showDialog(
      context: context, 
      builder: (_) => const InfoDialog(message: 'Your candidacy has been submitted'));
      
    // ignore: use_build_context_synchronously
    Navigator.of(context).pop(true);
  }

  Future<String?> requestPanelArgument(BuildContext context, { required String topic, required DiscussionSize size }) async {
    return showModalBottomSheet<String?>(
      context: context, 
      useRootNavigator: true,
      isScrollControlled: true,
      builder: (ctx) => BottomSheetTextField(
        inputLabel: 'panel argument', 
        wordLimit: size.argumentLength,
        header: CroppedTopicViewer(text: topic, label: 'Panel Topic')));
  }
}
