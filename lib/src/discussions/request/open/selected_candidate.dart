import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/request/open/provider.dart';
import 'package:debateit/src/discussions/user/views/preview.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';


class OpenRequestSelectedCandidates extends StatelessWidget {
  const OpenRequestSelectedCandidates({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = OpenRequestProvider.of(context, listen: true);
    
    return BottomSheet(
        onClosing: () {},
        builder: (innerContext) => ListTile(
          title: Text('${provider.selectedCandidates.length} candidates selected',
            style: innerContext.text.bodyLarge),

          trailing: Icon(Icons.arrow_drop_up, color: innerContext.colors.text),

          onTap: () => openSelectedCandidatesList(innerContext, context, provider)));
  }

  Future<void> submit(BuildContext context, OpenRequestProvider provider) async {
    // Close current bottom sheet
    Navigator.of(context).pop();

    try {
      await provider.submitSelectedCandidates(context);
    } on Exception catch (err) {
      await showDialog(
        context: context,
        builder: (_) => InfoDialog(message: err.toString()));
    }
    
  }

  void openSelectedCandidatesList(BuildContext context, BuildContext parentContext, OpenRequestProvider provider) {
    showModalBottomSheet(
      context: context, 
      builder: (_) {
        return StatefulBuilder(
          builder: (BuildContext builderContext, setState) {
            void removeCandidate(OpenRequestCandidacy candidate) {
              provider.removeCandidate(candidate);
              setState(() {});
            }

            final selectedCandidates = provider.selectedCandidates
              .map((candidate) => UserPreview(
                  user: candidate.profile,
                  onTap: (_) => removeCandidate(candidate),
                  side: IconButton(
                    icon: Icon(Icons.close, color: builderContext.colors.primary),
                    onPressed: () => removeCandidate(candidate))))
              .toList();


            return Container(
              padding: const EdgeInsets.all(Insets.md),

              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,

                children: [
                  Container(
                    constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(builderContext).size.height / 2 - 60),

                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: selectedCandidates))),
                  
                  PrimaryBtn(label: 'Select participants', onPressed: () => submit(parentContext, provider))
                ],
              )
            );
          },
        );
      }
    );
  }
}
