import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/request/open/candidates.dart';
import 'package:debateit/src/discussions/request/open/provider.dart';
import 'package:debateit/src/discussions/request/open/selected_candidate.dart';

import 'package:debateit/src/discussions/request/provider.dart';

import 'package:debateit/src/discussions/request/shared/body.dart';
import 'package:debateit/src/discussions/request/shared/header.dart';
import 'package:debateit/src/discussions/request/shared/sender.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class OpenRequestFullView extends StatefulWidget {
  OpenRequestFullView({ super.key, required this.request, RequestProvider<OpenRequest>? provider }) 
    : provider = OpenRequestProvider(provider?.request ?? request);

  final OpenRequest request;
  final OpenRequestProvider provider;

  @override
  State<OpenRequestFullView> createState() => _OpenRequestFullViewState();
}

class _OpenRequestFullViewState extends State<OpenRequestFullView> {
  Widget? bottomSheet;

  @override
  Widget build(BuildContext context) {
    final currentProfile = Services.profileService.info!.profile;

    final requestInfo = Card(
      child: Container(
        padding: const EdgeInsets.all(Insets.md),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: const [ RequestHeader<OpenRequest>(), RequestSender<OpenRequest>(), RequestBody<OpenRequest>() ])));


    final view = CustomScrollView(
      slivers: [
        FullViewAppBar(
          type: 'Request',
          target: widget.provider.request,
          category: widget.provider.request.info.category),

        SliverToBoxAdapter(child: requestInfo),

        if (currentProfile.isSelf)
          const OpenRequestCandidates(),

        const SliverPadding(padding: EdgeInsets.all(Insets.xl))
      ]
    );

    return ChangeNotifierProvider.value(
      value: widget.provider, 
      child: Scaffold(
        body: view,
        resizeToAvoidBottomInset: true,
        bottomSheet: currentProfile.isSelf && !widget.request.resolved
          ? const OpenRequestSelectedCandidates()
          : null));
  }
}
