import  'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/request/open.dart';

import 'package:debateit/src/discussions/request/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionAuthor.dart';


class OpenRequestSender extends StatelessWidget {
  const OpenRequestSender({super.key});

  @override
  Widget build(BuildContext context) {
    final request = RequestProvider.of<OpenRequest>(context).request;

    final style = context.text.headlineSmall
      ?.copyWith(color: context.colorScheme.primary);
    
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        DiscussionAuthor(sender: request.initiator),
        Column(
          children: [
            Text('Open', style: style),
            Text('${request.type.parameter} Request', style: style)
          ]
        )
      ],
    );
  }
}
