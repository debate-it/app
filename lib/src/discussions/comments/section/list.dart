part of comment_section;

typedef OpponentChoiceCallback = void  Function(Comment);

class CommentSectionList extends StatelessWidget {
  final String target;

  final CommentType type;
  final CommentOrder order;

  final OpponentChoiceCallback? onTap;

  final bool useSlivers;
  final bool scrollable;
  final bool includeUserComment;

  const CommentSectionList({
    super.key,
    required this.target, 
    required this.type,
    required this.order,
    this.onTap, 
    this.useSlivers = true, 
    this.scrollable = false, 
    this.includeUserComment = true, 
  }); 

  @override
  Widget build(BuildContext context) { 
    final provider = CommentSectionProvider.of(context, listen: true);

    if (useSlivers) {
      return SliverPaginationProvider.fromBloc(
        bloc: provider.bloc,
        scrollable: scrollable,
        padding: const EdgeInsets.all(Insets.xs),
      );
    }

    return PaginationProvider.fromBloc(
      bloc: provider.bloc,
      scrollable: scrollable,
      padding: const EdgeInsets.all(Insets.xs),
    );
  }
}
