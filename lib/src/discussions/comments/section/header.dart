part of comment_section;

class CommentSectionHeader extends StatelessWidget {
  final bool canComment;
  final int? wordLimit;

  const CommentSectionHeader({ super.key, this.canComment = true, this.wordLimit });
  
  @override
  Widget build(BuildContext context) {
    final provider = CommentSectionProvider.of(context, listen: true);

    return FutureBuilder(
      future: provider.fetchUserComment(),
      builder: (context, AsyncSnapshot<Comment?> snapshot) {
        if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
          return CommentWidget(response: snapshot.data!);
        } else if (!snapshot.hasData) {

          return canComment
            ? CommentInputPrompt(target: provider.target, type: provider.type, wordLimit: wordLimit)
            : const SizedBox(height: 0);
        }

        return Container();
      }
    );
  }
}
