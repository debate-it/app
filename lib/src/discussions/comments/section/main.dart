library comment_section;

import 'package:debateit/src/services.dart';
import 'package:debateit_graphql/queries/challenge.dart';
import 'package:debateit_graphql/queries/comment.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/discussions/comments/views/preview.dart';
import 'package:debateit/src/discussions/comments/widgets/prompt.dart';

part 'header.dart';
part 'list.dart';
part 'order.dart';
part 'provider.dart';


class CommentSection extends StatelessWidget {
  final String target;
  final CommentType type;

  final int? wordLimit;

  final Widget? header;

  final bool canComment;
  final bool canVote;

  final bool useSlivers;
  final bool scrollable;

  const CommentSection({
    super.key, 
    required this.target, 
    required this.type, 
    this.header,
    this.wordLimit,
    this.useSlivers = false, 
    this.scrollable = false, 
    this.canComment = true, 
    this.canVote = true,
  });

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      create: (ctx) => CommentSectionProvider(canVote: canVote, target: target, type: type),
      child: Builder(builder: buildContent));
  }

  Widget buildContent(BuildContext context) {
    final provider = CommentSectionProvider.of(context);

    final orderPanel = CommentSectionOrderPanel(onChanged: (order) => provider.changeSortOrder(order));
    final header = this.header ?? CommentSectionHeader(canComment: canComment);

    final comments = CommentSectionList(
      type: type,
      target: target,
      order: provider.order, 
      useSlivers: false, 
      scrollable: scrollable,
    );

    if (useSlivers) {
      return SliverToBoxAdapter(
        child: Column(
          children: [ orderPanel, header, comments ]));
    }
    
    return Column(children: [ orderPanel, header, comments ]);
  }
}
