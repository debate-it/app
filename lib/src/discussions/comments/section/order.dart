part of comment_section;

class CommentSectionOrderPanel extends StatefulWidget {
  final CommentOrder initial;
  final void Function(CommentOrder) onChanged;

  const CommentSectionOrderPanel({ super.key, required this.onChanged, this.initial = CommentOrder.NEW });

  @override
  _CommentSectionOrderPanelState createState() 
    => _CommentSectionOrderPanelState();
}

class _CommentSectionOrderPanelState extends State<CommentSectionOrderPanel> {

  CommentOrder? order;

  @override
  void initState() { 
    super.initState();
    order = widget.initial;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        height: 50,
        child: DropdownButtonHideUnderline(
          child: DropdownButton<CommentOrder>( 
            value: order, 
            isDense: true,
            isExpanded: true,
            onChanged: (value) {
              setState(() { order = value; }); 

              if (value == null) {
                return;
              }

              widget.onChanged(value);
            },
            items: [
              buildDropdownEntry(context,
                value: CommentOrder.NEW,
                icon: Icons.new_releases,
                label: 'NEW',
              ), 
              buildDropdownEntry(context,
                value: CommentOrder.TOP,
                icon: Icons.filter_hdr,
                label: 'TOP',
              ), 
              buildDropdownEntry(context,
                value: CommentOrder.OLD,
                icon: Icons.history,
                label: 'OLD',
              )
            ]))));
  }

  DropdownMenuItem<CommentOrder> buildDropdownEntry(BuildContext context, {
      required CommentOrder value, 
      required String label, 
      required IconData icon
    }) {

    final labelStyle = context.text.headlineSmall;

    return DropdownMenuItem<CommentOrder>(
      value: value,
      child: ListTile(
        dense: true,
        leading: Icon(icon, color: context.colorScheme.primary),
        title: Text(label, style: labelStyle),
        trailing: order == value 
          ? Icon(Icons.check_circle, color: context.colorScheme.secondary) 
          : null, 
      )
    );
  }
}
