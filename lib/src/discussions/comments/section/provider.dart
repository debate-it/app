part of comment_section;

class CommentSectionProvider extends ChangeNotifier {
  final PaginationBloc<Comment> bloc;

  final String target;
  final CommentType type;

  final OpponentChoiceCallback? onTap;

  final bool canVote;

  Comment? userComment;
  CommentOrder order;

  static CommentSectionProvider of(BuildContext context, {bool listen = false}) =>
    Provider.of<CommentSectionProvider>(context, listen: listen);

  CommentSectionProvider({
    required this.target, 
    required this.type, 
    this.onTap, 
    this.canVote = true, 
    this.order = CommentOrder.NEW,
  })
    : bloc = PaginationBloc(
        client: Services.client,
          config: CommentSectionConfig(
            onTap: onTap,
            target: target, 
            canVote: canVote,
            type: type,
            order: order), 
           );

  Future<Comment?> fetchUserComment() async {
    final result = await bloc.client.fetch(
      query: CommentQueries.FindUserComment,
      variables: {'target': target, 'type': type.name });

    if (result.data != null && result.data!['userComment'] != null) {
      return Comment.fromJson(result.data!['userComment'] as Map<String, dynamic>);
    }

    return null;
  }

  Future<void> addComment(String comment) async {
    final mutation = type == CommentType.Challenge
      ? ChallengeQueries.AddChallengeResponse
      : CommentQueries.AddComment;

    final variables = type == CommentType.Challenge
      ? { 'challenge': target, 'argument': comment }
      : {'target': target, 'type': type.name, 'comment': comment };

    final result = await bloc.client.mutate(mutation: mutation, variables: variables);

    if (result != null) {
      notifyListeners();
    }
  }

  void changeSortOrder(CommentOrder order) {
    bloc.config = CommentSectionConfig(
      target: target,
      type: type,
      order: order,
    );

    bloc.add(const PageRefresh());

    notifyListeners();
  }
}

class CommentSectionConfig extends PaginationConfig<Comment> {
  @override
  int get size => 20;

  @override
  String get query => CommentQueries.FindComments;
  @override
  String get queryName => 'comments';

  @override
  String get noItemText => 'There are no responses yet';

  final OpponentChoiceCallback? onTap;
  final bool canVote;

  CommentSectionConfig({
    required String target,
    required CommentType type,
    required CommentOrder order,

    this.canVote = true, 
    this.onTap,
  })
    : super({ 'target': target, 'type': type.name, 'order': orderString(order) });

  @override
  Comment buildItem(Map<String, dynamic> json) => Comment.fromJson(json);

  @override
  Widget buildWidget(BuildContext context, Comment item) =>
      CommentWidget(response: item, canVote: canVote, onTap: onTap);
}

String orderString(CommentOrder order) {
  switch (order) {
    case CommentOrder.NEW:
      return 'new';
    case CommentOrder.TOP:
      return 'top';
    case CommentOrder.OLD:
      return 'old';

    default:
      throw Exception('Invalid order value $order');
  }
}
