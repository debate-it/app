import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/comments/logic/bloc.dart';
import 'package:debateit/src/discussions/comments/section/main.dart';
import 'package:debateit/src/discussions/comments/buttons/vote.dart';
import 'package:debateit/src/discussions/comments/widgets/alignment.dart';

import 'package:debateit/src/discussions/report/menu.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';

import 'package:debateit/src/widgets/misc/creation_label.dart';


class CommentWidget extends StatelessWidget {
  final bool canVote;
  final Comment response;

  final OpponentChoiceCallback? onTap;

  const CommentWidget({
    super.key,
    required this.response,
    this.canVote = true,
    this.onTap 
  }); 

  @override
  Widget build(BuildContext context) {
    final avatar = Container(
      padding: const EdgeInsets.only(right: Insets.md),
      child: UserAvatar(
        radius: 15,
        gradient: response.author.progress.color,
        picture: response.author.info.picture,
        name: response.author.fullname ?? response.author.username));

    final username = Text('@${response.author.username}',
      textAlign: TextAlign.start,
      style: context.text.titleMedium);

    final date = CreationLabel(date: response.date);
    final optionsMenu = ReportMenu(id: response.id, target: 'Challenge');

    final header = Row(
      children: [ avatar, username, const Spacer(), date, optionsMenu ]);


    final body = Container(
      padding: const EdgeInsets.symmetric(vertical: Insets.md, horizontal: Insets.xs),
      child: Text(response.content,
        style: context.text.bodyLarge));



    const padding = EdgeInsets.only(
      top: Insets.md, bottom: Insets.xs,
      left: Insets.md, right: Insets.md
    );

    final voteButton = CommentVoteButton(canVote: canVote);
    final alignment = CommentAlignmentIndicator(comment: response);
    
    final footer = Row( 
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [voteButton, alignment]);

    final content = Container(
      padding: padding,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [ header, body, footer ]));

    
    return BlocProvider(
      create: (context) => CommentBloc(response),
      child: Card(
        elevation: 12.0,
        margin: const EdgeInsets.all(Insets.xs),
        child: InkWell(onTap: () => onTap?.call(response), child: content)));
  }

}
