part of 'bloc.dart';

abstract class CommentEvent extends Equatable {
  const CommentEvent();
}

class CommentVotedToggled extends CommentEvent {
  final Comment comment;

  const CommentVotedToggled(this.comment);

  @override
  List<Object?> get props => [comment];

}
