import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:debateit_graphql/queries/comment.dart';
import 'package:debateit_models/discussions/comment/main.dart';

import 'package:debateit/src/services.dart';

part 'event.dart';
part 'state.dart';


class CommentBloc extends Bloc<CommentEvent, CommentState> {
  CommentBloc(Comment comment) 
    : super(comment.voted ? CommentWithVote(comment) : CommentWithoutVote(comment)) {
    on<CommentVotedToggled>((event, emit) => vote(event.comment, emit));
  }

  Future<void> vote(Comment comment, Emitter<CommentState> emit) async {
    emit(const CommentLoading());

    final result = await _sendRequest(comment.id);

    final votedOn = result['voted'] as bool;

    if (votedOn) {
      emit(CommentWithVote(comment.copyWith(score: result['votes'] as int, votedOn: true)));
    } else {
      emit(CommentWithoutVote(comment.copyWith(score: result['votes'] as int, votedOn: false)));
    }
  }

  Future<Map<String, dynamic>> _sendRequest(String id) async {
    return Services.client.mutate<Map<String, dynamic>>(mutation: CommentQueries.Vote, variables: { 'id': id });
  }
}
