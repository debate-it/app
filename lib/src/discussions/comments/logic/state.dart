part of 'bloc.dart';

abstract class CommentState extends Equatable {
  const CommentState();
  
  @override
  List<Object?> get props => const [];
}

class CommentLoading extends CommentState {
  const CommentLoading();
}


abstract class LoadedCommentState extends CommentState {
  final Comment comment;

  const LoadedCommentState(this.comment);
  
  @override
  List<Object?> get props => [comment];
}

class CommentWithVote extends LoadedCommentState {
  const CommentWithVote(super.comment);
}

class CommentWithoutVote extends LoadedCommentState {
  const CommentWithoutVote(super.comment);
}
