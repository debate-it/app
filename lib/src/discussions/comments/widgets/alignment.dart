import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/comments/logic/bloc.dart';
import 'package:debateit/src/discussions/debate/provider.dart';


class CommentAlignmentIndicator extends StatelessWidget {
  final Comment comment;

  const CommentAlignmentIndicator({ required this.comment });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CommentBloc, CommentState>(
      buildWhen: (previous, current) => current is LoadedCommentState,
      builder: (context, state) {
        final comment = (state as LoadedCommentState).comment;
        
        if (comment.alignment == null) {
          return const SizedBox();
        }

        return RichText(
          text: TextSpan(
            children: [ 
              TextSpan(
                text: getAlignmentLabelText(),
                style: context.text.bodyLarge,
                children: const [TextSpan(text: ': ')]),

              TextSpan(
                text: getAlignmentText(context),
                style: context.text.bodyLarge
                  ?.copyWith(color: getAlignmentColor(context)))
            ]
          )
        );
      }
    );
  }

  String? getAlignmentText(BuildContext context) {
    switch (comment.type) {
      case CommentType.Debate:
      case CommentType.Panel:
        return DebateProvider.of(context)
          .participants[comment.alignment!]
          .user.username;

      case CommentType.Post:
        switch (comment.alignment) {
          case 0:
            return 'AGREES';
          case 1:
            return 'DISAGREES';
        }

        break;

      case CommentType.Challenge:
        return null;
    }

    throw Exception('Invalid comment type');
  }

  String getAlignmentLabelText() {
    switch (comment.type) {
      case CommentType.Debate:
      case CommentType.Panel:
        return 'VOTED FOR';

      case CommentType.Post:
        return 'POSITION';

      default:
        throw Exception('Unreachable');
    }
  }

  Color getAlignmentColor(BuildContext context) {
    if (comment.type == CommentType.Panel) {
      return context.colorScheme.secondary;
    }

    switch (comment.alignment) {
      case 0:
        return context.colorScheme.secondary;

      case 1:
        return context.colorScheme.primary;

      default:
        throw Exception('Unreachable');
    }
  }
}
