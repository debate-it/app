import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/user/widgets/avatar.dart';
import 'package:debateit/src/discussions/comments/section/main.dart';

import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';


class CommentInputPrompt extends StatelessWidget {
  final String target;
  final CommentType type;

  final int? wordLimit;
  

  const CommentInputPrompt({ super.key, required this.target, required this.type, this.wordLimit });

  @override
  Widget build(BuildContext context) {
    final currentUser = Services.profileService.info!.profile;

    final children = [
      Padding(
        padding: const EdgeInsets.only(right: Insets.lg),
        child: UserAvatar(
          picture: currentUser.info.picture,
          name: currentUser.fullname ?? currentUser.username,
          gradient: currentUser.progress.color,
          radius: 15)),

      Expanded(
        child: TextField(
          readOnly: true,
          enabled: false,
          decoration: InputDecoration( 
            hintText: 'Add new comment',
            suffixIcon: Icon(Icons.send, color: context.colorScheme.primary, size: 24)),
          style: context.text.headlineMedium))
    ];

    return Card(
      child: InkWell(
        onTap: () => openCommentDialog(context),
        child: Container(
          padding: const EdgeInsets.all(Insets.md),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: children))));
  }

  void openCommentDialog(BuildContext context) {
    showModalBottomSheet(
      context: context, 
      isScrollControlled: true,
      builder: (_) => BottomSheetTextField(
        inputLabel: 'comment',
        wordLimit: wordLimit ?? 250,
        validators: [Validators.required],
        onSubmit: (comment) async {
          await CommentSectionProvider.of(context).addComment(comment);
        }));
  } 
}
