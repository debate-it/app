import 'package:debateit_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/discussions/comments/logic/bloc.dart';


class CommentVoteButton extends StatelessWidget {
  final bool canVote;
  final bool isActive;

  const CommentVoteButton({ super.key, required this.canVote, this.isActive = true });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CommentBloc, CommentState>(
      builder: (context, state) {
        if (state is CommentLoading) {
          return const Padding(
            padding: EdgeInsets.all(Insets.md),
            child: SizedBox(
              height: 25, width: 25,
              child: CircularProgressIndicator()));
        }

        final comment = (state as LoadedCommentState).comment;
        final user = Services.profileService.info!.profile;

        final canVoteOnComment = canVote
          && isActive
          && user.username != comment.author.username;

        final voteIcon = comment.voted || !canVoteOnComment
          ? Icons.favorite
          : Icons.favorite_border;

        final color = comment.voted || !canVoteOnComment
          ? context.colorScheme.primary
          : context.colorScheme.onPrimary;

        return Row(
          children: [
            IconButton(
              iconSize: 16,
              icon: Icon(voteIcon, color: color),
              padding: EdgeInsets.zero,
              tooltip: 'Vote',
              onPressed: comment.author.id != user.id
                ? () => vote(context, comment)
                : null
            ),
            
            Text('${state.comment.score}',
              style: context.text.headlineSmall
                ?.copyWith(color: color))
          ],
        );
      },
    );
  }

  void vote (BuildContext context, Comment comment) {
    BlocProvider.of<CommentBloc>(context).add(CommentVotedToggled(comment));
  }
}
