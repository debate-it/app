import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart'; 
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/post/provider.dart';
import 'package:debateit/src/widgets/misc/labeled_icon.dart';


class PostAlignmentButton extends StatefulWidget {
  final AlignmentOption alignment;

  const PostAlignmentButton({ required this.alignment });

  @override
  _PostAlignmentButtonState createState() => _PostAlignmentButtonState();
}

class _PostAlignmentButtonState extends State<PostAlignmentButton> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    final provider = PostProvider.of(context, listen: true);

    Color color;
    IconData icon;
    String label;

    switch (widget.alignment) {
      case AlignmentOption.agree:
        label = 'AGREE';
        color = context.colorScheme.secondary;
        icon = Icons.arrow_circle_up_sharp;
        break;

      case AlignmentOption.disagree:
        label = 'DISAGREE';
        color = context.colorScheme.primary;
        icon = Icons.arrow_circle_down_sharp;
        break;
    }
    
    var iconColor = context.colors.text;

    if (provider.post.voted != null && provider.post.voted != widget.alignment) {
      final average = (0.299 * color.red + 0.587 * color.blue + 0.114 * color.green).toInt();

      color = Color.fromRGBO(average, average, average, color.opacity - 0.3);

      iconColor = iconColor
        .withBlue(iconColor.blue - 5)
        .withGreen(iconColor.green - 5)
        .withRed(iconColor.red - 5);
    }

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 4.0,
        backgroundColor: color,
        foregroundColor: context.colors.text,
        shape: const RoundedRectangleBorder(borderRadius: Corners.medBorder)),

      onPressed: () async {
        if (_loading) {
          return;
        }

        setState(() { _loading = true; });

        try { 
          await provider.align(context, widget.alignment);
        } finally {
          setState(() { _loading = false; });
        }
      },

      child: LabeledIcon(
        icon: icon,
        iconSize: 20,
        iconColor: iconColor,

        loading: _loading,
        
        label: Text('${provider.post.alignmentPoints(widget.alignment)} $label',
          style: context.text.titleLarge
            ?.copyWith(color: iconColor))));
  }
}
