import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/post/provider.dart';
import 'package:debateit/src/discussions/post/widgets/body.dart';



class PostPreview extends StatelessWidget {
  final Post post;

  const PostPreview({ super.key, required this.post });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PostProvider(post),
      child: const PostBody(preview: true)
    );
  }
}
