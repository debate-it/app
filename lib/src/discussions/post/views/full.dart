import 'package:debateit/src/discussions/shared/DiscussionFullView.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/post/provider.dart';
import 'package:debateit/src/discussions/post/widgets/body.dart';

import 'package:debateit/src/discussions/comments/section/main.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class PostFullView extends StatelessWidget {
  final Post post;
  final PostProvider provider;

  PostFullView({super.key, required this.post, PostProvider? provider})
    : provider = provider ?? PostProvider(post); 

  @override
  Widget build(BuildContext context) {
    final current = Services.profileService.info!.profile;

    final view = CustomScrollView(
      slivers: [
        FullViewAppBar(
          type: 'Post',
          target: provider.post,
          category: provider.post.category),

        const SliverToBoxAdapter(child: PostBody()),

        CommentSection(
          useSlivers: true,
          type: CommentType.Post,
          target: provider.post.id, 
          canComment: current.id != provider.post.author.id),

        const SliverPadding(padding: EdgeInsets.all(Insets.xs))
      ]
    );

    return ChangeNotifierProvider.value(
      value: provider,
      child: RefreshIndicator(
        onRefresh: () => provider.refresh(context),
        child: DiscussionFullView(provider: provider, child: view)));
  }
}
