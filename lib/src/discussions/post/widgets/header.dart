import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/post/provider.dart';
import 'package:debateit/src/discussions/report/menu.dart';

import 'package:debateit/src/widgets/misc/creation_label.dart';


class PostHeader extends StatelessWidget { 
  const PostHeader();

  @override
  Widget build(BuildContext context) {
    final post = PostProvider.of(context).post;

    final created = CreationLabel(date: post.created); 

    final optionsMenu = ReportMenu(id: post.id, target: 'Post');

    return Padding(
      padding: const EdgeInsets.only(left: Insets.xs),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [const Spacer(), created,  optionsMenu]));
  }
}
