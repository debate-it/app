import 'package:flutter/material.dart';

import 'package:debateit/src/discussions/post/provider.dart';

import 'package:debateit/src/discussions/post/widgets/creator.dart';
import 'package:debateit/src/discussions/post/widgets/footer.dart';
import 'package:debateit/src/discussions/post/widgets/header.dart';

import 'package:debateit/src/discussions/shared/DiscussionBody.dart';

import 'package:debateit/src/widgets/navigation/utils/link.dart';


class PostBody extends StatelessWidget {
  final bool preview;

  const PostBody({ super.key, this.preview = false });

  @override
  Widget build(BuildContext context) {
    final provider = PostProvider.of(context);

    final content = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        if (preview)
          const PostHeader(),

        PostCreator(standalone: !preview),
        DiscussionBody(discussion: provider.entity, wrapped: !preview, showCategory: false),
        PostFooter(preview: preview)
      ]
    );

    if (preview) {
      return Card(
        child: LinkWidget(
          path: '/discussion/${provider.entity.id}',
          payload: provider,
          child: content));
    }

    return content;
  }
}
