import 'package:flutter/material.dart'; 
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/post/provider.dart';

import 'package:debateit/src/discussions/shared/DiscussionAuthor.dart';
import 'package:debateit/src/discussions/shared/DiscussionCategoryInfo.dart';


class PostCreator extends StatelessWidget {
  final bool standalone;

  const PostCreator({ super.key, this.standalone = false });

  @override
  Widget build(BuildContext context) {
    final post = PostProvider.of(context).post;
    
    final creator = DiscussionAuthor( 
      sender: post.author,
      label: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          if (standalone) ...[
            Text('POSTED ON', 
              style: context.text.titleLarge
                ?.copyWith(color: context.colorScheme.primary)),
                
            Text('${post.created.day}/${post.created.month}/${post.created.year}',
              style: context.text.titleMedium)
          ] else ...[
            Text('Category', 
              style: context.text.titleLarge
                ?.copyWith(color: context.colors.text)),
            DiscussionCategoryInfo(category: post.category),
          ]
        ]
      ));

    return standalone ? Card(child: creator) : creator;
  }
}
