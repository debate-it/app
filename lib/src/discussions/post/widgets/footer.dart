import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/post/buttons/alignment.dart';
import 'package:debateit/src/discussions/post/provider.dart';



class PostFooter extends StatelessWidget {
  final bool preview;

  const PostFooter({ this.preview = true });

  @override
  Widget build(BuildContext context) {
    final provider = PostProvider.of(context, listen: true);
    final user = Services.profileService.info!.profile;

    if (preview || user.id == provider.post.author.id) {

      return _buildContainer([
        Expanded(
          child: PostAlignmentLabel(
            wrap: !preview,
            alignment: AlignmentOption.agree, 
            count: provider.post.alignment.agree)),

        if (preview)
          VerticalDivider(color: context.colors.text),
              
        Expanded(
          child: PostAlignmentLabel(
            wrap: !preview,
            alignment: AlignmentOption.disagree,
            count: provider.post.alignment.disagree))
          
      ]);
    }

    return _buildContainer(const [
      Expanded(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Insets.xs),
          child: PostAlignmentButton(alignment: AlignmentOption.agree))),
      Expanded(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Insets.xs),
          child: PostAlignmentButton(alignment: AlignmentOption.disagree))),
    ]);
  }

  Widget _buildContainer(List<Widget> children) {
    return Container(
      height: 60.0,
      padding: const EdgeInsets.all(Insets.xs),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: children));
  }
}

class PostAlignmentLabel extends StatelessWidget {
  final AlignmentOption alignment; 
  final int count;
  final bool wrap;

  const PostAlignmentLabel({super.key, required this.alignment, required this.count, this.wrap = false});

  @override
  Widget build(BuildContext context) {
    final color = alignment == AlignmentOption.agree
      ? context.colorScheme.secondary
      : context.colorScheme.primary;

    final text = alignment == AlignmentOption.agree
      ? 'AGREE' 
      : 'DISAGREE';

    final container = Container(
      alignment: Alignment.center,
      child: Text('$count $text',
        softWrap: true,
        textAlign: TextAlign.center,
        style: TextStyles.h3
          .copyWith(color: color)));

    if (wrap) { 
      return Card(margin: const EdgeInsets.all(Insets.xs), child: container);
    }

    return container;
  }

}
