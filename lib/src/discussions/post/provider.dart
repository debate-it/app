import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_graphql/queries/post.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/discussions/provider.dart';


class PostProvider extends EntityProvider<Post> {

  PostProvider(Post post)
    : super(entity: post);

  Post get post => entity;
  
  static PostProvider of(BuildContext context, { bool listen = false }) =>
    Provider.of<PostProvider>(context, listen: listen);

  static Future<Post?> initiate(BuildContext context, Map<String, dynamic> data) async { 
    final client = Services.client;
    final result = await client.mutate<Map<String, dynamic>>(mutation: PostQueries.CreatePost, variables: data);

    return Post.fromJson(result);
  }

  Future<void> refresh(BuildContext context) async {
    final client = Services.client;
    final result = await client.fetch(query: PostQueries.FindPost, variables: { 'id': entity.id, });

    entity = Post.fromJson(result.data!['post'] as Map<String, dynamic>);
    notifyListeners();
  }

  Future<void> align(BuildContext context, AlignmentOption alignment) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: PostQueries.AlignWithPost,
      variables: { 'post': entity.id, 'alignment': alignment.label });

    entity.alignment = PostAlignment.fromJson(result['alignment'] as Map<String, dynamic>);
    entity.voted = AlignmentOptionExtension.parse(result['votedOn'] as String?);

    notifyListeners();
  }

  @override
  Future<void> fetch() async {}
}
