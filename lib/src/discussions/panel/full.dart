import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/panel/body.dart';
import 'package:debateit/src/discussions/panel/comments.dart';
import 'package:debateit/src/discussions/panel/provider.dart';

import 'package:debateit/src/discussions/shared/DiscussionFullView.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class PanelFullView extends StatelessWidget {
  final PanelProvider provider;

  PanelFullView({ super.key, PanelProvider? provider, required Panel panel }) 
    : provider = provider ?? PanelProvider(panel: panel);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: provider,
      child: DiscussionFullView(
        provider: provider,
        child: CustomScrollView(
          slivers: [ 
            FullViewAppBar(
              type: 'Panel',
              target: provider.panel, 
              size: provider.panel.size,
              category: provider.panel.category),

            const SliverToBoxAdapter(child: PanelBody()), 
            const PanelComments() 
          ]
        ),
      )
    );
  }
}
