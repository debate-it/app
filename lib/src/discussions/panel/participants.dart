import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_models/discussions/panel/main.dart' as models;

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/panel/provider.dart';
import 'package:debateit/src/discussions/panel/vote.dart';

import 'package:debateit/src/discussions/shared/DisucssionWinner.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';

import 'package:debateit/src/widgets/misc/labeled_icon.dart';


class PanelParticipants extends StatelessWidget {
  final bool standalone;

  const PanelParticipants({ super.key, this.standalone = false });

  @override
  Widget build(BuildContext context) {
    return standalone
      ? const _PanelParticipantsFull()
      : const _PanelParticipantsPreview();
  }
}

class _PanelParticipantsPreview extends StatelessWidget {
  
  const _PanelParticipantsPreview();

  @override
  Widget build(BuildContext context) {
    final panel = PanelProvider.of(context, listen: true).panel;
    
    return Container(
      height: 60,
      width: double.infinity,

      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: Insets.lg),

      // constraints: BoxConstraints(maxWidth: width),
        
      child: LayoutBuilder(builder: (context, constraints) {
        final width = constraints.maxWidth;
        const entryWidth = 42;
        const step = entryWidth / 2 + 5;

        final participantWidth = (panel.participants.length + 1) * step;
        final offset = (width - participantWidth) / 2;

        final participants = panel.participants.reversed.toList()
          .asMap().entries
          .map((entry) => PositionedDirectional(
            height: 40,
            start: offset + entry.key * step,
            child: PanelParticipant(participant: entry.value)))
          .toList();

        return Stack(
          alignment: Alignment.center,
          children: participants);
      }));

  }
}


class PanelParticipant extends StatelessWidget {
  final models.PanelParticipant participant;
  final bool standalone;

  const PanelParticipant({ super.key, required this.participant, this.standalone = false });

  @override
  Widget build(BuildContext context) {
    return UserAvatar(
      radius: 16,
      picture: participant.user.info.picture,
      gradient: participant.user.progress.color,
      name: participant.user.fullname ??
        participant.user.username);
  }
}

class _PanelParticipantsFull extends StatefulWidget {
  const _PanelParticipantsFull();

  @override
  __PanelParticipantsFullState createState() => __PanelParticipantsFullState();
}

class __PanelParticipantsFullState extends State<_PanelParticipantsFull> {

  @override
  Widget build(BuildContext context) {
    final provider = PanelProvider.of(context, listen: true);
    final profile = Services.profileService;

    final votedOn = provider.panel.votedOn;
    final winner = provider.winner != null 
      ? provider.panel.participants.indexOf(provider.winner!)
      : null;

    final isParticipant = provider.panel.participants
      .any((element) => element.user.id == profile.info!.profile.id);

    final isFinished = provider.panel.status == models.PanelStatus.finished;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Insets.xs),
      child: Column(
        children: provider.panel.participants.asMap()
          .entries.map((entry) => 
            PanelParticipantEntry(
              index: entry.key, 
              participant: entry.value, 
              isWinner: winner == entry.key,
              voted: votedOn == entry.key,
              canVote: !isParticipant && !isFinished))
          .toList()));
  }
}

class PanelParticipantEntry extends StatefulWidget {
  final int index;

  final bool voted;
  final bool canVote;
  final bool isWinner;

  final models.PanelParticipant participant;

  const PanelParticipantEntry({
    super.key,
    required this.participant,
    required this.index,
    required this.voted, 
    required this.canVote, 
    this.isWinner = false
  });

  @override
  State<PanelParticipantEntry> createState() => _PanelParticipantEntryState();
}

class _PanelParticipantEntryState extends State<PanelParticipantEntry> with SingleTickerProviderStateMixin {

  bool isOpen = false;

  late final AnimationController controller = 
    AnimationController(duration: const Duration(milliseconds: 400), vsync: this);

  late final Animation<double> animation = CurveTween(curve: Curves.easeInOutCubicEmphasized).animate(controller);

  void toggleOpen() {
    if (isOpen) {
      controller.reverse();
    } else {
      controller.forward();
    }

    setState(() {
      isOpen = !isOpen;
    });
  }

  @override
  Widget build(BuildContext context) {
    final header = PanelParticipantHeader(
      isOpen: isOpen,
      onOpen: toggleOpen,
      participant: widget.participant.user, 
      score: widget.participant.score
    );

    final body = SizeTransition(
      sizeFactor: animation,
      child: Padding(
        padding: const EdgeInsets.all(Insets.md),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(widget.participant.argument,
              style: context.text.bodyLarge),

            if (widget.canVote)
              PanelVoteButton(
                canPress: true,
                voted: widget.voted,
                participant: widget.index)
          ]
        ),
      )
    );

    return Card(
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Corners.sm),
          border: Border.all(
            color: context.colors.accent, 
            style: widget.isWinner 
              ? BorderStyle.solid 
              : BorderStyle.none)),
    
        child: Stack(
          children: [
            if (widget.isWinner) const DiscussionWinner(),
    
            InkWell(
              child: Padding(
                padding: const EdgeInsets.all(Insets.xs),
                child: Column(children: [ header, body ])))
          ]
        )),
    );
  }
}

class PanelParticipantHeader extends StatelessWidget {
  final UserProfile participant;

  final int score;
  final bool isOpen;

  final VoidCallback onOpen;

  const PanelParticipantHeader({ 
    super.key,
    required this.participant,
    required this.score,
    required this.isOpen,
    required this.onOpen
  });

  @override
  Widget build(BuildContext context) {
    final avatar = Container(
      padding: const EdgeInsets.only(right: Insets.md),
      child: UserAvatar(
        radius: 15,
        picture: participant.info.picture,
        gradient: participant.progress.color,
        name: participant.fullname
          ?? participant.username));

    final username = Text('@${participant.username}',
      textAlign: TextAlign.start,
      style: context.text.titleMedium);

    final score = Container(
      decoration: BoxDecoration(
        color: context.colorScheme.background,
        borderRadius: BorderRadius.circular(15)),
      width: 50,
      child: LabeledIcon(
        icon: Icons.keyboard_arrow_up,
        iconColor: context.colors.accent,
        label: Text('${this.score}',
          style: TextStyles.callout2.copyWith(
            color: context.colorScheme.onSurface))));

    final button = IconButton(
      onPressed: onOpen, 
      icon: isOpen 
        ? const Icon(Icons.arrow_drop_up)
        : const Icon(Icons.arrow_drop_down));

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.xs),
      child: InkWell(onTap: onOpen, child: Row(children: [ avatar, username, const Spacer(), score, button ])));
  }
}
