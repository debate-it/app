import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/panel/provider.dart';
import 'package:debateit/src/widgets/misc/labeled_icon.dart';

class PanelVoteButton extends StatefulWidget {
  final bool canPress;
  final bool voted;

  final int participant;

  const PanelVoteButton({ 
    super.key, 
    required this.canPress,
    required this.participant, 
    this.voted = false 
  });

  @override
  _PanelVoteButtonState createState() => _PanelVoteButtonState();
}

class _PanelVoteButtonState extends State<PanelVoteButton> {
  late bool voted = widget.voted;

  @override
  void didUpdateWidget(covariant PanelVoteButton oldWidget) {
    if (oldWidget.voted != widget.voted) {
      voted = widget.voted;
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: widget.canPress ? () => _vote(context) : null,
        style: ElevatedButton.styleFrom(elevation: 8.0, backgroundColor: context.colorScheme.primary),
        child: LabeledIcon(
          label: Text('VOTE', style: context.text.titleLarge),
          iconColor: context.colorScheme.onSurface,
          icon: voted ? Icons.check : Icons.arrow_upward)));
  }

  Future<void> _vote(BuildContext context) async {
    await PanelProvider.of(context).vote(context, participant: widget.participant);
  }
}
