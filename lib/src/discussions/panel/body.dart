import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/panel/header.dart';
import 'package:debateit/src/discussions/panel/participants.dart';
import 'package:debateit/src/discussions/panel/provider.dart';

import 'package:debateit/src/discussions/shared/DiscussionBody.dart';
import 'package:debateit/src/discussions/shared/DiscussionPreview.dart';


class PanelBody extends StatelessWidget {
  final bool preview;

  const PanelBody({ super.key, this.preview = false });

  @override
  Widget build(BuildContext context) {
    final provider = PanelProvider.of(context);

    final content = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PanelHeader(standalone: !preview),
        DiscussionBody(
          discussion: provider.panel,
          wrapped: !preview,
          children: [ if (preview) const PanelParticipants() ]),

        if (!preview)
          const PanelParticipants(standalone: true)
      ],
    );

    if (preview) {
      return DiscussionPreview<Panel>(
        provider: provider,
        discussion: provider.entity,
        onTransitionBack: provider.update,
        child: content
      );
    }

    return content;
  }
}
