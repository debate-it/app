import 'package:flutter/material.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/comments/section/main.dart';
import 'package:debateit/src/discussions/panel/provider.dart';

class PanelComments extends StatelessWidget {
  const PanelComments({super.key});

  @override
  Widget build(BuildContext context) {
    final panel = PanelProvider.of(context);
    final profile = Services.profileService;

    final isParticipant = panel.entity.participants
      .any((participant) => participant.user.id == profile.info!.profile.id);

    return CommentSection(
      useSlivers: true, 
      target: panel.entity.id, 
      type: CommentType.Panel,
      canComment: !isParticipant);
  }
}
