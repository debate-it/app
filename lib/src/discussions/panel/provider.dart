import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/panel.dart';

import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/services.dart';


class PanelProvider extends EntityProvider<Panel> {

  PanelProvider({ required Panel panel })
    : super(entity: panel);
  
  static PanelProvider of(BuildContext context, { bool listen = false }) 
    => Provider.of<PanelProvider>(context, listen: listen);

  Panel get panel => entity;

  PanelParticipant? get winner => entity.status == PanelStatus.finished
    ? entity.participants.fold(null, (value, element) => element.score > (value?.score ?? 0) ? element : value)
    : null;

  Future<void> vote(BuildContext context, { required int participant }) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: PanelQueries.Vote,
      variables: { 'id': entity.id, 'participant': participant });

    final participants = result['participants'] as List<dynamic>;

    entity.votedOn = result['votedOn'] as int?;
    entity.participants = entity.participants
      .map((participant) {
          // Find corresponding score update
          final update = participants
            // ignore: avoid_dynamic_calls
            .firstWhere((element) => element['user']['id'] == participant.user.id) as Map<String, dynamic>;

          // Create new participant record with updated score
          return PanelParticipant(
            argument: participant.argument,
            user: participant.user,
            score: update['score'] as int);
      })
      .toList();

    notifyListeners();
  }

  Future<void> updateStatus(BuildContext context) async {
    final client = Services.client;

    final result = await client.fetch(
      query: PanelQueries.PanelStatus,
      variables: { 'id': entity.id} );

    final panel = result.data?['panel'] as Map<String, dynamic>;

    entity.status = PanelStatusExtension.parse(panel['panelStatus'] as String);

    notifyListeners();
  }

  @override
  Future<void> fetch() async {}
}
