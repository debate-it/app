import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/panel/provider.dart';
import 'package:debateit/src/discussions/report/menu.dart';

import 'package:debateit/src/widgets/misc/countdown_label.dart';
import 'package:debateit/src/widgets/misc/creation_label.dart';


class PanelHeader extends StatelessWidget {
  const PanelHeader({super.key, this.standalone = false });

  final bool standalone;
  
  static const _cardPadding = EdgeInsets.all(Insets.md);

  @override
  Widget build(BuildContext context) {
    final provider = PanelProvider.of(context, listen: true);
    final panel = provider.panel;

    final createionDate = CreationLabel(
      date: panel.created,
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0);

    final deadline = CountdownLabel(
      date: panel.deadline, 
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0,
      builder: (context) => const Text('Waiting'),
      onTimeout: () => provider.updateStatus(context),
      timeoutLabel: panel.status
        .toString()
        .replaceAll('PanelStatus.', '')
        .toUpperCase());

    if (standalone) {
      const margin = EdgeInsets.zero;
      return Padding(
        padding: const EdgeInsets.all(Insets.sm),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Card(
                margin: margin, 
                child: Center(
                  child: Padding(padding: _cardPadding, child: deadline)))),

            const SizedBox(width: Insets.sm),

            Expanded(
              child: Card(
                margin: margin,
                child: Center(
                  child: Padding(padding: _cardPadding, child: createionDate)))) 
          ]
        )
      );
    }
    
    final optionsMenu = ReportMenu(id: panel.id, target: 'Panel');
    final discussionType = Text('Panel',
      textAlign: TextAlign.center,
      style: context.text.titleLarge
        ?.copyWith(color: context.colorScheme.primary));

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[ 
        deadline, const Spacer(flex: 3),
        discussionType, const Spacer(flex: 2),
        createionDate, optionsMenu
      ]
    );
  }
}
