import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/panel/body.dart';
import 'package:debateit/src/discussions/panel/provider.dart';


class PanelPreview extends StatelessWidget {
  final Panel panel;

  const PanelPreview({ super.key, required this.panel });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PanelProvider(panel: panel), 
      child: const PanelBody(preview: true)
    );
  }
}
