import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/widgets/misc/labeled_icon.dart';


class VoteButton extends StatefulWidget {
  final bool voted;
  final String debateId;
  final int participant;


  const VoteButton({ super.key, required this.voted, required this.debateId, required this.participant });

  @override
  _VoteButtonState createState() => _VoteButtonState();
}

class _VoteButtonState extends State<VoteButton> {
  bool _loading = false;

  @override
  void initState() { 
    super.initState();
    _loading = false;
  }

  bool get canPress => !_loading;

  @override
  Widget build(BuildContext context) {
    final color = widget.participant.isOdd
      ? context.colorScheme.primary
      : context.colorScheme.secondary;

    return ElevatedButton(
      onPressed: canPress 
        ? () => _vote(context)
        : null,
      style: ElevatedButton.styleFrom(elevation: 8.0, backgroundColor: color),
      child: LabeledIcon(
        label: Text('VOTE', style: context.text.titleLarge),
        iconColor: context.colorScheme.onSurface,
        icon: widget.voted ? Icons.check : Icons.arrow_upward));
  }

  Future<void> _vote(BuildContext context) async { 
    if (_loading) {
      return;
    }
    
    setState(() { _loading = true; });

    await DebateProvider.of(context)
      .vote(context, participant: widget.participant);
    
    setState(() { _loading = false; });
  }
}
