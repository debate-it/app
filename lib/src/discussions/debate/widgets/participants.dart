import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/debate/buttons/vote.dart';
import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/shared/DisucssionWinner.dart';
import 'package:debateit/src/discussions/user/widgets/badge.dart';


class DebateParticipant extends StatelessWidget {
  final bool canVote;
  final bool reversed;
  final bool vertical;
  final bool transparent;

  final bool isWinner;

  final int side;
  final Color color;
  final DiscussionParticipant participant;

  final double radius;

  const DebateParticipant({
    super.key, 
    required this.color, 
    required this.participant,
    required this.radius,
    required this.side,
    
    this.canVote = false,
    this.reversed = false,
    this.vertical = false, 
    this.transparent = false,
    this.isWinner = false,
  });

  @override
  Widget build(BuildContext context) { 
    final debate = DebateProvider.of(context, listen: true);

    final decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(5),

      boxShadow: !transparent
        ? Shadows.universal
        : null,

      border: Border.all(
        color: context.colors.text, 
        style: isWinner 
          ? BorderStyle.solid 
          : BorderStyle.none),

      color: transparent
        ? Colors.transparent
        : context.colorScheme.surface);

    final content = Material(
      type: MaterialType.card,
      color: Colors.transparent,
      child: Column(
        mainAxisSize: vertical
          ? MainAxisSize.max
          : MainAxisSize.min,

        mainAxisAlignment: vertical
          ? MainAxisAlignment.spaceBetween
          : MainAxisAlignment.start,

        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            alignment: _alignment,

            child: UserBadge(
              profile: participant.user,
              vertical: vertical,
              transparent: transparent,
              radius: radius,
              reversed: !vertical && reversed,
              color: color,
              score: participant.score)),

          if (canVote) 
            VoteButton(debateId: debate.id, participant: side, voted: debate.value.votedOn == side)
        ]
      )
    );


    return Container(
      padding: const EdgeInsets.all(Insets.sm),

      decoration: decoration,

      child: Material(
        elevation: transparent ? 4.0 : 0.0,

        type: transparent 
          ? MaterialType.transparency
          : MaterialType.card,

        child: Stack(
          alignment: Alignment.topCenter,
          children: [ content, if (isWinner) DiscussionWinner(reversed: reversed) ] )));
  }

  Alignment get _alignment {
    if (vertical) {
      return Alignment.center;
    }

    return reversed 
      ? Alignment.centerRight 
      : Alignment.centerLeft;
  }
}


class DebateParticipants extends StatelessWidget {
  final bool standalone;
 
  const DebateParticipants({ this.standalone = false });
  
  @override
  Widget build(BuildContext context) { 
    final provider = DebateProvider.of(context, listen: true);

    final boxes = Row(
      mainAxisSize: MainAxisSize.min,

      crossAxisAlignment: standalone
        ? CrossAxisAlignment.stretch
        : CrossAxisAlignment.center,

      children: _buildBoxes(context));

    return Container(
      constraints: BoxConstraints(maxHeight: 150 + (provider.canVote ? 40 : 0)),
      padding: const EdgeInsets.symmetric(vertical: Insets.sm, horizontal: Insets.sm),

      child: standalone 
        ? boxes 
        : FittedBox(child: boxes));
  }

  List<Widget> _buildBoxes(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final provider = DebateProvider.of(context, listen: true);

    var participants = provider.participants
      .mapEntries<Widget>((participant, int idx) => 
        DebateParticipant(
          side: idx,
          reversed: idx.isOdd,
          vertical: standalone,
          transparent: !standalone,
          participant: participant,
          canVote: standalone && provider.canVote,
          radius: standalone ? 25.0 : 20.0,
          isWinner: provider.winner.index == idx,
          color: idx.isEven 
            ? context.colorScheme.primary 
            : context.colorScheme.secondary))
      .map<Widget>((participant) => 
        Container( 
          constraints: BoxConstraints(maxHeight: 150, maxWidth: width * 0.5),
          child: participant))
      .toList();

    if (standalone) {
      participants = participants
        .map<Widget>((participant) => Expanded(child: participant))
        .toList();

      participants.insert(1, const SizedBox(width: Insets.sm));
    } else {
      participants.insert(1, Padding(
        padding: const EdgeInsets.symmetric(horizontal: Insets.sm),
        child: Text('VS', style: TextStyles.h3)));
    }

    return participants;
  }

  Widget buildWinnerMessage(BuildContext context) {
    final provider = DebateProvider.of(context);

    if (provider.winner == DebateWinner.none) {
      return const SizedBox();
    }

    final style = TextStyles.h3.copyWith(
      color: context.colorScheme.onSurface,
    );

    final message = provider.winner == DebateWinner.draw
      ? Text('Debate ended in a draw', style: style)
      : _makeWinnerText(context, provider);

    if (standalone) {
      return Card(
        child: Container( 
          padding: const EdgeInsets.all(Insets.sm),
          alignment: Alignment.center,
          child: message));
    }

    return Center(child: message);
 
  }

  Widget _makeWinnerText(BuildContext context, DebateProvider provider) { 
    final style = TextStyles.h2.copyWith(
      color: context.colorScheme.onSurface
    );

    final username = provider.winner == DebateWinner.left
      ? provider.participants[0].user.username
      : provider.participants[1].user.username;

    final color = provider.winner == DebateWinner.left
      ? context.colorScheme.secondary
      : context.colorScheme.primary;

    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: username, style: style.copyWith(color: color)),
          TextSpan(text: ' has won the debate', style: style)
        ]
      )
    );
  }
}
