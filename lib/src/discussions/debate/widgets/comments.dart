import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/comments/section/main.dart';


class DebateComments extends StatelessWidget {
  const DebateComments({ super.key });

  @override
  Widget build(BuildContext context) {
    final debate = DebateProvider.of(context);
    final user = Services.profileService.info!.profile;

    final isParticipant = _isParticipant(user, debate.participants);

    if (isParticipant && debate.debate.status == DebateStatus.active) {
      return SliverToBoxAdapter( 
        child: Card(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: Insets.lg, vertical: Insets.md),
            child: Text('Participants will be able to see comments after all rounds are played',
              textAlign: TextAlign.center,
              style: TextStyles.h2))));
    }

    return CommentSection(
      useSlivers: true,
      target: debate.id, 
      type: CommentType.Debate,
      canComment: !isParticipant);
  }

  bool _isParticipant(UserProfile? user, DiscussionParticipants participants) {
    return participants.isParticipant(user);
  }
}
