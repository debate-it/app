import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/debate/provider.dart';

import 'package:debateit/src/discussions/debate/arguments/editor.dart';
import 'package:debateit/src/discussions/debate/arguments/view.dart';

import 'package:debateit/src/discussions/debate/widgets/header.dart';
import 'package:debateit/src/discussions/debate/widgets/participants.dart';

import 'package:debateit/src/discussions/shared/DiscussionBody.dart';
import 'package:debateit/src/discussions/shared/DiscussionPreview.dart';


class DebateBody extends StatelessWidget {
  final bool preview;

  const DebateBody({ super.key, this.preview = false });

  @override
  Widget build(BuildContext context) {
    final provider = DebateProvider.of(context);

    final content = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        DebateHeader(standalone: !preview), 

        DiscussionBody(
          discussion: provider.debate, wrapped: !preview,
          children: [ if (preview) const DebateParticipants() ]),

        if (!preview) ...[
          const DebateParticipants(standalone: true),
          const DebateArgumentEditor(),
          const DebateArguments(),
        ] 
      ],
    );

    if (preview) {
      return DiscussionPreview<Debate>(
        provider: provider,
        discussion: provider.entity, 
        // onTransitionBack: (result) async {
        //   if (result != null) {
        //     provider.update(result);
        //   }
        // } ,
        child: content
      );
    }

    return content;
  }
}
