import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/discussions/main.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/report/menu.dart';

import 'package:debateit/src/widgets/misc/countdown_label.dart';
import 'package:debateit/src/widgets/misc/creation_label.dart';


class DebateHeader extends StatelessWidget {
  const DebateHeader({ this.standalone = false });

  final bool standalone;
  
  static const _cardPadding = EdgeInsets.symmetric(vertical: Insets.sm, horizontal: Insets.md);

  @override
  Widget build(BuildContext context) {
    final provider = DebateProvider.of(context, listen: true);
    final debate = provider.debate;

    final createionDate = CreationLabel(
      date: debate.created,
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0);

    final deadline = CountdownLabel(
      date: debate.deadline, 
      builder: (_) => Text(debate.status.label),
      timeoutLabel: debate.status.name.toUpperCase(),
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0,
      onTimeout: () => onTimeout(context));


    if (standalone) {
      return Container(
        constraints: const BoxConstraints(maxHeight: 70),
        padding: const EdgeInsets.all(Insets.sm),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Card(
                margin: EdgeInsets.zero,
                child: Padding(
                  padding: _cardPadding, 
                  child: deadline))),

            const SizedBox(width: Insets.sm),

            Expanded(
              child: Card(
                margin: EdgeInsets.zero,
                child: Padding(
                  padding: _cardPadding,
                  child: createionDate)))
          ]
        )
      );
    }
    
    final optionsMenu = ReportMenu(id: debate.id, target: 'Debate');

    return Stack(
      alignment: Alignment.center,
      children: [
        Row(children: <Widget>[deadline, const Spacer(), createionDate, optionsMenu ]),
        
        Text('Debate',
          textAlign: TextAlign.center,
          style: context.text.titleLarge
            ?.copyWith(color: context.colorScheme.primary)), 
      ],
    );
  }

  // Sup
  Future<void> onTimeout(BuildContext context) async {
    final provider = DebateProvider.of(context);

    try {
      final bloc = BlocProvider.of<PaginationBloc<Debate>>(context);
      
      await provider.updateStatus();

      if (provider.removeOnExpiration) {
        bloc.add(PageRemoveItem(provider.debate.id));
      }
    } finally {
      provider.updateStatus();
    }
  }
}
