import 'package:flutter/material.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/debate/widgets/header.dart';
import 'package:debateit/src/discussions/debate/widgets/participants.dart';

import 'package:debateit/src/discussions/shared/DiscussionBody.dart';


class DebateInfo extends StatelessWidget {
  const DebateInfo({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = DebateProvider.of(context, listen: true);

    return Column(
      mainAxisSize: MainAxisSize.min, 
      children: [
        const DebateHeader(standalone: true), 
        DiscussionBody(discussion: provider.debate, wrapped: true),
        const DebateParticipants(standalone: true)
      ]
    );
  }
}
