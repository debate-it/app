import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/debate/provider.dart';

import 'package:debateit/src/widgets/misc/countdown.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/forms/bottom_sheet_field.dart';


class DebateArgumentEditor extends StatelessWidget { 
  const DebateArgumentEditor();

  @override
  Widget build(BuildContext context) {
    final provider = DebateProvider.of(context, listen: true);
    final user = Services.profileService.info!.profile;

    if (provider.entity.arguments == null) {
      return const Center(child: Loader());
    }

    if (provider.debate.status == DebateStatus.finished) {
      return const SizedBox();
    }
    
    if (provider.debate.status == DebateStatus.voting) {
      return const DebateVotingStageSign();
    }

    final isActive = provider.entity.status == DebateStatus.active;
    final isParticipant = provider.debate.participants.isParticipant(user);

    final userSide = provider.debate.participants.participantSide(user);
    final canAddArgument = isActive && isParticipant && provider.debate.currentTurn == userSide;

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(Insets.md),
        child: Builder(
          builder: (context) {
            if (!isParticipant) {
              return const SizedBox();
            }

            if (canAddArgument) {
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: Insets.lg), 
                child: PrimaryBtn(
                  onPressed: () => showEditorDialog(context, provider),
                  icon: Icons.code, label: 'ADD ARGUMENT'));
            } else {
              return ElevatedButton( 
                onPressed: null,
                style: ElevatedButton.styleFrom(
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0))),
                  child: Text("Wait for opponents's turn", style: TextStyles.h3, textAlign: TextAlign.center));
            } 
          }
        ),
      ),
    );
  }

  void showEditorDialog(BuildContext context, DebateProvider provider) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => BottomSheetTextField(
        inputLabel: 'argument',
        wordLimit: provider.debate.size.argumentLength,
        onSubmit: (arg) async {
          await provider.addArgument(context, arg);
        }));
  }
}

class DebateVotingStageSign extends StatelessWidget {
  const DebateVotingStageSign({super.key});

  @override
  Widget build(BuildContext context) {
    final debate = DebateProvider.of(context).entity;

    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: Insets.lg),
        child: Column(
          children: [
            Text('Debate has finished,\nvoting ends in',
              textAlign: TextAlign.center,
              style: context.text.titleLarge?.copyWith(color: context.colorScheme.onSurface)),
    
            Countdown(
              end: debate.deadline!,
              style: context.text.headlineLarge
                ?.copyWith(color: context.colorScheme.primary))
          ]
        )),
    );
  }

}
