import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/discussions/main.dart' as models;

import 'package:intl/intl.dart';


class DebateArgument extends StatelessWidget {
  final models.DebateArgument argument;

  const DebateArgument({ super.key, required this.argument });

  bool get reverseArgument => argument.participant.isOdd;

  @override
  Widget build(BuildContext context) {
    final header = _buildContentHeader(context);

    final content = Container(
      padding: const EdgeInsets.all(Insets.md),
      child: Text(argument.content,
        textAlign: reverseArgument
          ? TextAlign.end
          : TextAlign.start,
        style: context.text.titleMedium));

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Insets.md),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [ header, content ]));
  }

  Widget _buildContentHeader(BuildContext context) {
    final participants = DebateProvider.of(context).participants;
    final user = participants[argument.participant].user;

    final avatar = buildAvatar(context);
    final username = Text(user.username, 
      style: context.text.titleLarge
        ?.copyWith(color: argument.participant.isOdd
          ? context.colorScheme.primary 
          : context.colorScheme.secondary));

    final identity = Row(children: [avatar, username]);
    
    
    final format = DateFormat('h:mm a, MMM d, yy', Intl.getCurrentLocale());
    final date = Text(format.format(argument.time.toLocal()), style: context.text.bodyMedium);

    final children = [ identity, date ];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: reverseArgument
        ? children.reversed.toList()
        : children);
  }

  Widget buildAvatar(BuildContext context) {
    const avatar_radius = 10.0;
    
    final participants = DebateProvider.of(context).participants;
    final user = participants[argument.participant].user;
    final name = user.fullname ?? user.username;

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: Insets.xs),
      child: UserAvatar(
        name: name,
        picture: user.info.picture,
        radius: avatar_radius,
        gradient: argument.participant.isOdd
          ? context.colors.primaryGradient
          : context.colors.secondaryGradient)); 
  }
}
