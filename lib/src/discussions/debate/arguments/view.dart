import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit_models/discussions/main.dart' as models;

import 'package:debateit/src/discussions/debate/arguments/entry.dart';
import 'package:debateit/src/discussions/debate/provider.dart';

 
class DebateArguments extends StatelessWidget { 

  const DebateArguments();

  @override
  Widget build(BuildContext context) { 

    final arguments = _buildArguments(context);

    final title = Padding(
      padding: const EdgeInsets.symmetric(vertical: Insets.xs),
      child: Text('ARGUMENTS', 
        textAlign: TextAlign.center,
        style: TextStyles.h2.copyWith(color: context.colorScheme.onSurface)));
        
    final argumentsList = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [ title, arguments ]);

    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: Insets.lg, horizontal: Insets.md), 
        child: argumentsList));
  } 

  Widget _buildArguments(BuildContext context) {
    final arguments = DebateProvider.of(context, listen: true).arguments;

    return arguments?.isNotEmpty ?? false
      ? _buildArgumentList(context, arguments!)
      : _buildNoArgumentText(context);
  }

  Widget _buildArgumentList(BuildContext context, List<models.DebateArgument> arguments) {

    final argumentWidgets = arguments
      .map((arg) => DebateArgument(argument: arg))
      .expand((arg) => [const Divider(height: 40), arg])
      .toList();


    return Column(children: argumentWidgets);
  }

  Widget _buildNoArgumentText(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: Insets.md),
      child: Text('No arguments were posted yet',
        textAlign: TextAlign.center,
        style: TextStyles.h3.copyWith(color: context.colorScheme.onSurface)));
  }
}
