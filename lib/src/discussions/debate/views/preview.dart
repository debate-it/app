import 'package:flutter/material.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/debate/widgets/body.dart';


class DebatePreview extends StatelessWidget {
  final Debate debate;

  final bool removeOnExpiration;

  const DebatePreview({ super.key, required this.debate, this.removeOnExpiration = false});
 
  @override
  Widget build(BuildContext context) { 
    return DebateProvider.create(
      debate: debate, 
      removeOnExpiration: removeOnExpiration,
      child: const DebateBody(preview: true)
    );
  }
}
