import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/debate/widgets/body.dart';
import 'package:debateit/src/discussions/debate/widgets/comments.dart';

import 'package:debateit/src/discussions/shared/DiscussionFullView.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class DebateFullView extends StatelessWidget {
  final Debate debate;
  final DebateProvider? provider;

  const DebateFullView({ super.key, required this.debate, this.provider });

  @override
  Widget build(BuildContext context) {
    if (provider != null) {
      return ChangeNotifierProvider.value(value: provider, child: const _DebateWidget());
    }

    return DebateProvider.create(debate: debate, child: const _DebateWidget());
  }

}

class _DebateWidget extends StatefulWidget {

  const _DebateWidget();

  @override
  State<_DebateWidget> createState() => _DebateWidgetState();
}

class _DebateWidgetState extends State<_DebateWidget> {
  @override
  void initState() {
    super.initState();

    final user = Services.profileService.info!.profile;
    final provider = DebateProvider.of(context);

    final isParticipant = provider.debate.participants.isParticipant(user);
    final isActive = provider.debate.status == DebateStatus.active;
    
    if (!provider.listeningToUpdates && isParticipant && isActive) {
      provider.subscribeToUpdates(context);
    }
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) { 
    final provider = DebateProvider.of(context);
    final debate = provider.debate;

    return WillPopScope(
      onWillPop: () async {
        final navigator = Navigator.of(context);
        final debate = DebateProvider.of(context).entity;

        await DebateProvider.of(context).unsubsribeFromUpdates();
        
        navigator.pop(debate);

        return true;
      },
      child: DiscussionFullView(
        provider: provider, 
        child: CustomScrollView( 
          slivers: [
            FullViewAppBar(
              category: debate.category,
              size: debate.size,
              target: debate, 
              type: 'Debate'),

            const SliverToBoxAdapter(child: DebateBody()),
            const DebateComments(),
            
            const SliverPadding(padding: EdgeInsets.all(5))
          ]
        )
      )
    );
  }
}
