import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:debateit_graphql/queries/debate.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/services.dart';

 
enum DebateWinner { left, right, draw, none }

class DebateProvider extends EntityProvider<Debate> {
  static DebateProvider of(BuildContext context, { bool listen = false }) 
    => Provider.of<DebateProvider>(context, listen: listen);
    
  static ChangeNotifierProvider<DebateProvider> create({
    required Debate debate,
    required Widget child, 
    bool removeOnExpiration = false 
  }) =>
    ChangeNotifierProvider<DebateProvider>(
      child: child, 
      create: (context) =>
        DebateProvider(
          debate: debate,
          removeOnExpiration: removeOnExpiration));

  final bool removeOnExpiration;

  DebateProvider({ required Debate debate, this.removeOnExpiration = false }) 
    : super(entity: debate);

  Stream<QueryResult>? _updates;
  StreamSubscription<QueryResult>? _subscription;

  bool get listeningToUpdates => _updates != null;
  
  String get id => entity.id;
  Debate get debate => entity;
  DiscussionParticipants get participants => entity.participants;
  List<DebateArgument>? get arguments => entity.arguments;
  Debate get value => entity; 

  bool get canVote {
    final user = Services.profileService.info!.profile;

    final isVotingPeriod = debate.status == DebateStatus.voting;
    final isParticipant = debate.participants.isParticipant(user);
    final expiredForVote = debate.deadline != null && DateTime.now().isAfter(debate.deadline!);

    return !isParticipant && isVotingPeriod && !expiredForVote; 
  }

  DebateWinner get winner {
    if (entity.status != DebateStatus.finished) {
      return DebateWinner.none;
    }

    final participants = entity.participants;

    if (participants[0].score == participants[1].score) {
      return DebateWinner.draw;
    } 

    return (participants[0].score > participants[1].score)
      ? DebateWinner.left
      : DebateWinner.right; 
  }


  @override
  Future<void> fetch() async {
    final result = await client.fetch(
      query: DebateQueries.Debate,
      variables: { 'id': entity.id });

    entity = Debate.fromJson(result.data!['debate'] as Map<String, dynamic>); 

    notifyListeners();
  }

  void subscribeToUpdates(BuildContext context) {
    final client = Services.client;
    final user = Services.profileService.info!.profile;

    _updates = client.subscribe(
      subscription: DebateQueries.DebateArgumentUpdates,
      variables: { 'id': entity.id, 'participant': entity.participants.participantSide(user) });

    _subscription = _updates!.listen((result) async {
      if (result.data == null) {
        return;
      }

      final update = result.data!['debateRealtimeArguments'] as Map<String, dynamic>;

      final argument = DebateArgument.fromJson(update['argument'] as Map<String, dynamic>); 
      final index = update['index'] as int;

      if (entity.arguments?.length == index) {
        entity.arguments?.insert(index, argument);
      }

      updateStatus();
    });
  }

  Future<void> unsubsribeFromUpdates() async {
    await _subscription?.cancel();
    _updates = null;
  }
 
  Future<void> vote(BuildContext context, { required int participant }) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: DebateQueries.Vote,
      variables: { 'id': entity.id, 'participant': participant });

    // Get updated participant scores
    final participants = result['participants'] as List<dynamic>;

    entity.votedOn = result['votedOn'] as int?;
    entity.participants = DiscussionParticipants(
      participants: entity.participants
        .map((participant) {
          // Find corresponding score update
          final update = participants
            // ignore: avoid_dynamic_calls
            .firstWhere((element) => element['user']['id'] == participant.user.id) as Map<String, dynamic>;

          // Create new participant record with updated score
          return DiscussionParticipant(score: update['score'] as int, user: participant.user);
        })
        .toList());
    
    notifyListeners();
  }

  Future<void> updateStatus() async {
    final client = Services.client;

    final result = await client.fetch(
      query: DebateQueries.DebateStatus,
      variables: { 'id': entity.id });

    if (result.hasException) {
      throw result.exception!;
    }

    final data = result.data!['debate'] as Map<String, dynamic>;

    entity.status = DebateStatusExtension.parse(data['debateStatus'] as String);

    if (entity.status == DebateStatus.voting) {
      entity.deadline = DateTime.fromMillisecondsSinceEpoch(data['debateDeadline'] as int, isUtc: true);
    }

    notifyListeners();
  }

  Future<void> addArgument(BuildContext context, String argument) async {
    final client = Services.client; 

    try {
      final result = await client.mutate<Map<String, dynamic>>(
        mutation: DebateQueries.AddArgument,
        variables: { 'id': entity.id, 'argument': argument });

      // Get argument list and add the new one
      final arguments = result['arguments'] as List<dynamic>;
 
      entity.arguments?.add(DebateArgument.fromJson(arguments.last as Map<String, dynamic>));

      // update debate status and deadline
      entity.status = DebateStatusExtension.parse(result['status'] as String);

      if (entity.status == DebateStatus.voting) {
        entity.deadline = DateTime.fromMillisecondsSinceEpoch(result['deadline'] as int, isUtc: true);
      }

      notifyListeners();
        
    } on Exception catch (e) {
      throw Exception('Unable to submit an argument: $e');
    }
  }
}
