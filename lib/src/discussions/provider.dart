import 'package:flutter/material.dart';

import 'package:debateit_graphql/graphql.dart';
import 'package:debateit/src/services.dart';


abstract class EntityProvider<T> extends ChangeNotifier {

  T entity;
  BackendClient get client => Services.client;

  EntityProvider({ required this.entity });

  Future<void> fetch();

  void update(T? newValue) {
    if (newValue == null) {
      return;
    }
    
    entity = newValue;
    notifyListeners();
  }
}
