import 'package:flutter/material.dart'; 
import 'package:provider/provider.dart'; 

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/config/pagination/multi.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/widgets/info.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';

import 'package:debateit/src/widgets/navigation/icons/back.dart';

extension _UserAppBarHeight on UserProvider {
  double get height {
    var baseHeight = 250.0 + 60.0;

    if (info.about != null) {
      baseHeight += ((info.about?.length ?? 1) / 100).ceil() * 30.0;
    }

    return baseHeight += !isSelf ? 60.0 : 40.0;
  }
}

class UserFullView extends StatefulWidget { 
  final UserProfile profile;
  final UserProvider provider;
 
  UserFullView({ required this.profile, UserProvider? provider })
    : provider = provider ?? UserProvider(user: profile);

  @override
  State<UserFullView> createState() => _UserFullViewState();
}

class _UserFullViewState extends State<UserFullView> {
  late final ScrollController _controller = ScrollController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) { 
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Scaffold(
        resizeToAvoidBottomInset: true, 
        body: ChangeNotifierProvider.value(
          value: widget.provider,
          child: CustomScrollView(
            controller: _controller,
            physics: const BouncingScrollPhysics(),
            slivers: <Widget>[ 
              SliverAppBar(
                pinned: true,
                floating: true,

                leading: const BackIcon(),
                actions: const [ UserInfoAboutButton() ],
                
                expandedHeight: widget.provider.height,
                
                title: UserFullViewAppBar(controller: _controller, provider: widget.provider),

                flexibleSpace: const FlexibleSpaceBar(
                  collapseMode: CollapseMode.pin,
                  stretchModes: [ StretchMode.blurBackground, StretchMode.fadeTitle ],
                  background: Padding(padding: EdgeInsets.only(top: 50.0), child: UserInfoWidget()))), 
              // info, 
              SliverPaginationProvider(
                client: Services.client,
                key: const ValueKey('user-discussions'),
                physics: const BouncingScrollPhysics(),
                config: UserProfileContentPaginationConfig(id: widget.provider.user.id, username: widget.provider.username))            
            ]
          )
        )
      )
    ); 
  }
}

class UserFullViewAppBar extends StatefulWidget {
  const UserFullViewAppBar({super.key, required this.controller, required this.provider});

  final UserProvider provider;
  final ScrollController controller;

  @override
  State<UserFullViewAppBar> createState() => _UserFullViewAppBarState();
}

class _UserFullViewAppBarState extends State<UserFullViewAppBar> {
  late final double _infoHeight = widget.provider.height;

  bool _showBarTitle = false;

  @override
  void initState() {
    widget.controller.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    widget.controller.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (!_showBarTitle && widget.controller.offset + 30 >= _infoHeight) {
      setState(() { _showBarTitle = true; });
    } else if (_showBarTitle && widget.controller.offset - 30 < _infoHeight) {
      setState(() { _showBarTitle = false; });
    }
  }

  void _scrollToTop() {
    widget.controller.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.easeOutSine);
  }
  
  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: _showBarTitle ? 1.0 : 0.0,
      duration: const Duration(milliseconds: 100),
      child: AbsorbPointer(
        absorbing: !_showBarTitle,
        child: InkWell(
          onTap: _scrollToTop,
          child: Row(
            children: [
              UserAvatar(
                radius: 10,
                gradient: widget.provider.user.progress.color,
                name: widget.provider.user.fullname ?? widget.provider.user.username),
          
              const SizedBox(width: Insets.sm),
          
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Display full name, if user specified it
                  if (widget.provider.user.fullname != null)
                    Text(widget.provider.user.fullname!,
                      softWrap: true,
                      style: context.text.titleSmall
                        ?.copyWith(color: context.colors.accent)),
          
                  // Display username
                  Text('@${widget.provider.username}',
                    style: context.text.bodyMedium
                      ?.copyWith(color: context.colors.primary)),
              ])
            ],
          ),
        ),
      ), 
    );
  }
}
