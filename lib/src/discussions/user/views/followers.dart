import 'package:beamer/beamer.dart';
import 'package:debateit/src/repositories/user.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/config/pagination/user.dart';

import 'package:debateit/src/widgets/misc/page_header.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';

class UserFollowers extends StatelessWidget {
  const UserFollowers({super.key, this.profile, this.id})
    : assert(id != null || profile != null, 'User profile or ID should be provided');

  final String? id;
  final UserProfile? profile;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<UserProfile>(
      future: _getUser(),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.hasError) {
          showDialog(context: context, builder: (_) => InfoDialog(message: snapshot.error.toString()));
          Beamer.of(context).beamBack();
        }

        final profile = snapshot.data!;

        return FixedViewPage(
          body: PaginationProvider(
            client: Services.client,
            config: FollowersPaginationConfig(profile),
            header: PageHeader(text: '${profile.isSelf ? "Your" : "${profile.username}'s"} followers'),
            padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md)));
      }
    );
  }

  Future<UserProfile> _getUser() async {
    if (profile != null) {
      return profile!;
    }

    return UserRepository.read(id!);
  }
}
