import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/repositories/user.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/widgets/about.dart';

class UserAboutPage extends StatelessWidget {
  const UserAboutPage({super.key, required this.id, this.provider});

  final String id;
  final UserProvider? provider;

  @override
  Widget build(BuildContext context) {
    if (provider != null) {
      return UserAbout(provider: provider!);
    }

    return FutureBuilder<UserProfile>(
      future: UserRepository.read(id),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.data == null) {
          return const Center(child: Text('User not found'));
        }

        return ChangeNotifierProvider(
          create: (_) => UserProvider(user: snapshot.data!),
          child: Builder(builder: (context) => UserAbout(provider: UserProvider.of(context))));
      },
    );
  }
}
