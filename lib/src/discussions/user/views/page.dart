import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/discussions/user/views/full.dart';
import 'package:debateit/src/repositories/user.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';

class UserPage extends StatelessWidget {
  const UserPage({ super.key, required this.id });

  final String id;

  @override
  Widget build(BuildContext context){
    return FutureBuilder(
      future: UserRepository.read(id),
      builder: (context, AsyncSnapshot<UserProfile> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.hasError) {
          showDialog(context: context, builder: (_) => InfoDialog(message: snapshot.error.toString()));
          Beamer.of(context).beamBack();
        }

        return UserFullView(profile: snapshot.data!);
      },
    );
  }
}
