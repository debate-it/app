import 'package:animations/animations.dart';
import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/user/UserProfile.dart'; 

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/widgets/badge.dart';
import 'package:debateit/src/discussions/user/widgets/stats.dart';


class UserPreview extends StatelessWidget {
  final UserProfile user;
  final Widget? side;

  final void Function(UserProfile)? onTap;
  
  final bool minimal;
  final bool transparent;

  const UserPreview({
    super.key,
    required this.user,
    this.onTap,
    this.side,
    this.minimal = false,
    this.transparent = false
  });

  bool get onTapProvided => onTap != null;

  @override
  Widget build(BuildContext context) { 
    const margin = EdgeInsets.symmetric(vertical: Insets.xs);

    final badge = AbsorbPointer(
      absorbing: onTapProvided,
      child: UserBadge(radius: 20, transparent: true, profile: user));

    final firstLine = Row( 
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ badge, side ?? const Spacer() ]);

    final content = InkWell(
      enableFeedback: onTapProvided,
      onTap: () => onTap?.call(user),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: Insets.sm, horizontal: Insets.lg),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[firstLine, if (!minimal) const UserStatsWidget()])));

    return UserProvider.create(
      user: user, 
      child: transparent
        ? Padding(padding: margin, child: content)
        : Card(margin: margin, child: content));
  }
}
