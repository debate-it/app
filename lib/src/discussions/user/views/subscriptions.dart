import 'package:beamer/beamer.dart';
import 'package:debateit/src/repositories/user.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:debateit_models/models.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/profile.dart';

import 'package:debateit/src/config/pagination/profile.dart';

import 'package:debateit/src/widgets/misc/page_header.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';

class UserSubscriptionsPage extends StatelessWidget {
  const UserSubscriptionsPage({super.key, this.id, this.profile})
    : assert(id != null || profile != null, 'User profile or ID should be provided');

  final String? id;
  final UserProfile? profile;
  
  Future<UserProfile> _getUser() async {
    if (profile != null) {
      return profile!;
    }

    return UserRepository.read(id!);
  }
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getUser(),
      builder: (context, AsyncSnapshot<UserProfile> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (snapshot.hasError) {
          showDialog(context: context, builder: (_) => InfoDialog(message: snapshot.error.toString()));
          Beamer.of(context).beamBack();
        }

        return UserSubscriptions(profile: snapshot.data!);
      },
    );
  }

}

class UserSubscriptions extends StatefulWidget {
  const UserSubscriptions({super.key, required this.profile});

  final UserProfile profile;
  
  @override
  _UserSubscriptionsState createState() => _UserSubscriptionsState();
}

class _UserSubscriptionsState extends State<UserSubscriptions> with TickerProviderStateMixin {
  late TabController controller = TabController(length: 2, vsync: this);

  late PaginationBloc users;
  late PaginationBloc categories;
  
  ProfileService get service => Services.profileService;

  void update() {
    switch (controller.index) {
      case 0: users.add(const PageRefresh()); break;
      case 1: categories.add(const PageRefresh()); break;
    }
  }

  @override
  void initState() {
    service.addListener(update);
    
    users = PaginationBloc(
      client: Services.client,
      config: ProfileUserSubscriptionsPaginationConfig(
        (profile) => Beamer.of(context).beamToNamed('/user/${profile.id}')));

    categories = PaginationBloc(
      client: Services.client, 
      config: ProfileCategorySubscriptionConfig());

    super.initState();
  }

  @override
  void dispose() {
    service.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) { 
    return FixedViewPage( 
      bottom: TabBar(
        controller: controller,
        labelColor: context.colorScheme.onSurface,
        tabs: const [ Tab(text: 'Users'), Tab(text: 'Categories') ]),

      body: TabBarView(
        controller: controller,
        children: [
          PaginationProvider.fromBloc(
            header: const PageHeader(text: 'Your user subscriptions'),
            padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md),
            bloc: users),

          PaginationProvider.fromBloc(
            header: const PageHeader(text: 'Your category subscriptions'),
            padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md),
            bloc: categories)
        ],
      )
    );
  }
}
