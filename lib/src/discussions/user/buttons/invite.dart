import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart'; 
import 'package:beamer/beamer.dart';

import 'package:debateit_models/user/UserProfile.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';


class InviteButton extends StatelessWidget {
 
  const InviteButton();

  @override
  Widget build(BuildContext context) {
    final self = _getAccount(context);
    final user = UserProvider.of(context).user;

    final levelsMatch = self.progress.level >= user.progress.level;

    final onPressed = levelsMatch
      ? () => invite(context, user)
      : null;

    final tooltipText = !levelsMatch
      ? 'Users of higher level cannot be invited'
      : 'Invite to Debate';

    return Tooltip(
      message: tooltipText,
      preferBelow: false,
      child: OutlinedBtn(
        label: 'INVITE', 
        onPressed: onPressed,
        borderColor: context.colors.primary,
      ));
  }
  
  UserProfile _getAccount(BuildContext context) => 
    Services.profileService.info!.profile;

  void invite(BuildContext context, UserProfile profile) {
    context.beamToNamed('/initiate/debate?opponent=${profile.id}');
  } 
}
