import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';


class FollowButton extends StatefulWidget {

  const FollowButton();

  @override
  _FollowButtonState createState() => _FollowButtonState();
}

class _FollowButtonState extends State<FollowButton> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    final followed = UserProvider.of(context, listen: true).followed;

    return SecondaryBtn(
      loading: loading,
      label: followed ? 'FOLLOWED' : 'FOLLOW',
      icon: followed ? Icons.check : null,
      onPressed: () => follow(context));
  }

  Future<void> follow(BuildContext context) async {
    final provider = context.get<UserProvider>();

    setState(() { loading = true; });

    try {
      await provider.follow();
    } finally {
      setState(() { loading = false; });
    }
  }
}
