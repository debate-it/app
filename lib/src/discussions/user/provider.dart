import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit_graphql/queries/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



import 'package:debateit_models/user/UserProfile.dart';
import 'package:debateit_models/user/UserInfo.dart';
import 'package:debateit_models/user/UserStats.dart';

 
class UserProvider extends EntityProvider<UserProfile> {
  static UserProvider of(BuildContext context, { bool listen = false }) =>
    Provider.of<UserProvider>(context, listen: listen);

  static ChangeNotifierProvider<UserProvider> create({ required UserProfile user, required Widget child }) =>
    ChangeNotifierProvider<UserProvider>(child: child, create: (context) => UserProvider(user: user));

  UserProfile get user => entity;

  set user(UserProfile? profile) {
    if (profile != null) {
      entity = profile;
      notifyListeners(); 
    }
  }

  UserInfo get info => entity.info;
  UserStats get stats => entity.stats;

  bool get isSelf => entity.relation == Relation.self;
  
  bool get followed => 
    entity.relation == Relation.subscription ||
    entity.relation == Relation.friend;

  String get username => entity.username;
  String? get picture => entity.info.picture; 
 
  UserProvider({ required UserProfile user }) 
    : super(entity: user);

  @override
  Future<void> fetch() async {
    final result = await client.fetch(query: UserQueries.User, variables: { 'id': entity.id });

    entity = UserProfile.fromJson(result.data!['user'] as Map<String, dynamic>); 
    notifyListeners();
  } 

  Future<void> follow() async { 
    final result = await client.mutate<Map<String, dynamic>>(
      mutation: UserQueries.follow,
      variables: { 'id': entity.id });

    entity = UserProfile.fromJson(result); 
    notifyListeners();
  }
}
