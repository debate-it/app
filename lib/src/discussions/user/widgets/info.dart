import 'dart:io';

import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' show MultipartFile;

import 'package:debateit_core/core.dart';
import 'package:debateit_graphql/mutations/actions.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/report/menu.dart';

import 'package:debateit/src/discussions/user/provider.dart';

import 'package:debateit/src/discussions/user/buttons/follow.dart';
import 'package:debateit/src/discussions/user/buttons/invite.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';
import 'package:debateit/src/discussions/user/widgets/progress.dart';
import 'package:debateit/src/discussions/user/widgets/stats.dart';
import 'package:debateit/src/discussions/user/widgets/rank.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';


class UserInfoWidget extends StatelessWidget {

  const UserInfoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final provider = UserProvider.of(context, listen: true);

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Card(
          child: Padding(
            padding: const EdgeInsets.all(Insets.md),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // User's name and username
                const UserInfoNameSection(),
                
                if (provider.isSelf)
                  const UserProgressIndicator(),

                // About section, if user has it.
                if (provider.info.about != null)
                  Padding(
                    padding: const EdgeInsets.all(Insets.md),
                    child: Text(provider.info.about!,
                      style: context.text.caption)),
              ]))),

              
        // User's statistics
        const Card(
          child: Padding(
            padding: EdgeInsets.all(Insets.sm),
            child: UserStatsWidget())),
              
        // Section with Follow and Invite buttons, if it's not the
        if (!provider.isSelf ) ...[
          const UserInfoActions(),
        ],
        
        const Divider(indent: 10, endIndent: 10),
      ]
    );
  }
}

class UserInfoAvatar extends StatelessWidget {
  const UserInfoAvatar({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = UserProvider.of(context, listen: true);

    const radius = 40.0;

    final avatar = UserAvatar(
      radius: radius,
      picture: provider.info.picture,
      gradient: provider.user.progress.color,
      name: provider.user.fullname ?? provider.username);

    if (!provider.isSelf) {
      return avatar;
    }

    return Container(
      height: radius * 2 + 10,

      margin: const EdgeInsets.only(right: Insets.md, bottom: Insets.lg),

      constraints: const BoxConstraints(
        minHeight: radius * 2 + 40, 
        maxHeight: radius * 2 + 40,
        
        minWidth: radius * 2, 
        maxWidth: radius * 2 + 30,
      ),

      child: Stack(
        fit: StackFit.expand,
        children: [
          Padding(
            padding: const EdgeInsets.all(Insets.xs),
            child: avatar,
          ),

          Positioned(
            right: Insets.xs, 
            bottom: Insets.xs,

            child: DecoratedBox(
              decoration: BoxDecoration(color: context.colors.surface, shape: BoxShape.circle),
              child: IconButton(
                constraints: const BoxConstraints(minHeight: 40, minWidth: 40),
                padding: EdgeInsets.zero,
                icon: Icon(Icons.add_a_photo_rounded, size: 18, color: context.colors.text),
                onPressed: () => openPhotoSelectionDialog(context, provider))))
        ]
      )
    );
  }

  void openPhotoSelectionDialog(BuildContext context, UserProvider provider) {
    showModalBottomSheet(
      context: context, 

      isDismissible: true,
      isScrollControlled: false,

      builder: (ctx) => UserInfoPhotoSelector(provider: provider, onSubmit: () => Navigator.of(ctx).pop()));
  }
}

class UserInfoPhotoSelector extends StatefulWidget {
  final UserProvider provider;
  final VoidCallback onSubmit;

  const UserInfoPhotoSelector({super.key, required this.provider, required this.onSubmit});

  @override
  State<UserInfoPhotoSelector> createState() => _UserInfoPhotoSelectorState();
}

class _UserInfoPhotoSelectorState extends State<UserInfoPhotoSelector> {
  File? _image;

  @override
  Widget build(BuildContext context) {

    const radius = 50.0;

    final image =  FittedBox( 
      fit: BoxFit.fitWidth, 
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: Insets.sm, vertical: Insets.xs),

        child: UserAvatar(
          image: _image,
          radius: radius,

          picture: widget.provider.info.picture,
          name: widget.provider.user.fullname 
            ?? widget.provider.user.username)));

    final options = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(Insets.md),
            child: PrimaryBtn(
              label: 'Select a photo',
              onPressed: () => getImage(ImageSource.gallery)))),

        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(Insets.md),
            child: SecondaryBtn(
              label: 'Take a photo',
              onPressed: () => getImage(ImageSource.camera))))
      ],
    );

    final submission = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Expanded(
          flex: 2, 
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: Insets.sm),
            child: PrimaryBtn(label: 'Submit', onPressed: submitImage))),

        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: Insets.sm),
            child: SecondaryBtn(label: 'Cancel', onPressed: cancelImage)))
      ]
    );

    return Padding(
      padding: const EdgeInsets.all(Insets.md),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (_image != null) 
            ...[ image, submission ]
          else 
            options
        ],
      ),
    );
  }

  void cancelImage() {
    setState(() {
      _image = null;
    });
  }

  Future<void> submitImage() async {
    if (_image == null) {
      return;
    }

    final client = Services.client;
    final profile = Services.profileService;

    // showSnackBar(context, 'Updating profile image...');

    final image = MultipartFile.fromBytes('', await _image!.readAsBytes());

    await client.mutate(
      mutation: ActionQueries.uploadImage,
      variables: { 'file': image });

    // showSnackBar(context, 'Image updated successfully');

    await widget.provider.fetch();
    
    profile.refetch(); 
    widget.onSubmit();
  }

  Future<void> getImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(source: source);

    if (image == null) {
      return;
    }

    setState(() {
      _image = File(image.path);
    });
  }
}

class UserInfoNameSection extends StatelessWidget {
  const UserInfoNameSection({super.key});

  @override
  Widget build(BuildContext context) {
    final provider = UserProvider.of(context);

    const avatar = Padding(
      padding: EdgeInsets.only(right: Insets.lg),
      child: UserInfoAvatar());

    final name = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Display full name, if user specified it
        if (provider.user.fullname != null)
          Text(provider.user.fullname!,
            softWrap: true,
            style: context.text.headlineMedium
              ?.copyWith(color: context.colors.accent)),

        // Display username
        Text('@${provider.username}',
          style: context.text.titleMedium
            ?.copyWith(color: context.colors.primary)),

        // Make some space for rank badge
        const SizedBox(height: Insets.md),

        // Display user's rank info
        UserRankBadge.small(color: provider.user.progress.color, level: provider.user.progress.level)
      ]);

    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width - Insets.xl),

      child: Row(children: [avatar, name]));
  }
}

class UserInfoActions extends StatelessWidget {
  const UserInfoActions({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Insets.sm),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: const <Widget>[
          Expanded(flex: 2, child: FollowButton()),
          SizedBox(width: Insets.sm),
          Expanded(child: InviteButton())
        ]
      )
    );
  }
}

class UserInfoAboutButton extends StatelessWidget {
  const UserInfoAboutButton({super.key});

  @override
  Widget build(BuildContext context) {
    final provider = UserProvider.of(context);

    return Container(
      margin: const EdgeInsets.all(Insets.xs),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          IconButton(
            icon: const Icon(Icons.info),
            onPressed: () => onPressed(context, provider)),

          ReportMenu(id: provider.user.id, target: 'User')
        ],
      ));
  }

  void onPressed(BuildContext context, UserProvider provider) {
    if (provider.isSelf) {
      Beamer.of(context).beamToNamed('/profile/about', data: provider);
    } else {
      Beamer.of(context).beamToNamed('/user/${provider.entity.id}/about', data: provider);
    }
  }
}
