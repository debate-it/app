import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

class UserRankBadge extends StatelessWidget {
  final int level;
  final LinearGradient color;
  final bool small;

  const UserRankBadge({super.key, required this.level, required this.color}) 
    : small = false;

  const UserRankBadge.small({super.key, required this.level, required this.color}) 
    : small = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(Insets.xs),
      padding: EdgeInsets.symmetric(
        horizontal: small ? Insets.sm : Insets.md,
        vertical: small ? Insets.xs : Insets.sm
      ),
      decoration: BoxDecoration(
        color: context.colorScheme.onSurface,
        borderRadius: BorderRadius.circular(20)
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[ 
          Container(
            height: small ? 10 : 15,
            width: small ? 10 : 15,
            decoration: BoxDecoration(
              gradient: color,
              shape: BoxShape.circle)),

          if (small) 
            const SizedBox(width: Insets.sm)
          else 
            const SizedBox(width: Insets.lg),
          
          Text('Level $level',
            style: context.text.labelMedium
              ?.copyWith(color: context.colorScheme.surface)
          )
        ],
      )
    );
  }
}
