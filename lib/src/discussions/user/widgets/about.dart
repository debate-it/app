import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/user/provider.dart';

import 'package:debateit/src/widgets/forms/wrappers.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';


class UserAbout extends StatefulWidget {
  final UserProvider provider;

  const UserAbout({super.key, required this.provider});

  @override
  _UserAboutState createState() => _UserAboutState();
}

class _UserAboutState extends State<UserAbout> {
  late final canEdit = widget.provider.isSelf;

  @override
  void initState() {
    widget.provider.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    widget.provider.removeListener(update);
    super.dispose();
  }

  void update() {
    setState(() { });
  }

  @override
  Widget build(BuildContext context) {
    final info = widget.provider.info;

  return KeyboardVisibilityBuilder(
    builder: (context, isVisible) {
      final bottomPadding = MediaQuery.of(context).viewInsets.bottom + 5;

      return ScrollablePageView(
        title: Text('About', style: context.text.headlineMedium),
        padding: EdgeInsets.only(bottom: bottomPadding),
        body: Container(
          padding: const EdgeInsets.all(Insets.lg),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ListTile(
                dense: true,
                leading: const Icon(Icons.school, size: 18),
                title: Text('Education', style: context.text.bodyLarge),
                subtitle: Text(info.education ?? 'None', style: context.text.bodyMedium)),
                
              ListTile(
                dense: true,
                leading: const Icon(Icons.work, size: 18),
                title: Text('Occupation', style: context.text.bodyLarge),
                subtitle: Text(info.occupation ?? 'None', style: context.text.bodyMedium)),
                
              ListTile(
                dense: true,
                leading: const Icon(Icons.flag, size: 18),
                title: Text('Country', style: context.text.bodyLarge),
                subtitle: Text(info.country ?? 'None', style: context.text.bodyMedium)),
                
              ListTile(
                dense: true,
                leading: const Icon(Icons.adb, size: 18),
                title: Text('Religion', style: context.text.bodyLarge),
                subtitle: Text(info.religion ?? 'None', style: context.text.bodyMedium)),
      
              ListTile(
                dense: true,
                leading: const Icon(Icons.forum, size: 18),
                title: Text('Political Beliefs', style: context.text.bodyLarge),
                subtitle: Text(info.politics ?? 'None', style: context.text.bodyMedium)),
      
              if (canEdit)
                PrimaryBtn(
                  label: 'UPDATE',
                  icon: Icons.mode_edit,
                  onPressed: () async {
                    final color = context.get<ConfigProvider>().colors.background;
                    
                    showModalBottomSheet(
                      context: context, 
                      // backgroundColor: color,
                      isScrollControlled: true,
                      builder: (context) => SingleChildScrollView(
                        child: Container(
                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: UserAboutForm(info: info, onSubmit: _submit))));
      
                  })
              ]
            )
          )
        );
      }
    );
  }

  Future<void> _submit(Map<String, dynamic> payload) async {
    final navigator = Navigator.of(context);

    await Services.profileService.updateExtraInfo(payload);
    await showDialog(context: context, builder: _buildSubmitDialog);

    widget.provider.user = Services.profileService.info!.profile;

    // navigator.pop();
  }

  Widget _buildSubmitDialog(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        height: 150,
        alignment: Alignment.bottomCenter,
        padding: const EdgeInsets.all(Insets.md),
        decoration: BoxDecoration(
          borderRadius: Corners.medBorder,
          color: context.colorScheme.surface),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('Your profile was updated successfully', textAlign: TextAlign.center, style: context.text.bodyLarge),
            PrimaryBtn(label: 'OK', onPressed: () => Navigator.of(context).pop())
          ],
        )),
    );
  }
}

class UserAboutForm extends StatefulWidget {
  const UserAboutForm({ super.key, required this.info, required this.onSubmit});

  final UserInfo info;
  final void Function(Map<String, dynamic>) onSubmit;

  @override
  _UserAboutFormState createState() => _UserAboutFormState();
}

class _UserAboutFormState extends State<UserAboutForm> {
  late final form = FormGroup({
    'about': FormControl<String>(value: widget.info.about, validators: [Validators.maxLength(500)]),
    'country': FormControl<String>(value: widget.info.country ,validators: [Validators.maxLength(200)]),
    'education': FormControl<String>(value: widget.info.education, validators: [Validators.maxLength(200)]),
    'occupation': FormControl<String>(value: widget.info.occupation, validators: [Validators.maxLength(200)]),
    'politics': FormControl<String>(value: widget.info.politics, validators: [Validators.maxLength(200)]),
    'religion': FormControl<String>(value: widget.info.religion, validators: [Validators.maxLength(200)]),
  });

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      enableDrag: false,
      onClosing: () { },
      builder: (context) => ReactiveForm(
        formGroup: form,
        child: Padding(
          padding: const EdgeInsets.all(Insets.md),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const AppTextField(
                labelText: 'Write about yourself',
                formControlName: 'about',
                maxLines: 5, minLines: 3,
              ),

              const SizedBox(height: Insets.md),

              AppTextField(
                labelText: 'Country',
                formControlName: 'country',
                maxLength: 200,
                counterType: CounterType.charachters,
                validationMessages: {
                  ValidationMessage.maxLength: (_) => 'Please use 200 characters at most'
                },
              ),
              
              const SizedBox(height: Insets.md),
              
              AppTextField(
                labelText: 'Education',
                formControlName: 'education',
                validationMessages: {
                  ValidationMessage.maxLength: (_) => 'Please use 200 characters at most'
                },
              ),
              
              const SizedBox(height: Insets.md),
              
              AppTextField(
                labelText: 'Occupation',
                formControlName: 'occupation',
                validationMessages: {
                  ValidationMessage.maxLength: (_) => 'Please use 200 characters at most'
                },
              ),
              
              const SizedBox(height: Insets.md),
              
              AppTextField(
                labelText: 'Political Beliefs',
                formControlName: 'politics',
                validationMessages: {
                  ValidationMessage.maxLength: (_) => 'Please use 200 characters at most'
                },
              ),
              
              const SizedBox(height: Insets.md),
              
              AppTextField(
                labelText: 'Religious Beliefs',
                formControlName: 'religion',
                validationMessages: {
                  ValidationMessage.maxLength: (_) => 'Please use 200 characters at most'
                },
              ),
              
              const SizedBox(height: Insets.md),

              SubmitButton(label: 'Update', onPressed: () => widget.onSubmit(form.value))
            ],
          ),
        )
      ),
    );
  }
}
