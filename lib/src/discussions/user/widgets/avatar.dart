import 'dart:io';

import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';



class UserAvatar extends StatelessWidget {
  final String? picture;
  final String? name; 
  final double radius;
  final File? image;

  final LinearGradient? gradient;

  const UserAvatar({ 
    required this.radius,
    
    this.image,
    this.name,
    this.picture,
    this.gradient 
  })
    : assert(name != null || picture != null || image != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: radius * 2.0 + 10,
      width: radius * 2.0 + 10, 
      padding: const EdgeInsets.all(3),
      
      constraints: BoxConstraints(
        minHeight: radius * 2, maxHeight: radius * 2 + 10,
        minWidth: radius * 2, maxWidth: radius * 2 + 10),

      decoration: BoxDecoration(shape: BoxShape.circle, gradient: gradient),

      child: picture != null 
        ? _buildNormalAvatar(context)
        : _buildTextAvatar(context)
    ); 
  }

  Widget _buildNormalAvatar(BuildContext context) {
    final color = context.colorScheme.primary;

    if (image != null) {
      return CircleAvatar( 
        radius: radius, 
        backgroundColor: color,
        backgroundImage: FileImage(image!));
    }
    
    return CachedNetworkImage(
      imageUrl: picture!,
      imageBuilder: (context, provider) {
        return CircleAvatar(
          radius: radius,
          backgroundImage: provider,
          backgroundColor: color);
      },
      errorWidget: (context, url, error) => TextAvatar(gradient: gradient, name: name!, radius: radius),
      placeholder: (context, url) => Container(
        width: radius, height: radius, 
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: context.colorScheme.surface),
        child: SizedBox(
          width: radius * 0.5, height: radius * 0.5,
          child: const CircularProgressIndicator(strokeWidth: 2.0))),
    );
  }

  Widget _buildTextAvatar(BuildContext context) {
    if (image != null) {
      return _buildNormalAvatar(context);
    }

    return TextAvatar(gradient: gradient, name: name!, radius: radius);
  }
}

class TextAvatar extends StatelessWidget {
  final String name;
  final double radius;
  final LinearGradient? gradient;

  const TextAvatar({
    required this.name,
    required this.radius,

    this.gradient
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: radius * 2,
      width: radius * 2,

      decoration: BoxDecoration(shape: BoxShape.circle, gradient: gradient),
      
      child: Center(
        child: Text(name[0], 
          style: context.text.bodyLarge
            ?.copyWith(fontSize: radius * 0.8))));
  }
}
