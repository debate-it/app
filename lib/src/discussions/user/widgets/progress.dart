import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/user/provider.dart';


class UserProgressIndicator extends StatelessWidget {
  const UserProgressIndicator({ super.key });

  static const INDICATOR_HEIGHT = 5.0;

  @override
  Widget build(BuildContext context) {
    final progress = UserProvider.of(context, listen: true).entity.progress;

    return Padding(
      // padding: const EdgeInsets.all(Insets.sm),
      padding: EdgeInsets.zero,
      child: Row(
        children: [
          Expanded(
            child: LayoutBuilder(
              builder: (context, constraints) {
                return Stack(
                  children: [
                    DecoratedBox(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Corners.lgRadius),
                        color: Color(0x503a3a3a)),

                      child: SizedBox(height: INDICATOR_HEIGHT, width: constraints.maxWidth)),
                    
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Corners.lgRadius),
                          gradient: progress.color),

                        child: SizedBox(
                          height: INDICATOR_HEIGHT, 
                          width: constraints.maxWidth  * (progress.earned / progress.needed)))
                      ]
                    );
              }
            )
          ),

          const SizedBox(width: Insets.sm),

          Text('${progress.earned} / ${progress.needed}',
            style: context.text.labelMedium)
        ]
      )
    );
  }
}
