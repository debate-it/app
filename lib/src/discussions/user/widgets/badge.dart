import 'package:beamer/beamer.dart';
import 'package:debateit/src/discussions/user/widgets/avatar.dart';
import 'package:debateit/src/widgets/misc/labeled_icon.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/user/UserProfile.dart';



class UserBadge extends StatelessWidget {
  final UserProfile profile;
  final bool vertical;
  final bool transparent;
  final Color? color;
  final int? score;

  final bool reversed;

  final double radius; 
  final double fontSize;
  final double subtitleFontSize;

  final void Function(UserProfile)? onTap;

  
  const UserBadge({ 
    required this.profile, 
    this.color,
    this.onTap,
    this.score,
    this.radius = 25.0, 
    this.reversed = false,
    this.fontSize = 16.0,
    this.subtitleFontSize = 12.0,
    this.vertical = false, 
    this.transparent = false
  });

  @override
  Widget build(BuildContext context) { 
    final avatar = Container(
      padding: const EdgeInsets.all(Insets.sm),
      child: UserAvatar(
        radius: radius,
        picture: profile.info.picture,
        gradient: profile.progress.color,
        name: profile.fullname ?? profile.username));

    final avatarContainer = score == null
      ? avatar
      : Stack(
        alignment: Alignment.bottomRight,
        children: <Widget>[
          avatar,
          Container(
            margin: const EdgeInsets.only(bottom: Insets.xs),
            decoration: BoxDecoration(
              color: context.colorScheme.background,
              borderRadius: BorderRadius.circular(15)),
            width: 50,
            child: LabeledIcon(
              icon: Icons.keyboard_arrow_up,
              label: Text('$score', 
                style: context.text.labelSmall
                  ?.copyWith(color: context.colorScheme.onSurface))))
        ],
      );

    final title = Column( 
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: vertical
        ? CrossAxisAlignment.center
        : reversed
          ? CrossAxisAlignment.end
          : CrossAxisAlignment.start,
      children: [
        if (profile.fullname != null)
          Text(profile.fullname!, 
            maxLines: 3, 
            style: context.text.labelLarge
              ?.copyWith(color: context.colors.accent)),

        Text('@${profile.username}', 
          style: context.text.labelMedium
            ?.copyWith(color: color ?? context.colorScheme.secondary))
      ]
    );

    final padding = transparent
      ? EdgeInsets.zero
      : const EdgeInsets.symmetric(horizontal: Insets.md, vertical: Insets.xs);

    final decoration = BoxDecoration(
      borderRadius: BorderRadius.circular(15.0),
      color: transparent
        ? Colors.transparent
        : context.colorScheme.surface); 

    final children = <Widget>[ avatarContainer, title ];

    return Container(
      padding: padding,
      decoration: decoration,
      child: InkWell(
        onTap: onTap != null 
          ? () => onTap!(profile) 
          : () => _openUserProfile(context),
        child: Wrap(
          direction: vertical ? Axis.vertical : Axis.horizontal,
          alignment: reversed ? WrapAlignment.start : WrapAlignment.end,

          crossAxisAlignment: WrapCrossAlignment.center,
          
          children: reversed ? children.reversed.toList() : children)));
  }

  void _openUserProfile(BuildContext context) {
    Beamer.of(context).beamToNamed('/user/${profile.id}');
  }
}
