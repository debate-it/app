import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/widgets/navigation/utils/link.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

class UserStatsWidget extends StatelessWidget {

  const UserStatsWidget();

  @override
  Widget build(BuildContext context) { 
    final profile = UserProvider.of(context, listen: true);
    final stats = profile.stats;

    return FittedBox(
      fit: BoxFit.scaleDown,
      child: ConstrainedBox(
        constraints: BoxConstraints.loose(const Size.fromHeight(60)),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              UserStatEntry(title: 'Points', value: stats.points.toString()),
              UserStatEntry(title: 'Wins', value: stats.debates.toString()),

              LinkWidget(
                path: '/user/${profile.user.id}/followers',
                child: UserStatEntry(title: 'Followers', value: stats.followers.toString())),

              UserStatEntry(title: 'Best At', value: stats.category ?? 'None'),
            ]
          )));
  }
}

class UserStatEntry extends StatelessWidget {

  final String title;
  final String value;

  const UserStatEntry({
    required this.title,
    required this.value,
  }); 

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Insets.sm),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[ 
          Text(title, style: context.text.bodyMedium?.copyWith(color: context.colors.secondary)),
          Text(value, style: context.text.bodySmall)
        ]
    )); 
  }
}
