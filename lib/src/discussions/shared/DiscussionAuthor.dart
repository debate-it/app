import 'package:debateit/src/discussions/user/widgets/badge.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/user/UserProfile.dart';


class DiscussionAuthor extends StatelessWidget {
  final UserProfile sender;
  final Widget? label;
  
  const DiscussionAuthor({ super.key, required this.sender, this.label });

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    final avatar = ConstrainedBox( 
      constraints: BoxConstraints(maxWidth: width * 0.6),
      child: FittedBox(
        child: UserBadge(profile: sender, radius: 20, transparent: true)));

    return Padding(
      padding: const EdgeInsets.only(left: Insets.xs, right: Insets.lg),
      child: Row(
        children: <Widget>[ avatar, if (label != null) ...[ const Spacer(), label! ] ]));
  }
}
