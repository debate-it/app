import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/widgets/navigation/utils/link.dart';
import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

typedef TransitionBackHandler<T> = void Function(T?);

class DiscussionPreview<T> extends StatelessWidget {
  final Discussion discussion;

  final Widget child;
  final Object? payload;

  final EntityProvider<T> provider;

  final TransitionBackHandler<T>? onTransitionBack;

  const DiscussionPreview({
    super.key,

    required this.discussion,
    required this.provider,
    required this.child, 

    this.payload,
    this.onTransitionBack,
  });
  
  static const _padding = EdgeInsets.all(Insets.xs);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: LinkWidget(
        payload: provider,
        path: '/discussion/${discussion.id}',
        onTransition: _saveAnalytics,
        child: Padding(padding: _padding, child: child)));
  }

  Future<void> _saveAnalytics() async {
    await FirebaseAnalytics.instance.logEvent(
      name: 'view_discussion',
      parameters: {
        'type': discussion.runtimeType.toString(),
        'id': discussion.id,
        'size': discussion.size.label,
        'category': discussion.category.name,
      }
    );
  }
}
