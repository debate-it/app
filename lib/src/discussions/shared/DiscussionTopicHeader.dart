import 'package:flutter/material.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/shared/DiscussionCategoryInfo.dart';


class DiscussionTopicHeader extends StatelessWidget {
  final String label;
  final Discussion discussion;
  
  const DiscussionTopicHeader({super.key, required this.discussion, this.label = 'TOPIC' });

  @override
  Widget build(BuildContext context) {

    final discussionSizeInfo = 
      DiscussionCategoryInfo(
        category: discussion.category,
        size: discussion.size,
        horizontal: true);

    return Column(children: [discussionSizeInfo]);
  }
}
