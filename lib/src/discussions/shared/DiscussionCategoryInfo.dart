import 'package:debateit/src/widgets/navigation/utils/link.dart';
import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:flutter/material.dart';



class DiscussionCategoryInfo extends StatelessWidget {
  final DiscussionCategory category;
  final DiscussionSize? size;

  final bool horizontal;

  const DiscussionCategoryInfo({ super.key, required this.category, this.size, this.horizontal = false });
    
  String get _size => size.toString().replaceAll('DiscussionSize.', '').toUpperCase();

  @override
  Widget build(BuildContext context) {
    if (horizontal) {
      return _buildHorizontal(context);
    }

    final categoryLabel = Text(category.name,
      style: context.text.titleLarge?.copyWith(
        color: context.colorScheme.primary,
        fontSize: 14.0 ));

    final sizeLabel = size != null 
      ? Text(_size, style: context.text.bodyMedium)
      : const SizedBox();

    return LinkWidget(
      path: '/discussion/category/${category.id}',
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[categoryLabel, sizeLabel]));
  }


  Widget _buildHorizontal(BuildContext context) { 

    final debateTypeInfo = <Widget>[ 
      Text(category.name, 
        style: context.text.bodyLarge?.copyWith(
          color: context.colorScheme.primary,
          fontSize: size != null ? 16.0 : 18.0))    ,

      if (size != null)
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: Insets.xs),
          child: Icon(Icons.lens, size: 5, 
            color: context.colorScheme.onSurface)), 

      if (size != null)
        Text(_size, style: context.text.bodyMedium),
    ];
    
    return LinkWidget(
      path: '/discussion/category/${category.id}',
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        textBaseline: TextBaseline.ideographic,
        crossAxisAlignment: CrossAxisAlignment.baseline,
        children: debateTypeInfo));
    
  }
}
