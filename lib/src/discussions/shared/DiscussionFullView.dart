import 'package:flutter/material.dart';

import 'package:debateit_core/widgets/brand/loader.dart';

import 'package:debateit/src/discussions/provider.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';


class DiscussionFullView<T> extends StatelessWidget {
  const DiscussionFullView({super.key, required this.provider, required this.child});

  final EntityProvider<T> provider;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop();
        return true;
      },
      child: BarlessView(
        body: FutureBuilder<void>(
          future: provider.fetch(),
          builder: (context, AsyncSnapshot<void> snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return const Center(child: Loader());
            }

            if (snapshot.hasError) {
              return const Center(child: Text('Discussion not found'));
            }

            return child;
          }
        )
      ), 
    );
  }
}
