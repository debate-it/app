import 'package:debateit/src/discussions/shared/DiscussionCategoryInfo.dart';
import 'package:debateit/src/discussions/shared/DiscussionTags.dart';
import 'package:debateit/src/discussions/shared/DiscussionTopic.dart';
import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:flutter/material.dart';


class DiscussionBody extends StatelessWidget {
  final bool wrapped;
  final bool showCategory;
  final Discussion discussion;

  final List<Widget> children;

  const DiscussionBody({ 
    super.key, 
    required this.discussion, 
    this.wrapped = false,
    this.showCategory = true,
    this.children = const [],
  });

  @override
  Widget build(BuildContext context) {
    final content = Padding(
      padding: const EdgeInsets.symmetric(horizontal: Insets.lg, vertical: Insets.md),
      child: Column(
        children: [
          if (showCategory)
            DiscussionCategoryInfo(category: discussion.category, size: discussion.size),
            
          DiscussionTopic(discussion: discussion, showHeader: false),
          const SizedBox(height: Insets.md),

          ...children,

          DiscussionTags(tags: discussion.tags)
        ]
      )
    );

    if (wrapped) {
      return Card(child: content);
    }

    return content;
  }
}
