import 'package:debateit/src/discussions/shared/DiscussionTag.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';


class DiscussionTags extends StatelessWidget {
  final List<Tag> tags;

  const DiscussionTags({ super.key, required this.tags });

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.spaceAround, 
      runAlignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: Insets.xs, runSpacing: Insets.xs,
      children: tags.map((tag) => DiscussionTag(tag: tag)).toList());
  }
}
