import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';


class DiscussionWinner extends StatelessWidget {
  final bool reversed;
  const DiscussionWinner({ super.key, this.reversed = false });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,

      left: reversed ? Insets.xs : null,
      right: !reversed ? Insets.xs : null,
      
      child: Text('Winner',
        style: context.text.labelSmall
          ?.copyWith(color: context.colors.accent, fontStyle: FontStyle.italic)));
  }
}
