import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';


class DiscussionTag extends StatelessWidget {
  final Tag tag;
  final VoidCallback? onTap;
  final VoidCallback? onClose;

  const DiscussionTag({ super.key, required this.tag, this.onTap, this.onClose });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Insets.xs),
      decoration: BoxDecoration(borderRadius: Corners.smBorder, color: context.colors.accent),
      child: InkWell(
        onTap: () => _showTagSeach(context),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('#${tag.name}',
              style: context.text.bodyMedium?.copyWith(color: context.colors.surface)),

            if (onClose != null)
              ConstrainedBox(
                constraints: const BoxConstraints(maxHeight: 20, maxWidth: 20),
                child: IconButton(
                  onPressed: onClose, 
                  padding: EdgeInsets.zero,
                  color: context.colorScheme.primary,
                  alignment: Alignment.centerRight,
                  icon: const Icon(Icons.close, size: 14)))
              
          ],
        )));
  }
  
  void _showTagSeach(BuildContext context) {
    if (onTap != null) {
      return onTap?.call();
    }

    // showSearch(
    //   context: context, 
    //   query: tag.name,
    //   delegate: SearchBarDelegate(showResultsImmediately: true, context: context));
  }
}
