import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/shared/DiscussionTopicHeader.dart';


class DiscussionTopic extends StatelessWidget { 
  final String label;
  final Discussion discussion;

  final bool showHeader;

  const DiscussionTopic({ 
    super.key,
    required this.discussion,
    this.label = 'TOPIC',
    this.showHeader = true
  });

  @override
  Widget build(BuildContext context) { 
    final topicHeader = DiscussionTopicHeader(discussion: discussion, label: label);
    
    final topicBody = Text(discussion.topic,
      textAlign: TextAlign.start,
      textWidthBasis: TextWidthBasis.parent,
      softWrap: true,
      style: context.text.bodyLarge);

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [ if (showHeader) topicHeader, topicBody ]);
  }
}
