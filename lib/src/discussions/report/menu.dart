import 'package:debateit/src/widgets/forms/wrappers.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/config/graphql/report.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/dialogs/info.dart';


enum _MenuAction { report }

class ReportMenu extends StatelessWidget {
  final String target;
  final String id;

  const ReportMenu({ super.key, required this.target, required this.id });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      padding: EdgeInsets.zero,
      onSelected: (_MenuAction value) => _onSelect(context, value),
      itemBuilder: (_) => [
        const PopupMenuItem(value: _MenuAction.report, child: Text('Report'))
      ],
      child: const Padding(
        padding: EdgeInsets.only(left: Insets.xs),
        child: Icon(Icons.more_vert, size: 25)));
  }

  void _onSelect(BuildContext context, _MenuAction value) {
    switch (value) {
      case _MenuAction.report:
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          isDismissible: true,
          useRootNavigator: true,
          builder: (context) => _ReportDescriptionForm(id: id, target: target));
        break;

      default:
        throw Exception();
    }
  }
}

enum ReportReason {
  hateSpeech,
  ruleViolation,
  spam,
  other
}

class _ReportDescriptionForm extends StatefulWidget {
  final String id;
  final String target;

  const _ReportDescriptionForm({ required this.target, required this.id });

  @override
  __ReportDescriptionFormState createState() => __ReportDescriptionFormState();
}

class __ReportDescriptionFormState extends State<_ReportDescriptionForm> {
  final form = FormGroup({
    'reason': FormControl<ReportReason>(validators: [Validators.required]),
    'description': FormControl<String>(validators: [Validators.required, Validators.maxLength(1000)])
  });

  String? _error;

  @override
  void initState() {
    _error = null;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (_) => ReactiveForm(
        formGroup: form,
        child: Container(
          padding: const EdgeInsets.all(Insets.md)
            .add(EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.all(Insets.md),
                child: Text('Describe your report', style: context.text.titleLarge )),

              AppDropdownField(
                labelText: 'Specify the report reason',
                formControlName: 'reason',
                items: const [
                  DropdownMenuItem(value: ReportReason.hateSpeech, child: Text('Hate Speech')),
                  DropdownMenuItem(value: ReportReason.ruleViolation, child: Text('Rule Violation')),
                  DropdownMenuItem(value: ReportReason.spam, child: Text('Spam')),
                  DropdownMenuItem(value: ReportReason.other, child: Text('Other')),
                ],
                validationMessages: {
                  ValidationMessage.required:(error) => 'Please select report reason'
                },
              ),

              AppTextField(
                hintText: 'Describe the reason of your report',
                labelText: 'Describe the reason of your report',
                formControlName: 'description',
                validationMessages: {
                  ValidationMessage.required: (error) => 'Please describe your report',
                  ValidationMessage.maxLength: (error) => 'Your report can be 1000 character at most'
                },
              ),


              if (_error != null)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: Insets.sm),
                  child: Text(_error!,
                    style: context.text.bodyMedium
                      ?.copyWith(color: context.colorScheme.error))),

              PrimaryBtn(
                label: 'Submit',
                onPressed: () => submitReport(context))
            ],
          ))
      )
    );
  }

  Future<void> submitReport(BuildContext context) async {
    if (!form.valid) {
      return;
    }

    FocusScope.of(context).unfocus();

    final client = Services.client;
    final navigator = Navigator.of(context);

    try {
      await client.mutate(
        mutation: ReportQueries.Report, 
        variables: {
          'id': widget.id,
          'reason': (form.control('reason').value as ReportReason).name,
          'description': form.control('description').value as String
        });

      await showDialog(context: context, builder: (_) => 
        const InfoDialog(message: 'Your report was submitted'));

      navigator.pop();
    } on Exception catch (err) {
      setState(() {
        _error = err.toString();
      });
    }
  }
}
