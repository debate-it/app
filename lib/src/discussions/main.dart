import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/debate/provider.dart';
import 'package:debateit/src/discussions/panel/provider.dart';
import 'package:debateit/src/discussions/post/provider.dart';
import 'package:debateit/src/discussions/provider.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:debateit_graphql/queries/discussion.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/challenge/views/full.dart';
import 'package:debateit/src/discussions/debate/views/full.dart';
import 'package:debateit/src/discussions/panel/full.dart';
import 'package:debateit/src/discussions/post/views/full.dart';

import 'package:debateit/src/widgets/pages/FixedPage.dart';


class DiscussionPage<T extends Discussion, TProvider extends EntityProvider<T>> extends StatelessWidget {
  const DiscussionPage({super.key, required this.id, this.provider});

  final String id;
  final TProvider? provider;

  @override
  Widget build(BuildContext context) {
    return BarlessView(
      body: FutureBuilder(
        future: _fetch(),
        builder: (context, AsyncSnapshot<Discussion> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(child: Loader());
          }

          if (snapshot.hasError) {
            return const Center(child: Text('Discussion not found'));
          }

          final discussion = snapshot.data!;

          if (discussion is Debate) {
            return DebateFullView(debate: discussion, provider: provider as DebateProvider?);
          }

          if (discussion is Panel) {
            return PanelFullView(panel: discussion, provider: provider as PanelProvider?);
          }
          
          if (discussion is Post) {
            return PostFullView(post: discussion, provider: provider as PostProvider?);
          }
          
          if (discussion is Challenge) {
            return ChallengeFullView(challenge: discussion, provider: provider as ChallengeProvider?);
          }

          return const Center(child: Text('This discussion is not supported by this version of the app'));
        }
      )
    );
  }

  Future<Discussion> _fetch() async {
    final result = await Services.client.fetch(
      query: DiscussionQueries.Fetch, 
      variables: { 'id': id }
    );

    if (result.hasException) {
      throw result.exception!;
    }

    return Discussion.parse(result.data!['discussion'] as Map<String, dynamic>);
  }

}
