import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/challenge/widgets/choice_dialog.dart';

import 'package:debateit/src/widgets/buttons/buttons.dart';


class ChallengeSubmitButton extends StatelessWidget {
  const ChallengeSubmitButton({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = ChallengeProvider.of(context, listen: true);
    
    final user = Services.profileService.info!.profile; 
    final isCreator = user.username == provider.challenge.author.username;

    if (!isCreator) {
      return const SizedBox();
    }

    return Container(
      padding: const EdgeInsets.symmetric(vertical: Insets.xs, horizontal: Insets.sm),
      child: buildCreatorInterface(context));
  }

  Widget buildCreatorInterface(BuildContext context) {
    final provider = ChallengeProvider.of(context); 
    final challenge = provider.challenge;

    final expired = challenge.deadline.isBefore(DateTime.now()); 
    final finished = challenge.status == ChallengeStatus.finished;

    if (expired && !finished) {
      return buildOpponentChoiceButton(context);
    } else if (finished) {
      return buildFinishMessage(context);
    } else  {
      return const SizedBox();
    }
  }

  Widget buildOpponentChoiceButton(BuildContext context) {
    final provider = ChallengeProvider.of(context);

    dynamic onPressed() {
      showModalBottomSheet(
        context: context,
        builder: (_) => ChallengeOpponentChoiceDialog(provider: provider));
    }

    return SecondaryBtn(
      label: 'Choose Challenge Opponent',
      onPressed: onPressed);
  }

  Widget buildFinishMessage(BuildContext context) { 
    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(Insets.md),
        child: Text('Challenge has been Finished',
          textAlign: TextAlign.center,
          style: context.text.headlineMedium)));
  }

}
