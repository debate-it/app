import 'package:flutter/material.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/widgets/misc/labeled_text.dart';


class ChallengeOpinion extends StatelessWidget {
  const ChallengeOpinion({ super.key });

  @override
  Widget build(BuildContext context) { 
    final challenge = ChallengeProvider.of(context).challenge;

    final position = LabeledText(topic: challenge.position, label: 'Initial Position');
    const divider = Divider(indent: 15, endIndent: 15);

    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [ divider, position, divider ]));
  }
}
