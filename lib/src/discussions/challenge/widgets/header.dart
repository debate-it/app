import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/report/menu.dart';
import 'package:debateit/src/discussions/challenge/provider.dart';

import 'package:debateit/src/widgets/misc/countdown_label.dart';
import 'package:debateit/src/widgets/misc/creation_label.dart';


class ChallengeHeader extends StatelessWidget {
  final bool standalone;

  const ChallengeHeader({ super.key, this.standalone = false }); 
  
  static const _cardPadding = EdgeInsets.all(10);

  @override
  Widget build(BuildContext context) {
    final provider = ChallengeProvider.of(context); 
    final challenge = provider.challenge;
        
    final deadline = CountdownLabel(
      timeoutLabel: 'FINISHED',
      date: challenge.deadline,
      onTimeout: () => provider.reshresh(),
      builder: (context) {
        if (standalone) {
          return Text(challenge.status.label, softWrap: true);
        }

        return ConstrainedBox(
          constraints: BoxConstraints.loose(Size.fromWidth(MediaQuery.of(context).size.width / 4)),
          child: Text(challenge.status.label, softWrap: true,
            style: context.text.bodyMedium));
      } ,
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0);
      
    final createionDate = CreationLabel(
      date:challenge.created,
      iconSize: standalone ? 18.0 : 15.0,
      fontSize: standalone ? 16.0 : 12.0);

    if (standalone) {
      return Padding(
        padding: const EdgeInsets.all(Insets.sm),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Card(
                margin: EdgeInsets.zero,
                child: Center(
                  child: Padding(padding: _cardPadding, child: deadline)))),

            const SizedBox(width: Insets.sm),

            Expanded(
              child: Card(
                margin: EdgeInsets.zero,
                child: Center(
                  child: Padding(padding: _cardPadding, child: createionDate)))) 
          ]
        ),
      );
    }
    
    final optionsMenu = ReportMenu(id: challenge.id, target: 'Challenge');

    return Stack(
      alignment: Alignment.center,
      children: [
        Row(children: <Widget>[deadline, const Spacer(), createionDate, optionsMenu]),
        Text('Challenge', style: context.text.titleLarge ?.copyWith(color: context.colorScheme.primary))
      ],
    );
  }
}
