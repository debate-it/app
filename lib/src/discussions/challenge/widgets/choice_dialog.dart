import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/comments/section/main.dart';

import 'package:debateit/src/widgets/dialogs/info.dart';


class ChallengeOpponentChoiceDialog extends StatelessWidget {
  final ChallengeProvider provider;

  const ChallengeOpponentChoiceDialog({ super.key, required this.provider});

  @override
  Widget build(BuildContext context) { 
    final header = Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          Expanded(
            child: Text('Choose Your Opponent',
              textAlign: TextAlign.center,
              style: context.text.headlineMedium)),

          IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.close, color: context.colors.text))
        ]
      )
    );

    final responses = CommentSectionList(
      useSlivers: false,
      target: provider.challenge.id,
      type: CommentType.Challenge,
      order: CommentOrder.TOP,
      onTap: (response) => displayConfirmationDialog(context, response));

    return BottomSheet(
      elevation: 18.0,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      onClosing: () {  },
      // insetPadding: const EdgeInsets.symmetric(vertical: Insets.xl, horizontal: Insets.lg),
      builder: (context) => CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.all(Insets.md),
            sliver: ChangeNotifierProvider(
              create: (context) => CommentSectionProvider(
                type: CommentType.Challenge,
                target: provider.challenge.id,
                order: CommentOrder.TOP,
                onTap: (comment) => chooseOpponent(context, comment)),

              child: SliverToBoxAdapter(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [ header, responses ]))))
        ]
      )
    );
  }

  Future<void> displayConfirmationDialog(BuildContext context, Comment response) async { 
    final navigator = Navigator.of(context);

    final text = Text('Choose ${response.author.username} as opponent', 
      textAlign: TextAlign.center,
      style: context.text.headlineMedium);

    final result = await showDialog<bool>(
      context: context,
      builder: (context) => SimpleDialog(
        elevation: 18.0,
        title: text,
        contentPadding: const EdgeInsets.all(Insets.md),
        children: [ buildButtons(context, response) ]));

    if (result != null && result) {
      navigator.pop();
    }
  }

  Widget buildButtons(BuildContext context, Comment response) {
    final style = context.text.titleLarge;

    return Container(
      padding: const EdgeInsets.symmetric(vertical: Insets.xl, horizontal: Insets.lg),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () => chooseOpponent(context, response),
            child: Text('YES', style: style)),
            
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('NO', style: style))
        ],
      )
    );
  }

  Future<void> chooseOpponent(BuildContext context, Comment response) async { 
    final profile = Services.profileService;
    final navigator = Navigator.of(context);

    final result = await provider.chooseOpponent(context, response);

    if (result != null) {
      profile.updateIndicators();
    } 

    await showDialog(
      context: context,
      builder: (_) => InfoDialog(message: result != null ? 'Opponent has been chosen' : 'Error occured')
    );

    
    navigator.pop(result != null);
  }
}
