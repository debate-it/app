import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';

import 'package:debateit/src/discussions/challenge/buttons/submit.dart';

import 'package:debateit/src/discussions/challenge/widgets/creator.dart';
import 'package:debateit/src/discussions/challenge/widgets/footer.dart';
import 'package:debateit/src/discussions/challenge/widgets/header.dart';
import 'package:debateit/src/discussions/challenge/widgets/opinion.dart';

import 'package:debateit/src/discussions/shared/DiscussionBody.dart';
import 'package:debateit/src/discussions/shared/DiscussionPreview.dart';


class ChallengeBody extends StatelessWidget {
  final bool preview;

  const ChallengeBody({ super.key, this.preview = false });

  @override
  Widget build(BuildContext context) {
    final provider = ChallengeProvider.of(context);

    final content = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ChallengeHeader(standalone: !preview),
        ChallengeCreator(standalone: !preview),
        DiscussionBody(discussion: provider.entity, wrapped: !preview, showCategory: false),
        
        if (preview) ...[
          const Divider(),
          const ChallengeFooter()
        ] else ...[
          const ChallengeOpinion(),
          const ChallengeSubmitButton()
        ]
      ],
    );
    
    if (preview) {
      return DiscussionPreview<Challenge>(
        provider: provider,
        discussion: provider.entity,
        onTransitionBack: provider.update,
        child: content
      );
    }

    return content;
  }
}
