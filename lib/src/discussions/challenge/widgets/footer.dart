import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';


class ChallengeFooter extends StatelessWidget {
  const ChallengeFooter({ super.key });

  @override
  Widget build(BuildContext context) {
    final provider = ChallengeProvider.of(context, listen: true);
    final responses = provider.challenge.responses;

    return Container(
      alignment: Alignment.center,
      child: Text('Responses: $responses ',
        style: context.text.titleMedium));
  }
}
