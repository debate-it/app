import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/shared/DiscussionAuthor.dart';
import 'package:debateit/src/discussions/shared/DiscussionCategoryInfo.dart';
import 'package:flutter/material.dart'; 


class ChallengeCreator extends StatelessWidget {
  final bool standalone;

  const ChallengeCreator({ super.key, this.standalone = false });

  @override
  Widget build(BuildContext context) {
    final challenge = ChallengeProvider.of(context).challenge;
    final content = DiscussionAuthor(
      sender: challenge.author,
      label: DiscussionCategoryInfo(
        category: challenge.category,
        size: challenge.size));

    return standalone ? Card(child: content) : content;
  }
}
