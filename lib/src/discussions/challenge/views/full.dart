import 'package:debateit/src/discussions/shared/DiscussionFullView.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/challenge/widgets/body.dart';
import 'package:debateit/src/discussions/comments/section/main.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


class ChallengeFullView extends StatelessWidget {
  final Challenge challenge;
  final ChallengeProvider provider;

  ChallengeFullView({super.key, required this.challenge, ChallengeProvider? provider})
    : provider = provider ?? ChallengeProvider(challenge: challenge); 

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      key: ValueKey('Challenge ${provider.challenge.id} full-view'),
      value: provider,
      child: RefreshIndicator(
        onRefresh: () => provider.reshresh(),
        child: DiscussionFullView(
          provider: provider, 
          child: const _ChallengeView())));
  }
}

class _ChallengeView extends StatelessWidget {
  const _ChallengeView();

  @override
  Widget build(BuildContext context) {
    final provider = ChallengeProvider.of(context, listen: true);
    final current = Services.profileService.info!.profile; 

    return CustomScrollView(
      slivers:  [
        FullViewAppBar(
          category: provider.challenge.category,
          size: provider.challenge.size,
          target: provider.challenge,
          type: 'Challenge'),

        const SliverToBoxAdapter(child: ChallengeBody()),
        
        CommentSection(
          useSlivers: true,
          type: CommentType.Challenge,
          target: provider.challenge.id,
          wordLimit: provider.challenge.size.argumentLength,

          canVote: provider.challenge.status == ChallengeStatus.active,
          canComment: current.id != provider.challenge.author.id
            && provider.challenge.deadline.isAfter(DateTime.now())),

        const SliverPadding(padding: EdgeInsets.all(5))
      ]
    );
  }

}
