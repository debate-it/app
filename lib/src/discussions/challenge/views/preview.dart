import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit/src/discussions/challenge/provider.dart';
import 'package:debateit/src/discussions/challenge/widgets/body.dart';


class ChallengePreview extends StatelessWidget {
  final Challenge challenge;

  const ChallengePreview({ super.key, required this.challenge });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ChallengeProvider(challenge: challenge), 
      child: const ChallengeBody(preview: true)
    );
  }
}
