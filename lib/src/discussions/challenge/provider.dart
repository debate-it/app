import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_graphql/queries/challenge.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/discussions/provider.dart';


class ChallengeProvider extends EntityProvider<Challenge> {
  ChallengeProvider({ required Challenge challenge })
    : super(entity: challenge);

  static ChallengeProvider of(BuildContext context, { bool listen = false}) =>
    Provider.of<ChallengeProvider>(context, listen: listen);

  Challenge get challenge => entity;

  bool get isActive => entity.deadline.isAfter(DateTime.now());

  static Future<Challenge?> initiate(Map<String, dynamic> data) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: ChallengeQueries.CreateChallenge,
      variables: data);
      
    return Challenge.fromJson(result);
  }

  Future<void> reshresh() async {
    final client = Services.client;

    final result = await client.fetch(
      query: ChallengeQueries.FindChallenge,
      variables: { 'id': challenge.id }
    );

    if (result.data != null) {
      entity = Challenge.fromJson(result.data!['challenge'] as Map<String, dynamic>);
      notifyListeners();
    }
  }

  Future<Comment?> userResponse(BuildContext context) async {
    final client = Services.client;

    final result = await client.fetch(
      query: ChallengeQueries.FindUserResponse,
      variables: { 'challenge': challenge.id });

    final data = result.data!['challengeUserResponse'];

    if (data != null) {
      return Comment.fromJson(data as Map<String, dynamic>);
    } else {
      return null;
    }
  }

  Future<Comment?> respond(BuildContext context, String response) async {
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: ChallengeQueries.AddChallengeResponse,
      variables: { 'challenge': challenge.id, 'argument': response }
    );

    challenge.responses++;
    notifyListeners();

    return Comment.fromJson(result);
  }

  Future<Comment?> vote(BuildContext context, String id) async { 
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: ChallengeQueries.VoteOnResponse,
      variables: { 'response': id }
    );

    return Comment.fromJson(result);
  }

  Future<Debate?> chooseOpponent(BuildContext context, Comment response) async { 
    final client = Services.client;

    final result = await client.mutate<Map<String, dynamic>>(
      mutation: ChallengeQueries.ChooseChallengeOpponent,
      variables: {
        'challenge': challenge.id,
        'response': response.id
      }
    );

    challenge.status = ChallengeStatus.finished;
    notifyListeners();
    
    return Debate.fromJson(result);
  }

  @override
  Future<void> fetch() async {}
}
