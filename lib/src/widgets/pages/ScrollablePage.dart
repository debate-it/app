import 'package:debateit/src/widgets/pages/PageAppBar.dart';
import 'package:debateit/src/widgets/pages/PageContainer.dart';
import 'package:flutter/material.dart';

abstract class ScrollablePageContainer extends PageContainer { 
  final EdgeInsets? padding;
  final bool centerBody;

  const ScrollablePageContainer({
    super.key, 
    required super.body,
    super.appBar,
    this.padding = EdgeInsets.zero,
    this.centerBody = false
  });


  @override
  Widget build(BuildContext context) {
    final alignment = centerBody
      ? Alignment.center
      : Alignment.topCenter;

    return SafeArea(
      child: Scaffold(
        appBar: appBar,
        resizeToAvoidBottomInset: true,
        
        body: Container(
          alignment: alignment,
          child: SingleChildScrollView(
            primary: true,
            padding: padding,
            child: body))));
  }
 
}

class ScrollablePageView extends ScrollablePageContainer {
  ScrollablePageView({
    super.key, 
    super.padding,
    super.centerBody,
    
    required super.body,

    Widget? title,
    Widget? leading,
    PreferredSizeWidget? bottom, 
  })
    : super(appBar: ViewAppBar(bottom: bottom, title: title, leading: leading));
}

class ScrollableMainView extends ScrollablePageContainer {
  ScrollableMainView({
    super.key, 
    super.padding,
    PreferredSizeWidget? bottom, 

    required super.body,

    super.centerBody
  })
    : super(
        appBar: MainAppBar(bottom: bottom)
      );
}

class BarlessScrollableView extends ScrollablePageContainer {
  const BarlessScrollableView({ 
    super.key, 
    super.padding,

    required super.body,

    super.centerBody
  });
}
