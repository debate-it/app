import 'package:flutter/material.dart';

abstract class PageContainer extends StatelessWidget {
  final Widget body;
  final PreferredSizeWidget? appBar;
  
  const PageContainer({ 
    super.key,
    this.appBar,
    required this.body
  });
}
