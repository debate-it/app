import 'package:debateit/src/widgets/pages/EntityProvider.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:debateit_graphql/graphql.dart';
import 'package:flutter/material.dart';

class EntityPage<T, P extends EntityProvider<T>> extends StatelessWidget {
  final String id;

  final T? entity;
  final P? provider;

  final Widget Function(T, P?) builder;
  final T Function(Map<String, dynamic>) parser;

  final String query;
  final String queryPath;

  const EntityPage({
    super.key, 
    this.entity, 
    this.provider,

    required this.id, 
    required this.builder, 
    required this.parser,
    required this.query, 
    required this.queryPath,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fetch(context),
      builder: (context, AsyncSnapshot<T?> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        if (!snapshot.hasData) {
          return FixedViewPage(
            body: const Center(
              child: Text('Error occured')
            )
          );
        }
        
        return BarlessView(body: builder(snapshot.data as T, provider));
      });
  }

  Future<T?> _fetch(BuildContext context) async  { 
    if (provider != null) {
      await provider!.fetch();
      return provider!.entity;
    }

    final result = await BackendClient
      .of(context)
      .fetch(query: query, variables: { 'id': id });

    if (result.data![queryPath] == null) {
      return null;
    }

    return parser(result.data![queryPath] as Map<String, dynamic>);
  }
}
