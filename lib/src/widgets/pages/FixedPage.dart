import 'package:debateit/src/widgets/pages/PageAppBar.dart';
import 'package:debateit/src/widgets/pages/PageContainer.dart';
import 'package:flutter/material.dart';


abstract class FixedPageContainer extends PageContainer {
  final EdgeInsets? padding;
  final Widget? drawer;
  final Widget? bottomSheet;

  const FixedPageContainer({
    super.key, 
    super.appBar,
    
    required super.body,

    this.drawer,
    this.bottomSheet, 

    this.padding = EdgeInsets.zero
  });


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        bottomSheet: bottomSheet,
        appBar: appBar,
        drawer: drawer,
        body: Padding(
          padding: padding ?? EdgeInsets.zero,
          child: body)));

    // return SafeArea(
    //   child: Scaffold(
    //     appBar: appBar, drawer: drawer,
        
    //     body: Container(padding: padding, child: body)));
  }
}

class FixedViewPage extends FixedPageContainer {
  FixedViewPage({
    super.key, 

    super.padding,

    Widget? title,
    super.bottomSheet,

    PreferredSizeWidget? bottom, 
    required super.body,
  })
    : super(appBar: ViewAppBar(bottom: bottom, title: title));
}

class FixedMainView extends FixedPageContainer {
  FixedMainView({
    super.key, 
    super.padding,
    PreferredSizeWidget? bottom, 
    required super.body,
  })
    : super(appBar: MainAppBar(bottom: bottom));
}

class BarlessView extends FixedPageContainer {
  const BarlessView({
    super.key, 
    super.padding,
    required super.body
  });
}
