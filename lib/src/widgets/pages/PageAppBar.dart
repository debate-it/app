import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';
import 'package:debateit_core/widgets/brand/app_icon.dart';

import 'package:debateit/src/widgets/navigation/icons/back.dart';
import 'package:debateit/src/widgets/navigation/icons/notification.dart';
import 'package:debateit/src/widgets/navigation/icons/search.dart';

import 'package:debateit/src/discussions/report/menu.dart';
import 'package:debateit/src/discussions/shared/DiscussionCategoryInfo.dart';


abstract class PageAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? title;
  final Widget? leading;

  final PreferredSizeWidget? bottom;

  final bool centerTitle;

  @override
  Size get preferredSize {
    final bottomHeight = bottom?.preferredSize.height ?? 0.0;
    const defaultHeight = 60.0;
    
    return Size.fromHeight(defaultHeight + bottomHeight);
  }

  const PageAppBar({ 
    super.key, 
    this.title, 
    this.leading, 
    this.bottom,
    this.centerTitle = false
  }); 

  static const BarActions = <Widget>[
    NotificationsIcon(),
    SearchIcon() 
  ];
 
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: context.colorScheme.surface, 
      leading: leading, bottom: bottom, title: title,
      centerTitle: centerTitle,
      actions: BarActions,
      titleSpacing: 0.0,
    );
  }
}

class MainAppBar extends PageAppBar {
  const MainAppBar({ super.key, super.bottom }) 
    : super(
      centerTitle: false,
      title: const AppIcon());
}

class ViewAppBar extends PageAppBar {
  const ViewAppBar({ super.key, super.title, super.bottom, super.leading = const BackIcon() });
}

class FlexibleViewAppBar extends ViewAppBar {
  final double? expandedHeight; 
  final Widget? background;

  final bool floating;
  final bool pinned;

  final List<Widget>? actions;

  const FlexibleViewAppBar({ 
    super.key, 
    super.title,
    super.bottom,
    this.expandedHeight, 
    this.background,
    this.actions,
    this.floating = true,
    this.pinned = false
  }) 
    : assert((background != null && expandedHeight != null) || background == null);

  static const StretchModes = [ 
    StretchMode.fadeTitle,
    StretchMode.blurBackground,
    StretchMode.zoomBackground
  ];

  @override
  Widget build(BuildContext context) { 
    return SliverAppBar(
      title: title,
      bottom: bottom,
      centerTitle: true,
      actions: actions,
      actionsIconTheme: context.theme.iconTheme,
      leading: const BackIcon(),
      expandedHeight: expandedHeight,
      pinned: pinned,
      floating: floating,
      flexibleSpace: FlexibleSpaceBar(
        stretchModes: StretchModes,
        background: background));
  }
}


class FullViewAppBar extends StatelessWidget {
  final String type;
  final Pageable target;

  final DiscussionCategory category;
  final DiscussionSize? size;

  const FullViewAppBar({
    super.key,
    required this.type,
    required this.target,
    required this.category,
    this.size
  });

  static const _menuPadding = EdgeInsets.symmetric(horizontal: 10);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 12.0,
      
      backgroundColor: context.colorScheme.surface,

      leading: const BackIcon(),

      centerTitle: true,
      title: DiscussionCategoryInfo(size: size, category: category, horizontal: true),

      actions: [
        Padding(padding: _menuPadding, 
        child: ReportMenu(target: type, id: target.id)) 
      ] 
    );
  }
}
