import 'package:flutter/material.dart';

class AppSnackbar extends StatelessWidget {
  const AppSnackbar({super.key, required this.content});

  final Widget content;

  @override
  Widget build(BuildContext context) {
    return SnackBar(content: content);
  }
}
