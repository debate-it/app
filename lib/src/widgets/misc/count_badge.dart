import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';

class FloatingCountBadge extends StatelessWidget {
  final int value;
  final double fontSize;

  const FloatingCountBadge({ super.key, required this.value, this.fontSize = 10 });

  @override
  Widget build(BuildContext context) {
    if (value == 0) {
      return const SizedBox();
    }
      
    final style = context.text.bodyMedium
      ?.copyWith(
        color: context.colors.accent,
        fontWeight: FontWeight.bold,
        fontSize: fontSize
      );

    const shadows = [
      BoxShadow(
        color: Colors.black26,
        offset: Offset(1.2, 1.4),
        spreadRadius: 1.2,
        blurRadius: 0.8
      )
    ];

    final color = context.colorScheme.primary;

    final decoration = BoxDecoration(
      borderRadius: Corners.smBorder,
      boxShadow: shadows,
      color: color
        // .withBlue(color.blue - 25)
        // .withRed(color.red - 25)
        // .withGreen(color.green - 25)
        // .withOpacity(0.9)
    );
 
    return IgnorePointer(
      child: Container(
        constraints: const BoxConstraints(minWidth: 15),
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 2),
        decoration: decoration,
        child: FittedBox(
          fit: BoxFit.cover,
          child: Text(value.toString(), style: style))));
  }
}
