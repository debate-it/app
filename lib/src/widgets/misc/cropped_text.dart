import 'package:flutter/material.dart';

class CroppedText extends StatefulWidget {
  final String text;

  final TextStyle? style;

  final bool crop;

  const CroppedText({ super.key, required this.text, this.style, this.crop = true });

  @override
  State<CroppedText> createState() => _CroppedTextState();
}

class _CroppedTextState extends State<CroppedText> {
  late TextOverflow? overflow = widget.crop ? TextOverflow.ellipsis : null;
  late int? maxLines = widget.crop ? 3 : null;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.crop ? toggleCrop : null,
      child: Text(widget.text, 
        style: widget.style,
        overflow: overflow, 
        maxLines: maxLines
      )
    );
  }

  void toggleCrop() {
    setState(() {
      overflow = overflow == null ? TextOverflow.ellipsis : null;
      maxLines = maxLines == null ? 3 : null;
    });
  }
}
