import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';


class ErrorText extends StatelessWidget {
  const ErrorText(this.text, { super.key, this.style, this.textAlign, this.overflow, this.maxLines });

  final String text;

  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;

  final int? maxLines;

  @override
  Widget build(BuildContext context){
    return Text(text,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,
      style: (style ?? context.text.bodyLarge)
        ?.copyWith(color: context.colorScheme.error)
    );
  }
}
