import 'dart:async';

import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';


class Countdown extends StatefulWidget {
  const Countdown({super.key, required this.end, this.onTimeout, this.style, this.builder});

  final DateTime end;
  final TextStyle? style;
  final VoidCallback? onTimeout;

  final WidgetBuilder? builder;

  @override
  State<Countdown> createState() => _CountdownState();
}

class _CountdownState extends State<Countdown> {
  late Timer timer;
  late Duration countdown;

  @override
  void initState() {
    final now = DateTime.now();
    countdown = widget.end.difference(now);
    timer = Timer.periodic(const Duration(seconds: 1), update);

    if (countdown.isNegative) {
      countdown = Duration.zero;
      timer.cancel();
    }

    super.initState();
  }

  @override
  void dispose() {
    if (timer.isActive) {
      timer.cancel();
    }

    super.dispose();
  }

  void update(Timer timer) {
    final current = widget.end.difference(DateTime.now());

    if (current.isNegative) {
      setState(() { countdown = Duration.zero; });

      timer.cancel();
      widget.onTimeout?.call();
    } else {
      setState(() { countdown = current; });
    }
  }
  
  @override
  Widget build(BuildContext context) {
    if (countdown == Duration.zero && widget.builder != null) {
      return widget.builder!(context);
    }

    return Text(_format(countdown), style: widget.style ?? context.text.bodyMedium);
  }

  String _format(Duration value) {
    return 
      '${_pad(value.inHours)}:'
      '${_pad(value.inMinutes % 60)}:'
      '${_pad(value.inSeconds % 60)}';
  }

  String _pad(int value) => value.toString().padLeft(2, '0');
}
