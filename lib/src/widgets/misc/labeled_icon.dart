import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';


class LabeledIcon extends StatelessWidget {
  final Widget label;
  final IconData? icon;

  final bool vertical; 
  final bool inverted;
  final bool loading;

  final Color? iconColor;
  final double iconSize;

  const LabeledIcon({ 
    required this.label, 
    this.icon, 
    this.vertical = false,
    this.inverted = false,
    this.loading = false,
    this.iconSize = 15.0,
    this.iconColor, 
  });

  @override
  Widget build(BuildContext context) {
    return vertical
      ? _buildVertical(context)
      : _buildHorizontal(context);
  }

  Widget _buildVertical(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _buildChildren(context));
  }

  Widget _buildHorizontal(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: _buildChildren(context));
  }

  List<Widget> _buildChildren(BuildContext context) { 
    final children = <Widget> [];
    final color = iconColor ?? context.colors.primary;

    if (icon != null && !loading) {
      children.add(Icon(icon, color: color, size: iconSize));
    } else if (loading) {
      children.add(
        const SizedBox(
          height: 15,
          child: FittedBox(
            child: CircularProgressIndicator())));
    }
      
    children.add(const SizedBox(width: 3.0));
    children.add(label);

    return inverted
      ? children.reversed.toList()
      : children;
  }
}
