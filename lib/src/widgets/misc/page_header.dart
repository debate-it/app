import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

class PageHeader extends StatelessWidget {
  final String text;

  const PageHeader({ super.key, required this.text });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(Insets.md),
      child: Text(text, textAlign: TextAlign.center, style: TextStyles.h2));
  }
}
