import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/widgets/misc/labeled_icon.dart';


class CreationLabel extends StatelessWidget {
  final DateTime date;

  final double fontSize;
  final double iconSize;

  const CreationLabel({
    super.key,
    required this.date,
    this.fontSize = 12.0,
    this.iconSize = 15.0
  });

  @override
  Widget build(BuildContext context) { 
    final style = context.text.bodyMedium?.copyWith(fontSize: fontSize);

    return LabeledIcon(
      icon: Icons.calendar_today, iconSize: iconSize,
      label: Text('${date.month}/${date.day}/${date.year}', style: style));  
  }
}
