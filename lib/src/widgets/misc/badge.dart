import 'package:flutter/material.dart';

class Badge extends StatelessWidget {
  final Widget child;
  final Widget badge;

  final Alignment alignment;

  const Badge({ 
    super.key, 
    required this.badge, 
    required this.child, 
    this.alignment = Alignment.topRight
  });

  Badge.fromFuture({
    super.key,
    Future? future,

    required AsyncWidgetBuilder builder,
    required this.child,

    this.alignment = Alignment.topRight,
  }) 
    : badge = FutureBuilder(builder: builder, future: future);
 
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: alignment,
      children: [ Align(child: child), badge ]);
  }
}
