import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/widgets/misc/cropped_text.dart';


class LabeledText extends StatelessWidget { 
  final String? topic;
  final String label;

  final Widget? text;

  final bool crop;

  const LabeledText({ this.topic, this.text, this.label = 'TOPIC', this.crop = false,  })
    : assert((topic != null && text == null) || (topic == null && text != null));

  @override
  Widget build(BuildContext context) { 
    final topicLabel = Text(label,
      textAlign: TextAlign.center,
      style: context.text.headlineMedium
        ?.copyWith(color: context.colorScheme.primary));
    
    final style  =  context.text.bodyLarge;

    final body = text ?? (crop
      ? CroppedText(text: topic!, style: style)
      : Text(topic!,
          textAlign: TextAlign.start,
          textWidthBasis: TextWidthBasis.parent,
          softWrap: true,
          style: style));

 
    return Container(
      padding: const EdgeInsets.all(Insets.sm),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [ topicLabel, body ]));
  }
}
