import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/widgets/misc/countdown.dart';
import 'package:debateit/src/widgets/misc/labeled_icon.dart';


class CountdownLabel extends StatelessWidget {
  final DateTime? date;

  final String? timeoutLabel;
  final VoidCallback? onTimeout;
  final WidgetBuilder? builder;

  final double fontSize;
  final double iconSize;

  const CountdownLabel({
    super.key,
    required this.date,
    required this.timeoutLabel,
    this.builder,
    this.onTimeout,
    this.fontSize = 12.0,
    this.iconSize = 15.0
  })
    : assert(date != null || builder != null);

  @override
  Widget build(BuildContext context) {
    final style = context.text.bodyMedium?.copyWith(fontSize: fontSize);

    if (date == null) {
      return builder!(context);
    }
      
    return LabeledIcon(
      icon: Icons.timelapse, iconSize: iconSize,
      label: Countdown(end: date!, style: style, onTimeout: onTimeout, builder: builder)); 
  }
}
