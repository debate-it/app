import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';


class SideLabel extends StatelessWidget {
  final String side;

  const SideLabel({ super.key, required this.side });

  @override
  Widget build(BuildContext context) { 
    return Container(
      padding: const EdgeInsets.all(Insets.xs),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('SIDE',
            style: context.text.bodyLarge
              ?.copyWith(color: context.colorScheme.primary)),

          Text(side == 'left' ? 'FOR' : 'AGAINST',
            style: context.text.bodyMedium)
        ],
      )
    );
  }
}
