import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/widgets/misc/labeled_text.dart';


class CroppedTopicViewer extends StatefulWidget {
  final String text;
  final String label;


  const CroppedTopicViewer({super.key, required this.text, required this.label});

  @override
  State<CroppedTopicViewer> createState() => _CroppedTopicViewerState();
}

class _CroppedTopicViewerState extends State<CroppedTopicViewer> {
  final focus = FocusNode();

  @override
  Widget build(BuildContext context) {

    return LayoutBuilder(
      builder: (context, constraints) {
        final textIsTooLong = isTooLong(context, constraints);
        
        return InkWell(
          onTap: textIsTooLong ? () => showTopic(context) : null,
          child: LabeledText(
            crop: true,
            label: widget.label,
            text: Text(widget.text, 
              maxLines: 3,
              style: context.text.bodyLarge,
              overflow: TextOverflow.ellipsis)
          )
        );
      },
    );
  }

  bool isTooLong(BuildContext context, BoxConstraints constraints) {
    final text = TextSpan(text: widget.text, style: context.text.bodyLarge);
    final richTextWidget = Text.rich(text).build(context) as RichText;
    final renderObject = richTextWidget.createRenderObject(context);

    renderObject.layout(constraints);

    final boxes = renderObject
        .getBoxesForSelection(TextSelection(
            baseOffset: 0, extentOffset: text.toPlainText().length));

    final averageLineHeight = boxes.fold<double>(0, (acc, e) => acc + (e.bottom - e.top) / boxes.length);

    final textHeight = boxes.last.bottom - boxes.first.top;

    return textHeight / averageLineHeight > 3;
  }

  void showTopic(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => FutureBuilder(
        future: Future.delayed(const Duration(milliseconds: 800)),
        builder: (context, snapshot) {
          return Dialog(
            insetAnimationCurve: Curves.easeInCubic,
            insetPadding: const EdgeInsets.symmetric(horizontal: Insets.lg),
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                IconButton(
                  onPressed: () => Navigator.of(context).pop(), 
                  icon: const Icon(Icons.close)),

                Padding(
                  padding: const EdgeInsets.all(Insets.lg),
                  child: LabeledText(topic: widget.text))
              ],
            )
          );
        },
      )
    );
  }
}
