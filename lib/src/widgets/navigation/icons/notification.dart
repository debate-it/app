import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/widgets/misc/badge.dart';
import 'package:debateit/src/widgets/misc/count_badge.dart';


class NotificationsIcon extends StatefulWidget { 
  const NotificationsIcon({super.key });

  @override
  State<NotificationsIcon> createState() => _NotificationsIconState();
}

class _NotificationsIconState extends State<NotificationsIcon> {
  void update() => setState(() { });

  @override
  void initState() {
    Services.profileService.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    Services.profileService.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Badge(
      badge: Positioned(
        right: 8, top: 8,
        child: FloatingCountBadge(
          value: Services.profileService.info?.indicators.notifications ?? 0)),

      child: Center(
        child: IconButton(
          icon: const Icon(Icons.notifications),
          onPressed: () async {
            final navigator = Beamer.of(context);
            final route = ModalRoute.of(context);

            await Services.notificationRepository.updateNotificationSeen(context);
            await Services.profileService.updateIndicators();

            if (route!.settings.name == '/notifications') {
              return;
            }

            navigator.beamToNamed('/notifications');
          }
        )
      )
    );
  } 
}
