import 'package:flutter/material.dart';

import 'package:debateit/src/search/delegate.dart';


class SearchIcon extends StatefulWidget {
  const SearchIcon();
  
  @override
  _SearchIconState createState() => _SearchIconState();
}

class _SearchIconState extends State<SearchIcon> {

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.search),
      onPressed: () => _search(context));
  }

  Future<void> _search(BuildContext context) async {
    await showSearch(context: context, delegate: SearchBarDelegate(context: context)); 
  }
}
