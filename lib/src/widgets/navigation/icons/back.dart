import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';

class BackIcon extends StatelessWidget {
  const BackIcon({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back, color: context.colorScheme.onBackground), 
      onPressed: () => Beamer.of(context).beamBack());
  }
}

class CustomBackIcon extends StatelessWidget {
  const CustomBackIcon({super.key, required this.onPressed});

  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back, color: context.colorScheme.onBackground), 
      onPressed: onPressed);
  }
}
