import 'package:debateit/src/widgets/navigation/utils/link.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:timelines/timelines.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/notifications/activity.dart';


class ActivityTimeline extends StatelessWidget {
  const ActivityTimeline({ super.key, required this.activity });

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildActivityEntry(context, activity));
  }

  Widget _buildActivityEntry(BuildContext context, Activity entry) {
    final title = Container(
      padding: const EdgeInsets.all(Insets.md),
      child: Text(DateFormat.yMMMEd().add_jm().format(entry.from.toLocal()),
        style: TextStyles.h3));

    final timeline = FixedTimeline.tileBuilder(
      theme: TimelineThemeData(
        direction: Axis.vertical,
        indicatorTheme: IndicatorThemeData(size: 10, color: context.colorScheme.primary),
        connectorTheme: ConnectorThemeData(thickness: 2.0, color: context.colors.secondary)),

      builder: TimelineTileBuilder.connectedFromStyle(
        itemCount: entry.events.length,
        connectionDirection: ConnectionDirection.before,

        nodePositionBuilder: (ctx, index) => 0,

        firstConnectorStyle: ConnectorStyle.transparent,
        lastConnectorStyle: ConnectorStyle.transparent,

        connectorStyleBuilder: (ctx, index) => ConnectorStyle.solidLine,
        indicatorStyleBuilder: (ctx, index) => IndicatorStyle.dot,

        contentsBuilder: (context, index) {
          final event = entry.events.elementAt(index);

          final header = Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(event.title,
                style: context.text.titleLarge
                 ?.copyWith(fontSize: FontSizes.s12, color: context.colors.accent)),

              Text(DateFormat.jm().format(event.time),
                style: context.text.titleMedium
                  ?.copyWith(fontSize: FontSizes.s10))
            ]
          );

          final body = Text(event.message,
            style: context.text.bodyMedium);

          return Padding(
            padding: const EdgeInsets.only(left: Insets.xs),
            child: 
              LinkWidget(
                path: _openNotification(event.link),
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(Insets.sm),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [ header, body ]))));
        }));

    return Column(children: [title, timeline]);
  }

  String _openNotification(String link) { 
    final tokens = link.split('/');
    
    final target = tokens[0];
    final id = tokens[1];

    switch (target) {
      case 'DebateRequest':
      case 'PanelRequest':
      case 'OpenRequest':
      case 'Request':
        return '/request/$id';

      case 'Debate':
      case 'Challenge':
      case 'Post':
      case 'Panel':
        return '/discussion/$id';

      case 'User':
        return '/user/$id';

      case 'Comment':
        return '/comment/$id';
    }

    throw Exception('Unknown notification type');
  }
}
