import 'package:beamer/beamer.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';


typedef SimpleRouteBuilder = Widget Function(BuildContext);

class LinkWidget<T> extends StatelessWidget {

  final Widget child;
  final String path;
  final Object? payload;

  final VoidCallback? onTransition;

  const LinkWidget({ 
    super.key, 
    
    required this.child, 
    required this.path, 
    this.payload,
    
    this.onTransition, 
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: Corners.lgBorder,
      onTap: () async {
        onTransition?.call();

        Beamer.of(context).beamToNamed(path, data: payload);
      }, 
      child: child
    );
  }
}
