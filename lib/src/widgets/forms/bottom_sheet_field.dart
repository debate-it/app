import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';


class BottomSheetTextField extends StatefulWidget {
  final int wordLimit;

  final String inputLabel;

  final Widget? header;

  final Future<void> Function(String)? onSubmit;
  final List<Map<String, dynamic>? Function(AbstractControl<dynamic>)> validators;

  const BottomSheetTextField({
    super.key,
    required this.inputLabel,
    this.onSubmit,
    this.header,
    this.wordLimit = 250,
    this.validators = const []
  });

  @override
  _BottomSheetTextFieldState createState() => _BottomSheetTextFieldState();
}

class _BottomSheetTextFieldState extends State<BottomSheetTextField> {
  final TextEditingController _controller = TextEditingController();
  final FocusNode _focus = FocusNode();
  final form = FormGroup({
    'text': FormControl<String>(validators: [Validators.required])
  });

  double? bottomPadding;
  String? _error;

  bool _loading = false;
  bool _shouldPop = false;

  @override
  void initState() {
    _focus.addListener(() {
      if (!_focus.hasPrimaryFocus && _shouldPop) {
        Navigator.of(context).pop(_controller.text);
      } else {
        bottomPadding = MediaQuery.of(context).viewInsets.bottom + 5;
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _focus.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final viewInsets = MediaQuery.of(context).viewInsets;

    final padding = const EdgeInsets.all(5).add(EdgeInsets.only(bottom: viewInsets.bottom + 5));

    final suffixIcon = _loading
      ? const Padding(padding: EdgeInsets.all(Insets.md), child: CircularProgressIndicator())
      : IconButton(
          icon: Icon(Icons.send_sharp, color: context.colorScheme.primary, size: 24),
          onPressed: () => submit(form.control('text').value as String));

    final decoration = InputDecoration(
      hintText: 'Type your ${widget.inputLabel}...',
      contentPadding: const EdgeInsets.all(Insets.xs),
      alignLabelWithHint: true,
      errorText: _error,
      suffixIcon: suffixIcon);

    final style = context.text.bodyLarge;

    return BottomSheet(
      onClosing: () { _shouldPop = false; },
      builder: (context) {
        // if (bottomPadding != null && padding.vertical < bottomPadding! && _submitted) {
        //   Future.microtask(() => Navigator.of(context).pop());
        // } else {
        //   bottomPadding = padding.vertical;
        // }

        return ReactiveForm(
          formGroup: form,
          child: Container(
            padding: padding,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (widget.header != null) 
                  widget.header!,

                ReactiveTextField(
                  formControlName: 'text',

                  autofocus: true,

                  focusNode: _focus,
                  controller: _controller,

                  minLines: 1,
                  maxLines: 10,

                  textInputAction: TextInputAction.newline,
                  textAlignVertical: TextAlignVertical.center,

                  style: style,
                  decoration: decoration,

                  buildCounter: _buildCounter)
              ]
            ))
          );
        });
  }

  int _getWords(String? text) {
    return text?.split(RegExp('[ _\n\t]')).where((word) => word.isNotEmpty).length ?? 0;
  }

  Widget? _buildCounter(BuildContext context, {required int currentLength, required bool isFocused, required int? maxLength}) {
    final words = _getWords(_controller.text);

    final color = words > widget.wordLimit
      ? context.colorScheme.primary
      : context.colorScheme.secondary;

    return Text('$words/${widget.wordLimit}',
      style: context.text.bodyMedium?.copyWith(color: color));
  }

  Future<void> submit(String comment) async {
    final error = _validate(comment);

    if (error != null) {
      setState(() { _error = error; });
      return;
    }


    try {
      setState(() { _loading = true; });
      
      if (widget.onSubmit != null) {
        await widget.onSubmit!(comment);
      }

      _shouldPop = true;
      _focus.unfocus();

    } on Exception catch (err) {
      setState(() { _error = err.toString(); });
    } finally {
      setState(() { _loading = false; });
    }
  }

  String? _validate(String? value) {
    final words = _getWords(value);

    if (words == 0) {
      return 'Your ${widget.inputLabel} cannot be empty';
    }

    if (words > widget.wordLimit) {
      return 'Your ${widget.inputLabel} went over word limit';
    }

    return null;
  }
}
