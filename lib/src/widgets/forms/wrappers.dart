import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

enum CounterType { none, charachters, words }

int countWords(String? text) {
  if (text == null) {
    return 0;
  }

  return text.split(RegExp(r'[ _\n\t]')).where((word) => word.isNotEmpty).length;
}


extension WordCountValidationMessage on ValidationMessage {
  static const maxWordCount = 'maxWordCount';
}

extension WordCountValidation on Validators {
  static Map<String, dynamic>? Function(AbstractControl<dynamic>) maxWordLength(int maxLength) {
    return (control) {
      
      final numberOfWords = countWords(control.value as String?);

      return numberOfWords > maxLength 
        ? { WordCountValidationMessage.maxWordCount: true }
        : null;
    };
  }
}

class AppTextField extends StatefulWidget {
  const AppTextField({
    super.key, 
    
    this.formControlName, 
    this.formControl,
    
    this.obscureText,
    this.validationMessages,

    this.enabled = true, 

    this.hintText, 
    this.labelText, 

    this.prefix, 
    this.suffix, 

    this.minLines,
    this.maxLines,
    this.maxLength,

    this.controller,
    this.keyboardType,
    this.textInputAction,
    this.textAlign = TextAlign.start, 
    this.counterType = CounterType.none
  })
    : assert((formControlName != null && formControl == null) || (formControlName == null && formControl != null)); 
  
  final String? formControlName;
  final FormControl<String>? formControl;

  final bool enabled;
  final bool? obscureText;

  final TextEditingController? controller;
  final Map<String, String Function(Object)>? validationMessages;

  final TextAlign textAlign;

  final String? hintText;
  final String? labelText;

  final Widget? prefix;
  final Widget? suffix;
  
  final int? minLines;
  final int? maxLines;

  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;

  final int? maxLength;
  final CounterType counterType;

  @override
  State<AppTextField> createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  late final _controller = widget.controller ?? TextEditingController();

  @override
  Widget build(BuildContext context){
    return ReactiveTextField(
      obscureText: widget.obscureText ?? false,

      controller: _controller,

      formControl: widget.formControl,
      formControlName: widget.formControlName,

      validationMessages: widget.validationMessages,
      textAlign: widget.textAlign,
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,

      minLines: widget.minLines,
      maxLines: widget.maxLines,
      buildCounter: buildCounter,

      decoration: InputDecoration(
        enabled: widget.enabled,

        fillColor: context.colors.surface,
        focusColor: context.colors.secondary,

        hintText: widget.hintText,
        labelText: widget.labelText,

        hintStyle: context.text.bodyLarge?.copyWith(color: context.colors.accent),
        labelStyle: context.text.bodyLarge?.copyWith(color: context.colors.accent),

        suffix: widget.suffix,
        prefix: widget.prefix,

        errorMaxLines: 5,

        border: MaterialStateOutlineInputBorder.resolveWith((states) {
          return const OutlineInputBorder(
            gapPadding: 20,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: Colors.transparent));
        }),
        // (
        //   // gapPadding: 20,
        //   borderRadius: borderRadius,
        //   borderSide: BorderSide(color: borderColor)),

        filled: true,
        
        focusedBorder: OutlineInputBorder(
          gapPadding: 8,
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(color: context.colors.secondary))),
    );
  }

  Widget buildCounter(BuildContext context, {required int currentLength, required bool isFocused, required int? maxLength}) {
    switch (widget.counterType) {
      case CounterType.none:
        return const SizedBox();

      case CounterType.charachters:
      case CounterType.words:
        final currentLength = widget.counterType == CounterType.words
          ? _controller.text.split(RegExp(r'[ _\n\t]')).where((word) => word.isNotEmpty).length
          : _controller.text.length;

        final color = currentLength > widget.maxLength!
          ? context.colorScheme.primary
          : context.colorScheme.secondary;
        
        return Text('$currentLength${widget.maxLength == null ? '' : '/'}${widget.maxLength ?? ''}',
          style: context.text.bodyMedium?.copyWith(color: color));
    }
  }
}

class AppDropdownField<T> extends StatelessWidget {
  const AppDropdownField({
    super.key,

    required this.items,
    this.formControlName,
    this.formControl,

    this.hintText,
    this.labelText,

    this.prefix,
    this.suffix,

    this.enabled = true,
    this.validationMessages,

    this.isDense = true,
    this.isExpanded = false
  })
    : assert((formControlName != null && formControl == null) ||
        (formControlName == null && formControl != null));

  final bool enabled;
  final String? formControlName;
  final FormControl<T>? formControl;
  final List<DropdownMenuItem<T>> items;
  
  final Map<String, String Function(Object)>? validationMessages;

  final String? hintText;
  final String? labelText;

  final Widget? prefix;
  final Widget? suffix;

  final bool isDense;
  final bool isExpanded;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: ReactiveDropdownField<T>(
        formControlName: formControlName,
        items: items,
        isDense: isDense,
        isExpanded: isExpanded,
        validationMessages: validationMessages,
        decoration: InputDecoration(
          enabled: enabled,
    
          fillColor: context.colors.surface,
          focusColor: context.colors.secondary,
    
          hintText: hintText,
          labelText: labelText,
    
          hintStyle: context.text.bodyLarge?.copyWith(color: context.colors.accent),
          labelStyle: context.text.bodyLarge?.copyWith(color: context.colors.accent),
    
          suffix: suffix,
          prefix: prefix,
    
          border: MaterialStateOutlineInputBorder.resolveWith((states) {
            return const OutlineInputBorder(
            gapPadding: 20,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: Colors.transparent));
          }),
    
          filled: true,
          
          focusedBorder: OutlineInputBorder(
            gapPadding: 8,
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: context.colors.secondary)))),
    );
  }

}
