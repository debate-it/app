import 'package:flutter/material.dart';

import 'package:debateit_pagination/pagination.dart';
import 'package:debateit_graphql/queries/search.dart';
import 'package:debateit_models/discussions/debate/main.dart';


import 'package:debateit/src/discussions/debate/views/preview.dart';



/// Debate pagination config with [size] 10, that defines how
/// to build [Debate] objects from json and how to build [DebatePreview].
abstract class DebatePaginationConfig extends PaginationConfig<Debate> {
  @override int get size => 10;

  DebatePaginationConfig(super.variables);
 
  @override
  Debate buildItem(Map<String, dynamic> json) =>
    Debate.fromJson(json); 
  
  @override
  Widget buildWidget(BuildContext context, Debate item) =>
    DebatePreview(debate: item);
}

class DebateSearchPaginationConfig extends DebatePaginationConfig {
  @override String get query => SearchQueries.TopicSearch;
  @override String get queryName => 'deepSearch';

  @override String get noItemText => 'No debates found';

  final String term;

  DebateSearchPaginationConfig(this.term)
    : super({ 'query': term });
}

class DebateTagSearchPaginationConfig extends DebatePaginationConfig {
  @override String get query => SearchQueries.TagSearch;
  @override String get queryName => 'searchTag';

  @override String get noItemText => 'No debates found';

  final List<String> term;

  DebateTagSearchPaginationConfig(this.term)
    : super({ 'tags': term });
}
