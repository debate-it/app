import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit_graphql/queries/profile.dart';
import 'package:debateit_graphql/queries/request.dart';

import 'package:debateit_models/notifications/activity.dart';

import 'package:debateit/src/widgets/navigation/activity.dart';
import 'package:debateit/src/discussions/request/open/candidates.dart';


class OpenRequestCandidacyPaginationConfig extends PaginationConfig<OpenRequestCandidacy> {
  @override int get size => 10;

  @override String get query => OpenRequestQueries.OpenRequestCandidates;
  @override String get queryName => 'openRequest.candidates';

  @override String get noItemText => 'You have no open requests created yet';


  @override
  OpenRequestCandidacy buildItem(Map<String, dynamic> json) =>
    OpenRequestCandidacy.fromJson(json);

  @override
  Widget buildWidget(BuildContext context, OpenRequestCandidacy item) =>
    OpenRequestCandidate(candidate: item, onSelect: onSelect);

  final CandidacyOnSelect onSelect;

  OpenRequestCandidacyPaginationConfig({ required String id, required this.onSelect })
    : super({ 'id': id, 'withCandidates': true });
}

class NotificationsPaginationConfig extends PaginationConfig<Activity> {
  @override int get size => 5;

  @override String get query => ProfileQueries.Activity;
  @override String get queryName => 'profile.activity'; 

  @override String get noItemText => 'There is no recent activity';

  NotificationsPaginationConfig() : super({});
 
  @override
  Activity buildItem(Map<String, dynamic> json) =>
    Activity.fromJSON(json); 
    
  @override
  Widget buildWidget(BuildContext context, Activity item) =>
    ActivityTimeline(activity: item);
}
