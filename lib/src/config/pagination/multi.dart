import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit_graphql/queries/search.dart';
import 'package:debateit_graphql/queries/user.dart';

import 'package:debateit/src/discussions/challenge/views/preview.dart';
import 'package:debateit/src/discussions/debate/views/preview.dart';
import 'package:debateit/src/discussions/post/views/preview.dart';
import 'package:debateit/src/discussions/panel/preview.dart';


abstract class MultiTypePagination extends PaginationConfig<Discussion> {

  MultiTypePagination(super.variables);

  @override
  Discussion buildItem(Map<String, dynamic> json) {
    switch (json['__typename']) {
      case 'Debate':
        return Debate.fromJson(json);
      case 'Challenge':
        return Challenge.fromJson(json);
      case 'Post':
        return Post.fromJson(json);
      case 'Panel':
        return Panel.fromJson(json);

      default:
        throw Exception('Unknown feed element');
    }
  }

  @override
  Widget buildWidget(BuildContext context, Pageable item) {
    if (item is Debate) {
      return DebatePreview(debate: item);
    }
    
    if (item is Challenge) {
      return ChallengePreview(challenge: item);
    }
    
    if (item is Post) {
      return PostPreview(post: item);
    }

    if (item is Panel) {
      return PanelPreview(panel: item);
    }

    throw Exception('Unknown feed element');
  }
}



/// Pagination config that specifies how to fetch user debates
/// for given [id] and debate [status]
class UserProfileContentPaginationConfig extends MultiTypePagination {
  @override int get size => 10;

  @override String get query => UserQueries.UserDebates;
  @override String get queryName => 'user.discussions';

  @override String get noItemText => '$username has no discussions yet.'; 

  /// Target username
  final String id;
  final String username;

  UserProfileContentPaginationConfig({ required this.id, required this.username })
    : super({ 'id': id }); 
}

class SearchTopicPaginationConfig extends MultiTypePagination {
  @override
  String get noItemText => "No discussions found for '$search'";

  @override 
  String get query => SearchQueries.TopicSearch;

  @override 
  String get queryName => 'search.discussions';

  @override 
  int get size => 20;

  final String search;

  SearchTopicPaginationConfig(this.search) 
    : super({ 'query': search });
}

class SearchTagsPaginationConfig extends MultiTypePagination {
  @override
  String get noItemText => "No discussions found for '$search'";

  @override 
  String get query => SearchQueries.TagSearch;

  @override 
  String get queryName => 'search.tags';

  @override 
  int get size => 20;

  final String search;

  SearchTagsPaginationConfig(this.search) 
    : super({ 'query': search });
}

class SearchCategoryPaginationConfig extends MultiTypePagination {
  @override
  String get noItemText => "No discussions found for '$search'";

  @override 
  String get query => SearchQueries.CategorySearch;

  @override 
  String get queryName => 'search.categories';

  @override 
  int get size => 20;

  final String search;

  SearchCategoryPaginationConfig(this.search) 
    : super({ 'query': search });
}
