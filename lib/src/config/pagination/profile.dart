import 'package:debateit/src/discussions/debate/views/preview.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/discussions/main.dart';

import 'package:debateit_graphql/queries/profile.dart';
import 'package:debateit_graphql/queries/request.dart';

import 'package:debateit/src/config/pagination/category.dart';
import 'package:debateit/src/config/pagination/debate.dart';
import 'package:debateit/src/config/pagination/multi.dart';
import 'package:debateit/src/config/pagination/request.dart';
import 'package:debateit/src/config/pagination/user.dart';


/// Pagination config that specifies how to fetch feed.
class FeedPagination extends MultiTypePagination { 
  FeedPagination() : super({});

  @override int get size => 20;

  @override String get query => ProfileQueries.Feed;
  @override String get queryName => 'profile.feed';
  
  @override String get noItemText => "You don't have any debates in your feed yet"; 

} 

class ProfileCategorySubscriptionConfig extends CategoryPaginationConfig {
  ProfileCategorySubscriptionConfig() : super({});
  
  @override int get size => 10;
  @override String get noItemText => 'No subscribed categories found';

  @override String get query => ProfileQueries.SubscribedCategories;
  @override String get queryName => 'profile.categorySubscriptions';

}

class ProfileUserSubscriptionsPaginationConfig extends UserPaginationConfig {
  ProfileUserSubscriptionsPaginationConfig(UserOnTapCallback? onTap) : super({}, onTap);

  @override String get query => ProfileQueries.SubscribedUsers;
  @override String get queryName => 'profile.userSubscriptions';

  @override String get noItemText => 'You have no subscriptions yet';
}

class ProfileDebatePaginationConfig extends DebatePaginationConfig {
  ProfileDebatePaginationConfig([this.status = DebateStatus.active]) 
    : super({ 'status': status.name }); 

  final DebateStatus status;
  
  @override String get query => ProfileQueries.Debates;
  @override String get queryName => 'profile.debates';

  @override String get noItemText => 'You have no ${status.name} debates yet.'; 

  @override
  Widget buildWidget(BuildContext context, Debate item) =>
    DebatePreview(debate: item, removeOnExpiration: status == DebateStatus.active);
}

class ProfileDiscussionsPaginationConfig extends MultiTypePagination {
  ProfileDiscussionsPaginationConfig([this.status = DebateStatus.active])
    : super({ 'status': status.name });

  final DebateStatus status;

  @override
  String get noItemText => 'No discussions found';

  @override
  String get query => ProfileQueries.Discussions;

  @override
  String get queryName => 'profile.discussions';

  @override
  int get size => 20;

}

class ProfileRequestsPaginationConfig extends RequestPaginationConfig {
  @override String get query => ProfileQueries.Requests;
  @override String get queryName => 'profile.requests';

  @override String get noItemText => 'You have no requests yet';

  ProfileRequestsPaginationConfig([RequestSource super.source = RequestSource.incoming]);
}

class ProfileOpenRequestsPaginationConfig extends RequestPaginationConfig {
  ProfileOpenRequestsPaginationConfig() : super();

  @override String get query => OpenRequestQueries.UserOpenRequests;
  @override String get queryName => 'userOpenRequests';

  @override String get noItemText => 'You have no open requests created yet';
}
