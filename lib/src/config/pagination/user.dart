import 'package:debateit/src/discussions/user/views/preview.dart';
import 'package:debateit_graphql/queries/search.dart';
import 'package:debateit_graphql/queries/user.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/user/main.dart';
import 'package:debateit_pagination/pagination.dart';


typedef UserOnTapCallback = void Function(UserProfile);

/// User pagination config with [size] 10, that defines how
/// to build [UserProfile] objects from json and [UserPreview].
abstract class UserPaginationConfig extends PaginationConfig<UserProfile> {
  @override int get size => 10;

  final UserOnTapCallback? onTap;

  UserPaginationConfig(super.variables, [this.onTap]);
 
  @override
  UserProfile buildItem(Map<String, dynamic> json) =>
    UserProfile.fromJson(json); 
    
  @override
  Widget buildWidget(BuildContext context, UserProfile item) =>
    UserPreview(user: item, onTap: onTap);
}


class SearchUserPaginationConfig extends UserPaginationConfig {
  @override
  String get noItemText => "No users found for '$search'";

  @override 
  String get query => SearchQueries.UserSearch;

  @override 
  String get queryName => 'search.users';

  @override 
  int get size => 20;

  final String search;

  SearchUserPaginationConfig(this.search) 
    : super({ 'query': search });
}

class FollowersPaginationConfig extends UserPaginationConfig {
  @override String get query => UserQueries.Followers;
  @override String get queryName => 'user.followers';

  @override String get noItemText {
    if (profile.isSelf) {
      return 'You have no followers yet';
    } else {
      return '${profile.username} has no followers yet';
    }
  }

  final UserProfile profile;

  FollowersPaginationConfig(this.profile)
    : super({ 'id': profile.id }); 
}

class OpponentSearchPaginationConfig extends UserPaginationConfig {
  @override String get query => SearchQueries.UserSearchByFilters;
  @override String get queryName => 'opponents';

  @override String get noItemText => 'No users were found';

  OpponentSearchPaginationConfig(Map<String, dynamic> filters, UserOnTapCallback onTap)
    : super({ 'filters': filters }, onTap);
}

class FollowerSearchPaginationConfig extends UserPaginationConfig {
  @override String get query => UserQueries.Followers;
  @override String get queryName => 'user.followers';

  @override String get noItemText => 'No users were found';

  FollowerSearchPaginationConfig(String id, UserOnTapCallback onTap)
    : super({ 'id': id }, onTap);
}

class UserSubscriptionsPaginationConfig extends UserPaginationConfig {
  @override String get query => UserQueries.UserSubscriptions;
  @override String get queryName => 'user.userSubscriptions';

  @override String get noItemText {
    if (profile.isSelf) {
      return 'You have no subscriptions yet';
    } else {
      return '${profile.username} has no subscriptions yet';
    }
  }

  final UserProfile profile;

  UserSubscriptionsPaginationConfig(this.profile)
    : super({ 'id': profile.id }); 
}


class UserSearchPaginationConfig extends UserPaginationConfig {
  @override String get query => SearchQueries.UserSearch;
  @override String get queryName => 'searchUsersSimple';

  @override String get noItemText => 'No users found';

  final String term;

  UserSearchPaginationConfig(this.term)
    : super({ 'query': term });
}
