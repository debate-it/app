import 'package:debateit/src/discussions/challenge/views/preview.dart';
import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit_graphql/queries/challenge.dart';


abstract class ChallengePaginationConfig extends PaginationConfig<Challenge> {
  @override int get size => 10;

  ChallengePaginationConfig(super.variables);

  @override
  Challenge buildItem(Map<String, dynamic> json) =>
    Challenge.fromJson(json); 
    
  @override
  Widget buildWidget(BuildContext context, Challenge item) =>
    ChallengePreview(challenge: item);
}

class ChallengePagination extends ChallengePaginationConfig { 
  ChallengePagination() : super({});

  @override String get query => ChallengeQueries.FindChallenges;
  @override String get queryName => 'challenges'; 

  @override String get noItemText => 'There are no challenges available';
}
