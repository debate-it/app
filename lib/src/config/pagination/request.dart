import 'package:flutter/material.dart';

import 'package:debateit_models/request/main.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/discussions/request/open/preview.dart';
import 'package:debateit/src/discussions/request/views/debate.dart';
import 'package:debateit/src/discussions/request/views/panel.dart';


enum RequestSource {
  incoming,
  outgoing
}

/// Request pagination config with [size] 10, that defines how
/// to build [Request] objects from json and how to build [DebateRequestWidget].
abstract class RequestPaginationConfig extends PaginationConfig<Request> {
  @override int get size => 10;

  final RequestSource? source;

  RequestPaginationConfig([this.source]) 
    : super(getVariables(source));

  static Map<String, dynamic> getVariables(RequestSource? source) {
    if (source != null) {
      return { 'source': source.toString().replaceAll('RequestSource.', '') };
    }

    return {};
  }
 
  @override
  Request buildItem(Map<String, dynamic> json) =>
    Request.parse(json); 
  
  @override
  Widget buildWidget(BuildContext context, Request item) {
    if (item is DebateRequest) {
      return DebateRequestWidget.from(
        request: item, 
        outgoing: source != null && source == RequestSource.outgoing
      );
    }

    if (item is PanelRequest) {
      return PanelRequestWidget.from(
        request: item,
        outgoing: source != null && source == RequestSource.outgoing
      );
    }

    if (item is OpenRequest) {
      return OpenRequestWidget(request: item);
    }

    throw Exception('Invalid request type');
  }
}
