import 'package:flutter/material.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';
import 'package:debateit_graphql/queries/category.dart';

import 'package:debateit/src/config/pagination/multi.dart';
import 'package:debateit/src/discussions/category/views/preview.dart';


class CategoryFeedConfig extends MultiTypePagination { 
  CategoryFeedConfig(String id) : super({ 'id': id });

  @override int get size => 10;
  
  @override String get noItemText => 'No debates found for category';

  @override String get query => CategoryQueries.CategoryFeed;
  @override String get queryName => 'category.feed';
}

abstract class CategoryPaginationConfig extends PaginationConfig<DiscussionCategory> {
  CategoryPaginationConfig(super.variables);

  @override
  DiscussionCategory buildItem(Map<String, dynamic> json) =>
    DiscussionCategory.fromJson(json);

  @override
  Widget buildWidget(BuildContext context, DiscussionCategory item) =>
    CategoryTile(category: item);
}
 
