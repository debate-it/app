import 'package:debateit/src/config/pagination/misc.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/widgets/misc/page_header.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';


class ProfileNotificationPage extends StatelessWidget { 
  const ProfileNotificationPage();
 
  @override
  Widget build(BuildContext context) {
    return FixedViewPage(
      title: Text('Notifications', 
        style: context.text.headlineMedium),

      body: PaginationProvider(
        client: Services.client,
        config: NotificationsPaginationConfig(),
        header: const PageHeader(text: 'Your latest events'),
        padding: const EdgeInsets.symmetric(horizontal: Insets.md, vertical: Insets.lg)));
  } 
}
