import 'package:debateit/src/config/pagination/user.dart';
import 'package:debateit/src/widgets/misc/page_header.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';


class ProfileFollowersPage extends StatelessWidget { 
  const ProfileFollowersPage();

  @override
  Widget build(BuildContext context) { 
    return FixedViewPage( 
      body: PaginationProvider(
        client: Services.client,
        header: const PageHeader(text: 'Your followers'),
        config: FollowersPaginationConfig(Services.profileService.info!.profile),
        padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md)));
  } 
}
