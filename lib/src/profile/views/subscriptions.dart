import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/profile.dart';

import 'package:debateit/src/widgets/misc/page_header.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';

import 'package:debateit/src/config/pagination/profile.dart';


class ProfileSubscriptionsPage extends StatefulWidget {
  const ProfileSubscriptionsPage();
  
  @override
  _SubscriptionsPageState createState() => _SubscriptionsPageState();
}

class _SubscriptionsPageState extends State<ProfileSubscriptionsPage> with TickerProviderStateMixin {
  late TabController controller = TabController(length: 2, vsync: this);

  late PaginationBloc users;
  late PaginationBloc categories;
  
  ProfileService get service => Services.profileService;

  void update() {
    switch (controller.index) {
      case 0: users.add(const PageRefresh()); break;
      case 1: categories.add(const PageRefresh()); break;
    }
  }

  @override
  void initState() {
    service.addListener(update);
    
    users = PaginationBloc(
      client: Services.client,
      config: ProfileUserSubscriptionsPaginationConfig((profile) {
        final navigator = Beamer.of(context);
        final onPopPage = navigator.onPopPage;
        
        // Hacky way to add pop handler
        navigator.onPopPage = (context, route, result) {
          users.add(const PageRefresh());

          final popResult = onPopPage?.call(context, route, result) ?? true;

          navigator.onPopPage = onPopPage;

          return popResult;
        };

        navigator.beamToNamed('/user/${profile.id}');
      }
    ));

    categories = PaginationBloc(client: Services.client, config: ProfileCategorySubscriptionConfig());

    super.initState();
  }

  @override
  void dispose() {
    service.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) { 
    return FixedViewPage( 
      bottom: TabBar(
        controller: controller,
        labelColor: context.colorScheme.onSurface,
        tabs: const [ Tab(text: 'Users'), Tab(text: 'Categories') ]),

      body: TabBarView(
        controller: controller,
        children: [
          PaginationProvider.fromBloc(
            header: const PageHeader(text: 'Your user subscriptions'),
            padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md),
            bloc: users),

          PaginationProvider.fromBloc(
            header: const PageHeader(text: 'Your category subscriptions'),
            padding: const EdgeInsets.symmetric(horizontal: Insets.xs, vertical: Insets.md),
            bloc: categories)
        ],
      )
    );
  }
}
