import 'package:flutter/material.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/discussions/user/views/full.dart';


class ProfilePage extends StatelessWidget {
  const ProfilePage({ super.key });

  @override
  Widget build(BuildContext context){
    return UserFullView(profile: Services.profileService.info!.profile);
  }
}
