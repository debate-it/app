import 'package:flutter/material.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/views/about.dart';


class ProfileAboutPage extends StatelessWidget {
  const ProfileAboutPage({super.key, this.provider});
  
  final UserProvider? provider;

  @override
  Widget build(BuildContext context) {
    return UserAboutPage(
      id: Services.profileService.info!.profile.id,
      provider: provider,
    );
  }
}
