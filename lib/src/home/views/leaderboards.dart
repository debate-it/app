// ignore_for_file: avoid_dynamic_calls
import 'package:debateit/src/widgets/pages/FixedPage.dart';
import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:debateit_graphql/queries/leaderboard.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/home/widgets/leaderboards/challenge.dart';
import 'package:debateit/src/home/widgets/leaderboards/debate.dart';
import 'package:debateit/src/home/widgets/leaderboards/user.dart';

import 'package:debateit/src/widgets/misc/text.dart';
import 'package:debateit/src/widgets/misc/page_header.dart';


class LeaderboardsView extends StatefulWidget {
  const LeaderboardsView({super.key});

  @override
  _TrendingViewState createState() => _TrendingViewState();
}

class _TrendingViewState extends State<LeaderboardsView> { 
  bool _loading = false;

  String? _error;

  List<UserEntry>? users;
  List<DebateEntry>? debates;
  List<ChallengeEntry>? challenges;

  @override
  void initState() {
    fetchLeaderboard(); 
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(onRefresh: fetchLeaderboard, child: buildContent(context));
  }

  Widget buildContent(BuildContext context) {
    if (_loading) {
      return const Center(child: Loader());
    }

    if (_error != null) {
      return Container(
        padding: const EdgeInsets.all(10),
        child: ErrorText(_error!)
      );
    }

    return buildLeaderboard(context);
  }

  Widget buildLeaderboard(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(5),
      children: [
        const PageHeader(text: 'Top Debaters of the day'),
        buildDebaters(context),
        redirectLink(context, (context) => const UserLeaderboardPage()),

        const Divider(),

        const PageHeader(text: 'Top Debates of the day'),
        buildDebates(context),
        redirectLink(context, (context) => const DebateLeaderboardPage()),
        
        const Divider(),

        const PageHeader(text: 'Top Challenges of the day'),
        buildChallenges(context),
        redirectLink(context, (context) => const ChallengeLeaderboardPage()),
      ],
    );
  }

  Widget buildDebaters(BuildContext context) {
    Widget buildUserEntry(UserEntry entry) {
      return UserEntryWidget(entry: entry);
    }

    return Column(children: (users ?? []).map(buildUserEntry).toList());
  }
  
  Widget buildDebates(BuildContext context) {
    Widget buildDebateEntry(DebateEntry entry) {
      return DebateEntryWidget(entry: entry);
    }

    return Column(children: (debates ?? []).map(buildDebateEntry).toList());
  }
  
  Widget buildChallenges(BuildContext context) {
    Widget buildChallengeEntry(ChallengeEntry entry) {
      return ChallengeEntryWidget(entry: entry);
    }

    return Column(children: (challenges ?? []).map(buildChallengeEntry).toList());
  }

  Widget redirectLink(BuildContext context, WidgetBuilder builder) {
    return ListTile(
      dense: true,
      
      onTap: () => Navigator.of(context)
        .push(MaterialPageRoute(builder: builder)),

      trailing: Icon(Icons.arrow_right,
        color: context.colorScheme.primary,
        size: 18),

      title: Text('Show More',
        textAlign: TextAlign.end,
        style: TextStyle(
          color: context.colorScheme.primary,
          fontWeight: FontWeight.w500,
          fontSize: 16)));
  }


  Future<void> fetchLeaderboard() async { 
    setState(() { 
      _loading = true; 
      _error = null;
    });

    final client = Services.client;

    try {
      final result = await client.fetch(
        query: LeaderboardQueries.Overview 
      );

      final data = result.data!['leaderboards'];
      

      final userList = (data!['users']['page'] as List<dynamic>)
        .map<UserEntry>((entry) => UserEntry.fromJson(entry as Map<String, dynamic>))
        .toList();
        
      final debateList = (data!['debates']['page'] as List<dynamic>)
        .map<DebateEntry>((entry) => DebateEntry.fromJson(entry as Map<String, dynamic>))
        .toList();
        
      final challengeList = (data!['challenges']['page'] as List<dynamic>)
        .map<ChallengeEntry>((entry) => ChallengeEntry.fromJson(entry as Map<String, dynamic>))
        .toList();

      setState(() {
        users = userList;
        debates = debateList;
        challenges = challengeList;
      });

    } on Exception catch(err) { 
      setState(() { _error = err.toString(); }); 
    } finally {
      setState(() { _loading = false; });
    }
  }
}


class LeaderboardPage<T> extends StatelessWidget {

  final String header;

  final String query;
  final String queryName;

  final T Function(Map<String, dynamic>) itemBuilder;
  final Widget Function(T item) widgetBuilder;

  const LeaderboardPage({
    super.key,
    required this.header,
    required this.query,
    required this.queryName,
    required this.itemBuilder,
    required this.widgetBuilder
  });

  @override
  Widget build(BuildContext context) {
    return FixedViewPage(body: buildContent(context));
  }

  Widget buildContent(BuildContext context) {
    return FutureBuilder(
      future: fetchUserLeaderboard(context),
      builder: (context, AsyncSnapshot<List<T>> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: Loader());
        }

        return buildEntries(context, snapshot.data!);
      }
    );
  }

  Widget buildEntries(BuildContext context, List<T> entries) {
    final entryWidgets = entries.map((entry) => widgetBuilder(entry));

    return ListView(
      padding: const EdgeInsets.all(5),
      children: <Widget>[ PageHeader(text: header) ]
        .followedBy(entryWidgets)
        .toList()
    );
  }

  Future<List<T>> fetchUserLeaderboard(BuildContext context) async {
    final client = Services.client;

    final result = await client.fetch(query: query); 

    return _getTargetJson(result.data!, queryName)
      .map<T>((entry) => itemBuilder(entry as Map<String, dynamic>))
      .toList();
  }

  List<dynamic> _getTargetJson(Map<String, dynamic> json, String path) {
    var target = json;

    for (final part in path.split('.')) {
      target = target[part] as Map<String, dynamic>;
    }

    return target['page'] as List<dynamic>;
  }
}

class ChallengeLeaderboardPage extends StatelessWidget {
  const ChallengeLeaderboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return LeaderboardPage<ChallengeEntry>(
      header: 'Top Challenges of the day',
      query: LeaderboardQueries.Challenges,
      queryName: 'leaderboards.challenges',
      itemBuilder: (json) => ChallengeEntry.fromJson(json),
      widgetBuilder: (item) => ChallengeEntryWidget(entry: item),
    );
  }
}

class DebateLeaderboardPage extends StatelessWidget {
  const DebateLeaderboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return LeaderboardPage<DebateEntry>(
      header: 'Top Debates of the day',
      query: LeaderboardQueries.Debates,
      queryName: 'leaderboards.debates',
      itemBuilder: (json) => DebateEntry.fromJson(json),
      widgetBuilder: (item) => DebateEntryWidget(entry: item),
    );
  }
}

class UserLeaderboardPage extends StatelessWidget {
  const UserLeaderboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return LeaderboardPage<UserEntry>(
      header: 'Top Debaters of the day',
      query: LeaderboardQueries.Debaters,
      queryName: 'leaderboards.users',
      itemBuilder: (json) => UserEntry.fromJson(json),
      widgetBuilder: (item) => UserEntryWidget(entry: item));
  }
}
