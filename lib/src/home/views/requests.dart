import 'package:flutter/material.dart'; 
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/home/logic/bloc.dart';

import 'package:debateit/src/config/pagination/profile.dart';
import 'package:debateit/src/config/pagination/request.dart';


class RequestsView extends StatefulWidget { 
 
  const RequestsView({ super.key });

  @override
  _RequestsViewState createState() => _RequestsViewState();
}

class _RequestsViewState extends State<RequestsView> with TickerProviderStateMixin {
  RequestSource source = RequestSource.incoming;

  late PaginationBloc<Request> bloc = 
    PaginationBloc<Request>(
      client: Services.client,
      config: ProfileRequestsPaginationConfig(source));

  late final _controller = TabController(length: 2, vsync: this);

  @override
  void didChangeDependencies() {
    Future.microtask(() => 
      BlocProvider
        .of<HomePageBloc>(context)
        .add(HomePageAddedBottom(bottom: _buildTabBar(context)))
    );

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final header = Card(
      child: Container(

        padding: const EdgeInsets.symmetric(horizontal: Insets.sm),

        constraints: const BoxConstraints(minHeight: 50),

        child: DropdownButtonHideUnderline(
          child: DropdownButton<RequestSource>( 
            value: source, 
            isDense: true, isExpanded: true,
            onChanged: onSourceChanged,
            items: buildSourceEntries.toList()))));

    final views = TabBarView(
      controller: _controller,
      children: [
        PaginationProvider<Request>.fromBloc(
          key: const ValueKey('profile-requests'),
          bloc: bloc, header: header,
          padding: const EdgeInsets.symmetric(vertical: 10)),
          
        PaginationProvider<Request>(
          client: Services.client,
          key: const ValueKey('profile-open-requests'),
          config: ProfileOpenRequestsPaginationConfig(),
          padding: const EdgeInsets.symmetric(vertical: 10))
      ]
    );

    return Container(child: views);
  } 

  PreferredSizeWidget _buildTabBar(BuildContext context) {
    final text = Theme.of(context).textTheme;

    return TabBar(
      controller: _controller,
      tabs: [
        Tab(child: Text('Regular Requests', style: text.titleMedium)),
        Tab(child: Text('Open Requests', style: text.titleMedium)) 
      ]
    );
  }

  void onSourceChanged(RequestSource? value) {
    if (value == null) {
      return;
    }

    bloc.config = ProfileRequestsPaginationConfig(value);
    bloc.add(const PageRefresh());
    
    setState(() { source = value; });
  }

  Iterable<DropdownMenuItem<RequestSource>> get buildSourceEntries sync* {
    final labelStyle = context.text.headlineSmall;
    final selectedIcon = Icon(Icons.check_circle, color: context.colorScheme.secondary);
    
    yield DropdownMenuItem<RequestSource>(
      value: RequestSource.incoming,
      child: ListTile(
        dense: true,
        // leading: Icon(icon, color: context.colorScheme.primary),
        title: Text('Incoming Requests', style: labelStyle),
        trailing: source == RequestSource.incoming ? selectedIcon : null));

    yield DropdownMenuItem<RequestSource>(
      value: RequestSource.outgoing,
      child: ListTile(
        dense: true,
        // leading: Icon(icon, color: context.colorScheme.primary),
        title: Text('Outgoing Requests', style: labelStyle),
        trailing: source == RequestSource.outgoing ? selectedIcon : null));
          
    // yield DropdownMenuItem<String>(
    //   value: 'any',
    //   child: ListTile(
    //     dense: true,
    //     // leading: Icon(icon, color: context.colorScheme.primary),
    //     title: Text('All Requests', style: labelStyle),
    //     trailing: source == 'any' ? selectedIcon : null));
  }
}
