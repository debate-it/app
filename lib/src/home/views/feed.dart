import 'package:debateit_authentication/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/home/logic/bloc.dart';

import 'package:debateit/src/config/pagination/challenge.dart';
import 'package:debateit/src/config/pagination/profile.dart';

import 'package:debateit/src/discussions/request/open/feed.dart';
import 'package:debateit/src/widgets/misc/page_header.dart';


class FeedView extends StatefulWidget { 
  const FeedView({ super.key });

  @override
  _FeedViewState createState() => _FeedViewState();
}

class _FeedViewState extends State<FeedView> with TickerProviderStateMixin {
  late TabController controller = TabController(length: 2, vsync: this);

  late final feed = PaginationBloc(config: FeedPagination(), client: Services.client);

  @override
  void didChangeDependencies() {
    Future.microtask(() => 
      BlocProvider
        .of<HomePageBloc>(context)
        .add(HomePageAddedBottom(bottom: buildTabBar(context)))
    );

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) { 

    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        feed.add(const PageRefresh());
      },
      child: TabBarView(
        controller: controller,
        children: [ buildFeedView(context), buildChallengeView(context) ]),
    );
  } 

  PreferredSizeWidget buildTabBar(BuildContext context) {
    final text = Theme.of(context).textTheme;

    return TabBar(
      labelPadding: EdgeInsets.zero,
      controller: controller, 
      tabs: [
        Tab(child: Text('Feed', style: text.titleMedium)),
        Tab(child: Text('Challenges', style: text.titleMedium))
      ]
    );
  }

  Widget buildTab(BuildContext context, String label) { 
    return Tab(child: Text(label, style: context.text.titleMedium));
  }

  Widget buildFeedView(BuildContext context) {
    return PaginationProvider.fromBloc(
      bloc: feed,
      widgetToInsert: const OpenRequestFeed(),
      padding: const EdgeInsets.symmetric(vertical: 18),
      header: const PageHeader(text: 'Your latest updates'));
  }

  Widget buildChallengeView(BuildContext context) {
    return PaginationProvider(
      client: Services.client,
      config: ChallengePagination(),
      padding: const EdgeInsets.symmetric(vertical: 15),
      header: const PageHeader(text: 'Latest Challenges')); 
  }
}
