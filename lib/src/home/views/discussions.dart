import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/config/pagination/profile.dart';

class DiscussionsView extends StatefulWidget { 
  const DiscussionsView({ super.key });

  @override
  State<DiscussionsView> createState() => _DiscussionsViewState();
}

class _DiscussionsViewState extends State<DiscussionsView> {
  DebateStatus? status = DebateStatus.active;

  late PaginationBloc<Discussion> bloc = PaginationBloc(
    config: ProfileDiscussionsPaginationConfig(status!),
    client: Services.client);

  @override
  Widget build(BuildContext context) {
    return PaginationProvider.fromBloc(
      bloc: bloc,
      header: _buildHeader(context),
      key: const ValueKey<String>('profile-debates'),
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10));
  } 

  void onStatusChange(DebateStatus? value) {
    if (value == null) {
      return;
    }

    bloc.config = ProfileDiscussionsPaginationConfig(value);
    bloc.add(const PageRefresh());

    setState(() { status = value; });
  }

  Widget _buildHeader(BuildContext context) {
    final labelStyle = context.text.headlineSmall;
    final selectedIcon = Icon(Icons.check_circle, color: context.colorScheme.secondary);

    final items = [
      DropdownMenuItem<DebateStatus>(
        value: DebateStatus.active,
        child: ListTile(
          dense: true,
          // leading: Icon(icon, color: context.colorScheme.primary),
          title: Text('Active Discussions', style: labelStyle),
          trailing: status == DebateStatus.active ? selectedIcon : null)),

      DropdownMenuItem<DebateStatus>(
        value: DebateStatus.voting,
        child: ListTile(
          dense: true,
          // leading: Icon(icon, color: context.colorScheme.primary),
          title: Text('Voting Discussions', style: labelStyle),
          trailing: status == DebateStatus.voting ? selectedIcon : null)),
          
      DropdownMenuItem<DebateStatus>(
        value: DebateStatus.finished,
        child: ListTile(
          dense: true,
          // leading: Icon(icon, color: context.colorScheme.primary),
          title: Text('Finished Discussions', style: labelStyle),
          trailing: status == DebateStatus.finished ? selectedIcon : null)),
    ];

    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: Insets.sm),

        constraints: const BoxConstraints(maxHeight: 50, minHeight: 50),

        child: DropdownButtonHideUnderline(
          child: DropdownButton<DebateStatus>( 
            isDense: true, 
            isExpanded: true,

            items: items,
            value: status, 
            onChanged: onStatusChange))));
  }
}
