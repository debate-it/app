import 'package:debateit/src/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit/src/initiate/widgets/InitiateButton.dart';

import 'package:debateit/src/home/drawer.dart';
import 'package:debateit/src/home/logic/bloc.dart';
import 'package:debateit/src/home/navigation.dart';

import 'package:debateit/src/home/views/feed.dart';
import 'package:debateit/src/home/views/requests.dart';
import 'package:debateit/src/home/views/discussions.dart';
import 'package:debateit/src/home/views/leaderboards.dart';

import 'package:debateit/src/widgets/pages/PageAppBar.dart';


enum HomePageTab { feed, discussions, requests, leaderboards }

extension HomePageTabParser on HomePageTab {
  static HomePageTab? parse(String value) {
    switch (value) {
      case 'feed':
        return HomePageTab.feed;
      case 'discussions':
        return HomePageTab.discussions;
      case 'requests':
        return HomePageTab.requests;
      case 'leaderboards':
        return HomePageTab.leaderboards;

      default:
        return null;
    }
  }
}


class HomePage extends StatefulWidget {
  const HomePage({this.initialTab});

  factory HomePage.tab(String? tab) {
    if (tab == null) {
      return const HomePage();
    }
    
    return HomePage(initialTab: HomePageTabParser.parse(tab));
  }

  final HomePageTab? initialTab;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  static const Map<HomePageTab, Widget> pages = {
    HomePageTab.feed: FeedView(key: PageStorageKey('Feed')),
    HomePageTab.discussions: DiscussionsView(key: PageStorageKey('Debates')),
    HomePageTab.requests: RequestsView(key: PageStorageKey('Requests')),
    HomePageTab.leaderboards: LeaderboardsView(key: PageStorageKey('Trenging'))
  };

  PreferredSizeWidget? bottom;

  late final HomePageTab initialTab = widget.initialTab ?? HomePageTab.feed;
  final PageStorageBucket _bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomePageBloc(tab: initialTab),
      child: BlocBuilder<HomePageBloc, HomePageState>(
        buildWhen: (previous, current) => previous.tab != current.tab || current.bottom != previous.bottom,
        builder: (context, state) => SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: MainAppBar(bottom: state.bottom),

            bottomNavigationBar: const BottomAppBar(
              shape: CircularNotchedRectangle(),
              child: HomeBottomNavigationBar()
            ),

            floatingActionButton: InitiateButton(ticker: this),
            floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,

            drawer: const HomePageDrawer(),

            body:  PageStorage(
              key: ValueKey(Services.profileService.info!.profile.username),
              bucket: _bucket, 
              child: pages[state.tab]!)))));
  }
}
