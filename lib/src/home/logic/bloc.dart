library home_logic;

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:debateit/src/home/home.dart';
import 'package:flutter/material.dart';

part 'event.dart';
part 'state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  HomePageBloc({HomePageTab? tab}) : super(HomePageState(tab: tab ?? HomePageTab.feed)) {
    on<HomePageTabChanged>((event, emit) => emit(HomePageState(tab: event.tab)));
    on<HomePageAddedBottom>((event, emit) => emit(state.copyWith(bottom: event.bottom)));
  }
}
