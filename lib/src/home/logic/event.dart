part of home_logic;

abstract class HomePageEvent extends Equatable {
  const HomePageEvent();

  @override
  List<Object?> get props => const [];
}

class HomePageTabChanged extends HomePageEvent {
  final HomePageTab tab;

  const HomePageTabChanged({required this.tab});
  
  @override
  List<Object?> get props => [tab];
}

class HomePageAddedBottom extends HomePageEvent {
  final PreferredSizeWidget bottom;

  const HomePageAddedBottom({required this.bottom});
  
  @override
  List<Object?> get props => [bottom];
}
