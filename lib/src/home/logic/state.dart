part of home_logic;

class HomePageState extends Equatable {
  final HomePageTab tab;
  final PreferredSizeWidget? bottom;

  const HomePageState({this.tab = HomePageTab.feed, this.bottom });

  HomePageState copyWith({ HomePageTab? tab, PreferredSizeWidget? bottom}) =>
    HomePageState(tab: tab ?? this.tab, bottom: bottom ?? this.bottom);

  @override
  List<Object?> get props => [tab, bottom];
}
