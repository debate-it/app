import 'package:flutter/material.dart';
import 'package:debateit_models/discussions/main.dart';
import 'package:debateit/src/discussions/challenge/views/preview.dart';


class ChallengeEntry {
  int score;
  Challenge challenge;

  ChallengeEntry.fromJson(Map<String, dynamic> json)
    : challenge = Challenge.fromJson(json['entry'] as Map<String, dynamic>),
      score = json['score'] as int; 
}

class ChallengeEntryWidget extends StatelessWidget {
  final ChallengeEntry entry;
  
  const ChallengeEntryWidget({ super.key, required this.entry });

  @override
  Widget build(BuildContext context) {
    return ChallengePreview(challenge: entry.challenge);
  }
}
