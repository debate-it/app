import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_models/models.dart';

import 'package:debateit/src/discussions/user/views/preview.dart';

class UserEntry {
  int score;
  UserProfile user;

  UserEntry.fromJson(Map<String, dynamic> json)
    : user = UserProfile.fromJson(json['entry'] as Map<String, dynamic>),
      score = json['score'] as int; 
}

class UserEntryWidget extends StatelessWidget {
  final UserEntry entry;

  const UserEntryWidget({ super.key, required this.entry});

  @override
  Widget build(BuildContext context) {
      return UserPreview(
        user: entry.user,
        side: Column(
          children: [
            Text('${entry.score}', style: context.text.headlineMedium
              ?.copyWith(color: context.colors.primary)),

            Text('votes', style: context.text.headlineSmall
              ?.copyWith(color: context.colorScheme.secondary))
          ],
        ),
      );
  }
}
