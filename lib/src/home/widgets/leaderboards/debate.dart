import 'package:flutter/material.dart';
import 'package:debateit_models/discussions/main.dart';
import 'package:debateit/src/discussions/debate/views/preview.dart';


class DebateEntry {
  int score;
  Debate debate;

  DebateEntry.fromJson(Map<String, dynamic> json)
    : debate = Debate.fromJson(json['entry'] as Map<String, dynamic>),
      score = json['score'] as int; 
}

class DebateEntryWidget extends StatelessWidget {
  final DebateEntry entry;
  
  const DebateEntryWidget({ super.key, required this.entry});

  @override
  Widget build(BuildContext context) {
    return DebatePreview(debate: entry.debate);
  }
}
