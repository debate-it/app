import 'package:flutter/material.dart'; 
import 'package:debateit_core/core.dart';

import 'package:debateit/src/home/home.dart';
import 'package:debateit/src/home/logic/bloc.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/profile.dart';

import 'package:debateit/src/widgets/misc/badge.dart';
import 'package:debateit/src/widgets/misc/count_badge.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class HomeBottomNavigationBar extends StatefulWidget {
  const HomeBottomNavigationBar(); 

  @override
  State<HomeBottomNavigationBar> createState() => _HomeBottomNavigationBarState();
}

class _HomeBottomNavigationBarState extends State<HomeBottomNavigationBar> {
  ProfileService get service => Services.profileService;

  void update() => setState(() { });

  @override
  void initState() {
    service.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    service.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) { 
    final bloc = BlocProvider.of<HomePageBloc>(context);

    return BlocBuilder<HomePageBloc, HomePageState>(
      bloc: bloc,
      builder: (context, state) {
        final tab = state.tab;

        return SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 2,
                child: InkWell(
                  onTap: () => bloc.add(const HomePageTabChanged(tab: HomePageTab.feed)),
                  child: TabIcon(Icons.school, label: 'Feed', isActive: tab == HomePageTab.feed)),
              ),

              Expanded(
                flex: 2,
                child: InkWell(
                  onTap: () => bloc.add(const HomePageTabChanged(tab: HomePageTab.discussions)),
                  child: Badge(
                    badge: Positioned(
                      right: 18, top: 3,
                      child: FloatingCountBadge(value: service.info?.indicators.debates ?? 0)),
                    child: TabIcon(Icons.comment, label: 'Discussions', isActive: tab == HomePageTab.discussions))),
              ), 

              const Spacer(),

              Expanded(
                flex: 2,
                child: InkWell(
                  onTap: () => bloc.add(const HomePageTabChanged(tab: HomePageTab.requests)),
                  child: Badge(
                    badge: Positioned(
                      right: 18, top: 3,
                      child: FloatingCountBadge(value: service.info?.indicators.requests ?? 0)),
              
                    child: TabIcon(Icons.inbox, label: 'Requests', isActive: tab == HomePageTab.requests))),
              ),

              Expanded(
                flex: 2,
                child: InkWell(
                  onTap: () => bloc.add(const HomePageTabChanged(tab: HomePageTab.leaderboards)),
                  child: TabIcon(Icons.filter_hdr, label: 'Trending', isActive: tab == HomePageTab.leaderboards)),
              )
            ]
          ) 
        );
      },
    );
  }
}

class TabIcon extends StatelessWidget {
  final IconData iconData; 
  final String label; 

  final bool isActive;

  const TabIcon(this.iconData, {super.key, required this.label, this.isActive = false});

  @override
  Widget build(BuildContext context) { 
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(iconData,
          size: 18,
          color: isActive
            ? context.colors.primary
            : context.colors.accent
        ),

        Text(label, 
          maxLines: 1,
          overflow: TextOverflow.visible,
          style: context.text.caption
        ) 
      ]
    );

  }
}
