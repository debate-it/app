import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:beamer/beamer.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:debateit_core/core.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/services/profile.dart';

import 'package:debateit/src/discussions/user/provider.dart';
import 'package:debateit/src/discussions/user/widgets/badge.dart';
import 'package:debateit/src/discussions/user/widgets/progress.dart';
import 'package:debateit/src/discussions/user/widgets/stats.dart';


class HomePageDrawer extends StatelessWidget {
  const HomePageDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 18.0,
      child: ColoredBox(
        color: context.colors.background,
        child: Column( 
          children: [
            const DrawerProfileHeader(),
    
            DrawerEntry(
              title: 'Profile',
              subtitle: 'Access your user profile',
              icon: Icons.person,
              onTap: () => redirect(context, path: '/profile')),
    
            DrawerEntry(
              title: 'Followers',
              subtitle: 'List your followers',
              icon: Icons.people,
              onTap: () => redirect(context, path: '/profile/followers')),
            
            DrawerEntry(
              title: 'Subscriptions',
              subtitle: 'List your user and category subscriptions',
              icon: Icons.favorite,
              onTap: () => redirect(context, path: '/profile/subscriptions')),
    
            DrawerEntry(
              title: 'Settings',
              subtitle: 'Change the app settings',
              icon: Icons.settings,
              onTap: () => redirect(context, path: '/profile/settings')),
    
            DrawerEntry(
              title: 'Tutorial',
              subtitle: 'View the app tutorial',
              icon: Icons.info,
              onTap: () => redirect(context, path: '/tutorial')),
    
            DrawerEntry(
              title: 'Logout',
              subtitle: 'Log out from your profile',
              icon: Icons.directions_run,
              onTap: () => _safeLogout(context)),
    
            const Spacer(),
            const Divider(),
            const ThemeSwitch()
          ]
        )
      )
    );
  }

  void redirect(BuildContext context, {required String path}) => Beamer.of(context).beamToNamed(path);

  Future<void> _safeLogout(BuildContext context) async { 
    // final navigator = Beamer.of(context);
    
    Scaffold.of(context).closeDrawer();

    // Delete token to stop receiving notifications
    await FirebaseMessaging.instance.deleteToken();


    // Clean app session
    await Services.authentication.logout(); 
    await Services.profileService.unsetDeviceToken();

    // Clear the navigator stack
    // navigator.beamToReplacementNamed('/login');

  }
}


class DrawerEntry extends StatelessWidget {
  final String title;
  final String subtitle;
  final IconData icon;

  final VoidCallback onTap;

  const DrawerEntry({
    super.key,
    required this.title,
    required this.subtitle,
    required this.icon,
    required this.onTap
  });

  @override
  Widget build(BuildContext context) {
    final titleStyle = context.text.bodyLarge;
    final subtitleStyle = context.text.bodyMedium;

    return ListTile(
      dense: true,
      
      title: Text(title, style: titleStyle),
      subtitle: Text(subtitle, style: subtitleStyle),

      leading: Icon(icon),
      trailing: Icon(Icons.keyboard_arrow_right, color: context.colorScheme.primary),

      onTap: onTap,
    );
  }
}


class DrawerProfileHeader extends StatelessWidget {
  const DrawerProfileHeader({super.key});

  @override
  Widget build(BuildContext context) {
    const stats = SizedBox(width: double.infinity, child: UserStatsWidget());
    const progress = UserProgressIndicator();

    return ValueListenableBuilder(
      valueListenable: ValueNotifier(Services.profileService),
      builder: (BuildContext context, ProfileService value, child) {
        final avatar = Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: const EdgeInsets.all(Insets.xs), 
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: UserBadge(
                radius: 16,
                fontSize: 18.0,
                subtitleFontSize: 14.0,
                profile: value.info!.profile,
                transparent: true))));

        return ChangeNotifierProvider<UserProvider>(
          create: (_) => UserProvider(user: value.info!.profile),
          child: DrawerHeader(
            padding: const EdgeInsets.all(Insets.sm),
            decoration: BoxDecoration(color: context.colorScheme.surface),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[ avatar, progress, stats ])));
      }
    );
  } 
}

class ThemeSwitch extends StatefulWidget {
  const ThemeSwitch({ super.key });
  
  @override
  _ThemeSwitchState createState() => _ThemeSwitchState();
}

class _ThemeSwitchState extends State<ThemeSwitch> {
  late bool value;

  @override
  void initState() {
    value = true;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: Hive.box(ConfigProvider.themeBox).listenable(),
      builder: (BuildContext context, Box box, Widget? child) {  
        final value = box.get(ConfigProvider.darkModeKey) as bool? ?? false;

        return ListTile(
          dense: true,
          title: const Text('Dark mode'),
          trailing: Switch.adaptive(value: value, onChanged: onChange));
      },
    );
  }

  // ignore: avoid_positional_boolean_parameters
  Future<void> onChange(bool switchValue) async {
    final config = ConfigProvider.of(context);

    await config.changeTheme(switchValue ? AppThemeType.dark : AppThemeType.light);

    setState(() {
      value = switchValue;
    });
  }
}
