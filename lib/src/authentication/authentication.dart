library authentication;

import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_graphql/graphql.dart';
import 'package:debateit_graphql/mutations/main.dart';
import 'package:debateit_authentication/authentication.dart';

import 'package:debateit/src/services.dart';
import 'package:debateit/src/widgets/buttons/buttons.dart';
import 'package:debateit/src/widgets/misc/text.dart';
import 'package:debateit/src/widgets/pages/FixedPage.dart';
import 'package:debateit/src/widgets/pages/ScrollablePage.dart';
import 'package:debateit/src/widgets/forms/wrappers.dart';

part 'views/activation.dart';
part 'views/forgot.dart';
part 'views/login.dart';
part 'views/main.dart';
part 'views/signup.dart';
