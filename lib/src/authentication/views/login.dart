part of '../authentication.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final form = FormGroup({
    'username': FormControl<String>(validators: [
      Validators.required, 
      Validators.minLength(3),
      Validators.maxLength(32),
          Validators.pattern(RegExp('^[a-zA-Z][a-zA-Z0-9-._]+\$'))
    ]),
    'password': FormControl<String>(validators: [Validators.required])
  });

  String? error;

  @override
  void initState() {
    form.statusChanged.listen((event) => setState(() { }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BarlessScrollableView(
      padding: const EdgeInsets.all(Insets.xl),
      body: ReactiveForm(
        formGroup: form,
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,

          children: [
            const Image(
              image: AssetImage('assets/images/logo-base.png'),
              width: 150, height: 150),
              
            FittedBox(
              fit: BoxFit.scaleDown, 
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Insets.lg),
                child: Text('Login to Debate It', 
                  textAlign: TextAlign.center,
                  style: context.text.headlineMedium))),
                  
            const SizedBox(height: Insets.xl),
    
            AppTextField(
              labelText: 'Username',
              formControlName: 'username',
              textAlign: TextAlign.center,
              validationMessages: {
                ValidationMessage.required: (_) => 'Username should not be empty',
                ValidationMessage.minLength: (_) => 'Username should be at least 3 characters long',
                ValidationMessage.maxLength: (_) => 'Username should be at most 32 characters long',
                ValidationMessage.pattern: (control) => 'Username can only use latin characters, numbers or symbols _ . -'
              },
            ),

            const SizedBox(height: Insets.lg),
            
            AppTextField(
              maxLines: 1,
              obscureText: true,
              labelText: 'Password',
              formControlName: 'password',
              textAlign: TextAlign.center,
              validationMessages: {
                ValidationMessage.required: (control) => 'The password must not be empty',
              },
            ),
            
            if (error != null) 
              Padding(
                padding: const EdgeInsets.all(Insets.md),
                child: ErrorText(error!, textAlign: TextAlign.center)),

            const SizedBox(height: Insets.sm),
    
            SubmitButton(label: 'Sign In', onPressed: login),
                
            const SizedBox(height: Insets.lg),
            
            SecondaryBtn(
              label: 'Sign Up', 
              icon: Icons.person_add,
              onPressed: () => context.beamToNamed('/signup')),
    
            TextButton(
              onPressed: () => context.beamToNamed('/forgot-password'),
              child: Text('Forgot password?',
                style: context.text.titleLarge?.copyWith(
                  decoration: TextDecoration.underline,
                  color: context.colorScheme.primary)))
          ]
        )
      ),
    );
  }
  
  Future<void> login() async {
    try {
      await Services.authentication.login(
        username: form.control('username').value as String,
        password: form.control('password').value as String
      );
    } on QueryException catch (err) {
      setState(() { error = err.toString(); });
    }
  }
}
