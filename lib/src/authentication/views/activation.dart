part of '../authentication.dart';


class ActivationPage extends StatefulWidget {
  const ActivationPage({super.key});

  @override
  State<ActivationPage> createState() => _ActivationPageState();
}

class _ActivationPageState extends State<ActivationPage> {
  String? error;

  @override
  Widget build(BuildContext context) {
    final baseStyle = TextStyle(color: context.colorScheme.onSurface); 
    
    final titleStyle = baseStyle.merge(const TextStyle(fontWeight: FontWeight.w200, fontSize: 28)); 
    final descriptionStyle = baseStyle.merge(const TextStyle(fontWeight: FontWeight.w300, fontSize: 18));

    return BarlessView(
      padding: const EdgeInsets.all(Insets.sm),
      body: Container(
        padding: const EdgeInsets.all(Insets.md),
        
        child: ReactiveFormBuilder(
          form: () => FormGroup({
            'code': FormControl<String>(
              validators: [
                Validators.pattern(RegExp('[A-Z0-9]{6}')),
                Validators.maxLength(6)
              ]
            )
          }),
          builder: (context, form, child) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Enter Activation Code', 
                  style: titleStyle,
                  textAlign: TextAlign.center),

                Text('Activation code was sent to your email',
                  style: descriptionStyle,
                  textAlign: TextAlign.center),
                
                ReactiveTextField(
                  formControlName: 'code',
                  
                  autofocus: true,
                  autocorrect: false,

                  textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.center,

                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  textCapitalization: TextCapitalization.characters,

                  keyboardAppearance: Brightness.dark,
                  enableSuggestions: false, 

                  style: context.text.titleLarge,
        
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(6),
                    FilteringTextInputFormatter.allow(RegExp('[A-Z0-9]'))
                  ],
                  validationMessages: {
                    ValidationMessage.pattern: (control) => 'Please, enter a valid activation code'
                  },
                ),

                if (error != null) 
                  Padding(
                    padding: const EdgeInsets.all(Insets.md),
                    child: ErrorText(error!, textAlign: TextAlign.center)),

                const SizedBox(height: Insets.xl),

                SubmitButton(label: 'SUBMIT', onPressed: () => _activate(form.control('code').value as String) 
                    )
              ]
            );
          },
        )
      )
    );
  }

  Future<void> _activate(String code) async {
    try {
      await Services.authentication.activate(code: code);
    } on QueryException catch (err) {
      setState(() { error = err.message; });
    }
  }
}
