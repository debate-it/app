part of '../authentication.dart';

enum SignupStage { mainCredentials, extraInfo }


class SignupPage extends StatefulWidget {
  const SignupPage({super.key});

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  late SignupStage stage = SignupStage.mainCredentials;

  late final form = FormGroup({
    'main': FormGroup({
      'username': FormControl<String>(
        validators: [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(32),
          Validators.pattern(RegExp('^[a-zA-Z][a-zA-Z0-9-._]+\$'))
        ],
        asyncValidators: [_uniqueUsername]
      ),
      'email': FormControl<String>(
        validators: [Validators.required, Validators.email],
        asyncValidators: [_uniqueEmail]
      ),

      'password': FormControl<String>(validators: [Validators.required, Validators.minLength(8)]),
      'confirmPassword': FormControl<String>(validators: [Validators.required]),
    }, validators: [Validators.mustMatch('password', 'confirmPassword')]),

    'extra': FormGroup({
      'fullname': FormControl<String>(validators: [Validators.maxLength(250)]),
      'about': FormControl<String>(validators: [Validators.maxLength(500)]),

      'country': FormControl<String>(),
      'education': FormControl<String>(),
      'occupation': FormControl<String>(),
      'politics': FormControl<String>(),
      'religion': FormControl<String>(),
    })

  });

  Future<Map<String, dynamic>?> _uniqueEmail(AbstractControl<dynamic> control) async {
    try {
      return await Services.client.fetch(
        query: AuthQueries.verifyEmail,
        variables: { 'email': control.value }
      )
        .then((value) => null);
    } catch (err) {
      control.markAsTouched();
      return const { 'unique': false };
    }
  } 
  
  Future<Map<String, dynamic>?> _uniqueUsername(AbstractControl<dynamic> control) async {
    try {
      return await Services.client.fetch(
        query: AuthQueries.verifyUsername, 
        variables: { 'username': control.value }
      )
        .then((value) => null);

    } catch (err) {
      control.markAsTouched();
      return const { 'unique': false };
    }
  } 

  Future<void> _onNext() async {
    if (stage == SignupStage.mainCredentials) {
      setState(() {
        stage = SignupStage.extraInfo;
      });

      return;
    }
    
    final navigator = Beamer.of(context);

    await Services.authentication.signup(
      username: form.control('main.username').value as String,
      email: form.control('main.email').value as String,
      password: form.control('main.password').value as String,
      extra: form.control('extra').value as Map<String, dynamic>
    );

    navigator.beamBack();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      pages: [
        MaterialPage(
          child: _SignupMainCredentialsForm(
            form: form.control('main') as FormGroup, 
            onSave: _onNext)),
  
        if (stage.index > 0) 
          MaterialPage(
            child: _SignupExtraInfoForm(
              form: form.control('extra') as FormGroup, 
              onSave: _onNext)),
      ]
    );
  }
}

class _SignupMainCredentialsForm extends StatelessWidget {
  const _SignupMainCredentialsForm({required this.form, required this.onSave});
  
  final FormGroup form;
  final VoidCallback onSave;

  @override
  Widget build(BuildContext context) {
    return BarlessScrollableView(
      padding: const EdgeInsets.all(Insets.md),
      body: ReactiveForm(
        formGroup: form,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: Insets.xl * 3),

            Padding(
              padding: const EdgeInsets.all(Insets.lg),
              child: Center(
                child: Text('Signup to \nDebate It',
                textAlign: TextAlign.center,
                  style: context.text.headlineLarge
                    ?.copyWith(color: context.colors.accent))
              ),
            ),

            AppTextField(
              labelText: 'Username',
              formControlName: 'username',
              validationMessages: {
                ValidationMessage.required: (_) => 'Username should not be empty',
                ValidationMessage.minLength: (_) => 'Username should be at least 3 characters long',
                ValidationMessage.maxLength: (_) => 'Username should be at most 32 characters long',
                ValidationMessage.pattern: (control) => 
                  'Username has to start from a letter and can only use latin characters, numbers or symbols _ . -',

                'unique': (control) => 'Username is already taken'
              },
            ),
            
            const SizedBox(height: Insets.md),

            AppTextField(
              labelText: 'Email',
              formControlName: 'email',
              validationMessages: {
                ValidationMessage.required: (_) => 'Email should not be empty',
                ValidationMessage.email: (_) => 'Please enter a valid email address',
                'unique': (control) => 'There already is an account with that email'
              },
            ),
            
            const SizedBox(height: Insets.md),
            
            AppTextField(
              obscureText: true,
              maxLines: 1,
              labelText: 'Password',
              formControlName: 'password',
              validationMessages: {
                ValidationMessage.required: (_) => 'Password should not be empty',
              },
            ),

            const SizedBox(height: Insets.md),
            
            AppTextField(
              obscureText: true,
              maxLines: 1,
              labelText: 'Confirm Password',
              formControlName: 'confirmPassword',
              validationMessages: {
                ValidationMessage.mustMatch: (_) => "Passwords don't match"
              },
            ),
            
            const SizedBox(height: Insets.lg),
    
            SubmitButton(label: 'Continue', onPressed: onSave)
          ],
        )
      ),
    );
  }
}

class _SignupExtraInfoForm extends StatelessWidget {
  const _SignupExtraInfoForm({required this.form, required this.onSave});

  final FormGroup form;
  final VoidCallback onSave;

  @override
  Widget build(BuildContext context) {
    return BarlessScrollableView(
      padding: const EdgeInsets.all(Insets.md),
      body: ReactiveForm(
        formGroup: form,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: Insets.xl),

            Padding(
              padding: const EdgeInsets.all(Insets.xs),
              child: Text('Please tell us more about yourself',
                textAlign: TextAlign.center,
                style: context.text.headlineMedium),
            ),

            Padding(
              padding: const EdgeInsets.all(Insets.md),
              child: Text('None of these are required to be filled, but we will appreciate you sharing',
                style: context.text.titleMedium,
                textAlign: TextAlign.center,),
            ),

            const SizedBox(height: Insets.lg),

            AppTextField(
              labelText: 'Fullname',
              formControlName: 'fullname',
              validationMessages: {
                ValidationMessage.maxLength: (control) => 'Full name should be at most 250 characters'
              },
            ),
            
            const SizedBox(height: Insets.md),

            AppTextField(
              maxLines: 5,
              labelText: 'Tell us about yourself',
              formControlName: 'about',
              validationMessages: {
                ValidationMessage.maxLength: (control) => 'Please limit about section to 500 characters'
              },
            ),
            
            const SizedBox(height: Insets.md),
            
            const AppTextField(labelText: 'Country', formControlName: 'country'),
            const SizedBox(height: Insets.md),

            const AppTextField(labelText: 'Occupation', formControlName: 'occupation'),
            const SizedBox(height: Insets.md),

            const AppTextField(labelText: 'Education', formControlName: 'education'),
            const SizedBox(height: Insets.md),

            const AppTextField(labelText: 'Politics', formControlName: 'politics'),
            const SizedBox(height: Insets.md),

            const AppTextField(labelText: 'Religion', formControlName: 'religion'),
            const SizedBox(height: Insets.md),
    
            SubmitButton(label: 'Sign Up', onPressed: onSave)
          ],
        )
      ),
    );
  }
}
