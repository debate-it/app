part of '../authentication.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final form = FormGroup({
    'email': FormControl<String>(validators: [Validators.required, Validators.email])
  });

  String? error;

  @override
  Widget build(BuildContext context) {
    return BarlessScrollableView(
      centerBody: true,
      padding: const EdgeInsets.all(Insets.md),
      body:  Container(
        height: MediaQuery.of(context).size.height / 2,
        padding: const EdgeInsets.all(Insets.md),
        child: ReactiveForm(
          formGroup: form,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                padding: const EdgeInsets.all(Insets.lg),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(Insets.md),
                      child: Text('Password Reset', style: context.text.headlineMedium
                        ?.copyWith(color: context.colorScheme.onSurface))),

                    Text('Please, enter your email and we will send you a reset link',
                      textAlign: TextAlign.center,
                      style: context.text.headlineSmall
                        ?.copyWith(color: context.colorScheme.onSurface))
                  ],
                )
              ),

              ReactiveTextField(
                formControlName: 'email',
                validationMessages: {
                  ValidationMessage.required: (_) => 'Email should not be empty',
                  ValidationMessage.email: (_) => 'Please enter a valid email'
                },
              ),

              if (error != null)
                Padding(
                  padding: const EdgeInsets.all(Insets.md),
                  child: ErrorText(error!)),

              PrimaryBtn(label: 'Submit', onPressed: form.valid ? submit : null)

              // buildTitle(context),
              // buildField(context),
              // buildError(context),
              // buildSubmit(context)
            ],
          )
        ),
      )
    );
  }

  Future<void> submit() async {
    final navigator = Beamer.of(context);

    setState(() { error = null; });

    try {
      await Services.client
        .mutate(mutation: SettingsQueries.ForgotPassword, variables: form.value);

      await showDialog(context: context, builder: _buildDialog);

      // ignore: use_build_context_synchronously
      navigator.beamBack();
    } on Exception catch (err) {
      setState(() { error = err.toString(); });
    }
  }

  Widget _buildDialog(BuildContext context) {
    final baseStyle = context.text.bodyLarge;
    final titleStyle = context.text.headlineMedium;

    final content = Container(
      padding: const EdgeInsets.all(Insets.md),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text('Email Sent', style: titleStyle, textAlign: TextAlign.center),

          const Padding(padding: EdgeInsets.all(Insets.xs)),

          Text('We have sent you an email with password reset link',
            style: baseStyle, textAlign: TextAlign.center),

          const Padding(padding: EdgeInsets.all(Insets.xs)),

          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('OK', style: baseStyle))
        ],
      )
    );

    return Dialog(child: Card(child: content)); 
  }
}
