import 'package:debateit/src/widgets/pages/FixedPage.dart';
import 'package:debateit_core/core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';





class NotificationSettings extends StatelessWidget {
  const NotificationSettings({ super.key });
  
  static const boxName = 'notification-settings';

  static Future<void> initialize() async {
    await Hive.openBox<bool?>(boxName);
  }

  @override
  Widget build(BuildContext context) {
    return FixedViewPage(
      body: ListView(
        children: const [
          _NotificationSettingEntry(label: 'Followers', setting: 'followers'),
          _NotificationSettingEntry(label: 'Votes', setting: 'votes'),
          _NotificationSettingEntry(label: 'Comments', setting: 'comments'),
          _NotificationSettingEntry(label: 'Requests', setting: 'requests'),
          _NotificationSettingEntry(label: 'discussions', setting: 'discussions'),
        ]
      )
    );
  }
}

class _NotificationSettingEntry extends StatelessWidget {
  final String setting;
  final String label;

  const _NotificationSettingEntry({ required this.setting, required this.label });

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: Hive.box<bool?>(NotificationSettings.boxName).listenable(keys: [ setting ]),
      builder: (context, Box<bool?> box, child) {
        return SwitchListTile(
          value: box.get(setting) ?? true, 
          title: Text(label, style: context.text.bodyMedium),
          onChanged: (value) => box.put(setting, value));
      },
    );
  }
}
