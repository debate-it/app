import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/config/pagination/multi.dart';
import 'package:debateit/src/config/pagination/user.dart';

part 'event.dart';
part 'state.dart';


class SearchBloc extends Bloc<SearchQueryEvent, SearchState> {
  static const PageParams = { 'limit': 5 };

  SearchBloc() : super(const SearchUninitialized()) {
    on<SearchQueryEvent>((event, emit) {
      emit(const SearchLoading());

      emit(SearchFinished(
        byTags: PaginationBloc(client: Services.client, config: SearchTagsPaginationConfig(event.query)),
        byCategory: PaginationBloc(client: Services.client, config: SearchCategoryPaginationConfig(event.query)),
        byTopic: PaginationBloc(client: Services.client, config: SearchTopicPaginationConfig(event.query)),
        users: PaginationBloc(client: Services.client, config: SearchUserPaginationConfig(event.query)), 
      ));
    });
  }
}
