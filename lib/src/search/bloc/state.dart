part of 'bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override List<Object?> get props => const [];
}

class SearchUninitialized extends SearchState {
  const SearchUninitialized();
} 

class SearchLoading extends SearchState {
  const SearchLoading();
}

class SearchError extends SearchState {
  const SearchError();
}

class SearchFinished extends SearchState {
  final PaginationBloc<UserProfile> users;

  final PaginationBloc<Discussion> byTopic;
  final PaginationBloc<Discussion> byTags;
  final PaginationBloc<Discussion> byCategory;
  
  const SearchFinished({
    required this.byTags,
    required this.byCategory,
    required this.users,
    required this.byTopic
  });

}

// /// Describes what is contained in the result object
// enum SearchResultType {
//   discussions,
//   users
// }

// class SearchResult<T extends Pageable> {
//   final SearchResultType type;
//   final List<T> items;

//   SearchResult({ required this.type, required this.items });
// }
