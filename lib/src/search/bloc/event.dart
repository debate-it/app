part of 'bloc.dart';

class SearchQueryEvent extends Equatable {
  final String query;

  const SearchQueryEvent(this.query); 

  @override
  String toString() => 'SearchQueryEvent { query: $query }';
  
  @override
  List<Object?> get props => [query];
}
