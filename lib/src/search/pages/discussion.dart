import 'package:flutter/material.dart';

import 'package:debateit/src/search/pages/base.dart';
import 'package:debateit/src/config/pagination/debate.dart';
 

class DebateSearchPage extends StatelessWidget {
  final String query;

  const DebateSearchPage({ required this.query }); 

  @override
  Widget build(BuildContext context) {
    return SearchPage(
      config: DebateSearchPaginationConfig(query),
      queryText: query,
    );
  }
}
