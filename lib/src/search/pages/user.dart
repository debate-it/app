import 'package:flutter/material.dart';

import 'package:debateit/src/search/pages/base.dart';
import 'package:debateit/src/config/pagination/user.dart';


class UserSearchPage extends StatelessWidget {
  final String query;

  const UserSearchPage({ required this.query }); 

  @override
  Widget build(BuildContext context) {
    return SearchPage(config: UserSearchPaginationConfig(query), queryText: query);
  }
}
