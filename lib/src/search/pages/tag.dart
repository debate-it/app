import 'package:flutter/material.dart';

import 'package:debateit/src/search/pages/base.dart';
import 'package:debateit/src/config/pagination/debate.dart';


class DebateTagSearchPage extends StatelessWidget {
  final String query;

  const DebateTagSearchPage({ required this.query }); 

  @override
  Widget build(BuildContext context) {
    final tags = query.split(' ')
      .map((tag) => tag.replaceFirst('#', ''))
      .toList();

    final tagString = tags.map((t) => '#$t').join(', ');

    return SearchPage(
      config: DebateTagSearchPaginationConfig(tags),
      queryText: 'tags $tagString',
    );
  }
}
