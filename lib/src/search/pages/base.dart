import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/widgets/pages/FixedPage.dart';


class SearchPage<T extends Pageable> extends StatelessWidget {
  final String queryText;

  final PaginationConfig<T> config;

  const SearchPage({ super.key, required this.config, required this.queryText });

  @override
  Widget build(BuildContext context) {
    final header = Center(
      child: Text('Search results for $queryText',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: context.colorScheme.onSurface,
          fontWeight: FontWeight.w300,
          fontSize: 26
        )
      )
    );

    return FixedViewPage(
      body: PaginationProvider(
        client: Services.client,
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 15),
        config: config,
        header: header,
      )
    );
  }
}
