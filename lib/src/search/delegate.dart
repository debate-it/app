import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit/src/search/bloc/bloc.dart';
import 'package:debateit/src/search/results.dart';


class SearchBarDelegate extends SearchDelegate { 
  final bool showResultsImmediately;

  final bloc = SearchBloc();

  int tabIndex = 0;


  SearchBarDelegate({ 
    required BuildContext context, 
    this.showResultsImmediately = false 
  })
    : super(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.search,
      searchFieldStyle: context.get<ConfigProvider>().theme.textTheme.bodyLarge);



  @override
  ThemeData appBarTheme(BuildContext context) {
    if (showResultsImmediately) {
      showResults(context);
    }
    
    return context.theme.copyWith(
      inputDecorationTheme: const InputDecorationTheme(),
      appBarTheme: context.theme.appBarTheme
        .copyWith(backgroundColor: context.colors.surface, elevation: 0));
  } 


  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.close, semanticLabel: 'Close'),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }


  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back, semanticLabel: 'Back'),
      onPressed: () {
        close(context, null);
      },
    );
  }


  @override
  Widget buildResults(BuildContext context) {
    return SearchResults(query: query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // This method is called everytime the search term changes. 
    // If you want to add search suggestions as the user enters their search term, this is the place to do that.
    return Column();
  }
}
