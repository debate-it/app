import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_pagination/pagination.dart';

import 'package:debateit/src/services.dart';

import 'package:debateit/src/config/pagination/multi.dart';
import 'package:debateit/src/config/pagination/user.dart';


class SearchResults extends StatefulWidget {
  final String query;

  const SearchResults({ super.key, required this.query });

  @override
  _SearchResultsState createState() => _SearchResultsState();
}

typedef SearchResultBuilder = List<Widget>? Function(BuildContext);

class _SearchResultsState extends State<SearchResults> with TickerProviderStateMixin {
  late final pages = _createPaginationProviders(widget.query);
  
  final PageStorageBucket bucket = PageStorageBucket(); 

  int index = 0;

  @override
  void didUpdateWidget(covariant SearchResults oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final selectedStyle = context.text.labelMedium
      ?.copyWith(color: context.colors.text, fontWeight: FontWeight.bold);
    final unselectedStyle = selectedStyle?.copyWith(fontWeight: FontWeight.w400);

    final tabs = DecoratedBox(
      decoration: BoxDecoration(
        color: context.colors.surface
      ),
      child: DefaultTabController(
        length: 4, 
        child: TabBar(
        
          onTap: (idx) => setState(() { index = idx; }),

          tabs: [
            Tab(child: Text('Discussions', style: index == 0 ? selectedStyle : unselectedStyle)),
            Tab(child: Text('Users', style: index == 1 ? selectedStyle : unselectedStyle)),
            Tab(child: Text('Categories', style: index == 2 ? selectedStyle : unselectedStyle)),
            Tab(child: Text('By Tags', style: index == 3 ? selectedStyle : unselectedStyle))
          ],
        )));

    final page = pages[index];
    
    return SingleChildScrollView(
      child: PageStorage(
        bucket: bucket,
        child: Column(mainAxisSize: MainAxisSize.min, children: [ tabs, page ])));
  }
  
  List<PaginationProvider> _createPaginationProviders(String query) {
    return [
      PaginationProvider.fromBloc(
        key: const ValueKey('topic'),
        scrollable: false,
        bloc: PaginationBloc(client: Services.client, config: SearchTopicPaginationConfig(query))),
        
      PaginationProvider.fromBloc(
        key: const ValueKey('users'),
        scrollable: false,
        padding: const EdgeInsets.all(Insets.sm),
        bloc: PaginationBloc(client: Services.client, config: SearchUserPaginationConfig(query))),
        
      PaginationProvider.fromBloc(
        key: const ValueKey('category'),
        scrollable: false,
        bloc: PaginationBloc(client: Services.client, config: SearchCategoryPaginationConfig(query))),
        
      PaginationProvider.fromBloc(
        key: const ValueKey('tags'),
        scrollable: false,
        bloc: PaginationBloc(client: Services.client, config: SearchTagsPaginationConfig(query))),
    ];
  }
}
