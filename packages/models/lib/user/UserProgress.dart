import 'package:flutter/cupertino.dart';

const LEVEL_COLORS = [
  LinearGradient(colors: [Color(0xFF7F7FD5), Color(0xFF86A8E7), Color(0xFF91EAE4)]),
  LinearGradient(colors: [Color(0xFF2193b0), Color(0xFF6dd5ed)]),
  LinearGradient(colors: [Color(0xFF373B44), Color(0xFF4286f4)]),
  LinearGradient(colors: [Color(0xFF8E2DE2), Color(0xFF4A00E0)]),
  LinearGradient(colors: [Color(0xFFb92b27), Color(0xFF1565C0)]),
  LinearGradient(colors: [Color(0xFFc31432), Color(0xFF240b36)]),
  LinearGradient(colors: [Color(0xFFDA4453), Color(0xFF89216B)]),
  LinearGradient(colors: [Color(0xFFbc4e9c), Color(0xFFf80759)]),
  LinearGradient(colors: [Color(0xFF800080), Color(0xFFffc0cb)]),
  LinearGradient(colors: [Color(0xFF7F00FF), Color(0xFFE100FF)]),
  LinearGradient(colors: [Color(0xFF283c86), Color(0xFF45a247)]),
  LinearGradient(colors: [Color(0xFF34e89e), Color(0xFF0f3443)]),
  LinearGradient(colors: [Color(0xFF30E8BF), Color(0xFFFF8235)]),
  LinearGradient(colors: [Color(0xFFFF5F6D), Color(0xFFFFC371)]),
  LinearGradient(colors: [Color(0xFFff4b1f), Color(0xFFff9068)]),
  LinearGradient(colors: [Color(0xFFfe8c00), Color(0xFFf83600)]),
  LinearGradient(colors: [Color(0xFFEB3349), Color(0xFFF45C43)]),
  LinearGradient(colors: [Color(0xFFB79891), Color(0xFF94716B)]),
  LinearGradient(colors: [Color(0xFFBBD2C5), Color(0xFF536976)]),
  LinearGradient(colors: [Color(0xFFFF4E50), Color(0xFFF9D423)]) 
];

class UserProgress {
  final int level;

  final int earned;
  final int needed;

  const UserProgress({ required this.level, this.earned = 0, this.needed = 50 });

  UserProgress.fromJson(Map<String, dynamic> json)
    : level = json['level'] as int,
      earned = json['earned'] as int,
      needed = json['needed'] as int;

  LinearGradient get color => LEVEL_COLORS[level];
}
