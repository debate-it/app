class UserInfo {
  String? color;

  String? picture;
  String? about;
  
  String? country;
  String? education;
  String? occupation;
  String? religion;
  String? politics;

  UserInfo({ 
    required this.color, 
    this.picture, 
    this.about, 
    this.country,
    this.education,
    this.occupation,
    this.religion,
    this.politics
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    return UserInfo(
      color: json['color'] as String?, 
      picture: json['avatar'] as String?,
      about: json['about'] as String?,
      education: json['education'] as String?,
      occupation: json['occupation'] as String?,
      country: json['country'] as String?,
      religion: json['religion'] as String?,
      politics: json['politics'] as String?,
    );
  }
}
