class UserStats {
  int points;
  int debates;
  int followers;

  String? category;

  UserStats({ 
    required this.debates, 
    required this.followers, 
    required this.points,
    this.category, 
  });

  factory UserStats.fromJson(Map<String, dynamic> json) {
    return UserStats(
      followers: json['followers'] as int,
      debates:json['wins'] as int,
      points: json['points'] as int, 
      category: 
        json.containsKey('bestCategory') && 
        json['bestCategory'] != null
          ? (json['bestCategory'] as Map<String, dynamic>)['name'] as String
          : null,
    );
  }

  Map<String, String> toMap() { 
    final stats = <String, dynamic>{ 
      'POINTS': points, 
      'DEBATES WON': debates, 
      'FOLLOWERS': followers, 
      'BEST CATEGORY': category 
    };

    stats.removeWhere((key, value) => value == null);

    return stats.map((key, value) => MapEntry(key, value.toString()));
  }
}
