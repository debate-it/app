 class UserFormData { 
  String? email;
  String? username;
  String? password;
  String? confirm;

  String? country;
  String? education;
  String? occupation;

  UserFormData();

  Map<String, dynamic> toMap() {
    return {
      'username': username,
      'password': password,
      'email': email,

      'options': {
        'country': country,
        'education': education,
        'occupation': occupation
      }
    };
  }
}
