
import 'package:debateit_models/user/UserProgress.dart';

import 'package:debateit_models/user/UserInfo.dart';
import 'package:debateit_models/user/UserStats.dart';
import 'package:debateit_pagination/models/page.dart';


enum Relation { follower, subscription, friend, self, none } 

class UserProfile extends Pageable {
  @override
  final String id;
  final String username;
  final String? fullname;

  Relation? relation; 

  UserInfo info; 
  final UserStats stats;
  final UserProgress progress;

  DateTime? lastNotification;
  
  bool get isSelf => relation == Relation.self;
  bool get followed => relation == Relation.subscription;

  UserProfile({ 
    required this.id, 
    required this.username,

    required this.info, 
    required this.stats, 
    required this.progress, 

    this.fullname,
    this.relation,
    this.lastNotification 
  });

  static List<UserProfile> fromJsonList(List<dynamic> list) {
    return list.map((user) => UserProfile.fromJson(user as Map<String, dynamic>)).toList();
  }

  factory UserProfile.fromJson(Map<String, dynamic> json) {
    return UserProfile(
      id: json['id'] as String,
      username: json['username'] as String,
      fullname: json['fullname'] as String?,

      info: UserInfo.fromJson(json['info'] as Map<String, dynamic>), 
      stats: UserStats.fromJson(json['stat'] as Map<String, dynamic>),
      progress: UserProgress.fromJson(json['progress'] as Map<String, dynamic>),
      relation: relationFromString(json['relation'] as String?),

      lastNotification: json.containsKey('lastNotification')
        ? DateTime.fromMillisecondsSinceEpoch(json['lastNotification'] as int, isUtc: true)
        : null
    );
  } 

  static Relation? relationFromString(String? str) { 
    switch (str) {
      case 'follower': return Relation.follower;
      case 'subscription': return Relation.subscription;
      case 'friend': return Relation.friend;
      case 'self': return Relation.self;
      case 'none': return Relation.none;
      default:
        return null;
    }
  }

}
