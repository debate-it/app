import 'package:debateit_models/user/main.dart';

import 'package:debateit_models/request/base.dart';
import 'package:debateit_pagination/pagination.dart';

enum OpenRequestType {
  Debate,
  Panel
}

class OpenRequest extends Request {
  
  final bool resolved;
  final int candidacies;
  final OpenRequestType type;

  OpenRequest({
    required super.id, 
    required super.initiator, 
    required super.info, 
    required super.time,

    required this.type,
    required this.resolved,
    required this.candidacies, 
  });

  OpenRequest.fromJson(super.json)
    : type = OpenRequestTypeExtension.parse(json['type'] as String),
      candidacies = json['candidacies'] as int,
      resolved = json['resolved'] as bool,
      super.fromJson();
}

class OpenRequestCandidacy extends Pageable {
  @override
  final String id;

  final DateTime created; 
  final UserProfile profile; 
  final RequestStatus status;

  OpenRequestCandidacy({
    required this.id, 
    required this.created,
    required this.profile,
    required this.status
  });

  OpenRequestCandidacy.fromJson(Map<String, dynamic> json)
    : id = json['id'] as String,
      created = DateTime.fromMillisecondsSinceEpoch(json['created'] as int, isUtc: true),
      profile = UserProfile.fromJson(json['candidate'] as Map<String, dynamic>),
      status = RequestStatusExtension.parse(json['status'] as String);
}

extension OpenRequestTypeExtension on OpenRequestType {
  String get parameter {
    switch (this) {
      case OpenRequestType.Debate:
        return 'Debate';

      case OpenRequestType.Panel:
        return 'Panel';
    }
  }

  static OpenRequestType parse(String value) {
    switch (value) {
      case 'Debate':
        return OpenRequestType.Debate;

      case 'Panel':
        return OpenRequestType.Panel;
    }

    throw Exception('Invalid open request type: $value');
  }
}
