import 'package:debateit_models/discussions/main.dart';
import 'package:debateit_models/request/main.dart';
import 'package:debateit_models/user/main.dart';

import 'package:debateit_pagination/pagination.dart';


enum RequestStatus { sent, accepted, rejected }

enum ResolveStatus { accept , reject }

class RequestBase {
  final String topic;

  final DiscussionSize size;
  final DiscussionCategory category;

  final List<Tag> tags;

  RequestBase({
    required this.size, 
    required this.topic, 
    required this.category, 
    required this.tags
  });

  RequestBase.fromJson(Map<String, dynamic> json)
    : topic = json['topic'] as String,
      category = DiscussionCategory.fromJson(json['category'] as Map<String, dynamic>),
      tags = Tag.fromJsonList(json['tags'] as List<dynamic>),
      size = DiscussionSizeExtension.parse(json['size'] as String);
}


class Request extends Pageable {
  @override final String id;

  final UserProfile initiator;
  
  final DateTime time; 
  final RequestBase info;


  Request({ required this.id, required this.initiator, required this.info, required this.time });

  Request.fromJson(Map<String, dynamic> json)
    : initiator = UserProfile.fromJson(json['initiator'] as Map<String, dynamic>),
      info = RequestBase.fromJson(json['info'] as Map<String, dynamic>),
      time = DateTime.fromMillisecondsSinceEpoch(json['created'] as int, isUtc: true),
      id = json['id'] as String;

  factory Request.parse(Map<String, dynamic> json) {
    final type = json['__typename'];

    switch (type) {
      case 'DebateRequest':
        return DebateRequest.fromJson(json);

      case 'PanelRequest':
        return PanelRequest.fromJson(json);

      case 'OpenRequest':
        return OpenRequest.fromJson(json);
    }

    throw Exception('Unknown request type');
  }
}


extension RequestStatusExtension on RequestStatus {
  static RequestStatus parse(String value) {
    switch (value) {
      case 'submitted':
        return RequestStatus.sent;
      case 'accepted':
        return RequestStatus.accepted;
      case 'rejected':
        return RequestStatus.rejected;
      default:
        throw Exception('Invalid request status');
    }
  }

  String get string {
    
    switch (this) {
      case RequestStatus.sent:
        return 'submitted';

      case RequestStatus.rejected:
        return 'rejected';
        
      case RequestStatus.accepted:
        return 'accepted';

      default:
        throw Exception('Invlid Request status: $this');
    }
  }
}

extension ResovleStatusExtension on ResolveStatus {
  String get parameter {
    switch (this) {
      case ResolveStatus.accept:
        return 'accepted';
      case ResolveStatus.reject:
        return 'rejected';
    }
  }
}
