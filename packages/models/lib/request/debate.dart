import 'package:debateit_models/user/main.dart';
import 'package:debateit_models/request/base.dart';


class DebateRequest extends Request {
  final RequestStatus status;
  final UserProfile recipient;

  DebateRequest({
    required super.id, 
    required super.initiator, 
    required super.info, 
    required super.time,

    required this.status, 
    required this.recipient
  });

  DebateRequest.fromJson(super.json)
    : recipient = UserProfile.fromJson(json['recipient'] as Map<String, dynamic>),
      status = RequestStatusExtension.parse(json['status'] as String),
      super.fromJson();
}
