import 'package:debateit_models/user/main.dart';
import 'package:debateit_models/request/base.dart';


class PanelRequestRecipient {
  final UserProfile user;
  final RequestStatus status;

  PanelRequestRecipient({ required this.user, required this.status });
  
  PanelRequestRecipient.fromJson(Map<String, dynamic> json)
    : user = UserProfile.fromJson(json['user'] as Map<String, dynamic>),
      status = RequestStatusExtension.parse(json['status'] as String);

  static List<PanelRequestRecipient> fromJsonList(List<dynamic> arr) =>
    arr.map((entry) => PanelRequestRecipient.fromJson(entry as Map<String, dynamic>)).toList();
}

class PanelRequest extends Request {
  final DateTime? expiration;
  final int accepted;
  final bool closed;
  final List<PanelRequestRecipient> recipients;

  PanelRequest({ 
    required super.id, 
    required super.initiator, 
    required super.info, 
    required super.time,

    this.expiration,

    required this.accepted,
    required this.closed,
    required this.recipients
  });

  PanelRequest.fromJson(super.json)
    : recipients = PanelRequestRecipient.fromJsonList(json['recipients'] as List<dynamic>),
      expiration = json['expiration'] != null ? DateTime.fromMillisecondsSinceEpoch(json['expiration'] as int, isUtc: true) : null,
      accepted = json['accepted'] as int,
      closed = json['closed'] as bool,
      super.fromJson();
}
