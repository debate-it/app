import 'package:debateit_models/notifications/event.dart';
import 'package:debateit_pagination/pagination.dart';


class Activity implements Pageable {
  @override
  final String id;

  final DateTime from;
  final Iterable<Event> events;

  const Activity({ required this.id, required this.from, required this.events});

  Activity.fromJSON(Map<String, dynamic> json)
    : id = json['id'] as String,
      from = DateTime.fromMillisecondsSinceEpoch(json['from'] as int, isUtc: true),
      events = Event.fromJSONList(json['activity'] as List<dynamic>);
}
