class Event {
  final DateTime time; 

  final String link;
  final String title;
  final String message;

  const Event({
    required this.title,
    required this.time,
    required this.link,
    required this.message
  });

  Event.fromJSON(Map<String, dynamic> json)
    : time = DateTime.fromMillisecondsSinceEpoch(json['time'] as int, isUtc: true),
      link = json['link'] as String,
      title = json['title'] as String,
      message = json['message'] as String;


  static Iterable<Event> fromJSONList(List<dynamic> events)
    => events.map((entry) => Event.fromJSON(entry as Map<String, dynamic>)).toList();
}
