import 'package:debateit_models/discussions/base.dart';
import 'package:debateit_models/discussions/post/alignment.dart';

import 'package:debateit_models/user/UserProfile.dart';


class Post extends Discussion {
  final UserProfile author;

  AlignmentOption? voted;

  PostAlignment alignment;

  Post({
    required super.id,
    required super.topic,
    required super.category,
    required super.created,
    required super.tags,

    required this.author,
    required this.alignment,
    required this.voted
  }) 
    : super(size: DiscussionSize.long);

  Post.fromJson(super.json)
    : author = UserProfile.fromJson(json['author'] as Map<String, dynamic>),
      alignment = PostAlignment.fromJson(json['alignment'] as Map<String, dynamic>),
      voted = AlignmentOptionExtension.parse(json['votedOn'] as String?),
      super.fromJson();

  int alignmentPoints(AlignmentOption option) {
    switch (option) {
      case AlignmentOption.agree:
        return alignment.agree;

      case AlignmentOption.disagree:
        return alignment.disagree;

      default:
        throw Exception('Invalid alignment option');
    }
  }
}
