enum AlignmentOption {
  agree,
  disagree
}

class PostAlignment {
  final int agree;
  final int disagree;

  PostAlignment(this.agree, this.disagree);

  factory PostAlignment.fromJson(Map<String, dynamic> json) {
    return PostAlignment(json['agree'] as int, json['disagree'] as int);
  }
}

extension AlignmentOptionExtension on AlignmentOption {
  String get  label {
    switch (this) {
      case AlignmentOption.agree:
        return 'agree';
      case AlignmentOption.disagree:
        return 'disagree';
      default:
        throw Exception('Invalid alignmetn options');
    }
  }

  static AlignmentOption? parse(String? value) {
    switch (value) {
      case 'agree':
        return AlignmentOption.agree;
      case 'disagree':
        return AlignmentOption.disagree;

      default: 
        return null;
    }
  }
}
