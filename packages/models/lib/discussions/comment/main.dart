import 'package:debateit_models/user/UserProfile.dart';
import 'package:debateit_pagination/pagination.dart';

enum CommentOrder { NEW, TOP, OLD }
enum CommentType { Debate, Challenge, Post, Panel }
enum CommentAlignment { pro, con }

class Comment extends Pageable {
  @override final String id;
  
  final String content;

  final UserProfile author;
  final DateTime date;

  final CommentType type;
  final int? alignment;

  bool voted;
  int score;

  Comment({ 
    required this.id,
    required this.content,
    required this.author,
    required this.date,
    required this.score,
    required this.voted,
    required this.type,
    required this.alignment
  });

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
      id: json['id'] as String,
      content: json['content'] as String,

      author: UserProfile.fromJson(json['author'] as Map<String, dynamic>),
      date: DateTime.fromMillisecondsSinceEpoch(json['created'] as int, isUtc: true),

      type: CommentTypeExtension.parse(json['type'] as String),
      alignment: json['alignment'] as int?,

      score: json['votes'] as int,
      voted: json['voted'] as bool? ?? false 
    );
  }
  
  Comment copyWith({ bool? votedOn, int? score }) {
    return Comment(
      alignment: alignment,
      author: author,
      content: content,
      type: type,
      date: date,
      id: id,
      score: score ?? this.score,
      voted: votedOn ?? voted 
    );
  }
}


CommentAlignment parseCommentAlignment(String value) {
  switch (value) {
    case 'pro':
      return CommentAlignment.pro;
      
    case 'con':
      return CommentAlignment.con;
    
      
    default:
      throw Exception('Invalid comment alignment');
  }
}

extension CommentTypeExtension on CommentType {
  String get name {
    switch (this) {
      case CommentType.Debate:
        return 'Debate';
      case CommentType.Challenge:
        return 'Challenge';
      case CommentType.Post:
        return 'Post';
      case CommentType.Panel:
        return 'Panel';
    }
  }

  static CommentType parse(String value) {
    switch (value) {
      case 'Debate':
        return CommentType.Debate;
        
      case 'Challenge':
        return CommentType.Challenge;
        
      case 'Post':
        return CommentType.Post;
        
      case 'Panel':
        return CommentType.Panel;
        
      default:
        throw Exception('Invalid comment type');
    }
  }
}
