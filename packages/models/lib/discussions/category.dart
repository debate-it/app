import 'package:debateit_pagination/pagination.dart';

class DiscussionCategory extends Pageable {
  @override 
  final String id;

  final String name;
  final String? description;

  final String? picture;

  final DiscussionCategoryStat? stat;
  
  bool? following;

  DiscussionCategory({
    required this.id,

    required this.name,
    this.description,

    this.stat,

    this.picture,
    this.following
  });

  factory DiscussionCategory.fromJson(Map<String, dynamic> json) {
    return DiscussionCategory(
      id: json['id'] as String,

      name: json['name'] as String,
      description: json['description'] as String?,

      stat: DiscussionCategoryStat.fromJson(json['stat'] as Map<String, dynamic>),

      picture: json['picture'] as String?,
      following: json['isSubscribed'] as bool?,
    );
  }
}

class DiscussionCategoryStat {
  int followers;
  int discussions;

  DiscussionCategoryStat({ required this.followers, required this.discussions });

  factory DiscussionCategoryStat.fromJson(Map<String, dynamic> json) {
    return DiscussionCategoryStat(followers: json['followers'] as int, discussions: json['discussions'] as int);
  }
}
