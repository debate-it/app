class DebateArgument {
  final DateTime time;
  final int participant;
  final String content;

  DebateArgument({ required this.time, required this.participant, required this.content });
  
  DebateArgument.fromJson(Map<String, dynamic> json)
    : time = _parseTimeStamp(json['time']),
      participant = json['participant'] as int, 
      content = json['argument'] as String;

  static DateTime _parseTimeStamp(time) {
    if (time.runtimeType == int) {
      return DateTime.fromMillisecondsSinceEpoch(time as int, isUtc: true);
    } else {
      return DateTime.fromMillisecondsSinceEpoch(time as int, isUtc: true);
    }
  }

  static List<DebateArgument>? fromJsonArray(List<dynamic>? arr) {
    return arr?.map((item) => DebateArgument.fromJson(item as Map<String, dynamic>)).toList();
  }
}
