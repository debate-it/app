import 'package:debateit_models/discussions/base.dart';

import 'package:debateit_models/discussions/debate/arguments.dart';
import 'package:debateit_models/discussions/debate/participants.dart'; 
 
enum DebateStatus {
  active,
  voting,
  finished,
}

class Debate extends Discussion {
  int? votedOn;
 
  DateTime? deadline; 
  DebateStatus status;
  DiscussionParticipants participants;

  final List<DebateArgument>? arguments;

  Debate({ 
    required super.id, 
    required super.topic, 
    required super.category, 
    required super.created,
    required super.tags,
    required super.size,

    required this.status,
    required this.deadline,
    required this.votedOn,
    required this.participants,
    required this.arguments, 
  });

  Debate.fromJson(super.json) 
    : status = DebateStatusExtension.parse(json['debateStatus'] as String),
      deadline = json['debateDeadline'] != null
        ? DateTime.fromMillisecondsSinceEpoch(json['debateDeadline'] as int, isUtc: true)
        : null,
      arguments = DebateArgument.fromJsonArray(json['arguments'] as List<dynamic>?),
      participants = DiscussionParticipants.fromJson(json['participants'] as List<dynamic>),
      votedOn = json['votedOn'] as int?,
      super.fromJson();

  static List<Debate> fromJsonList(List<dynamic> arr) {
    return arr.map((item) => Debate.fromJson(item as Map<String, dynamic>)).toList();
  }

  void insert(int index, DebateArgument argument) {
    arguments?.insert(index, argument);
  }
  
  

  int get currentTurn => (arguments?.length ?? 0) % 2;
  bool get isFinished => status == DebateStatus.finished;
}

extension DebateStatusExtension on DebateStatus {
  String get label {
    switch (this) {
      case DebateStatus.active:
        return 'Active';
      case DebateStatus.voting:
        return 'Voting';
      case DebateStatus.finished:
        return 'Finished';
    }
  }

  static DebateStatus parse(String? value) {
    switch (value) {
      case 'active':
        return DebateStatus.active;
      case 'voting':
        return DebateStatus.voting;
      case 'finished':
        return DebateStatus.finished;
    }

    throw Exception('Invalid panel status');
  }
}
