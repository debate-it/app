import 'package:debateit_models/user/UserProfile.dart';

class DiscussionParticipant {
  final int score;
  final UserProfile user;

  DiscussionParticipant({ required this.score, required this.user });

  DiscussionParticipant.fromJson(Map<String, dynamic> json)
    : user = UserProfile.fromJson(json['user'] as Map<String, dynamic>),
      score = json['score'] as int;
}

class DiscussionParticipants extends Iterable<DiscussionParticipant> {

  final List<DiscussionParticipant> _participants;

  DiscussionParticipants({ required List<DiscussionParticipant> participants })
    : _participants = participants;
  
  DiscussionParticipants.fromJson(List<dynamic> json)
    : _participants = json.map((item) => DiscussionParticipant.fromJson(item as Map<String, dynamic>)).toList();
 
  DiscussionParticipant operator [](int index) {
    return _participants.elementAt(index);
  }

  bool isParticipant(UserProfile? user) {
    return user != null && _participants.any((element) => element.user.id == user.id);
  }

  int participantSide(UserProfile user) {
    return _participants.indexWhere((element) => element.user.id == user.id);
  }

  Iterable<S> mapEntries<S>(S Function(DiscussionParticipant element, int index) f) => 
    _participants.asMap().entries.map((entry) => f(entry.value, entry.key));


  @override
  Iterator<DiscussionParticipant> get iterator => _participants.iterator;


  // String userSide(UserProfile user) {
  //   if (user.username == left.user.username) {
  //     return 'left';
  //   } else if (user.username == right.user.username) {
  //     return 'right';
  //   } else {
  //     return null;
  //   }
  // }
}
