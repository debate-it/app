import 'package:debateit_models/discussions/base.dart';
import 'package:debateit_models/user/main.dart';

enum PanelStatus {
  waiting,
  active,
  finished
}

class Panel extends Discussion {
  final DateTime? deadline;

  int? votedOn;
  PanelStatus status;
  List<PanelParticipant> participants;


  Panel.fromJson(super.json)
    : participants = PanelParticipant.fromJsonList(json['participants'] as List<dynamic>),
      votedOn = json['votedOn'] as int?,
      status = PanelStatusExtension.parse(json['panelStatus'] as String),
      deadline = parseDeadline(json['panelDeadline'] as int),
      super.fromJson();

  static DateTime? parseDeadline(int? deadline) {
    if (deadline == null) {
      return null;
    }

    return DateTime.fromMillisecondsSinceEpoch(deadline, isUtc: true);
  }

}

class PanelParticipant {
  int score;

  final UserProfile user; 
  final String argument;

  PanelParticipant({ required this.user, required this.argument, this.score = 0 });
  PanelParticipant.fromJson(Map<String, dynamic> json)
    : score = json['score'] as int,
      user = UserProfile.fromJson(json['user'] as Map<String, dynamic>),
      argument = json['argument'] as String;

  static List<PanelParticipant> fromJsonList(List<dynamic> arr) => 
    arr.map((e) => PanelParticipant.fromJson(e as Map<String, dynamic>)).toList();
}

extension PanelStatusExtension on PanelStatus {
  static PanelStatus parse(String? value) {
    switch (value) {
      case 'waiting':
        return PanelStatus.waiting;
      case 'active':
        return PanelStatus.active;
      case 'finished':
        return PanelStatus.finished;
    }

    throw Exception('Invalid panel status');
  }
}
