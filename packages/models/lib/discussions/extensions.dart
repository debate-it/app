import 'package:debateit_models/discussions/base.dart';

extension DiscussionSizeExtension on DiscussionSize {
  String get label => toString().replaceAll('DiscussionSize.', '');

  int get argumentLength {
    switch (this) {
      case DiscussionSize.short:  return 150;
      case DiscussionSize.medium: return 200;
      case DiscussionSize.long:   return 250;

      default:
        throw Exception('Invalid discussion size value');
    }
  }

  String get explanation {
    switch (this) {
      case DiscussionSize.short:  return '150 words\n2 rounds';
      case DiscussionSize.medium: return '200 words\n3 rounds';
      case DiscussionSize.long:   return '250 words\n4 rounds';

      default:
        throw Exception('Invalid discussion size value');
    }
  }
  
  int get rounds {
    switch (this) {
      case DiscussionSize.short:  return 2;
      case DiscussionSize.medium: return 3;
      case DiscussionSize.long:   return 4;

      default:
        throw Exception('Invalid discussion size value');
    }
  }

  static DiscussionSize parse(String value) {
    switch (value) {
      case 'short': return DiscussionSize.short;
      case 'medium': return DiscussionSize.medium;
      case 'long': return DiscussionSize.long;

      default:
        throw Exception('Invalid discussion size value');
    }
  }
}
