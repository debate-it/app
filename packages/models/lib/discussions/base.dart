import 'package:debateit_models/models.dart';
import 'package:debateit_pagination/pagination.dart';

enum DiscussionSize {
  short,
  medium,
  long;

  dynamic toJson() => name;
}


class Discussion implements Pageable {
  @override
  final String id;
  final String topic;

  final DiscussionSize size;
  final DiscussionCategory category;
  final DateTime created;
  final List<Tag> tags;

  Discussion({ 
    required this.id, 
    required this.topic, 
    required this.category, 
    required this.tags, 
    required this.size, 
    required this.created
  });

  Discussion.fromJson(Map<String, dynamic> json)
    : id = json['id'] as String,
      topic = json['topic'] as String,
      category = DiscussionCategory.fromJson(json['category'] as Map<String, dynamic>),
      tags = Tag.fromJsonList(json['tags'] as List<dynamic>),
      size = DiscussionSizeExtension.parse(json['size'] as String),
      created = DateTime.fromMillisecondsSinceEpoch(json['created'] as int, isUtc: true);

    
  static List<Discussion> fromJsonList(List<dynamic> list) {
    return list.map((e) => Discussion.parse(e as Map<String, dynamic>)).toList();
  }

  factory Discussion.parse(Map<String, dynamic> json) {
    final type = json['__typename'];

    switch (type) {
      case 'Debate':
        return Debate.fromJson(json);

      case 'Post':
        return Post.fromJson(json);
        
      case 'Challenge':
        return Challenge.fromJson(json);

      case 'Panel':
        return Panel.fromJson(json);
    }

    throw Exception('Invalid discussion type: $type');
  }
}
