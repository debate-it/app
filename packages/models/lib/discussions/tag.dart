class Tag {
  final String name;

  const Tag({ required this.name });

  Tag.fromJSON(Map<String, dynamic> json)
    : name = json['name'] as String;

  static List<Tag> fromJsonList(List<dynamic> tags) {
    return tags.map((tag) => Tag.fromJSON(tag as Map<String, dynamic>)).toList();
  }
}
