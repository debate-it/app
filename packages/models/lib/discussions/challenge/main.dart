import 'package:debateit_models/user/UserProfile.dart'; 
import 'package:debateit_models/discussions/base.dart'; 

enum ChallengeStatus {
  active,
  waitingForOpponent,
  finished
}

class Challenge extends Discussion {
  final DateTime deadline;
  
  final UserProfile author;
  final String position;

  int responses;
  ChallengeStatus status;
  

  Challenge({ 
    required super.id,
    required super.topic,
    required super.created,
    required super.category,
    required super.size,
    required super.tags,

    required this.deadline,
    required this.author,
    required this.position,
    required this.status,

    this.responses = 0
  });

  Challenge.fromJson(super.json)
    : deadline = DateTime.fromMillisecondsSinceEpoch(json['challengeDeadline'] as int, isUtc: true),
      status = ChallengeStatusExtension.parse(json['challengeStatus'] as String),
      author = UserProfile.fromJson(json['author'] as Map<String, dynamic>),
      position = json['position'] as String,
      responses = json['responses'] as int,
      super.fromJson();

  bool get isFinished => DateTime.now().isAfter(deadline);
}

extension ChallengeStatusExtension on ChallengeStatus {
  static ChallengeStatus parse(String value) {
    switch (value) {
      case 'active':
        return ChallengeStatus.active;

      case 'waitingForOpponent':
        return ChallengeStatus.waitingForOpponent;

      case 'finished':
        return ChallengeStatus.finished;

      default:
        throw Exception('Invalid challenge status: $value');
    }
  }

  String get label  {
    switch (this) {
      case ChallengeStatus.active:
        return 'active';

      case ChallengeStatus.waitingForOpponent:
        return 'Waiting';

      case ChallengeStatus.finished:
        return 'Finished';
    }
  }
}
