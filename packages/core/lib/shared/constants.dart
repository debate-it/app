part of '../core.dart';

abstract class Constants {

  static const TextSpan appTitle = TextSpan(
    children: [
      TextSpan(
        text: 'Debate',
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.w400,
          letterSpacing: -0.5,

          color: Color(0xFFd12020))),

      TextSpan( 
        text: 'It', 
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.w400,
          letterSpacing: -0.5,

          color: Color(0xFF4cadd4))),
    ]);
}
