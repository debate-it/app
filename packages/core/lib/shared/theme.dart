part of '../core.dart';

abstract class AppStyles {
  static TextTheme text(BuildContext context) => Theme.of(context).textTheme; 
  static IconThemeData icon(BuildContext context) => Theme.of(context).iconTheme;
}

enum AppThemeType {
  dark,
  light
}

class AppTheme {
  AppColors get colors => AppColors(type);

  final AppThemeType type;

  const AppTheme.forType(this.type);

  ColorScheme get scheme => ColorScheme(
    brightness: colors.brightness,

    onSurface: colors.text,
    onBackground: colors.text,

    onPrimary: colors.accent,
    onSecondary: colors.accent,

    primary: colors.primary,
    secondary: colors.secondary,

    background: colors.background,
    surface: colors.surface,

    error: colors.primary.withAlpha(180),
    onError: colors.text,
  );

  ThemeData get base => 
    (scheme.brightness == Brightness.dark)
      ? ThemeData.dark()
      : ThemeData.light();

  ThemeData get data => base.copyWith(
    useMaterial3: true,

    pageTransitionsTheme: const PageTransitionsTheme(
      builders: {
        TargetPlatform.android: FadeUpwardsPageTransitionsBuilder()
      }
    ),

    colorScheme: scheme,

    brightness: scheme.brightness, 

    primaryColor: scheme.primary,

    scaffoldBackgroundColor: colors.background,

    textSelectionTheme: TextSelectionThemeData(cursorColor: scheme.primary),

    appBarTheme: AppBarTheme(
      elevation: 8,
      color: colors.surface,

      actionsIconTheme: IconThemeData(color: colors.accent),
      surfaceTintColor: Colors.transparent,

      systemOverlayStyle: SystemUiOverlayStyle(statusBarBrightness: scheme.brightness),

      iconTheme: IconThemeData(color: colors.accent)),

    cardColor: colors.surface,
    cardTheme: CardTheme(
      elevation: 4.0,
      margin: const EdgeInsets.symmetric(vertical: Insets.xs, horizontal: Insets.sm),
      color: colors.surface,
      surfaceTintColor: Colors.transparent
    ),



    switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.resolveWith((states) {
        if (states.any((state) => state == MaterialState.selected)) {
          return colors.primary;
        }

        return colors.surface;
      }),

      trackColor: MaterialStateProperty.resolveWith((states) {
        if (states.any((state) => state == MaterialState.selected)) {
          return colors.primary.withAlpha(150);
        }

        return colors.surface;
      })
    ),

    iconTheme: IconThemeData(color: colors.accent),
    primaryIconTheme: IconThemeData(color: colors.primary),

    dividerTheme: DividerThemeData(color: colors.accent, indent: Insets.md, endIndent: Insets.md),

    bottomAppBarTheme: BottomAppBarTheme(color: scheme.surface, elevation: 12.0),

    textTheme: TextTheme(
      headlineLarge: TextStyles.h1.copyWith(color: colors.text),
      headlineMedium: TextStyles.h2.copyWith(color: colors.text),
      headlineSmall: TextStyles.h3.copyWith(color: colors.text),

      bodyLarge: TextStyles.body1.copyWith(color: colors.text),
      bodyMedium: TextStyles.body2.copyWith(color: colors.text),


      labelLarge: TextStyles.callout1.copyWith(color: colors.text),
      labelMedium: TextStyles.callout2.copyWith(color: colors.text),
      labelSmall: TextStyles.caption.copyWith(color: colors.text),

      titleLarge: TextStyles.title1.copyWith(color: colors.text),
      titleMedium: TextStyles.title2.copyWith(color: colors.text),
    ),


    bottomSheetTheme: BottomSheetThemeData(elevation: 12.0, modalBackgroundColor: scheme.surface),

    tooltipTheme: TooltipThemeData(
      textStyle: TextStyles.body3,

      decoration: BoxDecoration(
        color: colors.surface,
        borderRadius: BorderRadius.circular(5)),

      padding: const EdgeInsets.all(Insets.md)),

    tabBarTheme: TabBarTheme(
      // labelColor: colors.surface,

      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(color: colors.primary))),

    indicatorColor: colors.primary,
  );
}
