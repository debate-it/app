part of '../core.dart';

abstract class AppColors {
  Brightness get brightness;

  Color get text;
  Color get error;

  Color get primary;
  Color get secondary;
  Color get accent;

  Color get surface;
  Color get background;

  LinearGradient get primaryGradient;
  LinearGradient get secondaryGradient;

  factory AppColors(AppThemeType type) {
    switch (type) {
      case AppThemeType.light:
        return const _AppColorsLight();

      default:
        return const _AppColorsDark();
    }
    
  }
}

class _AppColorsDark implements AppColors {
  const _AppColorsDark();

  @override Brightness get brightness => Brightness.dark;

  @override Color get text => Colors.white;
  @override Color get error => Colors.red.shade600;
  
  // @override Color get primary => const Color(0xFFfd5904);
  // @override Color get secondary => const Color(0xFF005d5f);

  @override Color get primary => Colors.redAccent.shade700;
  @override Color get secondary => Colors.lightBlue.shade700;
  @override Color get accent => Colors.amber.shade200;

  @override Color get surface => const Color.fromARGB(255, 37, 37, 53);
  @override Color get background => const Color.fromARGB(255, 45, 45, 66);

  @override
  LinearGradient get primaryGradient => 
    const LinearGradient(colors: [Color(0xFFff5858), Color(0xFFffc8c8)]);

  @override
  LinearGradient get secondaryGradient =>
    const LinearGradient(colors: [Color(0xFF696eff), Color(0xFFf8acff)]);
}

class _AppColorsLight implements AppColors {
  const _AppColorsLight();
  
  @override Brightness get brightness => Brightness.light;

  @override Color get text => Colors.black;
  @override Color get error => Colors.red.shade600;

  @override Color get primary => Colors.redAccent.shade700;
  @override Color get secondary => Colors.lightBlue.shade700;
  @override Color get accent => Colors.black87;

  @override Color get surface => const Color.fromARGB(255, 201, 201, 201);
  @override Color get background => const Color.fromARGB(255, 238, 237, 237);
  
  @override
  LinearGradient get primaryGradient => 
    const LinearGradient(colors: [Color(0xFFff5858), Color(0xFFffc8c8)]);

  @override
  LinearGradient get secondaryGradient =>
    const LinearGradient(colors: [Color(0xFF696eff), Color(0xFFf8acff)]);
}

// abstract class _DarkColors {
//   static const text = Colors.white;
//   static const accent = Color(0xFFe5c780);
//   static const primary = Color(0xFFd12020);
//   static const secondary = Color(0xFF4cadd4);
//   static const surface = Color(0xFF2B2B29);
//   static const background = Color(0xFF383836);
//   static const error = Color(0xFFB00020);
// }

  // Iteration 1
  // static const text = Color(0xCCe3ca78);
  // static const primary = Color(0xCCAD1827);
  // static const secondary = Color(0xCC0B67FD);
  // static const surface = Color(0xFF292929);
  // static const background = Color(0xCC474747);
  // static const error = Color(0xFFB00020);


  // Iteration 2  static const text = Color(0xCCe3ca78);
  // static const primary = Color(0xFFD71D30);
  // static const secondary = Color(0xFF3582FD);
  // static const surface = Color(0xCC292929);
  // static const background = Color(0xFF474747);
  // static const error = Color(0xFFB00020);
