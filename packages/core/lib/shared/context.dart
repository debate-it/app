part of '../core.dart';

extension ContextExtension on BuildContext {
  T get<T>({ bool listen = false }) {
    return Provider.of<T>(this, listen: listen);
  }

  AppColors get colors {
    return get<ConfigProvider>(listen: true).colors;
  }

  ColorScheme get colorScheme {
    return get<ConfigProvider>(listen: true).theme.colorScheme;
  }

  ThemeData get theme {
    return get<ConfigProvider>(listen: true).theme;
  }

  TextTheme get text {
    return get<ConfigProvider>(listen: true).theme.textTheme;
  }
  
}

class ConfigProvider with ChangeNotifier {
  static const themeBox = 'theme-switch';
  static const darkModeKey = 'darkMode';

  static ConfigProvider of(BuildContext context, { bool listen = false }) =>
    Provider.of<ConfigProvider>(context, listen: listen);

  late AppTheme _theme;

  static Future<void> initialize() async {
    await Hive.openBox(themeBox);
  }

  ConfigProvider() {
    final isDark = Hive.box(themeBox).get(darkModeKey) as bool? ?? false;

    _theme = isDark
      ? const AppTheme.forType(AppThemeType.dark)
      : const AppTheme.forType(AppThemeType.light);
  }

  ConfigProvider.dark()
    : _theme = const AppTheme.forType(AppThemeType.dark);
    
  ConfigProvider.light()
    : _theme = const AppTheme.forType(AppThemeType.light);
  
  AppColors get colors => _theme.colors;
  ThemeData get theme => _theme.data;

  Future<void> changeTheme(AppThemeType type) async {
    switch (type) {
      case AppThemeType.dark:
        _theme = const AppTheme.forType(AppThemeType.dark);
        break;

      case AppThemeType.light:
        _theme = const AppTheme.forType(AppThemeType.light);
        break;
    }

    await Hive.box(themeBox).put(darkModeKey, type == AppThemeType.dark);

    notifyListeners();
  }
}
