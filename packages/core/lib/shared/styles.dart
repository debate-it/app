part of '../core.dart';

abstract class Times {
  static const fastest = Duration(milliseconds: 150);
  static const fast = Duration(milliseconds: 250);
  static const medium = Duration(milliseconds: 350);
  static const slow = Duration(milliseconds: 700);
  static const slowest = Duration(milliseconds: 1000);
}

abstract class Insets {
  static const double scale = 1;
  static const double offsetScale = 1;

  // Paddings
  /// 4x inset
  static const double xs = scale * 4;

  /// 8x inset
  static const double sm = scale * 8;
  
  /// 12x inset
  static const double md = scale * 12;

  /// 16x inset
  static const double lg = scale * 16;

  /// 32x inset
  static const double xl = scale * 32;

  static const double offset = offsetScale * 40;
}

abstract class Corners {
  static const double sm = 3;
  static const Radius smRadius = Radius.circular(sm);
  static const BorderRadius smBorder = BorderRadius.all(smRadius);

  static const double med = 5;
  static const Radius medRadius = Radius.circular(med);
  static const BorderRadius medBorder = BorderRadius.all(medRadius);

  static const double lg = 8;
  static const Radius lgRadius = Radius.circular(lg);
  static const BorderRadius lgBorder = BorderRadius.all(lgRadius);
}

abstract class Strokes {
  static const double thin = 1;
  static const double thick = 4;
}

abstract class Shadows {
  static const List<BoxShadow> universal = [
    BoxShadow(color: Color(0x30333333), blurRadius: 10, offset: Offset(0, 10))
  ];
  static const List<BoxShadow> small = [
    BoxShadow(color: Color(0x30333333), blurRadius: 3, offset: Offset(0, 1))
  ];
}

/// Font Sizes
/// You can use these directly if you need, but usually there should be a predefined style in TextStyles.
abstract class FontSizes {
  /// Provides the ability to nudge the app-wide font scale in either direction
  static double get scale => 1;

  static double get s10 => 10 * scale;
  static double get s11 => 11 * scale;
  static double get s12 => 12 * scale;
  static double get s14 => 14 * scale;
  static double get s16 => 16 * scale;
  static double get s24 => 24 * scale;
  static double get s48 => 48 * scale;
}

abstract class Fonts {
  static const raleway = 'Raleway';
  static const montserrat = 'Montserrat';
}

abstract class TextStyles {
  // Base styles for each font
  static const raleway = TextStyle(fontFamily: Fonts.raleway, fontWeight: FontWeight.w300, height: 1);
  static const montserrat = TextStyle(fontFamily: Fonts.montserrat, fontWeight: FontWeight.w400, height: 1); 

  /// Font Size: 48
  /// Font Weight: 600
  static TextStyle get h1 => 
    raleway.copyWith(
      fontWeight: FontWeight.w600, 
      fontSize: FontSizes.s48, 
      letterSpacing: -1, 
      height: 1.17);

  /// Font Size: 24
  /// Font Weight: 600
  static TextStyle get h2 =>
    h1.copyWith(
      fontSize: FontSizes.s24, 
      letterSpacing: -0.5, 
      height: 1.16);
      
  /// Font Size: 14
  /// Font Weight: 600
  static TextStyle get h3 =>
    h1.copyWith(
      fontSize: FontSizes.s14, 
      letterSpacing: -0.05, 
      height: 1.29);

  /// Font Size: 16
  /// Font Weight: Bold
  static TextStyle get title1 => montserrat.copyWith(fontWeight: FontWeight.bold, fontSize: FontSizes.s16, height: 1.31);
  /// Font Size: 14
  /// Font Weight: 500
  static TextStyle get title2 => title1.copyWith(fontWeight: FontWeight.w500, fontSize: FontSizes.s14, height: 1.36);

  /// Font Size: 14
  /// Font Weight: 400
  static TextStyle get body1 => montserrat.copyWith(fontWeight: FontWeight.normal, fontSize: FontSizes.s14, height: 1.71);
  /// Font Size: 12
  /// Font Weight: 400
  static TextStyle get body2 => body1.copyWith(fontSize: FontSizes.s12, height: 1.5, letterSpacing: 0.2);
  /// Font Size: 12
  /// Font Weight: bold
  static TextStyle get body3 => body1.copyWith(fontSize: FontSizes.s12, height: 1.5, fontWeight: FontWeight.bold);

  /// Font Size: 11
  /// Font Weight: 500
  static TextStyle get caption => raleway.copyWith(fontWeight: FontWeight.w500, fontSize: FontSizes.s11, height: 1.36);
  
  /// Font Size: 12
  /// Font Weight: 800
  static TextStyle get callout1 =>
    raleway.copyWith(fontWeight: FontWeight.w800, fontSize: FontSizes.s12, letterSpacing: .5, height: 1.17);

  /// Font Size: 10
  /// Font Weight: 800
  static TextStyle get callout2 =>
    callout1.copyWith(fontSize: FontSizes.s10, letterSpacing: .25, height: 1);
}
