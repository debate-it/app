library core;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:hive/hive.dart'; 
import 'package:provider/provider.dart';

part 'shared/colors.dart';
part 'shared/constants.dart';
part 'shared/context.dart';
part 'shared/styles.dart';
part 'shared/theme.dart';
