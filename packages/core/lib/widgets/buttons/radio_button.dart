import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';

class RadioButtonData<T> {
  final String title;
  final T value;
 
  const RadioButtonData({ required this.title, required this.value }); 
}

class RadioButton<T> extends StatelessWidget {
  final T value;
  final T? groupValue;

  final double? width;
  final String title;
  
  final VoidCallback? onTap;

  const RadioButton({ 
    super.key, 
    required this.title, 
    required this.value, 
    this.width,
    this.groupValue, 
    this.onTap 
  });

  RadioButton.fromButtonData({ 
    super.key,
    required FormFieldState<T> state, 
    required RadioButtonData<T> data,
    this.width,
    this.onTap
  })
    : value = data.value,
      title = data.title, 
      groupValue = state.value;


  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        width: width,
        child: Row( 
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IgnorePointer(
              child: Radio<T>(
                activeColor: context.colorScheme.secondary, 
                groupValue: groupValue, value: value, onChanged: (_) {})),

            Text(title, 
              style: context.text.bodyLarge
                )
          ],
        )
      )
    );
  }
}
