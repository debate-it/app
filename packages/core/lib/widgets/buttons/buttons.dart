import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

import 'package:debateit_core/widgets/buttons/raw_button.dart';


class OutlinedBtn extends StatelessWidget {
  final VoidCallback onPressed;
  final String label;

  const OutlinedBtn({ super.key, required this.onPressed, required this.label });

  // final Color background;
  // final Color 

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed, 
      style: OutlinedButton.styleFrom(
        side: BorderSide(color: context.colorScheme.secondary),
        textStyle: context.text.titleSmall
          ?.copyWith(color: context.colorScheme.secondary)
      ),
      child: Text(label, style: context.text.titleSmall
        ?.copyWith(color: context.colorScheme.secondary)));
  }
}

/// Accent colored btn (orange), wraps RawBtn
class PrimaryBtn extends StatelessWidget {
  const PrimaryBtn({
    super.key,
    this.onPressed,
    this.label,
    this.icon,
    this.child,
    this.cornerRadius,
    this.leadingIcon = false,
    this.isCompact = false,
    this.loading = false
  });

  final Widget? child;
  final VoidCallback? onPressed;

  final String? label;
  final IconData? icon;

  final bool leadingIcon;
  final bool isCompact;
  final bool loading;

  final double? cornerRadius;


  @override
  Widget build(BuildContext context) {
    final theme = context.colorScheme;

    return RawBtn(
      onPressed: onPressed,
      cornerRadius: cornerRadius,
      normalColors: BtnColors(bg: theme.primary, fg: theme.onPrimary),
      hoverColors: BtnColors(bg: theme.primary, fg: theme.onPrimary),
      child: BtnContent(
        icon: icon,
        label: label,
        loading: loading,
        leadingIcon: leadingIcon,
        isCompact: isCompact, 
        child: child));
  }
}

/// Surface colors btn (white), wraps RawBtn
class SecondaryBtn extends StatelessWidget {
  const SecondaryBtn({
    super.key,
    this.onPressed,
    this.label,
    this.icon,
    this.child,
    this.cornerRadius,
    this.leadingIcon = false,
    this.isCompact = false,
    this.loading = false
  });

  final Widget? child;
  final VoidCallback? onPressed;

  final String? label;
  final IconData? icon;

  final bool leadingIcon;
  final bool isCompact;
  final bool loading;

  final double? cornerRadius;

  @override
  Widget build(BuildContext context) {
    final theme = context.colorScheme;
    final content = BtnContent(
      label: label, 
      icon: icon,
      loading: loading,
      leadingIcon: leadingIcon,
      isCompact: isCompact, 
      child: child);

    if (isCompact) {
      return RawBtn(
        cornerRadius: cornerRadius,
        enableShadow: false,
        normalColors: BtnColors(bg: theme.secondary, fg: theme.onSecondary, outline: theme.surface),
        hoverColors: BtnColors(bg: theme.secondary.withOpacity(.6), fg: theme.onSecondary, outline: theme.surface),
        onPressed: onPressed,
        child: content);
    }

    return RawBtn(
      normalColors: BtnColors(bg: theme.secondary, fg: theme.onSecondary),
      hoverColors: BtnColors(bg: theme.secondary, fg: theme.onSecondary),
      onPressed: onPressed,
      child: content,
    );
  }
}

/// Takes any child, applies no padding, and falls back to default colors
class SimpleBtn extends StatelessWidget {
  const SimpleBtn({
    super.key,
    required this.onPressed,
    required this.child,
    this.normalColors,
    this.hoverColors, 
    this.cornerRadius
  });

  final Widget child;
  final VoidCallback onPressed;

  final BtnColors? normalColors;
  final BtnColors? hoverColors;

  final double? cornerRadius;

  @override
  Widget build(BuildContext context) {
    return RawBtn(
      enableShadow: false,
      cornerRadius: cornerRadius,
      normalColors: normalColors,
      hoverColors: hoverColors,
      onPressed: onPressed,
      child: child
    );
  }
}

/// Text Btn - wraps a [SimpleBtn]
class TextBtn extends StatelessWidget {
  const TextBtn(this.label, {
    super.key, 
    required this.onPressed, 
    this.isCompact = false,
    this.style, 
    this.showUnderline = false
  });

  final String label;
  final VoidCallback onPressed;
  final bool isCompact;
  final TextStyle? style;
  final bool showUnderline;

  @override
  Widget build(BuildContext context) {
    final finalStyle = style ??
      TextStyles.caption.copyWith(
        fontWeight: FontWeight.w500,
        decoration: showUnderline 
          ? TextDecoration.underline 
          : TextDecoration.none);

    return SimpleBtn(
      onPressed: onPressed,
      child: AnimatedPadding(
        duration: Times.fast,
        curve: Curves.easeOut,
        padding: EdgeInsets.symmetric(
          horizontal: isCompact ? 0 : Insets.sm,
          vertical: Insets.xs,
        ),
        child: Text(label, style: finalStyle)));
  }
}

/// Icon Btn - wraps a [SimpleBtn]
class IconBtn extends StatelessWidget {
  const IconBtn(this.icon, {super.key, required this.onPressed, this.color, this.padding});

  final IconData icon;
  final VoidCallback onPressed;

  final Color? color;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return SimpleBtn(
      onPressed: onPressed,
      child: AnimatedPadding(
        duration: Times.fast,
        curve: Curves.easeOut,
        padding: padding ?? const EdgeInsets.all(Insets.xs),
        child: Icon(icon, color: color ?? context.colors.text, size: 20)));
  }
}
