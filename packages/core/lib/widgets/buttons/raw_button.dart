import 'package:flutter/material.dart';
import 'package:debateit_core/core.dart';

class BtnColors {
  const BtnColors({
    required this.bg, 
    required this.fg, 
    this.outline
  });

  final Color bg;
  final Color fg;
  final Color? outline;
}

enum BtnTheme { primary, secondary, raw }

// A core btn that takes a child and wraps it in a btn that has a FocusNode.
// Colors are required. By default there is no padding.
// It takes care of adding a visual indicator  when the btn is Focused.
class RawBtn extends StatefulWidget {
  const RawBtn({
    super.key,
    required this.child,
    this.onPressed,
    this.normalColors,
    this.hoverColors,
    this.padding,
    this.enableShadow,
    this.enableFocus,
    this.cornerRadius
  });

  final Widget child;

  final BtnColors? normalColors;
  final BtnColors? hoverColors;

  final VoidCallback? onPressed;

  final EdgeInsets? padding;
  
  final double? cornerRadius;

  final bool? enableShadow;
  final bool? enableFocus;


  @override
  _RawBtnState createState() => _RawBtnState();
}

class _RawBtnState extends State<RawBtn> {
  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode(canRequestFocus: widget.enableFocus ?? true);
    _focusNode.addListener(() => setState(() {}));
  }

  @override
  void didUpdateWidget(covariant RawBtn oldWidget) {
    _focusNode.canRequestFocus = widget.enableFocus ?? true;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MaterialStateProperty<T> getMaterialState<T>({required T normal, required T hover}) =>
      MaterialStateProperty.resolveWith<T>((Set states) =>
        (states.contains(MaterialState.hovered) || 
         states.contains(MaterialState.focused)) 
          ? hover 
          : normal
      );

    final theme = context.colorScheme;

    final shadows = (widget.enableShadow ?? true) ? Shadows.universal : <BoxShadow>[];
    final normalColors = widget.normalColors ?? BtnColors(fg: theme.onPrimary, bg: Colors.transparent);
    final hoverColors = widget.hoverColors ?? BtnColors(fg: theme.onPrimary, bg: theme.onPrimary.withOpacity(.1));

    final cornerRadius = widget.cornerRadius ?? Corners.med;

    return AnimatedOpacity(
      duration: Times.fast,
      opacity: widget.onPressed == null ? .7 : 1,
      child: Container(
        height: 40,
        // Add custom shadow
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(cornerRadius),
          boxShadow: shadows),

        child: TextButton(
          focusNode: _focusNode,
          onPressed: widget.onPressed,
          style: ElevatedButton.styleFrom().copyWith(
            minimumSize: MaterialStateProperty.all(Size.zero),
            padding: MaterialStateProperty.all(EdgeInsets.zero),

            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(cornerRadius))),

            side: getMaterialState(
              normal: BorderSide(color: normalColors.outline ?? Colors.transparent),
              hover: BorderSide(color: hoverColors.outline ?? Colors.transparent)),

            overlayColor: MaterialStateProperty.all(Colors.transparent),
            foregroundColor: getMaterialState(normal: normalColors.fg, hover: hoverColors.fg),
            backgroundColor: getMaterialState(normal: normalColors.bg, hover: hoverColors.bg)),

          child: Padding(
            padding: widget.padding ?? EdgeInsets.zero, 
            child: widget.child))));
  }
}

// BtnContent that takes care of all the compact sizing and spacing
// Accepts label, icon and child, with child taking precedence.
class BtnContent extends StatelessWidget {
  const BtnContent({
    super.key,
    this.label,
    this.icon,
    this.child,
    this.labelStyle,
    this.leadingIcon = false,
    this.isCompact = false,
    this.loading = false
  });

  final Widget? child;

  final String? label;
  final IconData? icon;

  final bool leadingIcon;
  final bool isCompact;
  final bool loading;

  final TextStyle? labelStyle;

  @override
  Widget build(BuildContext context) {
    final hasIcon = icon != null;
    final hasLbl = label != null && label!.trim().isNotEmpty;

    return AnimatedPadding(
      duration: Times.fast,
      curve: Curves.easeOut,
      padding: isCompact
        ? const EdgeInsets.symmetric(horizontal: Insets.lg, vertical: Insets.md - 1)
        : const EdgeInsets.symmetric(horizontal: Insets.md, vertical: Insets.md),
      child: child ??
        Row(
          mainAxisSize: MainAxisSize.min,
          textDirection: leadingIcon ? TextDirection.rtl : TextDirection.ltr,
          children: [
            /// Label?
            if (hasLbl) 
              Text(label!.toUpperCase(),
                style: labelStyle ?? (isCompact ? TextStyles.callout2 : TextStyles.callout1)),

            /// Spacer - Show if both pieces of content are visible
            if ((hasIcon || loading) && hasLbl)
              const SizedBox(width: Insets.sm),

            /// Icon?
            if (hasIcon && !loading)
              Icon(icon, size: isCompact ? 14 : 16)

            /// Is loading?
            else if (loading)
              SizedBox(
                height: 14, width: 14,
                child: CircularProgressIndicator(strokeWidth: 2, color: context.colors.text))
          ]
        )
    );
  }
}
