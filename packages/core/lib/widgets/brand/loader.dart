import 'package:flutter/material.dart'; 
import 'package:rive/rive.dart';

class Loader extends StatelessWidget {
  const Loader({super.key});
  
  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      width: 90, height: 60,
      child: RiveAnimation.asset('assets/animations/loader.riv'));
  }
}
