import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:debateit_core/core.dart';

class AppIcon extends StatelessWidget {
  final bool showLabel;

  const AppIcon({ super.key, this.showLabel = true });

  @override
  Widget build(BuildContext context) {
    final logo = Padding(
      padding: const EdgeInsets.only(right: Insets.xs),
      child: SvgPicture.asset('assets/images/logo.svg', height: 25.0));

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [ logo, if (showLabel) const Text.rich(Constants.appTitle) ]);
  }
}
