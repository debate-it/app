import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';

class ExitDialog extends StatelessWidget {
  
  const ExitDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final dialogTextStyle = context.text.bodyLarge;

    return SimpleDialog(
      title: Text('Exiting app', 
        textAlign: TextAlign.center,
        style: TextStyles.h2
          .copyWith(color: context.colors.primary)),
      
      children: [
        Container(
          margin: const EdgeInsets.all(Insets.md),
          child: Text('Do you want to exit the app?', style: dialogTextStyle)),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SimpleDialogOption(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('STAY', style: context.text.titleLarge)),

            SimpleDialogOption(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('LEAVE', style: context.text.titleLarge))
          ]
        )
      ]
    );
  }
}
