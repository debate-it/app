import 'package:flutter/material.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_core/widgets/buttons/buttons.dart';


class InfoDialog extends StatelessWidget {
  final String message;

  const InfoDialog({ super.key, required this.message });

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 16.0);

    return SimpleDialog(
      elevation: 20.0,
      shape: const RoundedRectangleBorder(borderRadius: Corners.medBorder),
      contentPadding: padding,
      title: Text(message,
        textAlign: TextAlign.center,
        style: TextStyles.h2
          ),
      children: [
        PrimaryBtn(label: 'OK', onPressed: () { Navigator.of(context).pop(); })
      ]
    );
  }
}
