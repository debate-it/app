import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:debateit_models/user/main.dart';
import 'package:debateit_authentication/repository.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc({required this.authenticationRepository}) 
    : super(const AuthenticationState.unknown()) {

    on<AuthenticationStatusChanged>(_onAuthenticationStatusChanged);
    on<AuthenticationLogoutRequested>(_onAuthenticationLogoutRequested);

    _authenticationStatusSubscription = authenticationRepository.status
      .listen((status) => add(AuthenticationStatusChanged(status)));
  }

  AuthenticationRepository authenticationRepository;

  late StreamSubscription<AuthenticationStatus> _authenticationStatusSubscription;


  Future<void> _onAuthenticationStatusChanged(AuthenticationStatusChanged event, Emitter<AuthenticationState> emit) async {
    switch (event.status) {
      case AuthenticationStatus.authenticated:
        final user = await authenticationRepository.fetchUser();

        if (user != null) {
          return emit(AuthenticationState.authenticated(user));
        }

        return emit(const AuthenticationState.unauthenticated());

      case AuthenticationStatus.notActivated:
        final user = await authenticationRepository.fetchUser();

        if (user != null) {
          return emit(AuthenticationState.notActivated(user));
        }

        return emit(const AuthenticationState.unauthenticated());

      case AuthenticationStatus.unauthenticated:
        return emit(const AuthenticationState.unauthenticated());
        
      default:
        return emit(const AuthenticationState.unknown());
    }
  }

  void _onAuthenticationLogoutRequested(AuthenticationLogoutRequested event, Emitter<AuthenticationState> emit) {
    authenticationRepository.logout();
  }
  

  @override
  Future<void> close() {
    _authenticationStatusSubscription.cancel();
    authenticationRepository.dispose();

    return super.close();
  }
}
