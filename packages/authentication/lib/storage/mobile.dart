part of 'storage.dart';

class MobileStorage implements Storage {
  final FlutterSecureStorage _store = const FlutterSecureStorage(); 

  @override
  Future<String?> read({ required String key }) async {
    return _store.read(key: key);
  }

  @override
  Future<void> delete({ required String key }) async {
    await _store.delete(key: key);
  }

  @override
  Future<void> write({ required String key, required String value }) async {
    await _store.write(key: key, value: value);
  }
}
