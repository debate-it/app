part of 'storage.dart';

class WebStorage implements Storage {
  @override
  Future<String?> read({ required String key }) async {
    final instance = await SharedPreferences.getInstance();

    return instance.getString(key);
  }

  @override
  Future<void> delete({ required String key }) async {
    final instance = await SharedPreferences.getInstance();

    await instance.remove(key);
  }

  @override
  Future<void> write({ required String key, required String value }) async {
    final instance = await SharedPreferences.getInstance();

    await instance.setString(key, value);
  }
}
