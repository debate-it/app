import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'mobile.dart';
part 'web.dart';

abstract class Storage {

  factory Storage() => kIsWeb ? WebStorage() : MobileStorage();

  Future<String?> read({ required String key });

  Future<void> write({ required String key, required String value });
  Future<void> delete({ required String key });
}
