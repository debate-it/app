import 'dart:async';

import 'package:debateit_authentication/storage/storage.dart';


import 'package:debateit_models/user/main.dart';

import 'package:debateit_graphql/graphql.dart';
import 'package:debateit_graphql/queries/auth.dart';
import 'package:debateit_graphql/queries/profile.dart';


enum AuthenticationStatus { unknown, authenticated, notActivated, unauthenticated }

class AuthenticationTokens {
  AuthenticationTokens({required this.session, required this.refresh});
  AuthenticationTokens.json(Map<String, dynamic> json)
    : session = json['session'] as String,
      refresh = json['refresh'] as String;

  final String session;
  final String refresh;
}

class AuthenticationResponse {
  AuthenticationResponse({required this.active, required this.tokens, required this.user});
  AuthenticationResponse.json(Map<String, dynamic> json)
    : active = json['active'] as bool,
      user = UserProfile.fromJson(json['user'] as Map<String, dynamic>),
      tokens = AuthenticationTokens.json(json['tokens'] as Map<String, dynamic>);
      
  final bool active;
  final UserProfile user;
  final AuthenticationTokens tokens;
}


class AuthenticationRepository  {
  AuthenticationRepository({required this.client});

  final BackendClient client;
  final Storage storage = Storage(); 
  final _controller = StreamController<AuthenticationStatus>();
  
  UserProfile? _user;
  UserProfile? get user => _user;

  bool get isLoggedIn => _user != null;

  Stream<AuthenticationStatus> get status async* {
    // yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<void> authenticate() async {
    final token = await storage.read(key: 'refresh-token');

    if (token == null) {
      return logout();
    }

    try {
      final result = await client.fetch(query: AuthQueries.recover, variables: { 'token': token });

      if (result.exception != null) {
        _controller.add(AuthenticationStatus.unauthenticated);

        throw result.exception!;
      }

      _onAuthenticated(AuthenticationResponse.json(result.data!['authenticate'] as Map<String, dynamic>));
    } catch (err) {
      logout();
    }
  }

  Future<void> login({required String username, required String password}) async {
    final result = await client.mutate(
      mutation: AuthQueries.login,
      variables: { 'username': username, 'password': password }
    );

    _onAuthenticated(AuthenticationResponse.json(result as Map<String, dynamic>));
  }

  Future<void> signup({
    required String username,
    required String email,
    required String password,
    Map<String, dynamic> extra = const {}
  }) async {
    final result = await client.mutate(
      mutation: AuthQueries.signup,
      variables: {
        'username': username,
        'email': email,
        'password': password,
        'extra': extra
      }
    );
    
    _onAuthenticated(AuthenticationResponse.json(result as Map<String, dynamic>));
  }

  Future<void> activate({required String code}) async {
    await client.mutate(
      mutation: AuthQueries.activate, 
      variables: { 'code': code });

    _controller.add(AuthenticationStatus.authenticated);
  }

  Future<void> _onAuthenticated(AuthenticationResponse response) async {
    _user = response.user;

    await _saveTokens(response.tokens.session, response.tokens.refresh);

    client.setToken(response.tokens.session);
    _controller.add(response.active
      ? AuthenticationStatus.authenticated 
      : AuthenticationStatus.notActivated);
  }

  Future<void> logout() async {
    _user = null;

    await _clearTokens();

    client.clearCache();

    _controller.add(AuthenticationStatus.unauthenticated);
  }
  
  /// Save authentication tokens in storage
  Future<void> _saveTokens(String accessToken, [String? refreshToken]) async {
    await Future.wait([
      storage.write(key: 'session-token', value: accessToken),

      if (refreshToken != null)
        storage.write(key: 'refresh-token', value: refreshToken)
    ]);
  }

  Future<UserProfile?> fetchUser() async {
    if (_user != null) return _user;

    final result = await client.fetch(query: ProfileQueries.Info);

    if (result.exception != null) {
      throw result.exception!;
    }
    
    final payload = result.data!['profile'] as Map<String, dynamic>;

    return UserProfile.fromJson(payload['info'] as Map<String, dynamic>);
  }

  /// Clear all authentication tokens
  Future<void> _clearTokens() async {
    await Future.wait([
      storage.delete(key: 'session-token'),
      storage.delete(key: 'refresh-token')
    ]);
  }

  void dispose() => _controller.close();
}
