import 'package:debateit_graphql/fragments.dart';

abstract class RequestQueries { 
  static const String CreateDebateRequest = '''
    mutation createDebateRequest(\$recipient: ID! \$discussion: DiscussionInput!) {
      action: createDebateRequest(recipient: \$recipient, discussion: \$discussion) {
        ${QueryFragments.RequestInfo}

        ...on DebateRequest {
          ...DebateRequestInfo
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateRequestInfo}
  ''';
  
  static const String CreatePanelRequest = '''
    mutation createPanelRequest(\$recipients: [ID!]! \$position: String! \$discussion: DiscussionInput!) {
      action: createPanelRequest(recipients: \$recipients, position: \$position, discussion: \$discussion) {
        ${QueryFragments.RequestInfo}

        ...on PanelRequest {
          ...PanelRequestInfo
        }
      }
    }

    ${QueryFragments.PanelRequestInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';
  

  static const String ResolveDebateRequest = '''
    mutation resolveRequest(\$id: ID!, \$status: RequestStatus!) {
      action: resolveDebateRequest(id: \$id, status: \$status) {
        ${QueryFragments.DiscussionInfo}
        ...DebateInfo
        ...ParticipantsInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';
  
  static const String ResolvePanelRequest = '''
    mutation resolveRequest(\$id: ID!, \$status: RequestStatus!, \$argument: String) {
      action: resolvePanelRequest(id: \$id, status: \$status, argument: \$argument) {
        ${QueryFragments.RequestInfo}

        ...PanelRequestInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PanelRequestInfo}
  ''';

  static const String DebateRequests = '''
    query debateRequests {
      requests(source: INCOMING) {
        ${QueryFragments.RequestInfo}
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String FindRequest = '''
    query findRequest(\$id: ID!) {
      request(id: \$id) {
        ${QueryFragments.RequestInfo}

        ...on PanelRequest {
          ...PanelRequestInfo
        }

        ...on DebateRequest {
          ...DebateRequestInfo
        }

        ...on OpenRequest {
          type
          resolved
          candidacies
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PanelRequestInfo}
    ${QueryFragments.DebateRequestInfo}
  '''; 
}

abstract class OpenRequestQueries {
  static const String OpenRequestInfo = '''
    id
    created
    
    type
    resolved
    candidacies

    initiator {
      ...UserPreviewInfo
    }

    info {
      topic
      size

      category {
        id
        name
        stat {
          followers
          discussions
        }
      }

      tags {
        name
      }
    }
  ''';

  static const String Sample = '''
    query openRequestSample {
      openRequestSample {
        $OpenRequestInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String OpenRequest = '''
    query openRequest(\$id: ID!) {
      openRequest(id: \$id) {
        $OpenRequestInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String OpenRequestCandidates = '''
    query openRequest(\$id: ID! \$page: PaginationInfo!) {
      openRequest(id: \$id) {
        id
        candidates(page: \$page) {
          hasMore
          next
          page {
            id
            created
            status
            candidate {
              ...UserPreviewInfo
            }
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String UserOpenRequests = '''
    query userOpenRequests(\$page: PaginationInfo!) {
      userOpenRequests(page: \$page) {
        hasMore
        next
        page {
          $OpenRequestInfo
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String CreateOpenRequest = '''
    mutation createOpenRequest(\$discussion: DiscussionInput! \$type: OpenRequestType!) {
      action: createOpenRequest(discussion: \$discussion type: \$type) {
        ${QueryFragments.RequestInfo}
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String SubmitDebateCandidacy = '''
    mutation submitDebateCandidacy(\$id: ID!) {
      action: submitDebateCandidacy(id: \$id) {
        id
        created
        status
        candidate {
          ...UserPreviewInfo
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String SubmitPanelCandidacy = '''
    mutation submitPanelCandidacy(\$id: ID! \$argument: String!) {
      action: submitPanelCandidacy(id: \$id argument: \$argument) {
        id
        created
        status
        candidate {
          ...UserPreviewInfo
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String ResolveOpenDebateRequest = '''
    mutation resolveOpenDebateRequest(\$id: ID! \$candidates: [ID!]!) {
      action: resolveOpenDebateRequest(id: \$id candidates: \$candidates) {
        ${QueryFragments.DiscussionInfo}

        ...DebateInfo
        ...ParticipantsInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';

  static const String ResolveOpenPanelRequest = '''
    mutation resolveOpenPanelRequest(\$id: ID! \$candidates: [ID!]! \$argument: String!) {
      action: resolveOpenPanelRequest(id: \$id candidates: \$candidates argument: \$argument) {
        ${QueryFragments.DiscussionInfo}

        ...on Panel {
          ...PanelInfo
        }
      }
    }

    ${QueryFragments.PanelInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';
}
