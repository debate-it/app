import 'package:debateit_graphql/fragments.dart';

abstract class PostQueries {
  static const String FindPost = '''
    query find(\$id: ID!) {
      post(id: \$id) {
        ${QueryFragments.DiscussionInfo}        
        
        ...PostInfo
      }
    }

    ${QueryFragments.PostInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String CreatePost = '''
    mutation createPost(\$discussion: DiscussionInput!) {
      action: createPost(discussion: \$discussion) {
        ${QueryFragments.DiscussionInfo}

        ...PostInfo
      }
    }

    ${QueryFragments.PostInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String AlignWithPost = '''
    mutation alignWithPost(\$post: ID!, \$alignment: PostAlignmentSide!) {
      action: alignWithPost(id: \$post, alignment: \$alignment) {
        alignment {
          agree
          disagree
        }

        votedOn
      }
    }
  ''';

}
