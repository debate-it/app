import 'package:debateit_graphql/fragments.dart';

abstract class LeaderboardQueries {
  static const String Overview = '''
    query overview {
      leaderboards {
        debates(page: { limit: 5 }) {
          page {
            score
            entry {
              ... on Debate {
                ${QueryFragments.DiscussionInfo}
                ...on Debate {
                  ...DebateInfo
                  ...ParticipantsInfo
                }
              }
            }
          }
        }

        posts(page: { limit: 5 }) {
          page {
            score
            entry {
              ...on Post {
                ${QueryFragments.DiscussionInfo}
                ...on Post {
                  ...PostInfo
                }
              }
            }
          }
        }

        challenges(page: { limit: 5 }) {
          page {
            score
            entry {
              ...on Challenge {
                ${QueryFragments.DiscussionInfo}
                ...on Challenge {
                  ...ChallengeInfo
                }
              }
            }
          }
        }

        users(page: { limit: 5 }) {
          page {
            score
            entry {
              ...on User {
                ...UserPreviewInfo
              }
            }
          }
        }
      }
    }

    ${QueryFragments.DebateInfo}
    ${QueryFragments.PostInfo}
    ${QueryFragments.ParticipantsInfo}
    ${QueryFragments.ChallengeInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String Debaters = '''
    query {
      leaderboards {
        users(page: { limit: 20 }) {
          page {
            score
            entry {
              ...on User {
                ...UserPreviewInfo
              }
            }
          }
        } 
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String Debates = '''
    query {
      leaderboards {
        debates(page: { limit: 10 }) {
          page {
            score
            entry {
              ...on Debate {
                ${QueryFragments.DiscussionInfo}
                ...DebateInfo
                ...ParticipantsInfo
              }
            }
          }
        }
      }
    }
    
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String Challenges = '''
    query {
      leaderboards {
        challenges(page: { limit: 10 }) {
          page {
            score
            entry {
              ...on Challenge {
                ${QueryFragments.DiscussionInfo}
                ...ChallengeInfo
              }
            }
          }
        }
      }
    }

    ${QueryFragments.ChallengeInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String Posts = '''
    query {
      leaderboards {
        posts(page: { limit: 10 }) {
          page {
            score
            entry {
              ...on Post {
                ${QueryFragments.DiscussionInfo}
                ...PostInfo
              }
            }
          }
        }
      }
    }

    ${QueryFragments.PostInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';
}
