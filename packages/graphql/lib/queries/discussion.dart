
import 'package:debateit_graphql/fragments.dart';

abstract class DiscussionQueries {
  static const Fetch = '''
    query discussion(\$id: ID!) {
      discussion(id: \$id) {
        ${QueryFragments.DiscussionInfo}
        
        ...on Debate {
          ...DebateInfo
          ...ParticipantsInfo
        }

        ...on Post {
          ...PostInfo
        }

        ...on Panel {
          ...PanelInfo
        }

        ...on Challenge {
          ...ChallengeInfo
        }
      }
    }
    
    ${QueryFragments.PostInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ChallengeInfo}
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';
}
