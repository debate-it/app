
import 'package:debateit_graphql/fragments.dart';

abstract class DebateQueries { 
  static const String Debate = '''
    query debate(\$id: ID!) {
      debate(id: \$id) {
        ${QueryFragments.DiscussionInfo}
        
        ...DebateInfo
        ...ParticipantsInfo

        votedOn

        arguments {
          time
          participant
          argument
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';
  
  static const String DebateStatus = '''
    query debate(\$id: ID!) {
      debate(id: \$id) {
        ...DebateInfo
      }
    }

    ${QueryFragments.DebateInfo}
  ''';
  
  
  static const String AddArgument = '''
    mutation addArgument(\$id: ID!, \$argument: String!) {
      action: addArgument(id: \$id, argument: \$argument) {
        status
        deadline

        arguments {
          time
          participant
          argument
        }
      }
    }
  ''';
  
  static const String Vote = '''
    mutation vote(\$id: ID!, \$participant: Int!) {
      action: voteOnDebate(id: \$id, participant: \$participant) {
        votedOn
        participants {
          score
          user {
            id
          }
        }
      }
    }
  ''';

  static const String DebateArgumentUpdates = '''
    subscription debateUpdates(\$id: ID!, \$participant: Int!) {
      debateRealtimeArguments(id: \$id, participant: \$participant) {
        index
        argument {
          time
          participant
          argument
        }
      }
    } 
  ''';
}
