import 'package:debateit_graphql/fragments.dart';

abstract class ProfileQueries {
  
  static const String Feed = '''
    query profileFeed(\$page: PaginationInfo!) {
      profile {
        feed(page: \$page) {
          hasMore
          next
          page {
            ${QueryFragments.DiscussionInfo}
            
            ...on Debate {
              ...DebateInfo
              ...ParticipantsInfo
            }

            ...on Post {
              ...PostInfo
            }

            ...on Panel {
              ...PanelInfo
            }
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PostInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';

  static const String Discussions = '''
    query profileDiscussions(\$page: PaginationInfo!, \$status: DebateStatus!) {
      profile {
        discussions(page: \$page, status: \$status) {
          hasMore
          next
          page {
            ${QueryFragments.DiscussionInfo}
            
            ...on Debate {
              ...DebateInfo
              ...ParticipantsInfo
            }

            ...on Post {
              ...PostInfo
            }

            ...on Panel {
              ...PanelInfo
            }
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PostInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';

  static const String Requests = '''
    query profileRequests(\$source: RequestSource!, \$status: RequestStatus, \$page: PaginationInfo!) {
      profile {
        requests(source: \$source, status: \$status, page: \$page) {
          hasMore
          next
          page {
            ${QueryFragments.RequestInfo}

            ...on PanelRequest {
              ...PanelRequestInfo
            }

            ...on DebateRequest {
              ...DebateRequestInfo
            }
          }
        }
      }
    }
    
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PanelRequestInfo}
    ${QueryFragments.DebateRequestInfo}
  ''';

  static const String Activity = '''
    query profileActivity(\$page: PaginationInfo!) {
      profile {
        activity(page: \$page) {
          hasMore
          next
          page {
            id
            from
            activity {
              type
              time
              link
              title
              message
            }
          }
        }
      }
    }
  ''';
  
  static const String CountNewNotifications = '''
    query profileCountNewNotifications {
      profile {
        indicators {
          notifications
        }
      }
    }
  ''';
  
  static const String Debates = '''
    query debates(\$page: PaginationInfo!, \$status: DebateStatus!) {
      profile {
        debates(page: \$page, status: \$status) {
          hasMore
          next
          page {
            ${QueryFragments.DiscussionInfo}
            ...DebateInfo
            ...ParticipantsInfo
          }
        }
      }
    }
    
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';

  
  static const String Info = '''
    query profileInfo {
      profile {
        info {
          ...UserInfo 
        }

        indicators {
          notifications
          requests
          debates
        }
      }
    }
    
    ${QueryFragments.UserInfo}
  ''';

  static const String Stats = '''
    query profileStats {
      profile {
        info {
          progress {
            level
            earned
            needed
          }

          stat {
            points
            wins
            followers
            bestCategory {
              name
            }
          }
        }
      }
    }
  ''';
  
  static const String Indicators = '''
    query profileInfo {
      profile {
        indicators {
          notifications
          requests
          debates
        }
      }
    }
  ''';

  static const String SetDeviceToken = '''
    mutation setDeviceToken(\$token: String!) {
      setDeviceToken(token: \$token)
    }
  ''';
  
  static const String UnsetDeviceToken = '''
    mutation unsetDeviceToken {
      unsetDeviceToken()
    }
  ''';

  static const String SubscribedUsers = '''
    query susbcribedUsers(\$page: PaginationInfo!) {
      profile {
        userSubscriptions(page: \$page) {
          hasMore
          next
          page {
            ...UserPreviewInfo
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String SubscribedCategories = '''
    query susbcribedUsers(\$page: PaginationInfo!) {
      profile {
        categorySubscriptions(page: \$page) {
          hasMore
          next
          page {
            id
            name
            description

            stat {
              discussions
              followers
            }
          }
        }
      }
    }
  ''';
  
  static const String UpdateAboutInfo = '''
    mutation updateInfo(\$options: UserInfoUpdateInput!) {
      action: updateInfo(options: \$options) { 
        ...UserInfo
      }
    }
    
    ${QueryFragments.UserInfo}
  ''';
}
