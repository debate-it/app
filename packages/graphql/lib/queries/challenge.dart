import 'package:debateit_graphql/fragments.dart';

abstract class ChallengeQueries {

  static const String FindChallenge = '''
    query findChallenge(\$id: ID!) {
      challenge(id: \$id) {
        ${QueryFragments.DiscussionInfo}
        ...ChallengeInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ChallengeInfo}
  ''';

  static const String FindChallenges = '''
    query findChallenges(\$page: PaginationInfo!) {
      challenges(page: \$page) {
        hasMore
        next
        page {
          ${QueryFragments.DiscussionInfo}

          ...ChallengeInfo 
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ChallengeInfo}
  ''';
  
  static const String FindChallengeResponses = '''
    query findChallengeResponses(
      \$challenge: ID!
      \$order: CommentOrder! 
      \$page: PaginationInfo!
    ) {

      comments(target: \$challenge, order: \$order, page: \$page) {
        hasMore
        next
        page {
          ...CommentInfo
        }
      } 
    }

    ${QueryFragments.CommentFragment}
    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String FindUserResponse = '''
    query findUserResponses(\$challenge: ID!) { 
      userComment(id: \$challenge) {
        ...CommentInfo
      }
    }

    ${QueryFragments.CommentFragment}
    ${QueryFragments.UserPreviewInfo}
  ''';
 
  static const String CreateChallenge = '''
    mutation createChallenge(\$discussion: DiscussionInput! \$hours: Int! \$position: String!) {
      action: initiateChallenge(discussion: \$discussion hours: \$hours position: \$position) {
        ${QueryFragments.DiscussionInfo}
        ...ChallengeInfo
      }
    }
    
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ChallengeInfo}
  ''';
  
  static const String AddChallengeResponse = '''
    mutation addResponse(\$challenge:ID!, \$argument: String!) {
      action: addChallengeResponse(id: \$challenge, response: \$argument) {
        ${QueryFragments.DiscussionInfo}
        ...ChallengeInfo
      } 
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ChallengeInfo}
  ''';

  static const String VoteOnResponse = '''
    mutation voteResponse(\$response: ID!) {
      action: voteOnComment(id: \$response) {
        ...CommentInfo
      } 
    }

    ${QueryFragments.CommentFragment}
    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String ChooseChallengeOpponent = '''
    mutation chooseOpponent(\$challenge: ID!, \$response: ID!) {
      action: chooseChallengeOpponent(id: \$challenge, opponent: \$response) {
        ${QueryFragments.DiscussionInfo}

        ...DebateInfo
        ...ParticipantsInfo
      }
    }
    
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';
}
