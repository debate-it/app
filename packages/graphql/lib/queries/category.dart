import 'package:debateit_graphql/fragments.dart';

abstract class CategoryQueries {
  static const String CategoryFragment = '''
    fragment CategoryInfo on DiscussionCategory {
      id
      name
      description

      isSubscribed

      stat {
        discussions
        followers
      }
    }
  ''';

  static const String FetchCategories = '''
    query {
      categories {
        id
        name
        description

        isSubscribed

        stat {
          discussions
          followers
        }
      }
    }
  ''';
  
  static const String SubscribedCategories = '''
    query {
      categorySubscriptions {
        id
        name
        description
      }
    }
  ''';

  static const String Category = '''
    query category(\$id: ID!) {
      category(id: \$id) { ...CategoryInfo }
    }

    $CategoryFragment
  ''';

  static const String CategoryFeed = '''
    query categoryFeed(\$id: ID!, \$page: PaginationInfo!) {
      category(id: \$id) {
        id
        feed(page: \$page) {
          
          hasMore
          next
          page {
            __typename

            ${QueryFragments.DiscussionInfo}

            ...on Debate {
              ...DebateInfo
              ...ParticipantsInfo
            }

            ...on Post {
              ...PostInfo
            }

            ...on Challenge {
              ...ChallengeInfo
            }

            ...on Panel {
              ...PanelInfo
            }
          }
        }
      }
    }

    ${QueryFragments.PostInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ParticipantsInfo}
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ChallengeInfo}
  ''';

  static const String Subscribe = '''
    mutation subscribe(\$id: ID!) {
      action: subscribe(target: \$id type: Category) {
        ...on DiscussionCategory {
          id
          name
          description

          isSubscribed

          stat {
            discussions
            followers
          }
        }
      }
    }
  ''';

  static const String InitialSubscription = '''
    mutation initialSubscription(\$categories: [ID!]!) {
      action: initialSubscription(categories: \$categories) {
        id
        name
        description
        
        stat {
          discussions
          followers
        }
      }
    }
  ''';
}
