import 'package:debateit_graphql/fragments.dart';

abstract class SearchQueries { 
  static const String UserSearchByFilters = '''
    query searchUsers(\$filters: UserFilters!, \$page: PaginationInfo!) {
      opponents(filters: \$filters, page: \$page) {
        page { ...on User { ...UserPreviewInfo } }
        next
        hasMore
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String Followers = '''
    query followers {
      profileFollowers {
        ...UserInfo 
      }
    }

    ${QueryFragments.UserInfo}
  ''';

  static const String Subscriptions = '''
    query subscriptions {
      profileSubscriptions {
        ...UserInfo 
      }
    }

    ${QueryFragments.UserInfo}
  ''';
  
  static const String TagSearch = '''
    query searchTag(\$query: String!, \$page: PaginationInfo!) {
      search {
        tags(query: \$query, page: \$page) {
          ...DiscussionSearch
        }
      }
    }
    
    $DiscussionFragment
  ''';

  static const String CategorySearch = '''
    query searchCategory(\$query: String!, \$page: PaginationInfo!) {
      search {
        categories(query: \$query, page: \$page) {
          ...DiscussionSearch
        }
      }
    }
    
    $DiscussionFragment
  ''';

  static const String TopicSearch = '''
    query search(\$query: String! \$page: PaginationInfo!) {
      search {
        discussions(query: \$query, page: \$page) {
          ...DiscussionSearch
        }
      }
    } 
    
    $DiscussionFragment
  '''; 

  static const String UserSearch = '''
    query simpleUserSearch(\$query: String!, \$page: PaginationInfo!) {
      search {
        users(query: \$query, page: \$page) {
          hasMore
          next
          page { ...UserPreviewInfo }
        }
      }
    }
    
    ${QueryFragments.UserPreviewInfo}
  ''';


  static const String GeneralSearch = '''
    query GeneralSearch(\$query: String!, \$page: PaginationInfo!) {
      search {
        tags(query: \$query, page: \$page) { ...DiscussionSearch }
        categories(query: \$query, page: \$page) { ...DiscussionSearch }
        discussions(query: \$query, page: \$page) { ...DiscussionSearch }

        users(query: \$query, page: \$page) {
          hasMore
          next
          page { ...UserPreviewInfo }
        }
      }
    }

    $DiscussionFragment
  ''';

  static const String DiscussionFragment = '''
    fragment DiscussionSearch on DiscussionPagination {
      hasMore
      next
      page {
        ${QueryFragments.DiscussionInfo}
        
        ...on Debate {
          ...DebateInfo
          ...ParticipantsInfo
        }

        ...on Post {
          ...PostInfo
        }

        ...on Panel {
          ...PanelInfo
        }

        ...on Challenge {
          ...ChallengeInfo
        }
      }
    }

    ${QueryFragments.PostInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.ChallengeInfo}
    ${QueryFragments.ParticipantsInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';
}
