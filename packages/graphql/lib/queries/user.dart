 
import 'package:debateit_graphql/fragments.dart';

abstract class UserQueries {

  static const String User = '''
    query user(\$id: ID!) {
      user(id: \$id) {
        ...UserInfo
      }
    }

    ${QueryFragments.UserInfo}
  ''';

  static const String Followers = '''
    query UserFollowers(\$id: ID!, \$page: PaginationInfo!) {
      user(id: \$id) {
        id
        followers(page: \$page) {
          hasMore
          next
          page {
            ...UserPreviewInfo
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';
  
  static const String UserSubscriptions = '''
    query UserSubscriptions(\$id: ID!, \$page: PaginationInfo!) {
      user(id: \$id) {
        id
        userSubscriptions(page: \$page) {
          hasMore
          next
          page {
            ...UserPreviewInfo
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
  ''';


  static const String UnresolvedRequests = '''
    query unresolvedRequests {
      unresolvedRequests
    }
  ''';


  static const String UserDebates = '''
    query debates(\$id: ID!, \$page: PaginationInfo!) {
      user(id: \$id) {
        id
        discussions(page: \$page) {
          hasMore
          next
          page {
            __typename

            ${QueryFragments.DiscussionInfo}

            ... on Debate {
              ...DebateInfo
              ...ParticipantsInfo
            }

            ... on Post {
              ...PostInfo
            }

            ...on Panel {
              ...PanelInfo
            }

            ...on Challenge {
              ...ChallengeInfo
            }
          }
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.PostInfo}
    ${QueryFragments.DebateInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ParticipantsInfo}
    ${QueryFragments.ChallengeInfo}
  ''';


  static const String follow = '''
    mutation follow(\$id: ID!) {
      action: subscribe(target: \$id type: User) {
        ...on User {
          relation
          ...UserInfo
        }
      }
    }

    ${QueryFragments.UserInfo}
  ''';
}
