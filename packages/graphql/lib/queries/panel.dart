
import 'package:debateit_graphql/fragments.dart';

abstract class PanelQueries {
  
  static const String Panel = '''
    query panel(\$id: ID!) {
      panel(id: \$id) {
        ${QueryFragments.DiscussionInfo}
        ...PanelInfo
      }
    }

    ${QueryFragments.PanelInfo}
    ${QueryFragments.UserPreviewInfo}
  ''';

  static const String PanelStatus = '''
    query panel(\$id: ID!) {
      panel(id: \$id) {
        panelStatus: status
      }
    }
  ''';

  static const String Vote = '''
    mutation vote(\$id: ID! \$participant: Int!) {
      action: voteOnPanel(id: \$id participant: \$participant) {
        votedOn
        participants {
          user {
            id
          }

          score
        }
      }
    }

  ''';
}
