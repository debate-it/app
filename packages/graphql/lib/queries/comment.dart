import 'package:debateit_graphql/fragments.dart';

abstract class CommentQueries {
  static const String FindComments = '''
    query fetchComments(\$target: ID!, \$type: CommentType!, \$order: CommentOrder!, \$page: PaginationInfo!) {
      comments(target: \$target, type: \$type, order: \$order, page: \$page) {
        hasMore
        next
        page {
          __typename
          ...CommentInfo
        }
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.CommentFragment}
  ''';

  static const String FindCommentWithDiscussion = '''
    query fetchCommetnWithDiscussion(\$id: ID!) {
      commentWithDiscussion(id: \$id) {
        discussion {
          ${QueryFragments.DiscussionInfo}
          
          ...on Debate {
            ...DebateInfo
            ...ParticipantsInfo
          }

          ...on Post {
            ...PostInfo
          }

          ...on Panel {
            ...PanelInfo
          }
          
          ...on Challenge {
            ...ChallengeInfo
          }
        }

        comment {
          ...CommentInfo
        }
      }
    }

    ${QueryFragments.DebateInfo}
    ${QueryFragments.PostInfo}
    ${QueryFragments.PanelInfo}
    ${QueryFragments.ChallengeInfo}
    ${QueryFragments.CommentFragment}
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';
  
  static const String FindUserComment = '''
    query fetchUserComment(\$target: ID!, \$type: CommentType!) {
      userComment(target: \$target, type: \$type) {
        ...CommentInfo
      }
    }
    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.CommentFragment}
  ''';

  static const String AddComment = '''
    mutation addComemnt(\$target: ID!, \$type: CommentType!, \$comment: String!) {
      action: createComment(target: \$target, type: \$type, content: \$comment) {
        ...CommentInfo
      }
    }

    ${QueryFragments.UserPreviewInfo}
    ${QueryFragments.CommentFragment}
  ''';

  static const String Vote = '''
    mutation upvoteComment(\$id: ID!) {
      action: voteOnComment(id: \$id) {
        votes
        voted
      }
    }
  ''';
}
