abstract class ReportQueries {
  static const String Report = '''
    mutation (\$id: ID!, \$reason: ReportReason!, \$description: String!) {
      action: submitReport(discussionId: \$id, reason: \$reason, description: \$description)
    }
  ''';
}
