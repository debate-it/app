import 'package:debateit_graphql/fragments.dart';

abstract class AuthQueries { 
  static const String _authFragment = '''
    fragment AuthFragment on AuthResponse {
      active
      tokens {
        session
        refresh
      }

      user {
        ...UserInfo
      }
    }

    ${QueryFragments.UserInfo}
  ''';

  static const String login = '''
    mutation login (\$username: String!, \$password: String!) {
      action: login (username: \$username, password: \$password) {
        ...AuthFragment
      }
    }

    $_authFragment
  ''';

  static const String recover = '''
    query recover(\$token: String!) {
      authenticate(token: \$token) {
        ...AuthFragment
      }
    }

    $_authFragment
  ''';

  static const String signup = '''
    mutation signup(\$username: String!, \$email: String!, \$password: String!, \$extra: ExtraInfo) {
      action: signup(username: \$username, email: \$email, password: \$password, extra: \$extra) {
        ...AuthFragment
      }
    }

    $_authFragment
  ''';
  
  static const String activate = '''
    mutation activate(\$code: String!) {
      action: activate(code: \$code)
    }
  ''';

  static const String verify = '''
    query verifyCredenials(\$username: String!, \$email: String!) {
      verifyUserCredentials(username: \$username, email: \$email)
    }
  ''';

  static const String verifyUsername = '''
    query verifyUsername(\$username: String!) {
      verifyUsername(username: \$username)
    }
  ''';
  
  static const String verifyEmail = '''
    query verifyEmail(\$email: String!) {
      verifyEmail(email: \$email)
    }
  ''';
}
