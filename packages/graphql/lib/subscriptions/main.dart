abstract class Subscriptions { 
  static const String Notifications = '''
    subscription notifications {
      updates {
        event
        ref 
        message
      }
    } 
  ''';
  
  static const String DebateArgumentUpdates = '''
    subscription debateUpdates(\$id: ID!, \$participant: Int!) {
      debateRealtimeArguments(id: \$id, participant: \$participant) {
        index
        argument {
          time
          participant
          argument
        }
      }
    } 
  ''';
}
