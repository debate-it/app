abstract class QueryFragments {
  static const DiscussionInfo = '''
      __typename

      id

      topic
      created
      size

      category {
        id
        name
        stat {
          followers
          discussions
        }
      }

      tags {
        name
      }
  ''';

  static const String ChallengeInfo = '''
    fragment ChallengeInfo on Challenge {
      challengeDeadline: deadline
      challengeStatus: status

      author {
        ...UserPreviewInfo
      } 

      position
      responses
    }
    
  ''';

  static const String UserPreviewInfo = '''
  fragment UserPreviewInfo on User {
    __typename

    id
    
    username
    fullname
    
    progress {
      level
      earned
      needed
    }

    info {
      avatar
    }
    
    stat {
      points
      wins
      followers
      bestCategory {
        name
      }
    }
  }
  ''';

  static const String UserInfo = '''
  fragment UserInfo on User {
    __typename

    id
    
    username
    fullname

    relation

    progress {
      level
      earned
      needed
    }

    info {
      about
      
      country
      education
      occupation
      religion
      politics
      
      avatar
    }
    
    stat {
      points
      wins
      followers
      bestCategory {
        name
      }
    }
  }
  ''';

  static const String UserDebates = '''
    fragment UserDebates on User {
      debates {
        ...DebateInfo
        ...ParticipantsInfo
      }
    }

    ${QueryFragments.DebateInfo}
    ${QueryFragments.ParticipantsInfo}
  ''';

  static const String PanelRequestInfo = '''
  fragment PanelRequestInfo on PanelRequest {
    expiration
    accepted
    closed

    recipients { 
      status
      user {
        ...UserPreviewInfo 
      }
    }
  }
  ''';

  static const String DebateRequestInfo = '''
  fragment DebateRequestInfo on DebateRequest {
    status
    recipient { 
      ...UserPreviewInfo
    }
  }
  ''';

  static const String RequestInfo = '''
    __typename

    id
    created 

    initiator {
      ...UserPreviewInfo
    }

    info {
      topic
      size

      category {
        id
        name
        stat {
          followers
          discussions
        }
      }

      tags {
        name
      }
    }
  ''';

  static const String PanelInfo = '''
    fragment PanelInfo on Panel {
      panelStatus: status
      panelDeadline: deadline

      participants {
        score
        argument
        user {
          ...UserPreviewInfo
        }
      }
    }
  ''';

  static const String ParticipantsInfo = '''
    fragment ParticipantsInfo on Debate {
      participants {
        score
        user {
          ...UserPreviewInfo
        }
      }
    } 
  ''';

  static const String DebateInfo = '''
    fragment DebateInfo on Debate {
      debateStatus: status
      debateDeadline: deadline

      #turn
    }
  ''';

  static const String UserAccountInfo = '''
    fragment UserAccountInfo on User {
      ...UserInfo
    }

    ${QueryFragments.UserInfo}
  ''';

  static const String PostInfo = '''
    fragment PostInfo on Post {
      __typename

      author {
        ...UserPreviewInfo
      }

      alignment {
        agree
        disagree
      }
    }
  ''';


  static const String CommentFragment = '''
    fragment CommentInfo on Comment {
      id
      created
      type

      author {
        ...UserPreviewInfo
      }

      content
      alignment
      
      votes
      voted
    }
  ''';
}
