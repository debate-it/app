abstract class SettingsQueries { 
  static const String ChangePassword = '''
    mutation changePassword(\$password: String!) {
      action: changePassword(password: \$password)
    }
  ''';

  static const String VerifyPassword = '''
    query verifyPassword(\$password: String!) {
      verifyPassword(password: \$password)
    }
  ''';
  
  static const String ForgotPassword = '''
    mutation forgotPassword(\$email: String!) {
      forgotPassword(email: \$email)
    }
  ''';
}
