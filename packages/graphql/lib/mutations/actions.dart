
import 'package:debateit_graphql/fragments.dart';

abstract class ActionQueries { 


  static const String updateInfo = '''
    mutation updateInfo(\$options: UserInfoUpdateInput!) {
      action: updateInfo(options: \$options) { 
        ...UserInfo
      }
    }
    
    ${QueryFragments.UserInfo}
  ''';

  static const String uploadImage = '''
    mutation uploadImage(\$file: Upload!) {
      action: updateProfileImage(file: \$file) 
    }
  ''';


  static const String SetAppToken = '''
    mutation setAppToken(\$token: String!) {
      action: setAppToken(token: \$token)
    }
  ''';

}
