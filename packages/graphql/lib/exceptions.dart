import 'package:graphql_flutter/graphql_flutter.dart';

class QueryException {
  QueryException(OperationException inner) 
    : message = inner.graphqlErrors.first.message,
      code = inner.graphqlErrors.first.extensions?['code'] as String?;

  final String message;
  final String? code;

  @override
  String toString() => message;
}
