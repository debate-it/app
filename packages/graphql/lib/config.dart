class BackendClientConfig {
  // static const BASE_URI = 'api.debateit.io';
  // static const BACKEND_URI = 'https://$BASE_URI/graphql';
  // static const WEBSOCKET_URI = 'wss://$BASE_URI/graphql';
  
  // static const baseUri = '10.0.2.2:9000';
  // static const backendUri = 'http://$baseUri/graphql';
  // static const websocketUri = 'ws://$baseUri/graphql';
 
  const BackendClientConfig({ 
    required this.backendUri,
    required this.websocketUri,
    this.token 
  });

  BackendClientConfig copyWithToken({ String? token }) {
    return BackendClientConfig(backendUri: backendUri, websocketUri: websocketUri, token: token);
  }

  final String backendUri;
  final String websocketUri;

  // String get uri => BackendClientConfig.backendUri;
  // String get subscriptionURI => BackendClientConfig.websocketUri;

  bool get subscriptionsEnabled => token != null && token!.isNotEmpty;

  final String? token;

}
