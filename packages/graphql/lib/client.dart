import 'dart:async';

import 'package:debateit_graphql/exceptions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:debateit_graphql/config.dart';


typedef CacheUpdateHandler = void Function(GraphQLCache, QueryResult);
 
class BackendClient with ChangeNotifier { 
  GraphQLClient _client;
  BackendClientConfig _config;
  
  static BackendClient of(BuildContext context) => 
    Provider.of<BackendClient>(context, listen: false); 

  BackendClient(BackendClientConfig config)
    : _config = config,
      _client = constructClient(config);

  bool get subscriptionsEnabled => _config.subscriptionsEnabled;

  Future<QueryResult> fetch({ required String query, Map<String, dynamic> variables = const {} }) async { 
    final options = QueryOptions(document: gql(query), variables: variables);
    final result = await _client.query(options);

    if (result.hasException) {
      throw QueryException(result.exception!);
    }

    return result;
  }

  Future<T> mutate<T>({ required String mutation, 
                        Map<String, dynamic> variables = const {}, 
                        FutureOr<void> Function(GraphQLDataProxy, QueryResult?)? update }) async {

    final options = MutationOptions(document: gql(mutation), variables: variables, update: update);

    final result = await _client.mutate(options);

    if (result.hasException) {
      throw QueryException(result.exception!);
    }

    return result.data!['action'] as T;
  }

  Stream<S> subscribe<S>({ required String subscription,
                           Map<String, dynamic>? variables,
                           S Function(QueryResult)? transform }) {

    final operation = SubscriptionOptions(
      document: gql(subscription),
      variables: variables ?? {}
    );

    final stream = _client.subscribe(operation);

    if (transform != null) {
      return stream.map<S>(transform);
    }

    return stream.cast();
  } 

  void clearCache() {
    _client.cache.store.reset();
  }

  void setToken(String token) {
    _config = _config.copyWithToken(token: token);
    createClient(_config);
  }

  void createClient(BackendClientConfig config) { 
    _client = constructClient(config);
    notifyListeners();
  }
  
  void reset() {
    _config = _config.copyWithToken();
    createClient(_config);
  } 

  GraphQLProvider connectBackendClient(Widget app) => 
    GraphQLProvider(client: ValueNotifier(_client), child: CacheProvider(child: app));

  static GraphQLClient constructClient(BackendClientConfig config) {
    final httpLink = HttpLink(config.backendUri);
    final authLink = AuthLink(getToken: () => config.token != null ? 'Bearer ${config.token}' : null);

    var link = authLink.concat(httpLink);

    // Create websocket link if auth token is available
    if (config.subscriptionsEnabled) {
      link = Link.split((request) => request.isSubscription, _createSubscriptionLink(config), link);
    }

    final cache = GraphQLCache(
      store: HiveStore(),
      dataIdFromObject: getObjectId,
      partialDataPolicy: PartialDataCachePolicy.accept);

    return GraphQLClient(
      cache: cache,
      link: link, 
      defaultPolicies: DefaultPolicies(
        query: Policies(fetch: FetchPolicy.networkOnly, error: ErrorPolicy.all, cacheReread: CacheRereadPolicy.ignoreAll),
        mutate: Policies(fetch: FetchPolicy.cacheAndNetwork, error: ErrorPolicy.all)));
  }

  static WebSocketLink _createSubscriptionLink(BackendClientConfig config) {
    assert(config.subscriptionsEnabled);

    return WebSocketLink(config.websocketUri, 
      config: SocketClientConfig( 
        inactivityTimeout: null,
        headers: { 'Authorization': config.token },
        delayBetweenReconnectionAttempts: const Duration(seconds: 10),
        initialPayload: { 'Authorization': config.token })); 
  }
  
  static String? getObjectId(Object object) {
    if (object is Map<String, dynamic>) {
      final typeName = object['__typename'] as String?;
      final id = object['id'] as String?;

      if (typeName != null && id != null) {
        return <String>[typeName, id].join('/');
      }
    }
    return null;
  }
}
  
