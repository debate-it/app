import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'package:debateit_pagination/models/page.dart';


typedef JsonPageBuilder<T> = Page<T> Function(List<dynamic>);

abstract class PaginationState<T extends Pageable> extends Equatable {
  @override List<Object?> get props => const [];

  const PaginationState();
}

class PaginationUninitialized<T extends Pageable> extends PaginationState<T> { 
  const PaginationUninitialized();
} 

class PaginationError<T extends Pageable> extends PaginationState<T> {
  final Exception inner;

  const PaginationError(this.inner);

  @override List<Object?> get props => [inner];
}

class PageLoading<T extends Pageable> extends PaginationState<T> {
  final ContentPage<T> buffer;

  const PageLoading(this.buffer);

  @override List<Object?> get props => [buffer, buffer.cursor, buffer.content, buffer.content.length];
}

class PageLoaded<T extends Pageable> extends PaginationState<T> { 

  const PageLoaded({ 
    required this.hasMore, 
    required this.page 
  }); 

  factory PageLoaded.fromJson({ required Map<String, dynamic> json, 
                                required PageItemBuilder<T> builder,
                                ContentPage<T>? previous }) { 
                                
    final page = ContentPage.fromJson(
      cursor: json['next'] as String?,
      json: json['page'] as List<dynamic>,
      builder: builder
    );

    return PageLoaded(
      hasMore: json['hasMore'] as bool,
      page: previous?.merge(page) ?? page
    ); 
  }

  final bool hasMore;
  final ContentPage<T> page; 
  
  @override List<Object?> get props => [hasMore, page, page.cursor, page.content, page.content.length];
  @override String toString() => 'PaginationLoaded { page: $page, hasMore: $hasMore }';

  Iterable<Widget> intoWidgets({ 
    required BuildContext context, 
    required Widget Function(BuildContext, T) builder
  }) {
    return page.iterable
      .map((e) => Container(key: ValueKey(e.id), child: builder(context, e) ));

  }
}
