import 'package:equatable/equatable.dart';

abstract class PaginationEvent extends Equatable {
  const PaginationEvent();
  @override List<Object> get props => const [];
}

class NextPage extends PaginationEvent {
  const NextPage();
}

class PreviousPage extends PaginationEvent { 
  const PreviousPage();
}

class PageRefresh extends PaginationEvent {
  const PageRefresh();
}

class PageRemoveItem extends PaginationEvent {
  final String id;

  const PageRemoveItem(this.id);

  @override List<Object> get props => [id];
}
