import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:debateit_graphql/graphql.dart';

import 'package:debateit_pagination/config.dart';
import 'package:debateit_pagination/models/page.dart';

import 'package:debateit_pagination/bloc/events.dart';
import 'package:debateit_pagination/bloc/state.dart';


class PaginationBloc<T extends Pageable> extends Bloc<PaginationEvent, PaginationState<T>> {
  PaginationState<T> get initialState => PaginationUninitialized<T>();

  PaginationBloc({ required this.config, required this.client }) : super(PaginationUninitialized<T>()) {
    on<NextPage>(_fetchNextPage);
    on<PageRemoveItem>(_removeItem);
    on<PageRefresh>(_refresh);
  }

  ContentPage<T> buffer = const ContentPage(content: []);
  PaginationConfig<T> config;

  final BackendClient client;


  FutureOr<void> _refresh(PageRefresh event, Emitter<PaginationState<T>> emit) {
    buffer = const ContentPage(content: []);

    emit(initialState);
  }

  FutureOr<void> _removeItem(PageRemoveItem event, Emitter<PaginationState<T>> emit) async { 
    assert (state is PageLoaded); 

    final loaded = state as PageLoaded;

    final hasMore = loaded.hasMore;
    final content = loaded.page.iterable
      .where((element) => element.id != event.id) 
      .cast<T>()
      .toList();

    emit(PageLoaded<T>(
      hasMore: hasMore,
      page: ContentPage<T>(
        cursor: loaded.page.cursor,
        content: content)));
  }

  FutureOr<void> _fetchNextPage(NextPage event, Emitter<PaginationState<T>> emit) async {
    final currentState = state; // save state for page merging

    emit(PageLoading<T>(buffer));

    try {
      final result = await client.fetch(
        query: config.query,
        variables: config.variables
          ..['page'] = {
            'cursor': buffer.cursor,
            'limit': config.size
          }
      ); 

      final json = _getTargetJson(result.data!, config.queryName); 

 
      final loaded = PageLoaded<T>.fromJson(
        json: json as Map<String, dynamic>,
        builder: config.buildItem,
        previous: (currentState is PageLoaded<T>)
          ? currentState.page
          : null 
      );

      buffer = loaded.page;

      emit(loaded);

    } on Exception catch(err) {
      emit(PaginationError<T>(err));
    }
  }

  dynamic _getTargetJson(Map<String, dynamic> json, String path) =>
    path.split('.').fold<Map<String, dynamic>>(json, (value, element) => value[element] as Map<String, dynamic>);
}
