import 'package:equatable/equatable.dart';

typedef PageItemBuilder<T> = T Function(Map<String, dynamic>);

abstract class Pageable {
  String get id;

  const Pageable();
}
 
class ContentPage<T extends Pageable> extends Equatable {
  const ContentPage({ this.cursor, required Iterable<T> content  })
    : _content = content;

  const ContentPage.empty()
    : _content = const[],
      cursor = null;
    
  /// Create Page using given item [builder]
  factory ContentPage.fromJson({ String? cursor, 
                                 required List<dynamic> json, 
                                 required PageItemBuilder<T> builder }) { 
    return ContentPage(
      cursor: cursor, 
      content: json
        .map((e) => builder(e as Map<String, dynamic>))
        .toList() 
    );
  }
 
  /// Index of the page or index of the last page
  /// that was merged together
  final String? cursor;

  final Iterable<T> _content;

  /// List of contents of the page
  List<T> get content => _content.toList();

  /// Iterable content of the page
  Iterable<T> get iterable => _content;


  @override List<Object?> get props => [cursor, _content, _content.length];
  @override String toString() => 'Page { index: $cursor, content: $content }'; 

  /// Merge page with [other]
  /// [cursor] for merged page will be taken from [other]
  ContentPage<T> merge(ContentPage<T> other) {
    final mergedContent = List<T>.from(_content.followedBy(other.iterable)); 
 
    return ContentPage(
      content: mergedContent,
      cursor: other.cursor
    );
  }
}
