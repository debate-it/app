import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:debateit_core/core.dart';
import 'package:debateit_core/widgets/brand/loader.dart';
import 'package:debateit_core/widgets/buttons/buttons.dart';

import 'package:debateit_graphql/client.dart';

import 'package:debateit_pagination/config.dart';
import 'package:debateit_pagination/models/page.dart';

import 'package:debateit_pagination/bloc/events.dart';
import 'package:debateit_pagination/bloc/logic.dart';
import 'package:debateit_pagination/bloc/state.dart';
 

const _defaultPadding = EdgeInsets.only(top: 10, bottom: 30);

abstract class _PaginationProvider<T extends Pageable> extends StatefulWidget {
  final PaginationConfig<T> config;
  final PaginationBloc<T> pagination;
  
  final Widget? header;
  final EdgeInsets? padding; 
  final ScrollPhysics? physics;

  final bool scrollable;
  
  final Widget? widgetToInsert;
  final void Function(Exception)? onError;

  const _PaginationProvider({
    super.key,
    required this.config, 
    required this.pagination, 
    this.header, 
    this.padding = _defaultPadding,
    this.physics = const AlwaysScrollableScrollPhysics(), 
    this.scrollable = true, 
    this.widgetToInsert,
    this.onError
  });

  List<Widget> buildWidgetsFromState({
    required BuildContext context, 
    required PaginationState<T> state,
    required PaginationConfig<T> config,
    required PaginationBloc<T> pagination,

    required bool mounted,

    Widget? widgetToInsert,
    Widget? header
  }) {
    final children = <Widget>[];

    if (!mounted) {
      return children;
    }
    
    if (state is PageLoaded<T>) {
      children.addAll(state.intoWidgets(
        context: context,
        builder: config.buildWidget
      )); 

      if (widgetToInsert != null) {
        final openRequestPosition = _getOpenRequestFeedPosition(state);

        if (openRequestPosition > 0) {
          children.insert(openRequestPosition, widgetToInsert);
        }
      }

      if (state.hasMore) {
        children.add(buildButton(context, pagination));
      }

    } 
    
    if (state is PaginationUninitialized) {
      pagination.add(const NextPage());
    }

    if (state is PageLoading<T>) {
      children.addAll(state.buffer.iterable
        .map((item) => config.buildWidget(context, item)));
    }

    if (state is PaginationError<T>) {
      Future.microtask(() => onError?.call(state.inner));
    }

    if (state is PageLoading<T> || state is PaginationUninitialized<T>) {
      children.add(const Padding(padding: EdgeInsets.only(top: 15), child: Loader()));
    }
    

    if (children.isEmpty) {
      children.add(buildNoItemMessage(context, config));
    }

    return [header ?? const SizedBox()].followedBy(children).toList();
  }
  
  static Widget buildNoItemMessage<T extends Pageable>(BuildContext context, PaginationConfig<T> config) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: Insets.xl, horizontal: Insets.lg),
      alignment: Alignment.center,
      child: Text(
        config.noItemText,
        textAlign: TextAlign.center,
        style: context.text.headlineMedium));
  }

  static Widget buildButton<T extends Pageable>(BuildContext context, PaginationBloc<T> pagination) {
    return Padding(
      padding: const EdgeInsets.all(Insets.md),
      child: PrimaryBtn(
        child: const SizedBox(width: 100, child: Align(child: Text('MORE'))),
        onPressed: () => 
          pagination.add(const NextPage())));
  }
  
  static int _getOpenRequestFeedPosition<T extends Pageable>(PaginationState<T> state) {
    if (state is! PageLoaded<T>) {
      return -1;
    }

    if (state.page.content.length < 5) {
      return -1;
    }

    final upperHalfStart = state.page.content.length ~/ 4;
    final upperHalfEnd = state.page.content.length - 2;

    if (upperHalfEnd <= 0) {
      return -1;
    }

    if (upperHalfStart > upperHalfEnd) {
      return Random().nextInt(upperHalfStart - upperHalfEnd) + upperHalfEnd;
    }

    return Random().nextInt(upperHalfEnd - upperHalfStart) + upperHalfStart;
  }
}

class PaginationProvider<T extends Pageable> extends _PaginationProvider<T> {

  PaginationProvider({ 
    super.key,

    required super.config, 
    required BackendClient client,

    super.header = const SizedBox(), 
    super.padding = _defaultPadding, 
    super.physics = const AlwaysScrollableScrollPhysics(),
    super.scrollable = true,
    super.widgetToInsert,
  })
    : super(pagination: PaginationBloc(config: config, client: client));

  PaginationProvider.fromBloc({ 
    super.key,
    required PaginationBloc<T> bloc,

    super.header = const SizedBox(), 
    super.padding = _defaultPadding, 
    super.physics = const AlwaysScrollableScrollPhysics(),
    super.scrollable = true,
    super.widgetToInsert,
  }) 
    : super(config: bloc.config, pagination: bloc);

  @override
  _PaginationProviderState<T> createState() => _PaginationProviderState<T>();
}

class _PaginationProviderState<T extends Pageable> extends State<PaginationProvider<T>> {
  late PaginationBloc<T> pagination = widget.pagination;

  @override
  Widget build(BuildContext context) {
    final content = BlocProvider<PaginationBloc<T>>.value(
      value: pagination,
      child: BlocBuilder<PaginationBloc<T>, PaginationState<T>>(builder: buildFromState));
 
    return RefreshIndicator(onRefresh: _refresh, child: content);
  }

  Widget buildFromState(BuildContext context, PaginationState<T> state) {
    final children = widget.buildWidgetsFromState(
      context: context, 
      mounted: mounted,

      state: state, 
      pagination: pagination,
      config: widget.config, 

      widgetToInsert: widget.widgetToInsert,
      header: widget.header
    );

    if (widget.scrollable) {
      return ListView(physics: widget.physics, padding: widget.padding, children: children);

    } else {
      return Container(padding: widget.padding, child: Column(children: children));
    }
  }

  Future<void> _refresh() async {
    pagination.add(const PageRefresh());
  }
}

class SliverPaginationProvider<T extends Pageable> extends _PaginationProvider<T> {
  
  SliverPaginationProvider({ 
    super.key,
    required super.config, 
    required BackendClient client,

    super.header = const SizedBox(), 
    super.padding = _defaultPadding, 
    super.physics = const AlwaysScrollableScrollPhysics(),
    super.scrollable = true,
    super.widgetToInsert,
  })
    : super(pagination: PaginationBloc(config: config, client: client));

  SliverPaginationProvider.fromBloc({ 
    super.key,
    required PaginationBloc<T> bloc,

    super.header = const SizedBox(), 
    super.padding = _defaultPadding, 
    super.physics = const AlwaysScrollableScrollPhysics(),
    super.scrollable = true,
    super.widgetToInsert,
  }) 
    : super(config: bloc.config, pagination: bloc);

  @override
  _SliverPaginationProviderState<T> createState() => _SliverPaginationProviderState<T>();
}

class _SliverPaginationProviderState<T extends Pageable> extends State<SliverPaginationProvider<T>> {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(bloc: widget.pagination, builder: buildFromState);
  }

  Widget buildFromState(BuildContext context , PaginationState<T> state) {
    return SliverPadding(
      padding: widget.padding ?? EdgeInsets.zero, 
      sliver: SliverList(
        delegate: SliverChildListDelegate(widget.buildWidgetsFromState(
          context: context, 
          state: state,
          mounted: mounted,

          config: widget.config,
          header: widget.header,
          pagination: widget.pagination,
          widgetToInsert: widget.widgetToInsert
        ))));
  }
}
