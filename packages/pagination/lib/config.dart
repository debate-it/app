import 'package:flutter/material.dart';

/// Base pagination config template
abstract class PaginationConfig<T> {
  /// Size of the page
  int get size;

  /// GraphQL query template
  String get query; 
  /// Query name used to extract the query data
  String get queryName;


  /// Text to be displayed if no items are available
  String get noItemText;
  
  /// Query variables
  final Map<String, dynamic> variables;

  PaginationConfig([this.variables = const {}]);

  /// Builds single item, given [json] data
  T buildItem(Map<String, dynamic> json);

  /// Builds single widget from given item
  Widget buildWidget(BuildContext context, T item); 
}
