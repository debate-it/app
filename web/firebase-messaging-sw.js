importScripts("https://www.gstatic.com/firebasejs/9.10.0/firebase-app-compat.js");
importScripts("https://www.gstatic.com/firebasejs/9.10.0/firebase-messaging-compat.js");

firebase.initializeApp({
  apiKey: "AIzaSyCNyCMa95UYTcdpwV3nBuK48BHFfddUT7o",
  authDomain: "debate-it-f37da.firebaseapp.com",
  databaseURL: "https://react-native-firebase-testing.firebaseio.com",
  projectId: "debate-it-f37da",
  storageBucket: "debate-it-f37da.appspot.com",
  messagingSenderId: "223221983313",
  appId: "1:223221983313:web:5213098a28f6d02cdb7070",
  measurementId: "G-YCC26THJZ1",
});

// Necessary to receive background messages:
const messaging = firebase.messaging();

// Optional:
messaging.onBackgroundMessage((m) => {
  console.log("onBackgroundMessage", m);
});